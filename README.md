# switched to Android

# E2iPlayer for Enigma2 based on python3
![](https://img.shields.io/badge/python-3.9|3.10|3.11-blue)
![](https://img.shields.io/badge/openatv-7.1|7.2|7.3-blue)
<br>
Use for educational purposes.

# Dependencies
- pycryptodome

# Javascript Interpreters and Engines
- Duktape

![](./IPTVPlayer/screenshot.jpg)
