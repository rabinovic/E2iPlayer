﻿# -*- coding: utf-8 -*-

import re
try:
    import json
except Exception:
    import simplejson as json

from os import path as os_path
from os import remove as os_remove

###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (GetSubtitlesDir,
                                                             printDBG,
                                                           printExc)

# INFO about subtitles format
# https://wiki.videolan.org/Subtitles#Subtitles_support_in_VLC

#def printDBG(data):
#    print "%s" % data


class IPTVSubtitlesHandler:
    SUPPORTED_FORMATS = ['srt', 'vtt', 'mpl']

    @staticmethod
    def getSupportedFormats():
        return IPTVSubtitlesHandler.SUPPORTED_FORMATS

    def __init__(self):
        printDBG("IPTVSubtitlesHandler.__init__")
        self.subAtoms = []
        self.pailsOfAtoms = {}
        self.CAPACITY = 10 * 1000 # 10s

    def _srtClearText(self, text):
        printDBG("IPTVSubtitlesHandler._srtClearText text[%s]" % text)
        return re.sub('<[^>]*>', '', text)
        #<b></b> : bold
        #<i></i> : italic
        #<u></u> : underline
        #<font color=”#rrggbb”></font>

    def _srtTc2ms2(self, tc):
        printDBG("IPTVSubtitlesHandler._srtTc2ms2 tc[%s]" % tc)
        sign = 1
        if tc[0] in "+-":
            sign = -1 if tc[0] == "-" else 1
            tc = tc[1:]

        match = self.TIMECODE_RE.match(tc)
        hh, mm, ss, ms = [0 if x is None else int(x) for x in match.groups()]
        return ((hh * 3600 + mm * 60 + ss) * 1000 + ms) * sign

    def _srtTc2ms(self, time):
        printDBG("IPTVSubtitlesHandler._srtTc2ms time[%s]" % time)
        if ',' in time:
            split_time = time.split(',')
        else:
            split_time = time.split('.')
        minor = split_time[1]
        major = split_time[0].split(':')
        return (int(major[0]) * 3600 + int(major[1]) * 60 + int(major[2])) * 1000 + int(minor)

    def _srtToAtoms(self, srtText):
        printDBG("IPTVSubtitlesHandler._srtToAtoms srtText[%s]" % srtText)

        subAtoms = []
        srtText = srtText.replace('\r\n', '\n').split('\n\n')

        line = 0
        for elem in srtText:
            line += 1
            st = elem.split('\n')
            #printDBG("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
            #printDBG(st)
            #printDBG("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
            if len(st) >= 2:
                try:
                    try:
                        tmp = int(st[0].strip())
                        i = 1
                    except Exception:
                        if '' == st[0]:
                            i = 1
                        else:
                            i = 0
                    if len(st) < (i + 2):
                        continue
                    split = st[i].split(' --> ')
                    subAtoms.append({'start': self._srtTc2ms(split[0].strip()), 'end': self._srtTc2ms(split[1].strip()), 'text': self._srtClearText('\n'.join(j for j in st[i + 1:len(st)]))})
                except Exception:
                    printExc("Line number [%d]" % line)
        return subAtoms

    def _mplClearText(self, text):
        printDBG("IPTVSubtitlesHandler._mplClearText text[%s]" % text)
        text = text.split('|')
        for elm in text:
            if elm.startswith('/'):
                elm = elm[1:]
        return re.sub('\{[^}]*\}', '', '\n'.join(text))

    def _mplTc2ms(self, time):
        printDBG("IPTVSubtitlesHandler._mplTc2ms time[%s]" % time)
        return int(time) * 100

    def _mplToAtoms(self, mplData):
        printDBG("IPTVSubtitlesHandler._mplToAtoms mplData[%s]" % mplData)
        # Timings          : Sequential Time
        # Timing Precision : 100 Milliseconds (1/10th sec)
        subAtoms = []
        mplData = mplData.replace('\r\n', '\n').split('\n')
        reObj = re.compile('^\[([0-9]+?)\]\[([0-9]+?)\](.+?)$')

        for s in mplData:
            tmp = reObj.search(s)
            if None is not tmp:
                subAtoms.append({'start': self._mplTc2ms(tmp.group(1)), 'end': self._mplTc2ms(tmp.group(2)), 'text': self._mplClearText(tmp.group(3))})
        return subAtoms

    def getSubtitles(self, currTimeMS):
        printDBG("OpenSubOrg.getSubtitles [%s]" % currTimeMS)
        #time1 = time.time()
        subsText = []
        for item in self.subAtoms:
            if currTimeMS >= item['start'] and currTimeMS < item['end']:
                subsText.append(item['text'])
        ret = '\n'.join(subsText)
        #time2 = time.time()
        #printDBG('>>>>>>>>>>getSubtitles function took %0.3f ms' % ((time2-time1)*1000.0))
        return ret


    def removeCacheFile(self, filePath):
        cacheFile = self._getCacheFileName(filePath)
        try:
            os_remove(cacheFile)
        except Exception:
            printExc()

    def _getCacheFileName(self, filePath):
        tmp = filePath.split('/')[-1]
        return GetSubtitlesDir(tmp + '.iptv')

    def _loadFromCache(self, orgFilePath, encoding='utf-8'):
        printDBG("IPTVSubtitlesHandler._loadFromCache  orgFilePath[%s], encoding[%s]" % ( orgFilePath, encoding))
        sts = False
        try:
            filePath = self._getCacheFileName(orgFilePath)
            try:
                with open(filePath, 'r', encoding=encoding, errors='replace') as fp:
                    self.subAtoms = json.loads( ph.ensure_str(fp.read(), encoding=encoding))
                if len(self.subAtoms):
                    sts = True
                    printDBG("IPTVSubtitlesHandler._loadFromCache orgFilePath[%s] --> cacheFile[%s]" % (orgFilePath, filePath))
            except Exception:
                printExc()
        except Exception:
            printExc()
        return sts

    def _saveToCache(self, orgFilePath, encoding='utf-8'):
        printDBG("OpenSubOrg._saveToCache orgFilePath[%s], encoding[%s]" % (orgFilePath, encoding))
        try:
            filePath = self._getCacheFileName(orgFilePath)
            with open(filePath, 'w', encoding=encoding) as fp:
                fp.write(json.dumps(self.subAtoms))
            printDBG("IPTVSubtitlesHandler._saveToCache orgFilePath[%s] --> cacheFile[%s]" % (orgFilePath, filePath))

        except Exception:
            printExc()

    def _fillPailsOfAtoms(self):
        self.pailsOfAtoms = {}
        for idx, elm in enumerate(self.subAtoms):
            tmp = elm['start'] / self.CAPACITY
            if tmp not in self.pailsOfAtoms:
                self.pailsOfAtoms[tmp] = [idx]
            elif idx not in self.pailsOfAtoms[tmp]:
                self.pailsOfAtoms[tmp].append(idx)

            tmp = elm['end'] / self.CAPACITY
            if tmp not in self.pailsOfAtoms:
                self.pailsOfAtoms[tmp] = [idx]
            elif idx not in self.pailsOfAtoms[tmp]:
                self.pailsOfAtoms[tmp].append(idx)

    def loadSubtitles(self, filePath, encoding='utf-8', fps=0):
        printDBG("OpenSubOrg.loadSubtitles filePath[%s] encoding[%s], fps[%s" % (filePath, encoding, fps))
        return self._loadSubtitles(filePath, encoding)

    def _loadSubtitles(self, filePath, encoding):
        printDBG("OpenSubOrg._loadSubtitles filePath[%s], encoding[%s]" % (filePath, encoding))
        saveCache = True
        self.subAtoms = []
        #time1 = time.time()
        sts = self._loadFromCache(filePath)
        if not sts:
            try:
                with open(filePath, 'r', encoding=encoding, errors='replace') as fp:
                    subText = ph.ensure_str(fp.read(), encoding=encoding, errors='replace')
                    if filePath.endswith('.srt'):
                        self.subAtoms = self._srtToAtoms(subText)
                        sts = True
                    elif filePath.endswith('.vtt'):
                        self.subAtoms = self._srtToAtoms(subText)
                        sts = True
                    elif filePath.endswith('.mpl'):
                        self.subAtoms = self._mplToAtoms(subText)
                        sts = True
            except Exception:
                printExc()
        else:
            saveCache = False

        self._fillPailsOfAtoms()

        if saveCache and len(self.subAtoms):
            self._saveToCache(filePath)

        #time2 = time.time()
        #printDBG('>>>>>>>>>>loadSubtitles function took %0.3f ms' % ((time2-time1)*1000.0))

        return sts


class IPTVEmbeddedSubtitlesHandler:
    def __init__(self):
        printDBG("IPTVEmbeddedSubtitlesHandler.__init__")
        self.subAtoms = []
        self.pailsOfAtoms = {}
        self.CAPACITY = 10 * 1000 # 10s

    def _srtClearText(self, text):
        return re.sub('<[^>]*>', '', text)
        #<b></b> : bold
        #<i></i> : italic
        #<u></u> : underline
        #<font color=”#rrggbb”></font>

    def addSubAtom(self, inAtom):
        try:
            textTab = inAtom['text'].split('\n')
            for text in textTab:
                text = self._srtClearText(text).strip()
                if text != '':
                    idx = len(self.subAtoms)
                    self.subAtoms.append({'start': inAtom['start'], 'end': inAtom['end'], 'text': text})

                    tmp = self.subAtoms[idx]['start'] / self.CAPACITY
                    if tmp not in self.pailsOfAtoms:
                        self.pailsOfAtoms[tmp] = [idx]
                    elif idx not in self.pailsOfAtoms[tmp]:
                        self.pailsOfAtoms[tmp].append(idx)

                    tmp = self.subAtoms[idx]['end'] / self.CAPACITY
                    if tmp not in self.pailsOfAtoms:
                        self.pailsOfAtoms[tmp] = [idx]
                    elif idx not in self.pailsOfAtoms[tmp]:
                        self.pailsOfAtoms[tmp].append(idx)
        except Exception:
            printExc()

    def getSubtitles(self, currTimeMS):
        printDBG("OpenSubOrg.getSubtitles [%s]" % currTimeMS)
        #time1 = time.time()
        subsText = []
        for item in self.subAtoms:
            if currTimeMS >= item['start'] and currTimeMS < item['end']:
                subsText.append(item['text'])
        ret = '\n'.join(subsText)
        #time2 = time.time()
        #printDBG('>>>>>>>>>>getSubtitles function took %0.3f ms' % ((time2-time1)*1000.0))
        return ret

    '''
    def getSubtitles(self, currTimeMS, prevMarker):
        printDBG("IPTVSubtitlesHandler.getSubtitles currTimeMS[%s], prevMarker[%s]" % (currTimeMS, prevMarker))
        #printDBG("IPTVSubtitlesHandler.getSubtitles self.pailsOfAtoms[%s]" % self.pailsOfAtoms)
        subsText = []
        tmp = currTimeMS / self.CAPACITY
        tmp = self.pailsOfAtoms.get(tmp, [])

        ret = None
        validAtomsIdexes = []
        for idx in tmp:
            item = self.subAtoms[idx]
            printDBG("IPTVSubtitlesHandler.getSubtitles item[%s" % item)
            if currTimeMS >= item['start'] and currTimeMS < item['end']:
                validAtomsIdexes.append(idx)

        marker = validAtomsIdexes
        printDBG("IPTVEmbeddedSubtitlesHandler.getSubtitles marker[%s] prevMarker[%s] %.1fs" % (marker, prevMarker, currTimeMS/1000.0))
        if prevMarker != marker:
            for idx in validAtomsIdexes:
                item = self.subAtoms[idx]
                subsText.append(item['text'])
            ret = '\n'.join(subsText)
        return marker, ret
    '''

    def flushSubtitles(self):
        self.subAtoms = []
        self.pailsOfAtoms = {}


if __name__ == "__main__":
    obj = IPTVSubtitlesHandler()
    obj.loadSubtitles('/hdd/test_subtitle.srt')
    obj.getSubtitles(10000)
