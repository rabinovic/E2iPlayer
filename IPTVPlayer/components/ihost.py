# -*- coding: utf-8 -*-

from abc import ABC, abstractmethod
from enum import  Enum, auto
import functools
import json
from dataclasses import dataclass
from typing import Any, Dict, List

from Components.config import config
from Plugins.Extensions.IPTVPlayer.components.asynccall import \
    MainSessionWrapper
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import IPTVPlayerNotificationList
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.pCommon import (common, getBaseUrl,
                                                        getDefaultUserAgent,
                                                        isValidUrl)
from Plugins.Extensions.IPTVPlayer.libs.urlparser import urlparser
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (
    CSearchHistoryHelper, GetCookieDir, GetHiddenItemsDir, GetLogoDir,
    printDBG, printExc)
from skin import parseColor
from Tools.Directories import fileExists


@dataclass
class UrlItem:
    name:str = ''
    url:str = ''
    url_needs_resolve:bool = False

class DisplayList(list):
    def __init__(self, iter_:list=None):
        if iter_ is None:
            iter_ = []
        list.__init__(self, iter_)
        self.nextPage = None

        for i in iter_:
            if 'more' == i.get('type', None):
                self.setNextPage(i)
                iter_.remove(i)

    def setNextPage(self, nextPage:dict):
        self.nextPage=nextPage

    def getNextPage(self):
        return self.nextPage

    def isNextPageAvailable(self):
        return self.nextPage is not None

    def getItemAt(self, idx):
        if idx < 0 or len(self) <= idx:
            return None
        return self[idx]

    def clear(self):
        super().clear()
        self.nextPage=None

class DisplayItemType(Enum):
    CATEGORY= auto()
    VIDEO = auto()
    AUDIO = auto()
    SEARCH = auto()
    ARTICLE = auto()
    PICTURE = auto()
    DATA = auto()
    MORE = auto()
    MARKER = auto()
    SUBTITLE = auto()
    SUB_PROVIDER = auto()
    UNKNOWN = auto()

    def isPlayable(self):
        return self in [DisplayItemType.VIDEO, DisplayItemType.AUDIO, DisplayItemType.ARTICLE, DisplayItemType.PICTURE]

    def isDownloadable(self):
        return self in [DisplayItemType.VIDEO, DisplayItemType.AUDIO, DisplayItemType.DATA]


class DisplayItem:

    """ define attribiutes for item of diplay list
        communicate display layer with host
    """

    def __init__(self,
                title:str='',
                description:str='',
                type:DisplayItemType=DisplayItemType.UNKNOWN,
                urlItems=None,
                urlSeparateRequest=0,
                iconimage:str='',
                possibleTypesOfSearch=None,
                pinLocked:bool=False,
                isGoodForFavourites:bool=False,
                isWatched:bool=False,
                textColor:str='',
                pinCode:str='',
                hidden:bool=False,
                withMiniCover:bool=False):

        self.title = title
        self.description = description
        self.type = type
        self.iconimage = iconimage
        self.pinLocked = pinLocked
        self.pinCode = pinCode
        self.isGoodForFavourites = isGoodForFavourites
        self.isWatched = isWatched
        self.textColor = textColor
        self.isHidden = hidden

        # used only for TYPE_VIDEO item
        self.urlItems = urlItems # url to VIDEO
        # links are not available the separate request is needed to get links
        self.urlSeparateRequest = urlSeparateRequest
        # used only for TYPE_SEARCH item
        self.possibleTypesOfSearch = possibleTypesOfSearch
        self.privateData = None
        self.itemIdx = -1
        self.withMiniCover=withMiniCover
        self.pos=None
        self.idx=0

    def isDownloadable(self):
        return self.type.isDownloadable()

    def iswithMiniCover(self):
        return self.withMiniCover

    def getDisplayTitle(self):
        return f"{'* ' if self.isWatched else ''}{self.title}"

    def getTextColor(self):
        try:
            if self.textColor:
                return parseColor(self.textColor).argb()
            if self.isWatched or self.isHidden:
                return parseColor(config.plugins.iptvplayer.watched_item_color.value).argb()
        except Exception:
            printExc()
        return None


class ArticleContent:
    VISUALIZER_DEFAULT = 'DEFAULT'
    # Posible args and values for richDescParams:
    RICH_DESC_PARAMS = ["alternate_title", "original_title", "station", "price", "age_limit", "views", "status", "type", "first_air_date", "last_air_date", "seasons", "episodes", "country", "language", "duration", "quality", "subtitles", "year", "imdb_rating", "tmdb_rating",
                               "released", "broadcast", "remaining", "rating", "rated", "genre", "genres", "category", "categories", "production", "director", "directors", "writer", "writers",
                               "creator", "creators", "cast", "actors", "stars", "awards", "budget", "translation", ]
    # labels here must be in english language
    # translation should be done before presentation using "locals" mechanism
    RICH_DESC_LABELS = {"alternate_title": "Alternate Title:",
                        "original_title": "Original Title:",
                        "station": "Station:",
                        "price": "Price:",
                        "status": "Status:",
                        "type": "Type:",
                        "age_limit": "Age limit:",
                        "first_air_date": "First air date:",
                        "last_air_date": "Last air date:",
                        "seasons": "Seasons:",
                        "episodes": "Episodes:",
                        "quality": "Quality:",
                        "subtitles": "Subtitles:",
                        "country": "Country:",
                        "language": "Language",
                        "year": "Year:",
                        "released": "Released:",
                        "broadcast": "Broadcast:",
                        "remaining": "Remaining:",
                        "imdb_rating": "IMDb Rating:",
                        "tmdb_rating": "TMDb Rating:",
                        "rating": "Rating:",
                        "rated": "Rated:",
                        "duration": "Duration:",
                        "genre": "Genre:",
                        "genres": "Genres:",
                        "category": "Category:",
                        "categories": "Categories:",
                        "production": "Production:",
                        "director": "Director:",
                        "directors": "Directors:",
                        "writer": "Writer:",
                        "writers": "Writers:",
                        "creator": "Creator:",
                        "creators": "Creators:",
                        "cast": "Cast:",
                        "actors": "Actors:",
                        "stars": "Stars:",
                        "awards": "Awards:",
                        "views": "Views:",
                        "budget": "Budget:",
                        "translation": "Translation:"
                        }

    def __init__(self, title='', text='', images:List=None, trailers:List=None, richDescParams:Dict=None, visualizer=None):
        self.title = title
        self.text = text
        self.images = images
        self.trailers = trailers
        self.richDescParams = richDescParams
        if None is visualizer:
            self.visualizer = ArticleContent.VISUALIZER_DEFAULT
        else:
            self.visualizer = visualizer


class FavItem:
    RESOLVER_DIRECT_LINK = 'DIRECT_LINK'
    RESOLVER_SELF = 'SELF'
    RESOLVER_URLLPARSER = 'URLLPARSER'

    def __init__(self, title='',
                  description='',
                  type=DisplayItemType.UNKNOWN,
                  iconimage='',
                  data='',
                  resolver=RESOLVER_SELF):
        self.title = title
        self.description = description
        self.type = type
        self.iconimage = iconimage
        self.data = data
        self.resolver = resolver
        self.hostName = ''

    def fromDisplayListItem(self, dispItem:DisplayItem):
        self.title = dispItem.title
        self.description = dispItem.description
        self.type = dispItem.type
        self.iconimage = dispItem.iconimage
        return self

    def setFromDict(self, data:dict):
        for key in data:
            setattr(self, key, data[key])
        # operation needed to deserialize enum
        self.type = DisplayItemType[self.type]
        return self

    def getAsDict(self):
        return vars(self)


class RetStatus(Enum):
    """Return status"""
    OK = auto()
    ERROR = auto()
    NOT_IMPLEMENTED = auto()

@dataclass
class CHostsGroupItem:
    name:str = ''
    title:str = ''

@dataclass
class RetHost:
    status:RetStatus
    value: list
    message:str = ''

def handleServiceDecorator(func):
    @functools.wraps(func)
    def wrapper(self, index, refresh=0, searchPattern='', searchType='', selItem=None):
        printDBG('handleService start')
        HostBase.handleService(self, index, refresh, searchPattern, searchType, selItem)
        self.currList = DisplayList()
        func(self, index, refresh, searchPattern, searchType)
        HostBase.endHandleService(self, refresh)
    return wrapper


class HostBase(ABC):

    def __init__(self, params:Dict=None, withSearchHistrory:Any=True, favouriteTypes:List=None):

        self.sessionEx = MainSessionWrapper()
        self.up = urlparser()
        if params is None:
            params={}
        proxyURL = params.get('proxyURL', '')
        useProxy = params.get('useProxy', False)
        self.cm = common(proxyURL, useProxy)
        self.currList:DisplayList = DisplayList()
        self.currItem = {}
        if history:= params.get('history', ''):
            self.history = CSearchHistoryHelper(history, params.get('history_store_type', False))
        if cookie:= params.get('cookie', ''):
            self.COOKIE_FILE = GetCookieDir(cookie)
        self.moreMode = False
        self.beforeMoreItemList = None
        self.afterMoreItemList = None
        self.isGeoBlockingChecked = False
        self.MAIN_URL=None

        self.DEFAULT_ICON_URL=''

        self.USER_AGENT = getDefaultUserAgent()
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Accept': 'text/html'}
        self.defaultParams = {'header': self.HEADER}
        self.cachedVideoLinks = {}

        self.withSearchHistrory = withSearchHistrory

        self.favouriteTypes = [] if favouriteTypes is None else favouriteTypes

        self.currIndex = -1
        self.listOfprevList = DisplayList()
        self.listOfprevItems = DisplayList()

        self.searchPattern = ''
        self.searchType = ''

    @abstractmethod
    def getCustomLinksForVideo(self, cItem) -> List:
        ...

    @abstractmethod
    def getCustomArticleContent(self, cItem) -> List:
        ...

    @abstractmethod
    def getCustomVideoLinks(self, videoUrl) -> List:
        ...

    @abstractmethod
    def listSearchResult(self, cItem, searchPattern, searchType):
        ...

    @abstractmethod
    def mainMenu(self, cItem:Dict):
        ...


    def getCustomActions(self,currSelIndex) -> RetHost:
        return RetHost(RetStatus.NOT_IMPLEMENTED, value=None)

    def isProtectedByPinCode(self):
        return False

    def getLinksForVideo(self, Index=0, item=None):
        return self.getLinksForItem(Index)

    def getSupportedFavoritesTypes(self):
        return RetHost(RetStatus.OK, value=self.favouriteTypes)

    def isValidIndex(self, Index, validTypes=None):
        listLen = len(self.currList)
        if listLen <= Index or Index < 0:
            printDBG(f"ERROR isValidIndex - current list is to short len: {listLen}, Index: {Index}")
            return False
        if None is not validTypes and self.converItem(self.currList[Index]).type not in validTypes:
            printDBG("ERROR isValidIndex - current item has wrong type")
            return False
        return True

    def withArticleContent(self, cItem):
        return False

    def getArticleContent(self, Index=0):
        retCode = RetStatus.ERROR
        retlist = []
        if not self.isValidIndex(Index):
            return RetHost(retCode, value=retlist)
        cItem = self.currList[Index]

        if not self.withArticleContent(cItem):
            return RetHost(retCode, value=retlist)
        hList = self.getCustomArticleContent(cItem)
        if None is hList:
            return RetHost(retCode, value=retlist)
        for item in hList:
            title = item.get('title', '')
            text = item.get('text', '')
            images = item.get("images", [])
            othersInfo = item.get('other_info', '')
            retlist.append(ArticleContent(title=title, text=text, images=images, richDescParams=othersInfo))
        if len(hList):
            retCode = RetStatus.OK
        return RetHost(retCode, value=retlist)

    def getResolvedURL(self, url):
        """resolve url to get direct url to video file"""
        printDBG("ihost.getResolvedURL url [%s]" % url)
        retlist = []
        # mark requested link as used one
        for key, value in self.cachedVideoLinks.items():
            for idx, elem in enumerate(value):
                if url in elem['url']:
                    if not elem['name'].startswith('*'):
                        elem['name'] = '* ' + elem['name']
                    break

        urlList = self.getCustomVideoLinks(url)
        if isinstance(urlList, list):
            for item in urlList:
                need_resolve = 0
                retlist.append(UrlItem(item["name"], item["url"], need_resolve))

        return RetHost(RetStatus.OK, value=retlist)

    def getFavouriteItem(self, Index=0):
        retCode = RetStatus.ERROR
        retlist = DisplayList()
        if not self.isValidIndex(Index, self.favouriteTypes):
            RetHost(retCode, value=retlist)

        cItem = self.currList[Index]
        data = self.getFavouriteData(cItem)
        if None is not data:
            favItem = FavItem(data=data)
            favItem.fromDisplayListItem(self.converItem(cItem))

            retlist = [favItem]
            retCode = RetStatus.OK

        return RetHost(retCode, value=retlist)

    def getLinksForFavourite(self, favItem):
        retlist = DisplayList()
        try:
            if self.MAIN_URL is None:
                self.selectDomain()
        except Exception:
            printExc()
        urlList = []
        try:
            cItem = json.loads(favItem.data)
            urlList = self._getLinksForItem(cItem)
        except Exception:
            printExc()

        if isinstance(urlList, list):
            for item in urlList:
                need_resolve = item.get("need_resolve", 0)
                name = ph.cleanHtml(item["name"])
                url = item["url"]
                retlist.append(UrlItem(name, url, need_resolve))
        return RetHost(RetStatus.OK, value=retlist)

    def setInitFavouriteItem(self, favItem):
        self.currIndex = -1
        self.listOfprevList = DisplayList()
        self.listOfprevItems = DisplayList()

        self.setCurrList(DisplayList())
        self.setCurrItem({})

        if self.setInitListFromFavouriteItem(favItem.data):
            return RetHost(RetStatus.OK, value=None)
        return RetHost(RetStatus.ERROR, value=None)

    def getInitList(self):
        """return first available list of item category or video or link"""
        self.currIndex = -1
        self.listOfprevList = DisplayList()
        self.listOfprevItems = DisplayList()

        self.handleService(self.currIndex)
        convList = self.convertList(self.getCurrList())

        return RetHost(RetStatus.OK, value=convList)

    def getListForItem(self, Index=0, refresh=0, selItem=None):
        """return List of item from current List for given Index
            1 == refresh - force to read data from server if possible server instead of cache
            selItem - object of DisplayItem for selected item"""
        self.listOfprevList.append(self.getCurrList(removeHidden=False))
        self.listOfprevItems.append(self.getCurrItem())

        self.currIndex = Index
        if self.withSearchHistrory:
            self.setSearchPattern()
            try:
                if self.searchPattern:
                    sts, prevPattern = CSearchHistoryHelper.loadLastPattern()
                    if sts and prevPattern != self.searchPattern:
                        CSearchHistoryHelper.saveLastPattern(self.searchPattern)
            except Exception:
                printExc()

        self.handleService(Index, refresh, self.searchPattern, self.searchType, selItem)
        convList = self.convertList(self.getCurrList())

        return RetHost(RetStatus.OK, value=convList)

    def getPrevList(self, refresh=0):
        """return prev requested List of item for given Index
            1 == refresh - force to read data from server if possible"""
        if len(self.listOfprevList) > 0:
            hostList = self.listOfprevList.pop()
            hostCurrItem = self.listOfprevItems.pop()
            self.setCurrList(hostList)
            self.setCurrItem(hostCurrItem)

            convList = self.convertList(hostList)
            return RetHost(RetStatus.OK, value=convList)
        return RetHost(RetStatus.ERROR, value=[])

    def getCurrentList(self, refresh=0):
        """return current List for given Index
            1 == refresh - force to read data from server if possible"""
        if refresh == 1:
            self.handleService(self.currIndex, refresh, self.searchPattern, self.searchType)
        convList = self.convertList(self.getCurrList())
        return RetHost(RetStatus.OK, value=convList)

    def getMoreForItem(self, Index=0, selItem=None):
        """return list of links for AUDIO, VIDEO, PICTURE for given Index,
            selItem - object of DisplayItem for selected item"""
        self.handleService(Index, 2, self.searchPattern, self.searchType, selItem)
        convList = self.convertList(self.getCurrList())
        return RetHost(RetStatus.OK, value=convList)

    def getSuggestionsProvider(self, Index=0):
        getProvider = getattr(self, "getSuggestionsProvider", None)
        if callable(getProvider):
            val = getProvider(Index)
            return RetHost(RetStatus.OK, value=[val])
        return RetHost(RetStatus.NOT_IMPLEMENTED, value=[])

    def getLogoPath(self):
        """return full path to player logo"""
        return RetHost(RetStatus.OK, value=[GetLogoDir(getattr(self, '__module__').rsplit('.',maxsplit=1)[-1][4:] + 'logo.png')])

    def getSearchItemInx(self):
        try:
            list_ = self.getCurrList()
            for idx, elem in enumerate(list_):
                if elem['category'] == 'search':
                    return idx
        except Exception:
            printDBG('getSearchItemInx EXCEPTION')
        return -1

    def setSearchPattern(self):
        try:
            list_ = self.getCurrList()
            if 'history' == list_[self.currIndex]['name']:
                pattern = list_[self.currIndex]['title']
                search_type = list_[self.currIndex]['search_type']
                self.history.addHistoryItem(pattern, search_type)
                self.searchPattern = pattern
                self.searchType = search_type
        except Exception:
            printDBG('setSearchPattern EXCEPTION')
            self.searchPattern = ''
            self.searchType = ''

    def convertList(self, cList:DisplayList):
        hostList = DisplayList()
        for cItem in cList:
            hostItem = self.converItem(cItem)
            if None is not hostItem:
                hostList.append(hostItem)
        hostList.setNextPage(cList.getNextPage())
        return hostList

    def getSearchTypes(self):
        searchTypesOptions = []
        return searchTypesOptions

    def converItem(self, cItem, needUrlResolve=1, needUrlSeparateRequest=1):
        hostLinks = []
        type_:DisplayItemType = DisplayItemType.UNKNOWN
        possibleTypesOfSearch = None

        match cItem['type']:
            case 'category':
                if cItem.get('search_item', False):
                    type_ = DisplayItemType.SEARCH
                    possibleTypesOfSearch = self.getSearchTypes()
                else:
                    type_ = DisplayItemType.CATEGORY
            case 'video':
                type_ = DisplayItemType.VIDEO
            case 'audio':
                type_ = DisplayItemType.AUDIO
            case 'picture':
                type_ = DisplayItemType.PICTURE
            case 'article':
                type_ = DisplayItemType.ARTICLE
            case 'more':
                type_ = DisplayItemType.MORE
            case 'marker':
                type_ = DisplayItemType.MARKER
            case 'data':
                type_ = DisplayItemType.DATA
            case _:
                printExc("type not defined")

        if type_ in [DisplayItemType.AUDIO, DisplayItemType.VIDEO,
                    DisplayItemType.PICTURE, DisplayItemType.ARTICLE,
                    DisplayItemType.DATA]:

            url = cItem.get('url', '')
            if '' != url:
                hostLinks.append(UrlItem("Link", url, needUrlResolve))

        if not (icon := self.getFullIconUrl(cItem.get('icon', ''))):
            icon = self.getDefaultIcon(cItem)

        return DisplayItem(title=cItem.get('title', ''),
                                    description=cItem.get('desc', ''),
                                    type=type_,
                                    urlItems=hostLinks,
                                    urlSeparateRequest=needUrlSeparateRequest,
                                    iconimage=icon,
                                    possibleTypesOfSearch=possibleTypesOfSearch,
                                    pinLocked=cItem.get('pin_locked', False),
                                    isGoodForFavourites=cItem.get('good_for_fav', False),
                                    textColor=cItem.get('text_color', ''),
                                    pinCode=cItem.get('pin_code', ''),
                                    hidden=cItem.get('hidden', False),
                                    withMiniCover=cItem.get('with_mini_cover', False)
                                    )

    def getSearchResults(self, searchpattern, searchType=None):
        if self.withSearchHistrory:
            self.history.addHistoryItem(searchpattern, searchType)

        self.searchPattern = searchpattern
        self.searchType = searchType

        searchItemIdx = self.getSearchItemInx()
        if searchItemIdx > -1:
            return self.getListForItem(searchItemIdx)

        return RetHost(RetStatus.ERROR, value=[])

    def isItemHidden(self, displayItem):
        itemHidden = False
        if hash_:= ph.getItemHash(displayItem):
            itemHidden = fileExists( GetHiddenItemsDir(f'/{hash_}.iptvhash'))
        return itemHidden

    def informAboutGeoBlockingIfNeeded(self, country, onlyOnce=True):
        try:
            if onlyOnce and self.isGeoBlockingChecked:
                return
        except Exception:
            self.isGeoBlockingChecked = False
        sts, data = self.cm.getPage('https://dcinfos.abtasty.com/geolocAndWeather.php')
        if not sts:
            return
        try:
            data = json.loads(data)
            mycountry = data.get('country', '')
            if mycountry != country:
                if not mycountry:
                    mycountry = '?'
                message = _('%s uses "geo-blocking" measures to prevent you from accessing the services from abroad.\n Host country: %s, your country: %s')
                IPTVPlayerNotificationList().push(message % (self.getMainUrl(), country, mycountry), 'info', 5)
            self.isGeoBlockingChecked = True
        except Exception:
            printExc()

    def listsTab(self, tab, cItem, type='dir'):
        defaultType = type
        for item in tab:
            params = dict(cItem)
            params.update(item)
            params['name'] = 'category'
            type = item.get('type', defaultType)
            if type == 'dir':
                self.addDir(params)
            elif type == 'marker':
                self.addMarker(params)
            else:
                self.addVideo(params)

    def listSubItems(self, cItem):
        printDBG("BaseHost.listSubItems")
        self.currList = DisplayList(cItem['sub_items'])

    def listToDir(self, cList, idx):
        return ph.listToDir(cList, idx)

    def getMainUrl(self):
        return self.MAIN_URL

    def setMainUrl(self, url):
        if isValidUrl(url):
            self.MAIN_URL = getBaseUrl(url)
            return True
        return False

    def getFullUrl(self, url, currUrl=None):
        if currUrl is None or not isValidUrl(currUrl):
            try:
                currUrl = self.getMainUrl()
            except Exception:
                currUrl = None
            if currUrl is None or not isValidUrl(currUrl):
                currUrl = 'http://fake/'
        return self.cm.getFullUrl(url, currUrl)

    def getFullIconUrl(self, url, currUrl=None):
        if currUrl is not None:
            return self.getFullUrl(url, currUrl)
        return self.getFullUrl(url)

    def getDefaultIcon(self, cItem=None):
         return self.DEFAULT_ICON_URL

    def getCurrList(self, removeHidden=True):
        if removeHidden:
            for i in self.currList:
                if self.isItemHidden(i):
                    i.update({'hidden':True})

            if config.plugins.iptvplayer.hide_items.value:
                self.currList[:] = [i for i in self.currList if not i.get('hidden', False)]

        return self.currList

    def setCurrList(self, list_):
        self.currList = list_

    def getCurrItem(self):
        return self.currItem

    def setCurrItem(self, item):
        self.currItem = item

    def addDir(self, params):
        params['type'] = 'category'
        self.currList.append(params)

    def addNext(self, params):
        if not isinstance(self.currList, DisplayList):
            self.currList = DisplayList(self.currList)
        self.currList.setNextPage(params)

    def addMore(self, params):
        params['type'] = 'more'
        self.currList.append(params)

    def addVideo(self, params):
        params['type'] = 'video'
        self.currList.append(params)

    def addAudio(self, params):
        params['type'] = 'audio'
        self.currList.append(params)

    def addPicture(self, params):
        params['type'] = 'picture'
        self.currList.append(params)

    def addData(self, params):
        params['type'] = 'data'
        self.currList.append(params)

    def addArticle(self, params):
        params['type'] = 'article'
        self.currList.append(params)

    def addMarker(self, params):
        params['type'] = 'marker'
        self.currList.append(params)

    def listsHistory(self, baseItem:dict=None, desc_key='plot', desc_base=(_("Type: "))):
        list_ = self.history.getHistoryList()

        if baseItem is None:
            baseItem={'name': 'history', 'category': 'search'}

        for histItem in list_:
            plot = ''
            try:
                if isinstance(histItem, dict):
                    pattern = histItem.get('pattern', '')
                    search_type = histItem.get('type', '')
                    if '' != search_type:
                        plot = desc_base + _(search_type)
                else:
                    pattern = histItem
                    search_type = None
                params = dict(baseItem)
                params.update({'title': pattern, 'search_type': search_type, desc_key: plot})
                self.addDir(params)
            except Exception:
                printExc()

    def getFavouriteData(self, cItem):
        try:
            return json.dumps(cItem)
        except Exception:
            printExc()
        return ''

    def selectDomain(self):
        return None

    def setInitListFromFavouriteItem(self, fav_data):
        try:
            if self.MAIN_URL is None:
                self.selectDomain()
        except Exception:
            printExc()
        try:
            params = json.loads(fav_data)
        except Exception:
            params = {}
            printExc()
            return False
        self.currList.append(params)
        return True

    def _getLinksForItem(self, cItem):
        printDBG("ihost.getLinksForItem [%s]" % cItem)

        cacheTab = self.cachedVideoLinks.get(cItem.get('url', ''), [])
        if cacheTab:
            return cacheTab

        retTab = self.getCustomLinksForVideo(cItem)
        if retTab:
            self.cachedVideoLinks[cItem.get('url', '')] = retTab
        return retTab

    def getLinksForItem(self, Index=0):
        retCode = RetStatus.ERROR
        retlist = []
        if not self.isValidIndex(Index):
            return RetHost(retCode, value=retlist)

        urlList = self._getLinksForItem(self.currList[Index])
        if isinstance(urlList, list):
            for item in urlList:
                need_resolve = item.get("need_resolve", 0)
                retlist.append(UrlItem(item["name"], item["url"], need_resolve))

        return RetHost(RetStatus.OK, value=retlist)

    def handleService(self, index, refresh=0, searchPattern='', searchType='', selItem=None):
        self.moreMode = False
        self.cachedVideoLinks={}
        if 0 == refresh:
            if len(self.currList) <= index:

                return
            if selItem and isinstance(selItem, dict):
                self.currItem=selItem
            else:
                if -1 == index:
                    self.currItem = {"name": None}
                else:
                    self.currItem = self.currList[index]
        elif 2 == refresh: # refresh for more items
            printDBG(">> endHandleService index[%s]" % index)
            # remove item more and store items before and after item more
            self.beforeMoreItemList = DisplayList(self.currList[0:index+1])
            self.beforeMoreItemList.setNextPage(self.currList.getNextPage())

            self.afterMoreItemList = DisplayList(self.currList[index + 2:])
            self.afterMoreItemList.setNextPage(self.currList.getNextPage())

            self.moreMode = True
            if selItem and isinstance(selItem, dict):
                self.currItem=selItem
            else:
                if -1 == index:
                    self.currItem = {"name": None}
                else:
                    self.currItem = self.currList[index]

        self.currList = DisplayList()
        name =   self.currItem.get("name", '')
        category = self.currItem.get("category", '')

        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))

        # MAIN MENU
        if name is None and category == '':
            self.mainMenu({'name': 'category'})
        #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
        #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            try:
                getattr(self, category)(self.currItem)
            except:
                printExc()

        self.endHandleService(refresh)

    def endHandleService(self, refresh):
        if 2 == refresh: # refresh for more items
            currList = self.currList
            self.currList = self.beforeMoreItemList
            self.currList.extend(currList)
            self.currList.extend(self.afterMoreItemList)
            self.currList.setNextPage(currList.getNextPage())
            self.beforeMoreItemList = None
            self.afterMoreItemList =  None
        self.moreMode = False
