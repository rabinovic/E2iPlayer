# -*- coding: utf-8 -*-

###################################################
# LOCAL import
###################################################
from typing import Any
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, DownloadFile, eConnectCallback
###################################################
# FOREIGN import
###################################################
from Tools.BoundFunction import boundFunction
from enigma import eConsoleAppContainer
from Tools.Directories import resolveFilename, fileExists, SCOPE_PLUGINS
from Components.config import config, configfile
from Components.Language import language
import gettext
import os
import sys
import threading
import time
###################################################

###################################################
# Globals
###################################################
gInitIPTVPlayer = True # is initialization of IPTVPlayer is needed
PluginLanguageDomain = "IPTVPlayer"
PluginLanguagePath = "Extensions/IPTVPlayer/locale"
gSetIPTVPlayerLastHostError = ""
gIPTVPlayerNotificationList = None

###################################################


def localeInit():
    lang = language.getLanguage()[:2] # getLanguage returns e.g. "fi_FI" for "language_country"
    os.environ["LANGUAGE"] = lang # Enigma doesn't set this (or LC_ALL, LC_MESSAGES, LANG). gettext needs it!
    printDBG(PluginLanguageDomain + " set language to " + lang)
    gettext.bindtextdomain(PluginLanguageDomain, resolveFilename(SCOPE_PLUGINS, PluginLanguagePath))


def TranslateTXT(txt):
    t = gettext.dgettext(PluginLanguageDomain, txt)
    if t == txt:
        t = gettext.gettext(txt)
    return t


localeInit()
language.addCallback(localeInit)


def IPTVPlayerNeedInit(value=None):
    global gInitIPTVPlayer
    if value in [True, False]:
        gInitIPTVPlayer = value
    return gInitIPTVPlayer


def SetIPTVPlayerLastHostError(value=""):
    global gSetIPTVPlayerLastHostError
    gSetIPTVPlayerLastHostError = value


def GetIPTVPlayerLastHostError(clear=True):
    global gSetIPTVPlayerLastHostError
    tmp = gSetIPTVPlayerLastHostError
    if clear:
        gSetIPTVPlayerLastHostError = ""
    return tmp


class IPTVPlayerNotification():
    def __init__(self, title, message, type, timeout, messageHash=None, timestamp=0):
        self.title = str(title)
        self.message = str(message)
        self.type = str(type) # "info", "error", "warning"
        self.timeout = int(timeout)
        self.messageHash = messageHash
        self.timestamp = timestamp

    def __eq__(self, a):
        return not self.__ne__(a)

    def __ne__(self, a):
        if a is None:
            return True

        if None is not self.messageHash and None is not a.messageHash:
            return self.messageHash != a.messageHash

        if self.title != a.title or \
           self.type != a.type or \
           self.message != a.message or \
           self.timeout != a.timeout:
            return True
        return False


class IPTVPlayerNotificationList:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls.notificationsList = []
            cls.repeatMessages = {}
            cls.mainLock = threading.Lock()
            # this flag will be checked without mutex
            # to less lock check
            cls.empty = True
            cls._instance = object.__new__(cls)

        return cls._instance


    @classmethod
    def clearQueue(cls):
        with cls.mainLock:
            cls.notificationsList = []
            cls.empty = True

    @classmethod
    def isEmpty(cls):
        return cls.empty

    @classmethod
    def push(cls, message, type="message", timeout=5, messageHash=None, repeatMessageTimeoutSec=0):
        ret = False
        if messageHash is None and repeatMessageTimeoutSec > 0:
            raise Exception("IPTVPlayerNotificationList.push call with repeatMessageTimeout but without messageHash")

        if repeatMessageTimeoutSec > 0:
            timestamp = time.time() + repeatMessageTimeoutSec
        else:
            timestamp = None

        with cls.mainLock:
            try:
                notification = IPTVPlayerNotification('IPTVPlayer', message, type, timeout, messageHash, timestamp)
                if messageHash is not None:
                    try:
                        cls.notificationsList.remove(notification)
                    except Exception:
                        pass
                cls.notificationsList.append(notification)
                cls.empty = False
                ret = True
            except Exception as e:
                print(str(e))
        return ret

    @classmethod
    def pop(cls, popAllSameNotificationsAtOnce=True):
        notification = None
        with cls.mainLock:
            try:
                notification = cls.notificationsList.pop()
                if popAllSameNotificationsAtOnce:
                    newList = []
                    for item in cls.notificationsList:
                        if item != notification:
                            newList.append(item)
                    cls.notificationsList = newList

                if notification.timestamp is not None:
                    timestamp = time.time()
                    cls.repeatMessages = dict((k, v) for k, v in list(cls.repeatMessages.items()) if v.timestamp > timestamp)
                    if notification.messageHash in cls.repeatMessages:
                        notification = None
                    else:
                        cls.repeatMessages[notification.messageHash] = notification
            except Exception as e:
                print(str(e))

            if 0 == len(cls.notificationsList):
                cls.empty = True
        return notification


class IPTVPlayerSleep:

    _instance = None

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls.mainLock = threading.Lock()
            cls.timeout = 0
            cls.startTimestamp = 0
            cls._instance = object.__new__(cls)

        return cls._instance

    @classmethod
    def sleep(cls, timeout, blocking=True):
        with cls.mainLock:
            cls.timeout = timeout
            cls.startTimestamp = time.time()
        if blocking:
            time.sleep(cls.timeout)

    @classmethod
    def reset(cls):
        with cls.mainLock:
            cls.startTimestamp = 0

    @classmethod
    def getTimeout(cls):
        ret = 0
        with cls.mainLock:
            if cls.timeout != 0:
                ret = int(cls.timeout - (time.time() - cls.startTimestamp))
                if ret <= 0:
                    cls.timeout = 0
                    ret = 0
        return ret

gIPTVPlayerSleep = IPTVPlayerSleep()


def GetIPTVSleep():
    global gIPTVPlayerSleep
    return gIPTVPlayerSleep
