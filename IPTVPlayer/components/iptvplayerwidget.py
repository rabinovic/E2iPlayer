﻿# -*- coding: utf-8 -*-

import threading
import traceback
from copy import copy
from dataclasses import dataclass
from os import path as os_path
from random import shuffle as random_shuffle
from math import ceil
from typing import Callable, List
from urllib.parse import quote as urllib_quote
import importlib

from enigma import ePicLoad, eTimer, getDesktop, eSize

from Components.ActionMap import ActionMap
from Components.AVSwitch import AVSwitch
from Components.config import config
from Components.Label import Label
from Components.Pixmap import Pixmap
from Components.Sources.StaticText import StaticText
from Screens.ChoiceBox import ChoiceBox
from Screens.MessageBox import MessageBox
from Screens.Screen import Screen
from skin import parseColor, BT_HALIGN_CENTER, BT_VALIGN_CENTER
from Tools.BoundFunction import boundFunction
from Tools.Directories import fileExists
from Tools.LoadPixmap import LoadPixmap

from Plugins.Extensions.IPTVPlayer.components import asynccall
from Plugins.Extensions.IPTVPlayer.components.articleview import ArticleView
from Plugins.Extensions.IPTVPlayer.components.configgroups import ConfigGroupsMenu
from Plugins.Extensions.IPTVPlayer.components.confighost import (
    ConfigHostMenu, ConfigHostsMenu)
from Plugins.Extensions.IPTVPlayer.components.cover import (Cover, Cover2,
                                                            Cover3)
from Plugins.Extensions.IPTVPlayer.components.e2ivkselector import GetVirtualKeyboard
from Plugins.Extensions.IPTVPlayer.components.iconmanager import IconManager
from Plugins.Extensions.IPTVPlayer.components.ihost import (ArticleContent,
                                                            DisplayItem,
                                                            DisplayItemType,
                                                            DisplayList,
                                                            FavItem, HostBase,
                                                            RetHost, RetStatus,
                                                            UrlItem)
from Plugins.Extensions.IPTVPlayer.components.iptvarticlerichvisualizer import IPTVArticleRichVisualizer
from Plugins.Extensions.IPTVPlayer.components.iptvchoicebox import (
    IPTVChoiceBoxItem, IPTVChoiceBoxWidget)
from Plugins.Extensions.IPTVPlayer.components.iptvconfigmenu import (
    ConfigMenu, GetListOfHostsNames, GetMoviePlayer,
    IsUpdateNeededForHostsChangesCommit)
from Plugins.Extensions.IPTVPlayer.components.iptvextmovieplayer import IPTVExtMoviePlayer
from Plugins.Extensions.IPTVPlayer.components.iptvfavouriteswidgets import (
    IPTVFavouritesAddItemWidget, IPTVFavouritesMainWidget)
from Plugins.Extensions.IPTVPlayer.components.iptvpictureplayer import IPTVPicturePlayerWidget
from Plugins.Extensions.IPTVPlayer.components.iptvpin import IPTVPinWidget
from Plugins.Extensions.IPTVPlayer.components.iptvplayer import (
    IPTVMiniMoviePlayer, IPTVStandardMoviePlayer)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import (
    GetIPTVPlayerLastHostError, IPTVPlayerNotificationList, IPTVPlayerSleep)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.playerselector import PlayerSelectorWidget
from Plugins.Extensions.IPTVPlayer.iptvdm.iptvbuffui import E2iPlayerBufferingWidget
from Plugins.Extensions.IPTVPlayer.iptvdm.iptvdmapi import DMItem, IPTVDMApi
from Plugins.Extensions.IPTVPlayer.iptvdm.iptvdmui import (IPTVDMNotification,
                                                           IPTVDMWidget)
from Plugins.Extensions.IPTVPlayer.iptvdm.iptvdownloadercreator import IsUrlDownloadable
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.urlparser import urlparser
from Plugins.Extensions.IPTVPlayer.tools.iptvfavourites import IPTVFavourites
from Plugins.Extensions.IPTVPlayer.tools.iptvhostgroups import IPTVHostsGroups
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (
    CFakeMoviePlayerOption, ClearTmpCookieDir, ClearTmpJSCacheDir,
    CMoviePlayerPerHost, CSearchHistoryHelper, GetAvailableIconSize,
    GetE2VideoMode, GetEnabledHostsList, GetFavouritesDir, GetHiddenItemsDir,
    GetHostsAliases, GetHostsList, GetHostsOrderList, GetIconDir,
    GetIPTVPlayerVersion, GetPluginDir, SaveHostsOrderList,
    SetE2VideoMode, SetTmpCookieDir, SetTmpJSCacheDir, SortHostsList,
    TestTmpCookieDir, TestTmpJsCacheDir, colorateText, eConnectCallback, formatBytes,
    get_free_space, iptv_system, isFreeSpaceAvailable, is_host_enabled,
    mkdirs, printDBG, printExc, rm, touch)

from Plugins.Extensions.IPTVPlayer.suggestions.filmweb import FilmwebSuggestionsProvider
from Plugins.Extensions.IPTVPlayer.suggestions.imdb import ImdbSuggestionsProvider
from Plugins.Extensions.IPTVPlayer.suggestions.google import GoogleSuggestionsProvider
from Plugins.Extensions.IPTVPlayer.suggestions.moviepilot import MoviepilotSuggestionsProvider

@dataclass(repr=True, unsafe_hash=True)
class Position:
    x:int
    y:int

class E2iPlayerWidget(Screen):

    downloadManager = None

    ICONS_FILESNAMES = {
        DisplayItemType.MARKER: 'MarkerItem.png',
        DisplayItemType.SUB_PROVIDER: 'CategoryItem.png',
        DisplayItemType.SUBTITLE: 'ArticleItem.png',
        DisplayItemType.CATEGORY: 'CategoryItem.png',
        DisplayItemType.MORE: 'MoreItem.png',
        DisplayItemType.VIDEO: 'VideoItem.png',
        DisplayItemType.AUDIO: 'AudioItem.png',
        DisplayItemType.SEARCH: 'SearchItem.png',
        DisplayItemType.ARTICLE: 'ArticleItem.png',
        DisplayItemType.PICTURE: 'PictureItem.png',
        DisplayItemType.DATA: 'DataItem.png',
        }

    def __init__(self, session):
        self.session = session
        super().__init__(session)

        self.IPTV_VERSION = GetIPTVPlayerVersion()
        printDBG(f"E2iPlayerWidget.__init__ desktop IPTV_VERSION[{self.IPTV_VERSION}]\n")

        self.initCachePath()
        self.enough_free_space = isFreeSpaceAvailable(config.plugins.iptvplayer.cachepath.value, 10)

        self.iconManager = self.initIconManager(self.enough_free_space)

        self.displItems:List[DisplayItem]=[]
        self.current_pos:Position = Position(x=1,y=1)

        self.index:int = 0

        # self.color_selected = "#1e5c61"
        self.color_selected = "#60611e" #"#60611e"
        self.color_cover_background = "#000b2729" # "#100b2729"
        self.color_not_selected = "#00061617" # "#061617"

        self.index_start=-1
        self.index_starts=[]

        self.positions:List[Position] =[]

        self.screenwidth = getDesktop(0).size().width()
        self.screenheight = getDesktop(0).size().height()

        # skin resolution
        self.skinResolutionType = 'sd'
        if self.screenwidth:
            if self.screenwidth > 1900:
                font_size = 18
                headertext_font_size = 20
                self.skinResolutionType = 'hd'
            elif self.screenwidth > 1200:
                font_size = 16
                headertext_font_size = 18
                self.skinResolutionType = 'hd_ready'

        self.back = False

        self.posters_area_x = self.screenwidth * 0.03
        self.posters_area_y = self.screenheight * 0.07

        self.posters_area_width = self.screenwidth-2*self.posters_area_x
        self.posters_area_height = self.screenheight-2*self.posters_area_y

        self.scrollbar_width = self.screenwidth * 0.006
        self.scrollbar_height = self.posters_area_height

        self.scrollbar_pos_x = self.posters_area_x + self.posters_area_width
        self.scrollbar_pos_y = self.posters_area_y

        self.scrollbar_line_width = 1
        self.scrollbar_line_height = self.posters_area_height

        self.scrollbar_line_pos_x = self.scrollbar_pos_x  + self.scrollbar_width/2
        self.scrollbar_line_pos_y = self.posters_area_y

        console_px = self.posters_area_x
        console_py = self.posters_area_y + self.posters_area_height + 5
        console_width = self.posters_area_width
        console_height = self.screenheight - console_py - 10

        key_width, key_height = 95, 27
        key_icon_width , key_icon_height = 95,4

        key_blue_x, key_blue_y = self.posters_area_x + self.posters_area_width -key_width, self.posters_area_y - 40
        key_green_x, key_green_y = key_blue_x - key_width, self.posters_area_y - 40
        key_yellow_x, key_yellow_y = key_green_x - key_width, self.posters_area_y - 40
        key_red_x, key_red_y = key_yellow_x - key_width, self.posters_area_y - 40

        key_blue_icon_x, key_blue_icon_y = self.posters_area_x + self.posters_area_width - key_icon_width, self.posters_area_y - 10
        key_green_icon_x, key_green_icon_y = key_blue_icon_x - key_icon_width, self.posters_area_y - 10
        key_yellow_icon_x, key_yellow_icon_y = key_green_icon_x - key_icon_width, self.posters_area_y - 10
        key_red_icon_x, key_red_icon_y = key_yellow_icon_x - key_icon_width, self.posters_area_y - 10

        statustext_x, statustext_y = 0, self.screenheight/2
        statustext_width, statustext_height = self.screenwidth,55

        spinner_x, spinner_y = self.screenwidth/2 - 32, statustext_y + statustext_height

        self.numColumns = 5
        if  columns:= config.plugins.iptvplayer.poster_columns:
            self.numColumns=int(columns.value)

        self.numRows = 3
        if rows:=config.plugins.iptvplayer.poster_rows:
            self.numRows=int(rows.value)

        self.MAX_ITEMS= self.numColumns * self.numRows
        self.dictPIX = {}

        self.init_icons()

        tmpX = (self.screenwidth - 2 * self.posters_area_x) / self.numColumns
        tmpY = (self.screenheight -2 * self.posters_area_y) / self.numRows

        self.positions = [Position(x, y) for x in range(1, self.numColumns + 1) for y in range(1, self.numRows + 1)]

        playerlogo_width = 120 # constant
        playerlogo_height = 40 # constant

        background = ''
        if config.plugins.iptvplayer.background.value:
            background = f'''<ePixmap pixmap="{GetIconDir('background.jpg')}" position="0,0" size="{self.screenwidth},{self.screenheight}" scale="1"/>'''

        skin = f"""
                <screen position="center,center" size="{self.screenwidth},{self.screenheight}"  title="E2iPlayer {self.IPTV_VERSION}" font="Regular; 10" flags="wfNoBorder">
                    {background}
                    <widget name="playerlogo" zPosition="4" position="{self.posters_area_x},{self.posters_area_y}-{playerlogo_height}-5"  size="{playerlogo_width},{playerlogo_height}" alphatest="blend" />
                    <widget name="headertext" zPosition="4" position="{self.posters_area_x}+{playerlogo_width}+5,{self.posters_area_y}-{playerlogo_height}-5" size="835,{playerlogo_height}" font="Regular; {headertext_font_size}" transparent="1" halign="left" valign="center" backgroundColor="#191919" foregroundColor="white" borderWidth="1" borderColor="black" />

                    <widget name="spinner"    zPosition="3" position="{spinner_x}, {spinner_y}"      size="16,16" transparent="1" alphatest="blend" />
                    <widget name="spinner_1"  zPosition="2" position="{spinner_x}, {spinner_y}"      size="16,16" transparent="1" alphatest="blend" />
                    <widget name="spinner_2"  zPosition="2" position="{spinner_x+16}, {spinner_y}"   size="16,16" transparent="1" alphatest="blend" />
                    <widget name="spinner_3"  zPosition="2" position="{spinner_x+2*16}, {spinner_y}" size="16,16" transparent="1" alphatest="blend" />
                    <widget name="spinner_4"  zPosition="2" position="{spinner_x+3*16}, {spinner_y}" size="16,16" transparent="1" alphatest="blend" />

                    <widget name="statustext" zPosition="10" position="{statustext_x}, {statustext_y}" size="{statustext_width}, {statustext_height}" font="Regular;20" halign="center" valign="center" transparent="1" backgroundColor="#191919" borderWidth="1" borderColor="black"  />
                    <widget name="sequencer"  zPosition="6"  position="234,43" size="979,385" font="Regular;160" halign="center" valign="center" transparent="1" backgroundColor="191919" />

                    <widget name="console"    zPosition="10" position="{console_px},{console_py}" size="{console_width}, {console_height}" font="Regular; {font_size-2}"  backgroundColor="#191919" foregroundColor="#ffcc99" transparent="1"/>

                    <eLabel position="{self.posters_area_x}-1,{self.posters_area_y}" size="1,{self.posters_area_height}" transparent="0" zPosition="21" backgroundColor="#666666" />
                    <eLabel position="{self.posters_area_x}-1,{self.posters_area_y}" size="{self.posters_area_width}, 1" transparent="0" zPosition="21" backgroundColor="#666666" />
                    <eLabel position="{self.posters_area_x}-1,{self.posters_area_y}+{self.posters_area_height}" size="{self.posters_area_width}, 1" transparent="0" zPosition="21" backgroundColor="#666666" />

                """

        for pos in self.positions:
                pX = (self.posters_area_x + tmpX * (pos.x - 1))
                pY = (self.posters_area_y + tmpY * (pos.y - 1))
                skin += f"""
                            <widget name="cover_back_{pos.x}{pos.y}"  zPosition="3" position="{pX},{pY}"            size="{tmpX}-5,{tmpY}-5" backgroundColor="{self.color_cover_background}" />
                            <widget name="cover_label_{pos.x}{pos.y}" zPosition="4" position="{pX},{pY + tmpY-tmpY*0.1-5}" size="{tmpX}-5,{tmpY*0.1}" font="Regular; {font_size}" noWrap="1" halign="center" valign="center" borderWidth="2" borderColor="black"  backgroundColor="{self.color_not_selected}" />
                            <widget name="cover_{pos.x}{pos.y}"       zPosition="5" position="{pX}+8,{pY}+10"       size="{tmpX}-20,{tmpY}-50" alphatest="blend"/>
                            <widget name="watched_{pos.x}{pos.y}"     zPosition="6" position="{pX}+8,{pY}+10"       size="{tmpX}-20,{tmpY}-50" transparent="1" alphatest="blend" />
                        """
        skin += f"""
                    <ePixmap pixmap="/usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer/icons/line_red.png"  position="{key_red_icon_x}, {key_red_icon_y}" size="{key_icon_width} , {key_icon_height}" scale="1" alphatest="blend" transparent="1" />
                    <widget source="key_red" render="Label" position="{key_red_x}, {key_red_y}" size="{key_width}, {key_height}" zPosition="1" font="Regular; {font_size}" halign="center" backgroundColor="#191919" transparent="1" foregroundColor="white" shadowColor="blak" shadowOffset="-1,-1"  borderWidth="1" borderColor="black" />

                    <ePixmap pixmap="/usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer/icons/line_yellow.png"  position="{key_yellow_icon_x}, {key_yellow_icon_y}" size="{key_icon_width} , {key_icon_height}" scale="1" alphatest="blend" zPosition="1" transparent="1" />
                    <widget source="key_yellow" render="Label" position="{key_yellow_x}, {key_yellow_y}" size="{key_width}, {key_height}" zPosition="1" font="Regular; {font_size}" halign="center" backgroundColor="#191919" transparent="1" foregroundColor="white" shadowColor="black" shadowOffset="-1,-1" borderWidth="1" borderColor="black"  />

                    <ePixmap pixmap="/usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer/icons/line_green.png"  position="{key_green_icon_x}, {key_green_icon_y}" size="{key_icon_width} , {key_icon_height}" scale="1" alphatest="blend" />
                    <widget name="key_green_ext" zPosition="1" position="{key_green_x}, {key_green_y}" size="{key_width}, {key_height}" font="Regular; {font_size}" halign="center" backgroundColor="#191919" transparent="1" foregroundColor="white" shadowColor="black" shadowOffset="-1,-1" borderWidth="1" borderColor="black" />

                    <ePixmap pixmap="/usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer/icons/line_blue.png" position="{key_blue_icon_x}, {key_blue_icon_y}" size="{key_icon_width} , {key_icon_height}" scale="1" alphatest="blend" transparent="1" />
                    <widget source="key_blue" render="Label" zPosition="1" position="{key_blue_x}, {key_blue_y}" size="{key_width} , {key_height}" font="Regular; {font_size}" halign="center" backgroundColor="#191919" transparent="1" foregroundColor="white" shadowColor="black" shadowOffset="-1,-1"  borderWidth="1" borderColor="black"  />

                    <widget name="scollbar_line" zPosition="5" position="{self.scrollbar_line_pos_x}, {self.scrollbar_line_pos_y}" size="{self.scrollbar_line_width}, {self.scrollbar_line_height}" transparent="0" backgroundColor="#666666" />
                    <widget name="scollbar"      zPosition="6" position="{self.scrollbar_pos_x},{self.scrollbar_pos_y}" size="{self.scrollbar_width},{self.scrollbar_height}" transparent="0" backgroundColor="#5F9EA0" />
                </screen>
                """

        self.skin = skin

        self.recorderMode = False

        self.currentService = self.session.nav.getCurrentlyPlayingServiceReference()
        if config.plugins.iptvplayer.disable_live.value:
            self.session.nav.stopService()

        self["key_red"] = StaticText(_("Exit"))
        self["key_green_ext"] = Label(_("Download"))
        self["key_yellow"] = StaticText(_("Refresh"))
        self["key_blue"] = StaticText(_("More"))

        self["statustext"] = Label("Loading...")
        self["headertext"] = Label()
        self["sequencer"] = Label()
        self["playerlogo"] = Cover2()

        self["actions"] = ActionMap(["IPTVPlayerListActions", "WizardActions", "DirectionActions", "ColorActions", "NumberActions"],
        {
            "red": self.red_pressed,
            "green": self.green_pressed,
            "yellow": self.yellow_pressed,
            "blue": self.blue_pressed,
            "ok": self.ok_pressed,
            "back": self.back_pressed,
            "info": self.info_pressed,
            "8": self.startAutoPlaySequencer,
            "0": self.ok_pressed0,
            "1": self.ok_pressed1,
            "2": self.ok_pressed2,
            "3": self.ok_pressed3,
            "4": self.ok_pressed4,
            "play": self.startAutoPlaySequencer,
            "menu": self.menu_pressed,
            "tools": self.blue_pressed,
            "record": self.green_pressed,
            'down': self.down_pressed,
            'right': self.right_pressed,
            'up': self.up_pressed,
            'left': self.left_pressed,
        }, -1)

        try:
            for idx in range(5):
                spinnerName = "spinner"
                if idx:
                    spinnerName += '_%d' % idx
                self[spinnerName] = Cover3()
        except Exception:
            printExc()

        self.spinnerPixmap = [LoadPixmap(GetIconDir('radio_button_on.png')), LoadPixmap(GetIconDir('radio_button_off.png'))]
        self.useAlternativePlayer = False

        self.showHostsErrorMessage = True
        self.iconDownloadProcesses = []

        self.onClose.append(self.__onClose)
        self.onShow.append(self.onStart)
        self.onLayoutFinish.append(self._layoutFinished)

        #Defs
        self.searchPattern = CSearchHistoryHelper.loadLastPattern()[1]
        self.searchType = None
        self.workThread = None
        self.fetchMoreItemsThead = None
        self.group = None
        self._groupObj = IPTVHostsGroups()
        self.host = None
        self.hostName = ''
        self.hostTitle = ''
        self.hostFavTypes = []

        self.nextSelIndex = 0
        self.currSelIndex = 0

        self.prevSelList = DisplayList()
        self.categoryList = DisplayList()

        self.currList = DisplayList()
        self.currItem = DisplayItem()

        self.visible = True
        self.bufferSize = config.plugins.iptvplayer.requestedBuffSize.value * 1024 * 1024
        self.activePlayer = None
        self.canRandomizeList = False
        self.prevVideoMode = None
        self.statusTextValue = ""
        self.enabledHostsListOld = []
        self.checkWrongImage = True
        self.downloadable = False
        self.colorEnabled = parseColor("#FFFFFF")
        self.colorDisabled = parseColor("#808080")

        self.hostsAliases = GetHostsAliases()

        # Init Proxy Queue
        # register function in main Queue
        if not asynccall.gMainFunctionsQueueTab[0]:
            asynccall.gMainFunctionsQueueTab[0] = asynccall.CFunctionProxyQueue(self.session)
        asynccall.gMainFunctionsQueueTab[0].clearQueue()
        asynccall.gMainFunctionsQueueTab[0].setProcFun(self.doProcessProxyQueueItem)

        #main Queue
        self.mainTimer = eTimer()
        self.mainTimer_conn = eConnectCallback(self.mainTimer.timeout, self.processProxyQueue)
        # every 100ms Proxy Queue will be checked
        self.mainTimer_interval = 100
        self.mainTimer.start(self.mainTimer_interval, True)

        # spinner timer
        self.spinnerTimer = eTimer()
        self.spinnerTimer_conn = eConnectCallback(self.spinnerTimer.timeout, self.updateSpinner)
        self.spinnerTimer_interval = 200
        self.spinnerEnabled = False

        # init download manager
        if not E2iPlayerWidget.downloadManager:
            IPTVDMNotification().dialogInit(session)
            printDBG('============Initialize Download Menager============')
            E2iPlayerWidget.downloadManager = IPTVDMApi(2, int(config.plugins.iptvplayer.IPTVDMMaxDownloadItem.value), IPTVDMNotification())
            if config.plugins.iptvplayer.IPTVDMRunAtStart.value:
                E2iPlayerWidget.downloadManager.runWorkThread()

        self.autoPlaySeqStarted = False
        self.autoPlaySeqTimer = eTimer()
        self.autoPlaySeqTimer_conn = eConnectCallback(self.autoPlaySeqTimer.timeout, self.autoPlaySeqTimerCallBack)
        self.autoPlaySeqTimerValue = 0

        self.checkTmpCookiesDir()
        self.checkTmpJsDir()

        self["console"] = Label()
        self.console_timer = eTimer()
        self.console_timer_conn = eConnectCallback( self.console_timer.timeout, self.updateBottomPanel)
        self.console_timer_intervall = 250

        # poster
        for pos in self.positions:
            self[f"cover_{pos.x}{pos.y}"] = Cover()
            self[f"cover_label_{pos.x}{pos.y}"] = Label()
            self.setcoverBackground(pos, Label())
            self[f"watched_{pos.x}{pos.y}"] = Cover3()

        self["scollbar"] = Label()
        self["scollbar_line"] = Label()

        self.hidden = False

        asynccall.setMainThreadId()

    def __del__(self):
        printDBG("E2iPlayerWidget.__del__")

    def __onClose(self):
        self.session.nav.playService(self.currentService)

        if self.iconManager:
            self.iconManager.clearDQueue()
            self.iconManager = None

        try:
            self.stopAutoPlaySequencer()
            self.autoPlaySeqTimer_conn = None
            self.autoPlaySeqTimer = None
        except Exception:
            printExc()

        try:
            asynccall.gMainFunctionsQueueTab[0].setProcFun(None)
            asynccall.gMainFunctionsQueueTab[0].clearQueue()
            iptv_system('echo 1 > /proc/sys/vm/drop_caches')
        except Exception:
            printExc()

        self.mainTimer_conn = None
        self.mainTimer = None
        self.spinnerTimer_conn = None
        self.spinnerTimer = None
        self.activePlayer = None

    def init_icons(self):
        for key, pixFile in self.ICONS_FILESNAMES.items():
            self.dictPIX[key] = GetIconDir(pixFile)

    def _hide(self):
        if not self.hidden:
            #self["playerlogo"].hide()
            #self["headertext"].hide()
            self["scollbar"].hide()
            #self["scollbar_line"].hide()
            self['console'].hide()

            for pos in self.positions:
                self.getCover(pos).hide()
                self.getCoverLabel(pos).hide()
                self.getWatchedIcon(pos).hide()
                self.getcoverBackground(pos).hide()

            self.hidden = True

    def _show(self):
        if self.hidden:
            #self["playerlogo"].show()
            #self["headertext"].show()
            self["scollbar"].show()
            #self["scollbar_line"].show()

            for pos in self.positions:
                self.getCover(pos).show()
                self.getCoverLabel(pos).show()
                self.getcoverBackground(pos).show()

            for i in self.displItems:
                if i.isWatched:
                    self.getWatchedIcon(i.pos).show()
            self.hidden = False

    def checkForMoreItems(self, res=None):
        if self.currList.isNextPageAvailable() and len(self.currList[self.displItems[-1].idx:]) < 12:
            self.requestListFromHost('ForMore', self.currItem.idx, '')


    def updateScrollbar(self):
        if not self.displItems:
            return

        idx = self.currItem.idx

        # position
        rows = ceil(len(self.currList)/self.numColumns)
        curr_row = idx // self.numColumns
        self["scollbar"].setPosition(self.scrollbar_pos_x, self.posters_area_y + (curr_row/rows)*self.posters_area_height)

        # size
        #tmp = ceil(len(self.currList)/6)
        hight = self.posters_area_height//rows
        self["scollbar"].resize(self.scrollbar_width, hight)

    def setCurrentPosition(self, pos:Position):
        self.current_pos= copy(pos)
        self.moveMarker(self.current_pos)

    def getCurrentIndex(self):
        if self.currItem:
            return self.currItem.idx
        return 0

    def getItemAtPosition(self, pos:Position):
        for i in self.displItems:
            if i.pos == pos:
                return i
        return None

    def getItemAt(self, x, y):
        return self.getItemAtPosition(Position(x,y))

    def getItemAtIndex(self, idx):
        for i in self.displItems:
            if i.idx == idx:
                return i
        return None

    def updateCurrentItem(self):
        self.currItem = self.getItemAtPosition(self.current_pos)

    def resetPosition(self):
        self.current_pos = Position(x=1,y=1)

    def setItems(self, refresh=0):
        self.resetPosition()

        # refresh=2 => load more items
        if refresh != 2:
            self.loadNextItems(True)

    def loadNextItems(self, isInit=False):

        self.displItems.clear()

        if isInit and not self.back:
            self.index = 0

        if self.back:
            self.back = False

        self.index_start = self.index + (0 if isInit else 1)
        next_index = self.index_start

        if next_index >= (self.numColumns * self.numRows)-self.numColumns:
            next_index -= self.numColumns

        tmp_list = self.currList[next_index:next_index+self.MAX_ITEMS]

        if not tmp_list:
            return

        for y in range(1, self.numRows+1):
            for x in range(1, self.numColumns +1):
                item = tmp_list.pop(0)
                item.idx = next_index
                item.pos = Position(x,y)
                self.displItems.append(item)
                if not tmp_list:
                    break
                next_index += 1
            else:
                continue
            break

        self.index = max(next_index - (self.numColumns if self.numRows > 2 else 0), 0)
        self.fillPosterFields()

    def loadPrevEtems(self):

        tmp_idx = self.getItemAt(1,1).idx-self.numColumns

        tmp_list = self.currList[tmp_idx:tmp_idx+self.MAX_ITEMS]

        if not tmp_list:
            return

        self.displItems.clear()

        for y in range(1, self.numRows+1):
            for x in range(1, self.numColumns +1):
                item = tmp_list.pop(0)
                item.idx = tmp_idx
                item.pos = Position(x, y)
                self.displItems.append(item)
                if not tmp_list:
                    break
                tmp_idx += 1
            else:
                continue
            break

        self.index = max(tmp_idx-self.numColumns, 0)

        self.fillPosterFields()

    def resetCovers(self):
        for pos in self.positions:
            self.getCover(pos).instance.setPixmap(None)

            self.getCoverLabel(pos).setText("")
            self.getCoverLabel(pos).instance.setBackgroundColor(parseColor(self.color_not_selected))
            self.getcoverBackground(pos).instance.setBackgroundColor(parseColor(self.color_cover_background))

    def fillPosterFields(self):
        self.resetCovers()
        sc = AVSwitch().getFramebufferScale()
        for i in self.displItems:
            if i.type == DisplayItemType.VIDEO or i.withMiniCover:
                iconPath = self.iconManager.getIconPathFromAAQueue(i.iconimage)
                picload = self.getCover(i.pos).picload
                picload.setPara((self.getCover(i.pos).instance.size().width(), self.getCover(i.pos).instance.size().height(),
                    sc[0],
                    sc[1],
                    1,
                    1,
                    '#00000000'))
                if picload.startDecode(iconPath, 0, 0, False) == 0:
                    ptr = picload.getData()
                    if ptr is not None:
                        self.getCover(i.pos).instance.setPixmap(ptr)
            else:
                iconPath = self.dictPIX.get(i.type, None)
                self.getCover(i.pos).instance.setPixmapScaleFlags(BT_HALIGN_CENTER | BT_VALIGN_CENTER)
                self.getCover(i.pos).instance.setPixmapFromFile(iconPath)
            #self.getCover(i.pos).instance.setBorderWidth(1)
            #self.getCover(i.pos).instance.setBorderColor(parseColor("#FFFFFF"))
            title = i.getDisplayTitle()
            if i.isWatched:
                title = colorateText(title, 'gray')
            elif i.isDownloadable():
                title = colorateText(title, 'yellow')
            self.getCoverLabel(i.pos).setText(title)
        self._show()
        asynccall.AsyncMethod(self.checkForMoreItems)()

    def getCover(self, pos: Position):
        return self[f"cover_{pos.x}{pos.y}"]

    def getCoverLabel(self, pos: Position):
        return self[f"cover_label_{pos.x}{pos.y}"]

    def getcoverBackground(self, pos: Position):
        return self[f"cover_back_{pos.x}{pos.y}"]

    def setcoverBackground(self, pos: Position, widget):
        self[f"cover_back_{pos.x}{pos.y}"] = widget

    def getWatchedIcon(self, pos: Position):
        return self[f"watched_{pos.x}{pos.y}"]

    def moveToIndex(self, idx):
        for i in self.displItems:
            if i.idx == idx:
                self.setCurrentPosition(i.pos)

    def moveDown(self, ret=None):
        try:
            prevPos = copy(self.current_pos)
            if self.current_pos.y == max(i.pos.y for i in self.displItems if i.pos.x == self.current_pos.x):
                if len(self.currList) > self.displItems[-1].idx + 1:
                    self.loadNextItems()
                    if not self.getItemAtPosition(self.current_pos):
                        self.current_pos.x = max(i.pos.x for i in self.displItems if i.pos.y == self.current_pos.y )
                else:
                    if [i for i in self.displItems if i.pos.y == self.current_pos.y+1]:
                        self.current_pos.y += 1
                        self.current_pos.x = max(i.pos.x for i in self.displItems if i.pos.y == self.current_pos.y )
            else:
                if self.getItemAt(self.current_pos.x, self.current_pos.y + 1):
                    self.current_pos.y += 1
            self.moveMarker(prevPos)
        except Exception:
            printExc()

        self.updateScrollbar()

    def moveUp(self):
        try:
            prevPos = copy(self.current_pos)
            if self.current_pos.y == 1:
                if self.getItemAt(1,1).idx > 0:
                    self.loadPrevEtems()
            else:
                self.current_pos.y -= 1

            self.moveMarker(prevPos)
        except Exception:
            printExc()

        self.updateScrollbar()

    def moveRight(self):
        try:
            # at then end
            prevPos = copy(self.current_pos)
            if self.current_pos.x == max(i.pos.x for i in self.displItems if i.pos.y==self.current_pos.y):
                self.current_pos.x = 1
            else:
                if self.getItemAt(self.current_pos.x + 1, self.current_pos.y):
                    self.current_pos.x += 1

            self.moveMarker(prevPos)
        except Exception:
            printExc()

    def moveLeft(self):
        try:
            # at the beginning
            prevPos = copy(self.current_pos)
            if self.current_pos.x == 1:
                self.current_pos.x = max(i.pos.x for i in self.displItems if i.pos.y == self.current_pos.y)
            else:
                self.current_pos.x -= 1

            self.moveMarker(prevPos)
        except Exception:
            printExc()

    def moveMarker(self, prevPos):
        self.getCoverLabel(prevPos).instance.setBackgroundColor(parseColor(self.color_not_selected))
        self.getCoverLabel(prevPos).instance.invalidate()

        self.getCoverLabel(self.current_pos).instance.setBackgroundColor(parseColor('#494a16'))
        self.getCoverLabel(self.current_pos).instance.invalidate()

        self.getcoverBackground(prevPos).instance.setBackgroundColor(parseColor(self.color_cover_background))
        self.getcoverBackground(prevPos).instance.invalidate()

        self.getcoverBackground(self.current_pos).instance.setBackgroundColor(parseColor(self.color_selected))
        self.getcoverBackground(self.current_pos).instance.invalidate()

        self.updateCurrentItem()
        self.updateDownloadButton()
        self.startButtomConsoleTimer()

    def startButtomConsoleTimer(self):
        self["console"].hide()
        self.console_timer.start(self.console_timer_intervall)

    def checkTmpJsDir(self):
        """test if path for js temporary files is writable"""
        try:
            TestTmpJsCacheDir()
            ClearTmpJSCacheDir()
        except Exception as e:
            SetTmpJSCacheDir()
            msg1 = _("Critical Error – JS can't be saved!")
            msg2 = _("Last error:\n%s" % str(e))
            msg3 = _("Please make sure that the folder for cache data (set in the configuration) is writable.")
            IPTVPlayerNotificationList().push('%s\n\n%s\n\n%s' % (msg1, msg2, msg3), 'error', 20)

    def checkTmpCookiesDir(self):
        """test if path for cookies temporary files is writable"""
        try:
            TestTmpCookieDir()
            ClearTmpCookieDir()
        except Exception as e:
            SetTmpCookieDir()
            msg1 = _("Critical Error – cookie can't be saved!")
            msg2 = _("Last error:\n%s" % str(e))
            msg3 = _("Please make sure that the folder for cache data (set in the configuration) is writable.")
            IPTVPlayerNotificationList().push('%s\n\n%s\n\n%s' % (msg1, msg2, msg3), 'error', 20)


    def initIconManager(self, enough_free_space:bool):
        if config.plugins.iptvplayer.showcover.value:
            iconManager = IconManager(enough_free_space)
            #iconManager.setUpdateCallBack(self.checkIconCallBack)
        return iconManager

    def initCachePath(self):
        if not os_path.exists(config.plugins.iptvplayer.cachepath.value):
            mkdirs(config.plugins.iptvplayer.cachepath.value)

    def updateDownloadButton(self):
        self.downloadable = False
        try:
            if self["cover_11"].visible: # any cover
                self.downloadable = self.currItem.isDownloadable()
                if self.downloadable and self.currItem.urlItems[0].url.startswith('file://'): # workaround for LocalMedia
                    self.downloadable = False
        except Exception:
            pass

        try:
            if self.downloadable:
                self["key_green_ext"].instance.setForegroundColor(self.colorEnabled)
            else:
                self["key_green_ext"].instance.setForegroundColor(self.colorDisabled)
        except Exception:
            printExc()

    def getSkinResolutionType(self):
        return self.skinResolutionType

    def setStatusTex(self, msg):
        self.statusTextValue = msg
        self["statustext"].setText(msg)

    def loadSpinner(self):
        try:
            if "spinner" in self:
                self["spinner"].setPixmap(self.spinnerPixmap[0])
                for idx in range(4):
                    spinnerName = 'spinner_%d' % (idx + 1)
                    self[spinnerName].setPixmap(self.spinnerPixmap[1])
        except Exception:
            printExc()

    def showSpinner(self):
        if None is not self.spinnerTimer:
            self._setSpinnerVisibility(True)
            self.spinnerTimer.start(self.spinnerTimer_interval, True)

    def hideSpinner(self):
        self._setSpinnerVisibility(False)

    def _setSpinnerVisibility(self, visible=True):
        self.spinnerEnabled = visible
        try:
            if "spinner" in self:
                for idx in range(5):
                    spinnerName = "spinner"
                    if idx:
                        spinnerName += '_%d' % idx
                    self[spinnerName].visible = visible
        except Exception:
            printExc()


    def updateSpinner(self):
        try:
            if self.spinnerEnabled and self.workThread:
                if self.workThread.isAlive():
                    timeout = IPTVPlayerSleep().getTimeout()
                    msg = _(f"wait {timeout} second{'s' if timeout > 1 else ''}")
                    msg = f'{self.statusTextValue} ({msg})'
                    self["statustext"].setText(msg if timeout > 0 else self.statusTextValue)

                    if "spinner" in self:
                        x, y = self["spinner"].getPosition()
                        x += self["spinner"].getWidth()
                        if x > self["spinner_4"].getPosition()[0]:
                            x = self["spinner_1"].getPosition()[0]
                        self["spinner"].setPosition(x, y)

                    if self.spinnerTimer:
                        self.spinnerTimer.start(self.spinnerTimer_interval, True)
                        return

                elif not self.workThread.isFinished():
                    if self.hostName not in GetHostsList(fromList=True, fromHostFolder=False):
                        message = _('It seems that the host "%s" has crashed.') % self.hostName
                        message += _('\nThis host is not integral part of the E2iPlayer plugin.\nIt is not supported by E2iPlayer team.')
                        self.session.open(MessageBox, message, type=MessageBox.TYPE_ERROR)
                    else:
                        message = _('It seems that the host "%s" has crashed. Do you want to report this problem?') % self.hostName
                        message += "\n"
                        message += _('\nMake sure you are using the latest version of the plugin.')
                        message += _('\nYou can also report problem here: \n https://gitlab.com/rabinovic/e2iplayer/issues')
                        self.session.openWithCallback(self.reportHostCrash, MessageBox, text=message, type=MessageBox.TYPE_YESNO)
            self.hideSpinner()
        except Exception:
            printExc()

    def reportHostCrash(self, ret):
        try:
            if ret:
                try:
                    exceptStack = self.workThread.getExceptStack()
                    reporter = GetPluginDir('iptvdm/reporthostcrash.py')
                    msg = urllib_quote('%s|%s|%s|%s' % ('HOST_CRASH', self.IPTV_VERSION, self.hostName, self.getCategoryPath()))
                    self.crashConsole = iptv_system('python "%s" "http://iptvplayer.vline.pl/reporthostcrash.php?msg=%s" "%s" 2&>1 > /dev/null' % (reporter, msg, exceptStack))
                    printDBG(msg)
                except Exception:
                    printExc()
            self.workThread = None
            self.prevSelList = []
            self.back_pressed()
        except Exception:
            printExc()

    def processIPTVNotify(self, callbackArg1=None, callbackArg2=None):
        try:
            notifyObj = IPTVPlayerNotificationList()
            if not notifyObj.isEmpty():
                notification = notifyObj.pop()
                if notification:
                    typeMap = {'info': MessageBox.TYPE_INFO,
                               'error': MessageBox.TYPE_ERROR,
                               'warning': MessageBox.TYPE_WARNING,
                              }
                    self.session.openWithCallback(self.processIPTVNotify, MessageBox, notification.message, type=typeMap.get(notification.type, MessageBox.TYPE_INFO), timeout=notification.timeout)
                    return
        except Exception:
            printExc()
        self.processProxyQueue()

    def processProxyQueue(self):
        if self.mainTimer:
            funName = asynccall.gMainFunctionsQueueTab[0].peekClientFunName()
            notifyObj = IPTVPlayerNotificationList()
            if funName and notifyObj and not notifyObj.isEmpty() and funName in ['showArticleContent', 'selectMainVideoLinks', 'selectResolvedVideoLinks', 'reloadList']:
                self.processIPTVNotify()
            else:
                asynccall.gMainFunctionsQueueTab[0].processQueue()
                self.mainTimer.start(self.mainTimer_interval, True)

    def doProcessProxyQueueItem(self, item):
        #printDBG('doProcessProxyQueueItem callback from old workThread [%s]' % (item.retValue))
        try:
            if None is  item.retValue[0] or self.workThread == item.retValue[0]:
                getattr(self, item.clientFunName)(item.retValue[1])
            else:
                printDBG('doProcessProxyQueueItem callback from old workThread[%r][%s]' % (self.workThread, item.retValue))
        except Exception:
            printExc()

    def getArticleContentCallback(self, thread, ret):
        asynccall.gMainFunctionsQueueTab[0].addToQueue("showArticleContent", [thread, ret])

    def selectHostVideoLinksCallback(self, thread, ret):
        asynccall.gMainFunctionsQueueTab[0].addToQueue("selectMainVideoLinks", [thread, ret])

    def getResolvedURLCallback(self, thread, ret):
        asynccall.gMainFunctionsQueueTab[0].addToQueue("selectResolvedVideoLinks", [thread, ret])

    def callbackGetList(self, addParam, thread, ret):
        asynccall.gMainFunctionsQueueTab[0].addToQueue("reloadList", [thread, {'add_param': addParam, 'ret': ret}])

    # method called from IconManager when a new icon has been dowlnoaded
    # def checkIconCallBack(self, ret):
    #     asynccall.gMainFunctionsQueueTab[0].addToQueue("displayIcon", [None, ret])

    def isInWorkThread(self):
        return None is not self.workThread and (not self.workThread.isFinished() or self.workThread.isAlive())

    def down_pressed(self):
        self.moveDown()

    def up_pressed(self):
        self.moveUp()

    def left_pressed(self):
        self.moveLeft()

    def right_pressed(self):
        self.moveRight()

    def red_pressed(self):
        self.stopAutoPlaySequencer()
        #self.close()
        #self.selectHost()
        self.selectHostFromGroup()

    def green_pressed(self):
        self.stopAutoPlaySequencer()
        self.updateDownloadButton()
        self.recorderMode = self.downloadable
        if self.downloadable:
            self.ok_pressed('green')

    def yellow_pressed(self):
        self.stopAutoPlaySequencer()
        self.getRefreshedCurrList()

    def blue_pressed(self):
        # For Keyboard test
        if False:
           from Plugins.Extensions.IPTVPlayer.components.e2ivksuggestion import AutocompleteSearch
           from Plugins.Extensions.IPTVPlayer.suggestions.google import SuggestionsProvider
           self.session.open(GetVirtualKeyboard(), additionalParams={'autocomplete':AutocompleteSearch(SuggestionsProvider(True))})
           return

        #For subtitles test
        if False:
            from Plugins.Extensions.IPTVPlayer.components.iptvsubdownloader import IPTVSubDownloaderWidget
            self.session.open(IPTVSubDownloaderWidget, params={'movie_title': 'elementary s02e03'})
            return

        #For player test
        if False:
            self.session.open(IPTVExtMoviePlayer, filesrcLocation='', FileName='Test', iconimage='http://www.ngonb.ru/files/res_media.png')
            return

        self.stopAutoPlaySequencer()
        options = []

        if -1 < self.canBeAddedToFavourites()[0]:
            options.append((_("Add item to favourites"), "ADD_FAV"))
            #options.append((_("Edit favourites"), "EDIT_FAV"))
        elif 'favourites' == self.hostName:
            options.append((_("Edit favourites"), "EDIT_FAV"))
            if self.currItem:
                options.append((_("Remove from favourites"), "DELETE_FAV"))

        if None is not self.activePlayer.get('player', None):
            title = _('Change active movie player')
        else:
            title = _('Set active movie player')
        options.append((title, "SetActiveMoviePlayer"))

        if self.canRandomizeList and self.visible and self.currList and not self.isInWorkThread():
            options.append((_('Randomize a playlist'), "RandomizePlayableItems"))
            options.append((_('Reverse a playlist'), "ReversePlayableItems"))

        try:
            host = importlib.import_module(f'Plugins.Extensions.IPTVPlayer.hosts.host{self.hostName}')
            if len(host.GetConfigList()) > 0:
                options.append((_("Configure host"), "HostConfig"))
        except Exception:
            printExc()
        options.append((_("Info"), "info"))
        options.append((_("Download manager"), "IPTVDM"))

        if len(self.categoryList) == 1 and self.categoryList[0] == _("Search history"):
            options.append((_("Remove Item"), "REMOVE_ITEM_FROM_SEARCH_HISTORY"))
            options.append((_("Clear search history"), "CLEAR_SEARCH_HISTORY"))
        else:
            if self.currItem: #self['list'].currentSelection:
                if self.currItem.isHidden:
                    options.append((_("Unhide Item"), "UNHIDE_ITEM"))
                else:
                    options.append((_("Hide Item"), "HIDE_ITEM"))
        self.session.openWithCallback(self.blue_pressed_next, ChoiceBox, title=_("Select option"), list=options)

    def pause_pressed(self):
        printDBG('pause_pressed')
        self.stopAutoPlaySequencer()

    def startAutoPlaySequencer(self):
        if not self.autoPlaySeqStarted:
            self.autoPlaySeqStarted = True
            self.autoPlaySequencerNext(False)

    def stopAutoPlaySequencer(self):
        if self.autoPlaySeqStarted:
            if not config.plugins.iptvplayer.disable_live.value:
                self.session.nav.playService(self.currentService)

            if config.plugins.iptvplayer.autoplay_start_delay.value == 0:
                self.showWindow()

            self.autoPlaySeqTimer.stop()
            self["sequencer"].setText("")
            self.autoPlaySeqStarted = False
            return True
        return False

    def autoPlaySequencerNext(self, goToNext=True):
        if not self.autoPlaySeqStarted:
            printDBG("ERROR in autoPlaySequencerNext - sequencer stopped")
            return

        idx = self.getSelIndex()
        if -1 != idx:
            # find next playable item
            if goToNext:
                idx += 1
                if config.plugins.iptvplayer.autoplay_start_delay.value == 0:
                    self.hideWindow()

            while idx < len(self.currList):
                if self.currList[idx].type in [DisplayItemType.VIDEO, DisplayItemType.AUDIO, DisplayItemType.PICTURE, DisplayItemType.MORE]:
                    break
                idx += 1
            if idx < len(self.currList):
                self.moveToIndex(idx)
                self.sequencerPressOK()
                return
        self.stopAutoPlaySequencer()

    def sequencerPressOK(self):
        self.autoPlaySeqTimerValue = config.plugins.iptvplayer.autoplay_start_delay.value

        if self.autoPlaySeqTimerValue == 0:
            self.ok_pressed('sequencer')
        else:
            self["sequencer"].setText(str(self.autoPlaySeqTimerValue))
            self.autoPlaySeqTimer.start(1000)

    def autoPlaySeqTimerCallBack(self):
        self.autoPlaySeqTimerValue -= 1
        if self.autoPlaySeqTimerValue > 0:
            self["sequencer"].setText(str(self.autoPlaySeqTimerValue))
        else:
            self["sequencer"].setText("")
            self.autoPlaySeqTimer.stop()
            self.ok_pressed('sequencer')

    def checkAutoPlaySequencer(self):
        if self.autoPlaySeqStarted:
            self.autoPlaySequencerNext()
            return True
        return False

    def blue_pressed_next(self, ret):
        TextMSG = ''
        if ret:
            match ret[1]:
                case "info": #information about plugin
                    TextMSG = _("Lead programmer: ") + "\n\t- rabinovic\n"
                    TextMSG += _("E-mail: ") + "\n\t- civonibar@gmail.com\n"

                    self.session.open(MessageBox, TextMSG, type=MessageBox.TYPE_INFO)
                case "IPTVDM":
                    self.runIPTVDM()
                case "HostConfig":
                    self.runConfigHostIfAllowed()
                case "SetActiveMoviePlayer":
                    self.setActiveMoviePlayer()
                case 'ADD_FAV':
                    currSelIndex = self.canBeAddedToFavourites()[0]
                    self.requestListFromHost('ForFavItem', currSelIndex, '')
                case 'EDIT_FAV':
                    self.session.openWithCallback(self.editFavouritesCallback, IPTVFavouritesMainWidget)
                case 'DELETE_FAV':
                    self.session.openWithCallback(self.deletefavouriteItem, MessageBox, _("Definitely remove from favorites?"), type=MessageBox.TYPE_YESNO, timeout=10)
                case 'HIDE_ITEM':
                    self.session.openWithCallback(self.hideItem, MessageBox, _("Hide Item"), type=MessageBox.TYPE_YESNO, timeout=5)
                case 'UNHIDE_ITEM':
                    self.session.openWithCallback(self.unhideItem, MessageBox, _("Unhide Item"), type=MessageBox.TYPE_YESNO, timeout=5)
                case 'REMOVE_ITEM_FROM_SEARCH_HISTORY':
                    self.session.openWithCallback(self.removeFromHistory, MessageBox, _("Remove Item"), type=MessageBox.TYPE_YESNO, timeout=5)
                case 'CLEAR_SEARCH_HISTORY':
                    self.session.openWithCallback(self.clearHistory, MessageBox, _("Clear seach history"), type=MessageBox.TYPE_YESNO, timeout=5)
                case 'RandomizePlayableItems':
                    self.randomizePlayableItems()
                case 'ReversePlayableItems':
                    self.reversePlayableItems()

    def setActiveMoviePlayer(self):
        options:List[IPTVChoiceBoxItem] = []
        options.append(IPTVChoiceBoxItem(_("Auto selection based on the settings"), "", {}))
        player = self.getMoviePlayer(True, False)
        printDBG("SetActiveMoviePlayer [%r]" % dir(player))
        options.append(IPTVChoiceBoxItem(_("[%s] with buffering") % player.getText(), "", {'buffering': True, 'player': player}))
        player = self.getMoviePlayer(True, True)
        options.append(IPTVChoiceBoxItem(_("[%s] with buffering") % player.getText(), "", {'buffering': True, 'player': player}))
        player = self.getMoviePlayer(False, False)
        options.append(IPTVChoiceBoxItem(_("[%s] without buffering") % player.getText(), "", {'buffering': False, 'player': player}))
        player = self.getMoviePlayer(False, True)
        options.append(IPTVChoiceBoxItem(_("[%s] without buffering") % player.getText(), "", {'buffering': False, 'player': player}))

        currIdx = -1
        for idx, option in enumerate(options):
            try:
                if option.privateData.get('buffering', None) == self.activePlayer.activePlayer.get('buffering', None) and \
                option.privateData.get('player', CFakeMoviePlayerOption('', '')).value == \
                self.activePlayer.activePlayer.get('player', CFakeMoviePlayerOption('', '')).value:
                    currIdx = idx
            except Exception:
                printExc()
            if idx == currIdx:
                option.type = IPTVChoiceBoxItem.TYPE_ON
            else:
                option.type = IPTVChoiceBoxItem.TYPE_OFF

        if self.getSkinResolutionType() == 'hd':
            width = 900
        elif self.getSkinResolutionType() == 'hd_ready':
            width = 600
        else:
            width = 400

        self.session.openWithCallback(self.setActiveMoviePlayerCallback, IPTVChoiceBoxWidget, {'width': width, 'height': 250, 'current_idx': currIdx, 'title': _("Select movie player"), 'options': options})

    def removeFromHistory(self, confirmed):
        if confirmed:
            selection = self.currItem
            self.host.history.removeHistoryItem(selection)
            self.getRefreshedCurrList()

    def clearHistory(self, confirmed):
        if confirmed:
            self.host.history.clearHistory()
            self.getRefreshedCurrList()

    def hideItem(self, confirmed):
        if confirmed:
            selection=self.currItem
            hashData = ph.getItemHash(selection)
            if hashData:
                flagFilePath = GetHiddenItemsDir(f'/{hashData}.iptvhash')
                if touch(flagFilePath):
                    self.getRefreshedCurrList()

    def unhideItem(self, confirmed):
        if confirmed:
            selection=self.currItem
            hashData = ph.getItemHash(selection)
            if hashData:
                flagFilePath = GetHiddenItemsDir(f'/{hashData}.iptvhash')
                if rm(flagFilePath):
                    self.getRefreshedCurrList()

    def deletefavouriteItem(self, confirmed):
        if not confirmed or self.hostName != 'favourites':
            return

        found = False
        favourites = IPTVFavourites(GetFavouritesDir())
        sts = favourites.load()
        if not sts:
            return

        for group in favourites.getGroups():
            group_id = group['group_id']
            if self.currItem.title.lower() == group_id:
                favourites.delGroup(group_id)
                found = True
            else:
                for idx, item in enumerate(group['items']):
                    if self.currItem.title == item.title:
                        favourites.delGroupItem(idx, group_id)
                        found = True
                        break
            if found:
                break
        if found:
            favourites.save()
            self.session.openWithCallback(self.loadHost, MessageBox, _("Item %s removed!") % self.currItem.title, type=MessageBox.TYPE_INFO, timeout=5)

    def editFavouritesCallback(self, ret:bool=False):
        if ret and 'favourites' == self.hostName: # we must reload host
            self.loadHost()

    def setActiveMoviePlayerCallback(self, ret):
        if not isinstance(ret, IPTVChoiceBoxItem):
            return
        self.activePlayer.set(ret.privateData)

    def runIPTVDM(self, callback:Callable=None):
        if E2iPlayerWidget.downloadManager:
            if callback:
                self.session.openWithCallback(callback, IPTVDMWidget, E2iPlayerWidget.downloadManager)
            else:
                self.session.open(IPTVDMWidget, E2iPlayerWidget.downloadManager)
        elif callback:
            callback()

    def updateBottomPanel(self):
        selItem = self.currItem
        if selItem:
            data =  '\c008fce00' + selItem.title
            data += (r'\n\c00ffcc99' + selItem.description) if selItem.description else ''
            sData = data.replace('[/br]', '\n')
            self["console"].setText(sData)
        else:
            self["console"].setText('')
        self.console_timer.stop()
        self["console"].show()

    def back_pressed(self):
        for process in self.iconDownloadProcesses:
            asynccall.async_raise(process.ident, SystemExit)

        if self.stopAutoPlaySequencer() and self.autoPlaySeqTimerValue:
            return
        try:
            if self.isInWorkThread():
                if self.workThread.kill():
                    self.workThread = None
                    self.setStatusTex(_("Operation aborted!"))
                return
        except Exception:
            return

        if self.visible:
            if self.prevSelList:
                if self.index_starts:
                    self.index = self.index_starts.pop()
                    self.back = True
                self.nextSelIndex = self.prevSelList.pop()
                self.categoryList.pop()
                printDBG("back_pressed prev sel index %s" % self.nextSelIndex)
                self.requestListFromHost('Previous')
            else:
                #There is no prev categories, so exit
                #self.close()
                if self.group is None:
                    self.selectHost()
                else:
                    self.selectHostFromGroup()
        else:
            self.showWindow()

    def info_pressed(self):
        printDBG('info_pressed')
        if self.visible and not self.isInWorkThread():
            try:
                item = self.getSelItem()
            except Exception:
                printExc()
                item = None
            if item:
                self.stopAutoPlaySequencer()
                self.currSelIndex = currSelIndex = self.currItem.idx
                self.requestListFromHost('ForArticleContent', currSelIndex)

    def ok_pressed0(self):
        self.activePlayer.set({})
        self.ok_pressed(useAlternativePlayer=False)

    def ok_pressed1(self):
        player = self.getMoviePlayer(True, False)
        self.activePlayer.set({'buffering': True, 'player': player})
        self.ok_pressed(useAlternativePlayer=True)

    def ok_pressed2(self):
        player = self.getMoviePlayer(True, True)
        self.activePlayer.set({'buffering': True, 'player': player})
        self.ok_pressed(useAlternativePlayer=True)

    def ok_pressed3(self):
        player = self.getMoviePlayer(False, False)
        self.activePlayer.set({'buffering': False, 'player': player})
        self.ok_pressed(useAlternativePlayer=False)

    def ok_pressed4(self):
        player = self.getMoviePlayer(False, True)
        self.activePlayer.set({'buffering': False, 'player': player})
        self.ok_pressed(useAlternativePlayer=True)

    def ok_pressed(self, eventFrom='remote', useAlternativePlayer=False):
        self.useAlternativePlayer = useAlternativePlayer
        if eventFrom != 'green':
            self.recorderMode = False

        if 'sequencer' != eventFrom:
            self.stopAutoPlaySequencer()

        if self.visible or 'sequencer' == eventFrom:
            try:
                if self.currList and (not self["cover_11"].getVisible() and 'sequencer' != eventFrom):
                    printDBG("ok_pressed -> ignored /\\")
                    return
            except Exception:
                printExc()

            if not self.currItem :
                printDBG("ok_pressed sel is None")
                self.stopAutoPlaySequencer()
                self.getInitialList()
            elif not self.currList:
                printDBG("ok_pressed list is empty")
                self.stopAutoPlaySequencer()
                self.getRefreshedCurrList()
            else:
                printDBG("ok_pressed selected item: %s" % (self.currItem.title))

                #Get current selection
                currSelIndex = self.currItem.idx
                #remember only prev categories
                match self.currItem .type:
                    case DisplayItemType.VIDEO | DisplayItemType.AUDIO | DisplayItemType.PICTURE | DisplayItemType.DATA:
                        if DisplayItemType.AUDIO == self.currItem.type:
                            self.bufferSize = config.plugins.iptvplayer.requestedAudioBuffSize.value * 1024
                        else:
                            self.bufferSize = config.plugins.iptvplayer.requestedBuffSize.value * 1024 * 1024
                        # check if separete host request is needed to get links to VIDEO
                        if self.currItem.urlSeparateRequest == 1:
                            printDBG("ok_pressed selected TYPE_VIDEO.urlSeparateRequest")
                            self.requestListFromHost('ForVideoLinks', currSelIndex)
                        else:
                            printDBG("ok_pressed selected TYPE_VIDEO.selectLinkForCurrVideo")
                            self.selectLinkForCurrVideo()
                    case DisplayItemType.CATEGORY:
                        printDBG(f"ok_pressed selected {self.currItem.type}")
                        self.stopAutoPlaySequencer()
                        self.currSelIndex = currSelIndex

                        if self.currItem.pinLocked:
                            from Plugins.Extensions.IPTVPlayer.components.iptvpin import IPTVPinWidget
                            self.session.openWithCallback(boundFunction(self.check_pin, self.requestListFromHost, 'ForItem', currSelIndex, '', self.currItem.pinCode), IPTVPinWidget, title=_("Enter pin"))
                        else:
                            self.requestListFromHost('ForItem', currSelIndex, '')
                    case DisplayItemType.MORE:
                        printDBG("ok_pressed selected TYPE_MORE")
                        self.currSelIndex = currSelIndex
                        self.requestListFromHost('ForMore', currSelIndex, '')
                    case DisplayItemType.ARTICLE:
                        printDBG("ok_pressed selected TYPE_ARTICLE")
                        self.info_pressed()
                    case DisplayItemType.SEARCH:
                        printDBG("ok_pressed selected TYPE_SEARCH")
                        self.stopAutoPlaySequencer()
                        self.currSelIndex = currSelIndex
                        self.startSearchProcedure(self.currItem.possibleTypesOfSearch)
        else:
            self.showWindow()

    def check_pin(self, callbackFun:Callable, *arg, pinCode:str, pin:str=None):
        if pin:
            if 4 != len(pinCode):
                pinCode = config.plugins.iptvplayer.pin.value # use default pin code if custom has wrong length
            if pin == pinCode:
                callbackFun(*arg)
            else:
                self.session.open(MessageBox, _("Pin incorrect!"), type=MessageBox.TYPE_INFO, timeout=5)

    def leaveArticleView(self):
        printDBG("leaveArticleView")

    def showArticleContent(self, ret:RetHost):
        printDBG("showArticleContent")
        self.setStatusTex("")
        self._show()

        artItem = None
        if ret.status != RetStatus.OK or 0 == len(ret.value):
            item = self.currList[self.currSelIndex]
            if item.description:
                artItem = ArticleContent(title=item.title, text=item.description, images=[{'title': 'Fot.', 'url': item.iconimage}]) #richDescParams={"alternate_title":"***alternate_title", "year":"year", "rating":"rating",  "duration":"duration",  "genre":"genre",  "director":"director",  "actors":"actors",  "awards":"awards"}
        else:
            artItem = ret.value[0]
        if None is not artItem:
            if artItem.images and artItem.images[0]['url'].startswith('http'):
                self.session.openWithCallback(self.leaveArticleView, IPTVArticleRichVisualizer, artItem, {'buffering_path': config.plugins.iptvplayer.bufferingPath.value})
            else:
                self.session.openWithCallback(self.leaveArticleView, ArticleView, artItem)

    def selectMainVideoLinks(self, ret:RetHost):
        printDBG("selectMainVideoLinks")
        self.setStatusTex("")
        self._show()

        # ToDo: check ret.status if not OK do something :P
        printDBG("++++++++++++++++++++++ selectHostVideoLinksCallback ret.status = %s" % ret.status)
        printDBG("++++++++++++++++++++++ selectHostVideoLinksCallback ret.value = %s" % ret.value)
        if ret.status == RetStatus.OK:
            # update links in List
            self.currItem.urlItems = ret.value
        self.selectLinkForCurrVideo()

    def selectResolvedVideoLinks(self, ret:RetHost):
        printDBG("selectResolvedVideoLinks")
        self.setStatusTex("")
        self._show()
        linkList = []
        if ret.status == RetStatus.OK and isinstance(ret.value, list):
            for item in ret.value:
                if isinstance(item, UrlItem):
                    item.url_needs_resolve = 0 # protection from recursion
                    linkList.append(item)
                elif isinstance(item, str):
                    linkList.append(UrlItem(item, item, 0))
                else:
                    printExc("selectResolvedVideoLinks: wrong resolved url type!")
        else:
            printExc()
        self.selectLinkForCurrVideo(linkList)

    def getSelIndex(self):
        currSelIndex = self.currItem.idx
        if len(self.currList) > currSelIndex:
            return currSelIndex
        return -1

    def getSelItem(self):
        currSelIndex = self.currItem.idx
        if len(self.currList) <= currSelIndex:
            printDBG("ERROR: getSelItem there is no item with index: %d, listOfItems.len: %d" % (currSelIndex, len(self.currList)))
            return None
        return self.currList[currSelIndex]

    def getSelectedItem(self):
        return self.currItem

    def onStart(self):
        self.onShow.remove(self.onStart)
        #self.onLayoutFinish.remove(self.onStart)
        #self.setTitle('E2iPlayer ' + GetIPTVPlayerVersion())
        self.loadSpinner()
        self.hideSpinner()
        self.selectHost()

    def _layoutFinished(self):
        for p in self.positions:
            self.getWatchedIcon(p).setPixmap(LoadPixmap(GetIconDir("green.png")))

    def selectHost(self, arg1=None):
        printDBG(">> selectHost")
        self.group = None
        self.host = None
        self.hostName = ''
        self.nextSelIndex = 0
        self.prevSelList = []
        self.categoryList = []
        self.currList = DisplayList()
        self.currItem = DisplayItem()

        if config.plugins.iptvplayer.group_hosts.value is False or 0 == GetAvailableIconSize():
            self.selectHostFromSingleList()
        else:
            self.selectGroup()

    def selectGroup(self):
        printDBG(">> selectGroup")

        self.displayGroupsList = []
        groupsList = self._groupObj.getGroupsList()
        for item in groupsList:
            self.displayGroupsList.append((item.title, item.name))
        self.displayGroupsList.append((_('All'), 'all'))
        self.displayGroupsList.append((_("Configuration"), "config"))

        self.newDisplayGroupsList = []
        self.session.openWithCallback(self.selectGroupCallback, PlayerSelectorWidget, inList=self.displayGroupsList, outList=self.newDisplayGroupsList, numOfLockedItems=self.getNumOfSpecialItems(self.displayGroupsList), groupName='selectgroup')

    def selectGroupCallback(self, ret):
        printDBG(">> selectGroupCallback")
        # save groups order if user change it at player selection
        if self.newDisplayGroupsList != self.displayGroupsList:
            numOfSpecialItems = self.getNumOfSpecialItems(self.newDisplayGroupsList)
            groupList = []
            for idx in range(len(self.newDisplayGroupsList) - numOfSpecialItems):
                groupList.append(self.newDisplayGroupsList[idx][1])
            self._groupObj.setGroupList(groupList)

        self.selectGroupCallback2(ret)

    def selectGroupCallback2(self, ret):
        printDBG(">> selectGroupCallback2")
        self.selectItemCallback(ret, 'selectgroup')

    def selectHostFromGroup(self, ret=None):
        printDBG(">> selectHostFromGroup")
        self.host = None
        self.hostName = ''
        self.nextSelIndex = 0
        self.prevSelList = []
        self.categoryList = []
        self.currList = DisplayList()
        self.currItem = DisplayItem()

        self.displayHostsList = []
        if self.group != 'all':
            hostsList = self._groupObj.getHostsList(self.group)
        else:
            hostsList = []
            sortedList = SortHostsList(GetHostsList(fromList=False, fromHostFolder=True))
            for hostName in sortedList:
                if is_host_enabled(hostName):
                    hostsList.append(hostName)

        brokenHostList = []
        for hostName in hostsList:
            try:
                title = self.hostsAliases.get('host' + hostName, '')
                if not title:
                    _temp = importlib.import_module(f'Plugins.Extensions.IPTVPlayer.hosts.host{hostName}')
                    title = _temp.gettytul()
            except Exception:
                printExc('get host name exception for host "%s"' % hostName)
                brokenHostList.append('host' + hostName)
                continue
            self.displayHostsList.append((title, hostName))

        # if there is no order hosts list use old behavior for all group
        if self.group == 'all' and 0 == len(GetHostsOrderList()):
            try:
                self.displayHostsList.sort(key=lambda t: tuple(str(t[0]).lower()))
            except Exception:
                self.displayHostsList.sort()

        # prepare info message when some host or update cannot be used
        errorMessage = ""
        if len(brokenHostList) > 0:
            errorMessage = _("Following host are broken or additional python modules are needed.") + '\n' + '\n'.join(brokenHostList)

        if "" != errorMessage and self.showHostsErrorMessage is True:
            self.showHostsErrorMessage = False
            self.session.openWithCallback(self.displayListOfHostsFromGroup, MessageBox, errorMessage, type=MessageBox.TYPE_INFO, timeout=10)
        else:
            self.displayListOfHostsFromGroup()

    def displayListOfHostsFromGroup(self, arg=None):
        printDBG(">> displayListOfHostsFromGroup")
        self.newDisplayHostsList = []
        if self.displayHostsList:
            self.session.openWithCallback(self.selectHostFromGroupCallback, PlayerSelectorWidget, inList=self.displayHostsList, outList=self.newDisplayHostsList, numOfLockedItems=0, groupName=self.group, groupObj=self._groupObj)
        else:
            msg = _('There is no hosts in this group.')
            self.session.openWithCallback(self.selectHost, MessageBox, msg, type=MessageBox.TYPE_INFO, timeout=10)

    def selectHostFromGroupCallback(self, ret):
        printDBG(">> selectHostFromGroupCallback")

        # save hosts order if user change it at player selection
        if self.newDisplayHostsList != self.displayHostsList:
            hostsList = [x[1] for x in self.newDisplayHostsList]
            if self.group != 'all':
                self._groupObj.setHostsList(self.group, hostsList)
            else:
                SaveHostsOrderList(hostsList)
        self._groupObj.flushAddedHosts()
        self.selectHostFromGroupCallback2(ret)

    def selectHostFromGroupCallback2(self, ret):
        printDBG(">> selectHostFromGroupCallback2")
        self.selectItemCallback(ret, 'selecthostfromgroup')

    def selectHostFromSingleList(self):
        self.displayHostsList = []
        brokenHostList = []

        sortedList = SortHostsList(GetHostsList(fromList=False, fromHostFolder=True))
        for hostName in sortedList:
            if not is_host_enabled(hostName):
                continue

            try:
                title = self.hostsAliases.get('host' + hostName, '')
                if not title:
                    _temp = importlib.import_module(f'Plugins.Extensions.IPTVPlayer.hosts.host{hostName}')
                    title = _temp.gettytul()
            except Exception:
                printExc('get host name exception for host "%s"' % hostName)
                brokenHostList.append('host' + hostName)
                continue

            # The 'http...' in host titles is annoying on regular choiceBox and impacts sorting.
            # To simplify choiceBox usage and clearly show service is a webpage, list is build using the "<service name> (<service URL>)" schema.
            if (config.plugins.iptvplayer.graphicalList.value is False or 0 == GetAvailableIconSize()) and title[:4] == 'http':
                title = ('%s   (%s)') % ('.'.join(title.replace('://', '.').replace('www.', '').split('.')[1:-1]), title)

            self.displayHostsList.append((title, hostName))

        # if there is no order hosts list use old behavior
        if not GetHostsOrderList():
            try:
                self.displayHostsList.sort(key=lambda t: tuple(str(t[0]).lower()))
            except Exception:
                self.displayHostsList.sort()

        self.displayHostsList.append((_("Configuration"), "config"))

        if brokenHostList and self.showHostsErrorMessage is True:
            errorMessage = _("Following host are broken or additional python modules are needed.") + '\n' + '\n'.join(brokenHostList)
            self.showHostsErrorMessage = False
            self.session.openWithCallback(self.displayListOfHosts, MessageBox, errorMessage, type=MessageBox.TYPE_INFO, timeout=10)
        else:
            self.displayListOfHosts()

    def displayListOfHosts(self, arg=None):
        if config.plugins.iptvplayer.graphicalList.value is False or 0 == GetAvailableIconSize():
            self.newDisplayHostsList = None
            self.session.openWithCallback(self.selectHostCallback, ChoiceBox, title=_("Select service"), list=self.displayHostsList)
        else:
            self.newDisplayHostsList = []
            self.session.openWithCallback(self.selectHostCallback, PlayerSelectorWidget, inList=self.displayHostsList, outList=self.newDisplayHostsList, numOfLockedItems=self.getNumOfSpecialItems(self.displayHostsList), groupName='selecthost')

    def getNumOfSpecialItems(self, inList, filters=['config', 'update', 'all']):
        numOfSpecialItems = 0
        for item in inList:
            if item[1] in filters:
                numOfSpecialItems += 1
        return numOfSpecialItems

    def selectHostCallback(self, ret):
        printDBG(">> selectHostCallback")
        # save hosts order if user change it at player selection
        if self.newDisplayHostsList and self.newDisplayHostsList != self.displayHostsList:
            numOfSpecialItems = self.getNumOfSpecialItems(self.newDisplayHostsList)
            hostsList = []
            for idx in range(len(self.newDisplayHostsList) - numOfSpecialItems):
                hostsList.append(self.newDisplayHostsList[idx][1])
            SaveHostsOrderList(hostsList)
        self.selectHostCallback2(ret)

    def selectHostCallback2(self, ret):
        printDBG(">> selectHostCallback2")
        self.selectItemCallback(ret, 'selecthost')

    def selectItemCallback(self, ret, type):
        printDBG(f">> selectItemCallback ret[{ret}] type[{type}]")
        nextFunction = None
        prevFunction = None
        protectedByPin = False
        if ret:
            match ret[1]:
                case "config":
                    nextFunction = self.runConfig
                    prevFunction = self.selectHost
                    protectedByPin = config.plugins.iptvplayer.configProtectedByPin.value
                case "config_hosts":
                    nextFunction = self.runConfigHosts
                    if type == 'selecthost':
                        prevFunction = self.selectHost
                    else:
                        prevFunction = self.selectHostFromGroup
                    protectedByPin = config.plugins.iptvplayer.configProtectedByPin.value
                case "config_groups":
                    nextFunction = self.runConfigGroupsMenu
                    prevFunction = self.selectHost
                    protectedByPin = config.plugins.iptvplayer.configProtectedByPin.value
                case "noupdate":
                    self.close()
                    return
                case "update":
                    #self.session.openWithCallback(self.selectHost, IPTVUpdateWindow, UpdateMainAppImpl(self.session))
                    return
                case "IPTVDM":
                    if type in ['selecthost', 'selectgroup']:
                        self.runIPTVDM(self.selectHost)
                    elif type == 'selecthostfromgroup':
                        self.runIPTVDM(self.selectHostFromGroup)
                    return
                case _:
                    if type in ['selecthost', 'selecthostfromgroup']:
                        self.hostTitle = ret[0]
                        self.hostName = ret[1]
                        self.loadHost()
                    elif type == 'selectgroup':
                        self.group = ret[1]
                        self.selectHostFromGroup()
                        return

            if not self.enough_free_space:
                self.enough_free_space = True
                self.session.open(MessageBox, (_("There is no free space on the drive [%s].") % config.plugins.iptvplayer.cachepath.value) + "\n" + _("New icons will not be available."), type=MessageBox.TYPE_INFO, timeout=10)
        elif type in ['selecthost', 'selectgroup']:
            self.close()
            return
        else:
            self.selectHost()
            return

        if nextFunction and prevFunction:
            if protectedByPin:
                self.session.openWithCallback(boundFunction(self.checkPin, nextFunction, prevFunction), IPTVPinWidget, title=_("Enter pin"))
            else:
                nextFunction()

    def runConfigHosts(self):
        self.enabledHostsListOld = GetEnabledHostsList()
        self.session.openWithCallback(self.configHostsCallback, ConfigHostsMenu, GetListOfHostsNames())

    def configHostsCallback(self, arg1=None):
        if IsUpdateNeededForHostsChangesCommit(self.enabledHostsListOld):
#            message = _('Some changes will be applied only after plugin update.\nDo you want to perform update now?')
#            self.session.openWithCallback(self.askForUpdateCallback, MessageBox, text=message, type=MessageBox.TYPE_YESNO)
            self.selectHost()
        elif self.group is not None:
            self.selectHostFromGroup()
        else:
            self.selectHost()

    def runConfigGroupsMenu(self):
        self.session.openWithCallback(self.selectHost, ConfigGroupsMenu)

    def runConfig(self):
        self.session.openWithCallback(self.configCallback, ConfigMenu)

    def runConfigHostIfAllowed(self):
        if config.plugins.iptvplayer.configProtectedByPin.value:
            from .iptvpin import IPTVPinWidget
            self.session.openWithCallback(boundFunction(self.checkPin, self.runConfigHost, None), IPTVPinWidget, title=_("Enter pin"))
        else:
            self.runConfigHost()

    def runConfigHost(self):
        self.session.openWithCallback(self.runConfigHostCallBack, ConfigHostMenu, hostName=self.hostName)

    def runConfigHostCallBack(self, confgiChanged=False):
        if confgiChanged:
            self.loadHost()

    def checkPin(self, callbackFun, failCallBackFun, pin=None):
        if pin is not None:
            if pin == config.plugins.iptvplayer.pin.value:
                callbackFun()
            else:
                self.session.openWithCallback(self.displayListOfHostsFromGroup, MessageBox, _("Pin incorrect!"), type=MessageBox.TYPE_INFO, timeout=5)
        else:
            if failCallBackFun:
                failCallBackFun()

    def loadHost(self, ret=None):
        self.hostFavTypes = []
        try:
            _temp = importlib.import_module(f'Plugins.Extensions.IPTVPlayer.hosts.host{self.hostName}')
            self.host: HostBase = _temp.IPTVHost()
            if not isinstance(self.host, HostBase):
                printDBG("Host [%r] does not inherit from HostBase" % self.hostName)
                self.close()
                return
        except Exception as e:
            printExc('Cannot import class IPTVHost for host [%r]' % self.hostName)
            errorMessage = [_('Loading %s failed due to following error:') % self.hostName]
            elines = traceback.format_exc().splitlines()
            errorMessage.append("%s" % '\n'.join(elines[-3:]))
            self.session.openWithCallback(self.selectHostFromGroup, MessageBox, '\n'.join(errorMessage), type=MessageBox.TYPE_ERROR, timeout=10)
            self.setStatusTex(_("Failed: %s") % e)
            return
        try:
            protectedByPin = self.host.isProtectedByPinCode()
        except Exception:
            printExc()

        if protectedByPin:
            from .iptvpin import IPTVPinWidget
            self.session.openWithCallback(boundFunction(self.checkPin, self.loadHostData, self.selectHost), IPTVPinWidget, title=_("Enter pin"))
        else:
            self.loadHostData()

    def loadHostData(self):
        self.session.summary.setText(self.hostName)
        self.activePlayer = CMoviePlayerPerHost(self.hostName)

        # change logo for player
        self["playerlogo"].hide()
        self.session.summary.LCD_hide('LCDlogo')
        try:
            hRet = self.host.getLogoPath()
            if hRet.status == RetStatus.OK and len(hRet.value):
                logoPath = hRet.value[0]
                if logoPath != '' and fileExists(logoPath):
                    printDBG('Logo Path: ' + logoPath)
                    self["playerlogo"].updateIcon(logoPath)
                    #self["playerlogo"].show()
                else:
                    pass
                    #self["playerlogo"].hide()

                self.session.summary.LCD_showPic('LCDlogo', logoPath)
        except Exception:
            printExc()

        # get types of items which can be added as favourites
        self.hostFavTypes = []
        try:
            hRet = self.host.getSupportedFavoritesTypes()
            if hRet.status == RetStatus.OK:
                self.hostFavTypes = hRet.value
        except Exception:
            printExc('The current host crashed')

        # request initial list from host
        self.getInitialList()

    def selectLinkForCurrVideo(self, customUrlItems=None):
        printDBG("selectLinkForCurrVideo: customUrlItems [%s]" % customUrlItems)
        if not self.visible and not (self.autoPlaySeqStarted and
           config.plugins.iptvplayer.autoplay_start_delay.value == 0):
            self.setStatusTex("")
            self.showWindow()

        item = self.currItem
        printDBG("selectLinkForCurrVideo: self.currItem [%s]" % self.currItem)
        if item.type not in [DisplayItemType.VIDEO, DisplayItemType.AUDIO,
                             DisplayItemType.PICTURE, DisplayItemType.DATA]:
            printDBG("Incorrect item type[%s]" % item.type)
            return

        if None is customUrlItems:
            printDBG("selectLinkForCurrVideo: item.urlItems [%s]" % item.urlItems)
            links = item.urlItems
        else:
            links = customUrlItems

        options = []
        iconPath = self.iconManager.getIconPathFromAAQueue(item.iconimage)
        for link in links:
            printDBG("selectLinkForCurrVideo: |%s| |%s| |%s|" % (link.name, link.url, iconPath))
            options.append((link.name, link.url, link.url_needs_resolve, iconPath))

        #There is no free links for current video
        numOfLinks = len(links)
        if 0 == numOfLinks:
            if not self.checkAutoPlaySequencer():
                message = _("No valid links available.")
                lastErrorMsg = GetIPTVPlayerLastHostError()
                if '' != lastErrorMsg:
                    message += "\n" + _('Last error: "%s"') % lastErrorMsg
                self.session.open(MessageBox, message, type=MessageBox.TYPE_INFO, timeout=10)
            return
        elif 1 == numOfLinks or self.autoPlaySeqStarted:
            #call manualy selectLinksCallback - start VIDEO without links selection
            arg = [
                    " ", #name of item - not displayed so empty
                    links[0].url,
                    links[0].url_needs_resolve,
                    iconPath
                ]
            self.selectLinksCallback(arg)
            return

        #options.sort(reverse=True)
        self.session.openWithCallback(self.selectLinksCallback, ChoiceBox, title=_("Select link"), list=options)

    def selectLinksCallback(self, retArg):
        #printDBG("iptvplayerwidget.selectLinksCallback  %s "  % len(retArg))
        # retArg[0] - name
        # retArg[1] - url src
        # retArg[2] - url_needs_resolve
        # retArg[3] - local iconpath
        if retArg and 4 == len(retArg):
            #check if we have URL
            if isinstance(retArg[1], str):
                videoUrl = retArg[1]
                if len(videoUrl) > 3:
                    #check if we need to resolve this URL
                    if str(retArg[2]) == '1':
                        #call resolve link from host
                        self.requestListFromHost('ResolveURL', -1, videoUrl)
                    else:
                        list_ = [videoUrl, retArg[3]]
                        self.playVideo(RetHost(status=RetStatus.OK, value=list_))
                    return
            self.playVideo(RetHost(status=RetStatus.ERROR, value=[]))

    def checkBuffering(self, url):
        # check flag forcing of the using/not using buffering
        if 'iptv_buffering' in url.meta:
            if "required" == url.meta['iptv_buffering']:
                # iptv_buffering was set as required, this is done probably due to
                # extra http headers needs, at now extgstplayer and exteplayer can handle this headers,
                # so we skip forcing buffering for such links. at now this is temporary
                # solution we need to add separate filed iptv_extraheaders_need!
                if url.startswith("http") and self.getMoviePlayer(False, False).value in ['extgstplayer', 'exteplayer']:
                    pass # skip forcing buffering
                else:
                    return True
            elif "forbidden" == url.meta['iptv_buffering']:
                return False
        if "|" in url:
            return True

        # check based on protocol
        protocol = url.meta.get('iptv_proto', '')
        if protocol in ['f4m', 'uds']:
            return True # supported only in buffering mode

        if protocol in ['http', 'https']:
            return config.plugins.iptvplayer.buffering.value

        if 'rtmp' == protocol:
            return config.plugins.iptvplayer.buffering_rtmp.value

        if protocol in ['m3u8', 'em3u8']:
            return config.plugins.iptvplayer.buffering_m3u8.value

    def isUrlBlocked(self, url, type):
        protocol = url.meta.get('iptv_proto', '')
        if ".wmv" == self.getFileExt(url, type) and config.plugins.iptvplayer.ZablokujWMV.value:
            return True, _("Format 'wmv' blocked in configuration.")
        elif '' == protocol:
            return True, _("Unknown protocol [%s]") % url
        return False, ''

    def getFileExt(self, url, type):
        format = url.meta.get('iptv_format', '')
        if '' != format:
            return '.' + format
        protocol = url.meta.get('iptv_proto', '')

        fileExtension = ''
        tmp = url.lower().split('?', 1)[0]
        for item in ['avi', 'flv', 'mp4', 'ts', 'mov', 'wmv', 'mpeg', 'mpg', 'mkv', 'vob', 'divx', 'm2ts', 'mp3', 'm4a', 'ogg', 'wma', 'fla', 'wav', 'flac']:
            if tmp.endswith('.' + item):
                fileExtension = '.' + item
                break

        if '' == fileExtension:
            if protocol in ['mms', 'mmsh', 'rtsp']:
                fileExtension = '.wmv'
            elif protocol in ['f4m', 'uds', 'rtmp']:
                fileExtension = '.flv'
            else:
                if type == DisplayItemType.VIDEO:
                    fileExtension = '.mp4' # default video extension
                else:
                    fileExtension = '.mp3' # default audio extension
        return fileExtension

    def getMoviePlayer(self, buffering=False, useAlternativePlayer=False):
        printDBG("getMoviePlayer")
        return GetMoviePlayer(buffering, useAlternativePlayer)

    def writeCurrentTitleToFile(self, title):
        titleFilePath = config.plugins.iptvplayer.curr_title_file.value
        if "" != titleFilePath:
            try:
                with open(titleFilePath, 'w') as titleFile:
                    titleFile.write(title)
            except Exception:
                printExc()
        if config.plugins.iptvplayer.set_curr_title.value:
            try:
                from enigma import evfd
                title = ph.getNormalizeStr(title)
                evfd.getInstance().vfd_write_string(title[0:17])
            except Exception:
                printExc()

    def playVideo(self, ret):
        printDBG("playVideo")
        url = ''
        iconimage = None
        if RetStatus.OK == ret.status and ret.value:
            url = ret.value[0]
            iconimage = ret.value[1]

        self.setStatusTex("")
        self._show()

        if url != '' and DisplayItemType.PICTURE == self.currItem.type:
            self.session.openWithCallback(self.leavePicturePlayer, IPTVPicturePlayerWidget, url, config.plugins.iptvplayer.bufferingPath.value, self.currItem.title, {'seq_mode': self.autoPlaySeqStarted})
            return

        if url != '' and self.currItem.isDownloadable():
            printDBG("playVideo url[%s]" % url)

            url = urlparser.decorateUrl(url)
            fileExtension = self.getFileExt(url, self.currItem.type)

            blocked, reaseon = self.isUrlBlocked(url, self.currItem.type)
            if blocked:
                self.session.open(MessageBox, reaseon, type=MessageBox.TYPE_INFO, timeout=10)
                return

            titleOfMovie = self.currItem.title.replace('/', '-').replace(':', '-').replace('*', '-').replace('?', '-').replace('"', '-').replace('<', '-').replace('>', '-').replace('|', '-')

            isBufferingMode = False if url.startswith('file://') else self.activePlayer.get('buffering', self.checkBuffering(url))
            bufferingPath = config.plugins.iptvplayer.bufferingPath.value
            downloadingPath = config.plugins.iptvplayer.userpath.value
            recorderMode = True if self.currItem.type == DisplayItemType.DATA else self.recorderMode
            destinationPath = downloadingPath if recorderMode else bufferingPath

            if recorderMode or isBufferingMode:
                errorTab = []
                if not os_path.exists(destinationPath):
                    mkdirs(destinationPath)

                if not os_path.isdir(destinationPath):
                    errorTab.append(_("Directory \"%s\" does not exists.") % destinationPath)
                    errorTab.append(_("Please set valid %s in the %s configuration.") % (_("downloads location") if recorderMode else _("buffering location"), 'E2iPlayer'))
                else:
                    requiredSpace = 3 * 512 * 1024 * 1024 # 1,5 GB
                    availableSpace = get_free_space(destinationPath, unitDiv=1)
                    if requiredSpace > availableSpace:
                        errorTab.append(_("There is no enough free space in the folder \"%s\".") % destinationPath)
                        errorTab.append(_("\tDisk space required: %s") % formatBytes(requiredSpace))
                        errorTab.append(_("\tDisk space available: %s") % formatBytes(availableSpace))

                if errorTab:
                    errorTab.append("\n")
                    errorTab.append(_("Tip! You can connect USB flash drive to fix this problem."))
                    self.stopAutoPlaySequencer()
                    self.session.open(MessageBox, '\n'.join(errorTab), type=MessageBox.TYPE_INFO, timeout=10)
                    return

            if recorderMode:
                if E2iPlayerWidget.downloadManager:
                    if IsUrlDownloadable(url):
                        fullFilePath = downloadingPath + '/' + titleOfMovie + fileExtension
                        ret = E2iPlayerWidget.downloadManager.addToDQueue(DMItem(url, fullFilePath))
                    else:
                        ret = False
                        self.session.open(MessageBox, _("File can not be downloaded. Protocol [%s] is unsupported") % url.meta.get('iptv_proto', ''), type=MessageBox.TYPE_INFO, timeout=10)

                    if ret:
                        if not self.checkAutoPlaySequencer():
                            if config.plugins.iptvplayer.IPTVDMShowAfterAdd.value:
                                self.runIPTVDM()
                            else:
                                self.session.open(MessageBox, _("File [%s] was added to downloading queue.") % titleOfMovie, type=MessageBox.TYPE_INFO, timeout=10)
                    else:
                        self.stopAutoPlaySequencer()
                else:
                    self.stopAutoPlaySequencer()
            else:
                self.prevVideoMode = GetE2VideoMode()
                printDBG("Current video mode [%s]" % self.prevVideoMode)
                gstAdditionalParams = {'defaul_videomode': self.prevVideoMode, 'host_name': self.hostName, 'external_sub_tracks': url.meta.get('external_sub_tracks', []), 'iptv_refresh_cmd': url.meta.get('iptv_refresh_cmd', '')} #default_player_videooptions
                if self.currItem.type == DisplayItemType.AUDIO:
                    gstAdditionalParams['show_iframe'] = config.plugins.iptvplayer.show_iframe.value
                    gstAdditionalParams['iframe_file_start'] = config.plugins.iptvplayer.iframe_file.value
                    gstAdditionalParams['iframe_file_end'] = config.plugins.iptvplayer.clear_iframe_file.value
                    gstAdditionalParams['iframe_continue'] = 'sh4' == config.plugins.iptvplayer.plarform.value

                self.writeCurrentTitleToFile(titleOfMovie)
                self.session.nav.stopService()

                if isBufferingMode:
                    player = self.activePlayer.get('player', self.getMoviePlayer(True, self.useAlternativePlayer))
                    self.session.openWithCallback(self.leaveMoviePlayer, E2iPlayerBufferingWidget, url, bufferingPath, downloadingPath, titleOfMovie, player.value, self.bufferSize, gstAdditionalParams, E2iPlayerWidget.downloadManager, fileExtension, iconimage)
                else:
                    player = self.activePlayer.get('player', self.getMoviePlayer(False, self.useAlternativePlayer))

                    if "mini" == player.value:
                        self.session.openWithCallback(self.leaveMoviePlayer, IPTVMiniMoviePlayer, url, titleOfMovie)
                    elif "standard" == player.value:
                        self.session.openWithCallback(self.leaveMoviePlayer, IPTVStandardMoviePlayer, url, titleOfMovie)
                    else:
                        if "extgstplayer" == player.value:
                            playerVal = 'gstplayer'
                            gstAdditionalParams['download-buffer-path'] = ''
                            gstAdditionalParams['ring-buffer-max-size'] = 0
                            if 'sh4' == config.plugins.iptvplayer.plarform.value: # use default value, due to small amount of RAM
                                #use the default value, due to small amount of RAM
                                #in the future it will be configurable
                                gstAdditionalParams['buffer-duration'] = -1
                                gstAdditionalParams['buffer-size'] = 0
                            else:
                                gstAdditionalParams['buffer-duration'] = 18000 # 300min
                                gstAdditionalParams['buffer-size'] = 10240 # 10MB
                        else:
                            assert "exteplayer" == player.value
                            playerVal = 'eplayer'
                        self.session.openWithCallback(self.leaveMoviePlayer, IPTVExtMoviePlayer, url, titleOfMovie, None, playerVal, iconimage, gstAdditionalParams)
        else:
            #There was problem in resolving direct link for video
            if not self.checkAutoPlaySequencer():
                self.session.open(MessageBox, _("No valid links available."), type=MessageBox.TYPE_INFO, timeout=10)

    def leaveMoviePlayer(self, answer=None, lastPosition=None, clipLength=None, *args, **kwargs):
        self.writeCurrentTitleToFile("")
        videoMode = GetE2VideoMode()
        printDBG("Current video mode [%s], previus video mode [%s]" % (videoMode, self.prevVideoMode))
        if None not in [self.prevVideoMode, videoMode] and self.prevVideoMode != videoMode:
            printDBG("Restore previus video mode")
            SetE2VideoMode(self.prevVideoMode)

        try:
            if answer is not None:
                self.stopAutoPlaySequencer()
        except Exception:
            printExc()

        if not config.plugins.iptvplayer.disable_live.value and not self.autoPlaySeqStarted:
            self.session.nav.playService(self.currentService)

        if 'favourites' == self.hostName and lastPosition is not None and clipLength is not None:
            try:
                if config.plugins.iptvplayer.favourites_use_watched_flag.value and (lastPosition * 100 / clipLength) > 80:
                    # currSelIndex = self["list"].getCurrentIndex()
                    currSelIndex = self.currItem.idx
                    self.requestListFromHost('MarkItemAsViewed', currSelIndex)
                    return
            except Exception:
                printExc()

        self.checkAutoPlaySequencer()

    def leavePicturePlayer(self, answer=None, lastPosition=None, *args, **kwargs):
        self.checkAutoPlaySequencer()

    def requestListFromHost(self, type, currSelIndex=-1, privateData=''):

        if self.isInWorkThread():
            return

        if type != 'ForMore':
            self._hide()

        IPTVPlayerSleep().reset()

        if type not in ['ForVideoLinks', 'ResolveURL', 'ForArticleContent', 'ForFavItem', 'PerformCustomAction']:
            #hide bottom panel
            self["console"].setText('')

        if type in ['ForItem','ForSearch']:
            self.index_starts.append(self.index_start)
            self.index_start = -1
            self.prevSelList.append(self.currSelIndex)
            if type == 'ForSearch':
                self.categoryList.append(_("Search results"))
            else:
                self.categoryList.append(self.currItem.title)
            #new list, so select first index
            self.nextSelIndex = 0

        selItem = self.currItem # None
        #if currSelIndex > -1 and len(self.currList) > currSelIndex:
        #   selItem = self.currList[currSelIndex]
        if selItem and selItem.type.isPlayable(): #and selItem.itemIdx > -1 and len(self.currList) > selItem.itemIdx:
            currSelIndex = selItem.itemIdx

        dots = _(" ...")
        IDS_DOWNLOADING = _("Downloading") + dots
        IDS_LOADING = _("Loading") + dots
        IDS_REFRESHING = _("Refreshing") + dots
        try:
            match type:
                case 'Refresh':
                    self.setStatusTex(IDS_REFRESHING)
                    self.workThread = asynccall.AsyncMethod(self.host.getCurrentList, boundFunction(self.callbackGetList, {'refresh': 1, 'selIndex': currSelIndex}), True)(1)
                case 'ForMore':
                    #self.setStatusTex(IDS_DOWNLOADING)
                    self.workThread = asynccall.AsyncMethod(self.host.getMoreForItem, boundFunction(self.callbackGetList, {'refresh': 2}), True)(len(self.currList),self.currList.getNextPage())
                case 'Initial':
                    self.setStatusTex(IDS_DOWNLOADING)
                    self.workThread = asynccall.AsyncMethod(self.host.getInitList, boundFunction(self.callbackGetList, {}), True)()
                case 'Previous':
                    self.setStatusTex(IDS_DOWNLOADING)
                    self.workThread = asynccall.AsyncMethod(self.host.getPrevList, boundFunction(self.callbackGetList, {}), True)()
                case 'ForItem':
                    self.setStatusTex(IDS_DOWNLOADING)
                    self.workThread = asynccall.AsyncMethod(self.host.getListForItem, boundFunction(self.callbackGetList, {}), True)(currSelIndex, 0, selItem)
                case 'ForVideoLinks':
                    self.setStatusTex(IDS_LOADING)
                    self.workThread = asynccall.AsyncMethod(self.host.getLinksForVideo, self.selectHostVideoLinksCallback, True)(currSelIndex, selItem)
                case 'ResolveURL':
                    self.setStatusTex(IDS_LOADING)
                    self.workThread = asynccall.AsyncMethod(self.host.getResolvedURL, self.getResolvedURLCallback, True)(privateData)
                case 'ForSearch':
                    self.setStatusTex(IDS_LOADING)
                    self.workThread = asynccall.AsyncMethod(self.host.getSearchResults, boundFunction(self.callbackGetList, {}), True)(self.searchPattern, self.searchType)
                case 'ForArticleContent':
                    self.setStatusTex(IDS_DOWNLOADING)
                    self.workThread = asynccall.AsyncMethod(self.host.getArticleContent, self.getArticleContentCallback, True)(currSelIndex)
                case 'ForFavItem':
                    self.setStatusTex(IDS_LOADING)
                    self.workThread = asynccall.AsyncMethod(self.host.getFavouriteItem, self.getFavouriteItemCallback, True)(currSelIndex)
                case 'PerformCustomAction':
                    self.workThread = asynccall.AsyncMethod(self.host.performCustomAction, self.performCustomActionCallback, True)(privateData)
                case 'MarkItemAsViewed':
                    self.workThread = asynccall.AsyncMethod(self.host.markItemAsViewed, self.markItemAsViewedCallback, True)(currSelIndex)
                case _:
                    printDBG('requestListFromHost unknown list type: ' + type)

            if type != 'ForMore':
                self.showSpinner()
        except Exception:
            printExc('The current host crashed')

    def startSearchProcedure(self, searchTypes):
        sts, prevPattern = CSearchHistoryHelper.loadLastPattern()
        if sts:
            self.searchPattern = prevPattern
        if searchTypes:
            self.session.openWithCallback(self.selectSearchTypeCallback, ChoiceBox, title=_("Search type"), list=searchTypes)
        else:
            self.searchType = None
            self.doSearchWithVirtualKeyboard()

    def selectSearchTypeCallback(self, ret=None):
        if ret:
            self.searchType = ret[1]
            self.doSearchWithVirtualKeyboard()

    def doSearchWithVirtualKeyboard(self):
        printDBG("doSearchWithVirtualKeyboard")
        caps = {}
        virtualKeyboard = GetVirtualKeyboard(caps)

        if caps.get('has_additional_params'):
            try:
                additionalParams = {}
                if caps.get('has_suggestions') and config.plugins.iptvplayer.osk_allow_suggestions.value:

                    if suggestionsProvider:=self.getSuggetionsProvider():
                        from Plugins.Extensions.IPTVPlayer.components.e2ivksuggestion import AutocompleteSearch
                        additionalParams['autocomplete'] = AutocompleteSearch(suggestionsProvider)

                self.session.openWithCallback(self.enterPatternCallBack, virtualKeyboard, title=(_("Your search entry")), text=self.searchPattern, additionalParams=additionalParams)
                return
            except Exception:
                printExc()
        self.session.openWithCallback(self.enterPatternCallBack, virtualKeyboard, title=(_("Your search entry")), text=self.searchPattern)

    def getSuggetionsProvider(self):
        suggestionsProvider = None
        try:
            # we have to be careful here as we will call method
            # directly from host it must be non blocking!!!
            if self.visible and not self.isInWorkThread():
                currSelIndex = self.currItem.itemIdx
                hRet = self.host.getSuggestionsProvider(currSelIndex)
                if hRet.status == RetStatus.OK and hRet.value and hRet.value[0]:
                    suggestionsProvider = hRet.value[0] if hRet.value[0] is not None else False
        except Exception:
            printExc()

        if suggestionsProvider is None:
            match self.getProviderAlias():
                case 'filmweb':
                    suggestionsProvider = FilmwebSuggestionsProvider()
                case 'imdb':
                    suggestionsProvider = ImdbSuggestionsProvider()
                case 'google':
                    suggestionsProvider = GoogleSuggestionsProvider()
                case 'moviepilot':
                    suggestionsProvider = MoviepilotSuggestionsProvider()

        return suggestionsProvider

    def getProviderAlias(self)->str:
        providerAlias = config.plugins.iptvplayer.osk_default_suggestions.value
        if not providerAlias:
            if self.hostName in self._groupObj.PREDEFINED_HOSTS['moviesandseries']:
                if self.hostName in self._groupObj.PREDEFINED_HOSTS['polish']:
                    providerAlias = 'filmweb'
                elif self.hostName in self._groupObj.PREDEFINED_HOSTS['german']:
                    providerAlias = 'moviepilot'
                else:
                    providerAlias = 'imdb'
            else:
                providerAlias = 'google'
        return providerAlias


    def enterPatternCallBack(self, callback=None):
        if callback is not None and len(callback):
            self.searchPattern = callback
            CSearchHistoryHelper.saveLastPattern(self.searchPattern)
            self.requestListFromHost('ForSearch')

    def configCallback(self):
        self.selectHost()

    def randomizePlayableItems(self, randomize=True):
        printDBG("randomizePlayableItems")
        self.stopAutoPlaySequencer()
        if self.visible and len(self.currList) > 1 and not self.isInWorkThread():
            randList = DisplayList()
            for item in self.currList:
                if isinstance(item, DisplayItem) and item.type.isPlayable():
                    randList.append(item)
            if randomize:
                random_shuffle(randList)
            reloadList = False
            if len(self.currList) == len(randList):
                randList.reverse()
                self.currList = randList
                reloadList = True
            elif len(randList) > 1:
                newList = DisplayList()
                for item in self.currList:
                    if isinstance(item, DisplayItem) and item.type.isPlayable():
                        newList.append(randList.pop())
                    else:
                        newList.append(item)
                reloadList = True
                self.currList = newList
            if reloadList:
                self.setItems()

    def reversePlayableItems(self):
        printDBG("reversePlayableItems")
        self.randomizePlayableItems(False)

    def reloadList(self, params):
        printDBG("reloadList")
        refresh = params['add_param'].get('refresh', 0)
        selIndex = params['add_param'].get('selIndex', -1)
        ret = params['ret']
        printDBG("> E2iPlayerWidget.reloadList refresh[%s], selIndex[%s]" % (refresh, selIndex))

        if 0 < refresh and -1 < self.getSelIndex():
            self.nextSelIndex =  self.getSelIndex()

        if ret.status != RetStatus.OK:
            printDBG(f"+ reloadList ret.status = {ret.status.name}")
            self.stopAutoPlaySequencer()

        self.canRandomizeList = False
        numPlayableItems = 0

        for idx, elem in enumerate(ret.value):
            if isinstance(elem, DisplayItem):
                elem.itemIdx = idx
                if elem.type.isPlayable():
                    numPlayableItems += 1

        if numPlayableItems > 1:
            self.canRandomizeList = True

        self.currList = ret.value

        ####################################################
        #                   iconManager
        ####################################################
        # fill icon List for icon manager
        # if an user whant to see icons
        self.downloadIcons()

        self.setItems(refresh)

        self["headertext"].setText(self.getCategoryPath())

        if len(self.currList) == 0:
            disMessage = _("No item to display. \nPress OK to refresh.\n")
            if ret.message and ret.message != '':
                disMessage += ret.message
            lastErrorMsg = GetIPTVPlayerLastHostError()
            if lastErrorMsg != '':
                disMessage += "\n" + _('Last error: "%s"' % lastErrorMsg)
            self.setStatusTex(disMessage)
            self._hide()
        else:
            #restor previous selection
            if len(self.currList) > self.nextSelIndex:
                self.moveToIndex(self.nextSelIndex)
            #else:
            #selection will not be change so manualy call
            self.updateBottomPanel()

            self.setStatusTex("")
            self._show()

        self.updateDownloadButton()

        if 2 == refresh:
            self.autoPlaySequencerNext(False)

        elif 1 == refresh:
            self.autoPlaySequencerNext()

        self.updateScrollbar()

    def downloadIcons(self):
        if config.plugins.iptvplayer.showcover.value and self.iconManager:
            for it in self.currList:
                if it.iconimage != '':
                    process = threading.Thread(target=self.iconManager.dowloadIcon, args=(it.iconimage,))
                    self.iconDownloadProcesses.append(process)

            for process in self.iconDownloadProcesses:
                process.start()

            for process in self.iconDownloadProcesses:
                process.join()

            self.iconDownloadProcesses = []

    def getCategoryPath(self):
        def _getCat(cat, num):
            if '' == cat:
                return ''
            cat = ' > ' + cat
            if 1 < num:
                cat += (' (x%d)' % num)
            return cat

        str_ = self.hostTitle
        #str_ = ""
        prevCat = ''
        prevNum = 0
        for cat in self.categoryList:
            if prevCat != cat:
                str_ += _getCat(prevCat, prevNum)
                prevCat = cat
                prevNum = 1
            else:
                prevNum += 1
        str_ += _getCat(prevCat, prevNum)
        return str_

    def getRefreshedCurrList(self):
        currSelIndex = self.currItem.idx
        self.requestListFromHost('Refresh', currSelIndex)

    def getInitialList(self):
        self.nexSelIndex = 0
        self.prevSelList = []
        self.categoryList = []
        self.currList = DisplayList()
        self.currItem = DisplayItem()
        self["headertext"].setText(self.getCategoryPath())
        self.requestListFromHost('Initial')

    def hideWindow(self):
        self.visible = False
        self._hide()

    def showWindow(self):
        self.visible = True
        self._show()

    def createSummary(self):
        return IPTVPlayerLCDScreen

    def canBeAddedToFavourites(self):
        try:
            favouritesHostActive = config.plugins.iptvplayer.hostfavourites.value
        except Exception:
            favouritesHostActive = False
        cItem = None
        index = -1
        # we need to check if fav is available
        if not self.isInWorkThread() and favouritesHostActive and self.visible:
            cItem = self.currItem
            if None is not cItem and (cItem.isGoodForFavourites or cItem.type in self.hostFavTypes):
                index = self.getSelIndex()
            else:
                cItem = None
        return index, cItem

    def getFavouriteItemCallback(self, thread, ret):
        asynccall.gMainFunctionsQueueTab[0].addToQueue("handleFavouriteItemCallback", [thread, ret])

    def handleFavouriteItemCallback(self, ret):
        printDBG("E2iPlayerWidget.handleFavouriteItemCallback")
        self.setStatusTex("")
        #self["list"].show()
        self._show()
        if ret.status == RetStatus.OK and isinstance(ret.value, list) and 1 == len(ret.value) and isinstance(ret.value[0], FavItem):
            favItem = ret.value[0]
            if FavItem.RESOLVER_SELF == favItem.resolver:
                favItem.resolver = self.hostName
            if '' == favItem.hostName:
                favItem.hostName = self.hostName
            self.session.open(IPTVFavouritesAddItemWidget, favItem)
        else:
            self.session.open(MessageBox, _("No valid links available."), type=MessageBox.TYPE_INFO, timeout=10)

    def menu_pressed(self):
        printDBG("E2iPlayerWidget.menu_pressed")
        # we have to be careful here as we will call method
        # directly from host
        options = []
        try:
            if self.visible and not self.isInWorkThread():

                try:
                    item = self.getSelItem()
                except Exception:
                    printExc()
                    item = None

                currSelIndex = -1 if item is None else item.itemIdx

                hRet = self.host.getCustomActions(currSelIndex)
                if hRet and hRet.status == RetStatus.OK and hRet.value:
                    for item in hRet.value:
                        if isinstance(item, IPTVChoiceBoxItem):
                            options.append(item)
            if options:
                self.stopAutoPlaySequencer()
                self.session.openWithCallback(self.requestCustomActionFromHost, IPTVChoiceBoxWidget, {'width': 600, 'current_idx': 0, 'title': _("Select action"), 'options': options})
        except Exception:
            printExc()

    def requestCustomActionFromHost(self, ret):
        printDBG("E2iPlayerWidget.requestCustomActionFromHost ret[%r]" % [ret])
        if isinstance(ret, IPTVChoiceBoxItem):
            self.requestListFromHost('PerformCustomAction', -1, ret.privateData)

    def performCustomActionCallback(self, thread, ret):
        asynccall.gMainFunctionsQueueTab[0].addToQueue("handlePerformCustomActionCallback", [thread, ret])

    def handlePerformCustomActionCallback(self, ret):
        printDBG("E2iPlayerWidget.handlePerformCustomActionCallback")
        self.setStatusTex("")
        self._show()
        if ret.status == RetStatus.OK and isinstance(ret.value, list) and 1 == len(ret.value):
            self.yellow_pressed()
        elif ret.status == RetStatus.ERROR and isinstance(ret.value, list) and 1 == len(ret.value) and isinstance(ret.value[0], str):
            self.session.open(MessageBox, ret.value[0], type=MessageBox.TYPE_ERROR)

    def markItemAsViewedCallback(self, thread, ret):
        asynccall.gMainFunctionsQueueTab[0].addToQueue("handleMarkItemAsViewedCallback", [thread, ret])

    def handleMarkItemAsViewedCallback(self, ret):
        printDBG("E2iPlayerWidget.handleMarkItemAsViewedCallback")
        self.setStatusTex("")
        self._show()
        if ret.status == RetStatus.OK and isinstance(ret.value, list) and 1 == len(ret.value) and 'refresh' in ret.value:
            self.getRefreshedCurrList()
        elif ret.status == RetStatus.ERROR and isinstance(ret.value, list) and 1 == len(ret.value) and isinstance(ret.value[0], str):
            self.session.open(MessageBox, ret.value[0], type=MessageBox.TYPE_ERROR)
        else:
            self.checkAutoPlaySequencer()

class IPTVPlayerLCDScreen(Screen):
    try:
        summary_screenwidth = getDesktop(1).size().width()
        summary_screenheight = getDesktop(1).size().height()
    except Exception:
        summary_screenwidth = 132
        summary_screenheight = 64
    if summary_screenwidth >= 800 and summary_screenheight >= 480:
        skin = """
    <screen position="0,0" size="800,480" title="E2iPlayer">
        <widget name="text1" position="10,0"  size="800,70" font="Regular;50" halign="center" valign="center" foregroundColor="#05F7F3"/>
        <widget name="text2" position="10,80" size="800,70" font="Regular;40" halign="center" valign="center" foregroundColor="#FFFF00"/>
        <widget name="LCDlogo" position="0,210" zPosition="4" size="800,267" alphatest="blend" />
    </screen>"""
    elif summary_screenwidth >= 480 and summary_screenheight >= 320:
        skin = """
    <screen position="0,0" size="480,320" title="E2iPlayer">
        <widget name="text1" position="10,0" size="460,70" font="Regular;50" halign="center" valign="center" foregroundColor="#05F7F3"/>
        <widget name="text2" position="10,80" size="460,70" font="Regular;40" halign="center" valign="center" foregroundColor="#FFFF00"/>
        <widget name="LCDlogo" position="0,160" zPosition="4" size="480,160" alphatest="blend" />
    </screen>"""
    elif summary_screenwidth >= 220 and summary_screenheight >= 176:
        skin = """
    <screen position="0,0" size="220,176" title="E2iPlayer">
        <widget name="text1" position="5,0" size="210,26" font="Regular;24" halign="center" valign="center" foregroundColor="#05F7F3"/>
        <widget name="text2" position="5,30" size="210,65" font="Regular;22" halign="center" valign="center" foregroundColor="#FFFF00"/>
        <widget name="LCDlogo" position="5,106" size="210,70" zPosition="4" alphatest="blend" />
    </screen>"""
    else:
        skin = """
    <screen position="0,0" size="132,64" title="E2iPlayer">
        <widget name="text1" position="4,0" size="132,14" font="Regular;12" halign="center" valign="center"/>
        <widget name="text2" position="4,14" size="132,49" font="Regular;10" halign="center" valign="center"/>
        <widget name="LCDlogo" zPosition="4" position="4,70" size="240,80" alphatest="blend" />
    </screen>"""

    def __init__(self, session, parent):
        Screen.__init__(self, session)
        try:
            self["text1"] = Label("E2iPlayer")
            self["text2"] = Label("")
            self["LCDlogo"] = Pixmap()
        except Exception:
            pass

    def setText(self, text):
        try:
            self["text2"].setText(text[0:39])
        except Exception:
            pass

    def LCD_showPic(self, widgetName, picPath):
        try:
            self[widgetName].instance.setScale(1)
            self[widgetName].instance.setPixmap(LoadPixmap(picPath))
            self[widgetName].show()
        except Exception:
            pass

    def LCD_hide(self, widgetName):
        try:
            self[widgetName].hide()
        except Exception:
            pass
