# -*- coding: utf-8 -*-
#

from Components.config import ConfigYesNo, getConfigListEntry

from Plugins.Extensions.IPTVPlayer.components.configbase import (
    ConfigBaseWidget, Save_Cancel_OPeration)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvhostgroups import IPTVHostsGroups
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG

class ConfigGroupsMenu(ConfigBaseWidget):

    def __init__(self, session):
        printDBG("ConfigGroupsMenu.__init__ -------------------------------")
        self.list = []
        self.inList = []
        self.groupObj = IPTVHostsGroups()

        ConfigBaseWidget.__init__(self, session)
        self.setup_title = _("E2iPlayer enable/disabled groups")
        self.__preparLists()

    def __del__(self):
        printDBG("ConfigGroupsMenu.__del__ -------------------------------")

    def __onClose(self):
        printDBG("ConfigGroupsMenu.__onClose -----------------------------")
        ConfigBaseWidget.__onClose(self)

    def layoutFinished(self):
        ConfigBaseWidget.layoutFinished(self)
        self.setTitle(self.setup_title)

    def runSetup(self):
        ConfigBaseWidget.runSetup(self)

    def saveOrCancel(self, operation:Save_Cancel_OPeration=Save_Cancel_OPeration.SAVE):
        if Save_Cancel_OPeration.SAVE == operation:
            groupList = []
            currentList = self.groupObj.getGroupsList()
            for item in currentList:
                # find idx
                validIdx = False
                idx = -1
                for idx, elem in enumerate(self.inList):
                    if elem.name == item.name:
                        validIdx = True
                        break

                if not validIdx or self.list[idx][1].value:
                    groupList.append(item.name)

            for idx, elem in enumerate(self.list):
                if elem[1].value and self.inList[idx].name not in groupList:
                    groupList.append(self.inList[idx].name)

            self.groupObj.setGroupList(groupList)

    def __preparLists(self):
        currentList = self.groupObj.getGroupsList()
        predefinedList = self.groupObj.getPredefinedGroupsList()
        self.list = []
        self.inList = []
        for item in predefinedList:
            enabled = False
            for it in currentList:
                if item.name == it.name:
                    enabled = True
                    break
            optionEntry = ConfigYesNo(default=enabled)
            self.list.append(getConfigListEntry(item.title, optionEntry))
            self.inList.append(item)
