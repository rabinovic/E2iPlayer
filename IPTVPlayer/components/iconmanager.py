﻿# -*- coding: utf-8 -*-

import threading
import time
from urllib.parse import urljoin
from binascii import hexlify
import os
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.asynccall import AsyncMethod
from Plugins.Extensions.IPTVPlayer.libs.crypto.hash.md5Hash import MD5
from Plugins.Extensions.IPTVPlayer.libs.pCommon import common, isValidUrl, getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.libs.urlparser import urlparser
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import isFreeSpaceAvailable, mkdirs, \
                      printDBG, printExc, RemoveOldDirsIcons, RemoveAllFilesIconsFromPath, \
                      RemoveAllDirsIconsFromPath, GetIconsFilesFromDir, GetNewIconsDirName, \
                      GetIconsDirs, RemoveIconsDirByPath
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.libs import ph
###################################################

###################################################
# E2 import
###################################################
from Components.config import config
###################################################


#config.plugins.iptvplayer.showcover (true|false)
#config.plugins.iptvplayer.cachepath = ConfigText(default = "/hdd/IPTVCache")

class IconManager:
    HEADER = {'User-Agent': getDefaultUserAgent(), 'Accept': '*/*', 'Accept-Encoding': 'gzip, deflate'}

    def __init__(self, downloadNew:bool = True):
        printDBG("IconManager.__init__")
        self.DOWNLOADED_IMAGE_PATH_BASE = config.plugins.iptvplayer.cachepath.value
        self.cm = common()

        # download queue
        self.queueDQ = []
        self.lockDQ = threading.Lock()
        self.workThread = None

        # already available
        self.queueAA = {}
        self.lockAA = threading.Lock()

        #this function will be called after a new icon will be available
        # self.updateFun = None

        #new icons dir for each run
        self.currDownloadDir = os.path.join(self.DOWNLOADED_IMAGE_PATH_BASE, GetNewIconsDirName())
        if not os.path.exists(self.currDownloadDir):
            mkdirs(self.currDownloadDir)

        #load available icon from disk
        #will be runned in separeted thread to speed UP start plugin
        AsyncMethod(self.loadHistoryFromDisk)(self.currDownloadDir)

        # this is called to remove icons which are stored in old version
        AsyncMethod(RemoveAllFilesIconsFromPath)(self.DOWNLOADED_IMAGE_PATH_BASE)

        self.stopThread = False

        self.checkSpace = 0 # if 0 the left space on disk will be checked
        self.downloadNew = downloadNew

    def __del__(self):
        printDBG("IconManager.__del__ -------------------------------")
        self.clearDQueue()
        self.clearAAQueue()

        if config.plugins.iptvplayer.cachepath.value == self.DOWNLOADED_IMAGE_PATH_BASE and config.plugins.iptvplayer.showcover.value:
            AsyncMethod(RemoveOldDirsIcons)(self.DOWNLOADED_IMAGE_PATH_BASE, config.plugins.iptvplayer.deleteIcons.value)
        else:
            # remove all icons as they are not more needed due to config changes
            AsyncMethod(RemoveAllDirsIconsFromPath)(self.DOWNLOADED_IMAGE_PATH_BASE)
        printDBG("IconManager.__del__ end")

    # def setUpdateCallBack(self, updateFun):
    #     self.updateFun = updateFun

    def stopWorkThread(self):
        with self.lockDQ:
            if self.workThread is not None and self.workThread.Thread.is_alive():
                self.stopThread = True

    def runWorkThread(self):
        if self.workThread is None or not self.workThread.Thread.is_alive():
            self.workThread = AsyncMethod(self.processDQ)()

    def clearDQueue(self):
        with self.lockDQ:
            self.queueDQ = []

    def addToDQueue(self, addQueue=None):
        if addQueue is None:
            addQueue = []
        with self.lockDQ:
            self.queueDQ.extend(addQueue)
            #self.queueDQ.append(addQueue)
            self.runWorkThread()

    ###############################################################
    #            Already Available (AA) queue
    ###############################################################
    def loadHistoryFromDisk(self, currDownloadDir):
        path = self.DOWNLOADED_IMAGE_PATH_BASE
        printDBG('+++++++++++++++++++++++++++++++++++++++ IconManager.loadHistoryFromDisk path[%s]' % path)
        iconsDirs = GetIconsDirs(path)
        for item in iconsDirs:
            if os.path.normcase(os.path.join(path, item)) != os.path.normcase(currDownloadDir):
                self.loadIconsFromPath(os.path.join(path, item))

    def loadIconsFromPath(self, path):
        printDBG('IconManager.loadIconsFromPath path[%s]' % path)
        iconsFiles = GetIconsFilesFromDir(path)
        if 0 == len(iconsFiles):
            RemoveIconsDirByPath(path)
        for item in iconsFiles:
            self.addItemToAAQueue(path, item)
            #printDBG('IconManager.loadIconsFromPath path[%s], name[%s] loaded' % (path, item))

    def addItemToAAQueue(self, path, name):
        with self.lockAA:
            self.queueAA[name] = path

    def clearAAQueue(self):
        with self.lockAA:
            self.queueAA = {}

    def isItemInAAQueue(self, item, hashed=False):
        file = item
        if not hashed:
            file = self.getHashedFileName(item)

        return bool(self.queueAA.get(file, False))


    def getHashedFileName(self, item):
        hashAlg = MD5()
        name = hashAlg(item)
        return hexlify(name).decode("utf-8", "strict") + '.jpg'

    def getIconPathFromAAQueue(self, item):
        printDBG("getIconPathFromAAQueue item[%s]" % item)
        if item.startswith('file://'):
            return item[7:]

        filename = self.getHashedFileName(item)

        with self.lockAA:
            file_path = self.queueAA.get(filename, '')
            if file_path != '':
                try:
                    file_path = os.path.normcase(os.path.join(file_path, filename))
                    if os.path.normcase(self.currDownloadDir) != os.path.normcase(file_path):
                        new_path = os.path.normcase(os.path.join(self.currDownloadDir,filename))
                        os.rename(file_path, new_path)
                        self.queueAA[filename] = os.path.normcase(self.currDownloadDir)
                        file_path = new_path
                except Exception:
                    printExc()

        printDBG("getIconPathFromAAQueue A file_path[%s]" % file_path)
        return file_path

    def dowloadIcon(self, iconURL):
        file_ = self.getHashedFileName(iconURL)
        #check if this image is not already available in cache AA list
        if self.isItemInAAQueue(file_, True):
            return

        if self.download_img(iconURL, file_):
            # add to AA list
            self.addItemToAAQueue(self.currDownloadDir, file_)
            # if self.updateFun:
            #     self.updateFun(iconURL)

    def processDQ(self):
        printDBG("IconManager.processDQ: Thread started")

        while True:
            die = False
            url = ''

            #getFirstFromDQueue
            with self.lockDQ:
                if not self.stopThread:
                    if self.queueDQ:
                        url = self.queueDQ.pop(0)
                    else:
                        self.workThread = None
                        die = True
                else:
                    self.stopThread = False
                    self.workThread = None
                    die = True

            if die:
                return

            printDBG("IconManager.processDQ url: [%s]" % url)
            if url != '':
                self.dowloadIcon(url)


    def download_img(self, img_url, filename):
        # if at start there was NOT enough space on disk
        # new icon will not be downloaded
        if not self.downloadNew:
            return False

        if len(self.currDownloadDir) < 4:
            printDBG('IconManager.download_img: wrong path for IPTVCache')
            return False

        path = self.currDownloadDir + '/'

        # if at start there was enough space on disk
        # we will check if we still have free space
        if 0 >= self.checkSpace:
            printDBG('IconManager.download_img: checking space on device')
            if not isFreeSpaceAvailable(path, 10):
                printDBG('IconManager.download_img: not enough space for new icons, new icons will not be downloaded any more')
                self.downloadNew = False
                return False
            # for another 50 check again
            self.checkSpace = 50
        else:
            self.checkSpace -= 1
        file_path = "%s%s" % (path, filename)

        params = {} #{'maintype': 'image'}

        if config.plugins.iptvplayer.allowedcoverformats.value != 'all':
            subtypes = config.plugins.iptvplayer.allowedcoverformats.value.split(',')
            params['check_first_bytes'] = []
            if 'jpeg' in subtypes:
                params['check_first_bytes'].extend([b'\xFF\xD8', b'\xFF\xD9'])
            if 'png' in subtypes:
                params['check_first_bytes'].append(b'\x89\x50\x4E\x47')
            if 'gif' in subtypes:
                params['check_first_bytes'].extend([b'GIF87a', b'GIF89a'])
            # formato webp	'RI'
            if 'webp' in subtypes:
                params['check_first_bytes'].extend([b'RI'])
        else:
            params['check_first_bytes'] = [b'\xFF\xD8', b'\xFF\xD9', b'\x89\x50\x4E\x47', b'GIF87a', b'GIF89a', b'RI']

        params_cfad = {}
        param_png = {}

        if img_url.endswith('|cf'):
            img_url = img_url[:-3]
            params_cfad = {'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True}
            domain = urlparser.getDomain(img_url, onlyDomain=True)
            if domain.startswith("www."):
                domain = domain[4:]
            params_cfad['cookiefile'] = f'/hdd/IPTVCache//cookies/{domain}.cookie'
        elif img_url.endswith('|webpToPng'):
            param_png = {'webp_convert_to_png': True}
        elif img_url.endswith('need_resolve.jpeg'):
            domain = urlparser.getDomain(img_url)
            if domain.startswith('www.'):
                domain = domain[4:]
            # link need resolve, at now we will have only one img resolver,
            # we should consider add img resolver to urlparser if more will be needed
            sts, data = self.cm.getPage(img_url)
            if not sts:
                return False
            if 'imdb.com' in domain:
                img_url = ph.getDataBetweenMarkers(data, 'class="poster"', '</div>')[1]
                img_url = ph.getSearchGroups(img_url, 'src="([^"]+?)"')[0]
                if not isValidUrl(img_url):
                    img_url = ph.getDataBetweenMarkers(data, 'class="slate"', '</div>')[1]
                    img_url = ph.getSearchGroups(img_url, 'src="([^"]+?)"')[0]
            elif 'bs.to' in domain:
                baseUrl = img_url
                img_url = ph.getSearchGroups(data, '(<img[^>]+?alt="Cover"[^>]+?>)')[0]
                img_url = ph.getSearchGroups(img_url, 'src="([^"]+?)"')[0]
                if img_url.startswith('/'):
                    img_url = urljoin(baseUrl, img_url)
            elif 'watchseriesmovie.' in domain or 'gowatchseries' in domain:
                baseUrl = img_url
                img_url = ph.getDataBetweenNodes(data, ('<div', '>', 'picture'), ('</div', '>'), False)[1]
                img_url = ph.getSearchGroups(img_url, '<img[^>]+?src="([^"]+?)"')[0]
                if img_url.startswith('/'):
                    img_url = urljoin(baseUrl, img_url)
            elif 'classiccinemaonline.com' in domain:
                baseUrl = img_url
                img_url = ph.getDataBetweenNodes(data, ('<center>', '</center>', '<img'), ('<', '>'))[1]
                img_url = ph.getSearchGroups(img_url, '<img[^>]+?src="([^"]+?\.(:?jpe?g|png)(:?\?[^"]+?)?)"')[0]
                if img_url.startswith('/'):
                    img_url = urljoin(baseUrl, img_url)
            elif 'allbox.' in domain:
                baseUrl = img_url
                img_url = ph.getDataBetweenNodes(data, ('<img', '>', '"image"'), ('<', '>'))[1]
                if img_url != '':
                    img_url = ph.getSearchGroups(img_url, '<img[^>]+?src="([^"]+?\.(:?jpe?g|png)(:?\?[^"]+?)?)"')[0]
                else:
                    img_url = ph.getSearchGroups(data, 'url\(([^"^\)]+?\.(:?jpe?g|png)(:?\?[^"^\)]+?)?)\);')[0].strip()
                if img_url.startswith('/'):
                    img_url = urljoin(baseUrl, img_url)
            elif 'efilmy.' in domain:
                baseUrl = img_url
                img_url = ph.getDataBetweenNodes(data, ('<img', '>', 'align="left"'), ('<', '>'))[1]
                img_url = ph.getSearchGroups(img_url, '<img[^>]+?src="([^"]+?\.(:?jpe?g|png)(:?\?[^"]+?)?)"')[0]
                img_url = self.cm.getFullUrl(img_url, baseUrl)

            if not isValidUrl(img_url):
                return False
        else:
            img_url = strwithmeta(img_url)
            if img_url.meta.get('icon_resolver', None) is not None:
                try:
                    img_url = img_url.meta['icon_resolver'](self.cm, img_url)
                except Exception:
                    printExc()
                    return False

        if not isValidUrl(img_url):
            return False

        params = params | params_cfad | param_png

        printDBG(f"Calling saveWebFile file_path:'{file_path}' img_url:'{img_url}'")
        return self.cm.saveWebFile(file_path, img_url, addParams=params)['sts']
