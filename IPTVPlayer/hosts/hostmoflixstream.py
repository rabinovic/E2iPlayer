# -*- coding: utf-8 -*-

import json
import os
from typing import List

from urllib.parse import quote, quote_plus
from Plugins.Extensions.IPTVPlayer.components.iptvchoicebox import IPTVChoiceBoxItem
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, RetHost, RetStatus

from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
###################################################
# Config options for HOST
###################################################
from Components.config import (ConfigSelection, ConfigYesNo, config,
                               getConfigListEntry)
# movies:
# https://moflix-stream.xyz/api/v1/channel/345?returnContentOnly=true&restriction=&order=rating:desc&paginate=simple&perPage=50&query=&page=2
# order : created_at, popularity, rating,

# series:
# https://moflix-stream.xyz/api/v1/channel/352?returnContentOnly=true&restriction=&order=channelables.order:asc&paginate=simple&perPage=50&query=&page=2
# order : channelables, created_at, popularity, rating,


def gettytul():
    return 'moflix-stream'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__({'history': 'moflix-stream','cookie': 'moflix-stream.cookie'}, True)

        self.DEFAULT_ICON = 'https://moflix-stream.xyz/storage/branding_media/ff7de36e-1737-45db-bdbd-d3789ede30a5.png'
        self.MAIN_URL = 'https://moflix-stream.xyz/'

        self.USER_AGENT = getDefaultUserAgent()
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Referer':self.MAIN_URL, 'Accept': 'application/json'}
        self.defaultParams = {'header':self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        # Hoster
        self.HOSTER = self.MAIN_URL + 'api/v1/titles/%s?load=images,genres,productionCountries,keywords,videos,primaryVideo,seasons,compactCredits'

        self.orderby = 'popularity'

    def getPage(self, baseUrl, addParams=None, post_data = None):
        if addParams is None:
            addParams = dict(self.defaultParams)

        return self.cm.getPage(baseUrl, addParams, post_data)

    def mainMenu(self, cItem):
        printDBG(f"moflix-stream.listMainMenu cItem [{cItem}]")

        MAIN_CAT_TAB = [
                        {'category':'listItems', 'title': 'Kürzlich hinzugefügt', 'url':self.MAIN_URL+'api/v1/channel/now-playing?channelType=channel&restriction=&paginate=simple&loader=channelPage', 'page':1, 'icon':self.DEFAULT_ICON_URL},
                        {'category':'listItems', 'title': 'Angesagte Serien', 'url':self.MAIN_URL+'api/v1/channel/trending-tv?channelType=channel&restriction=&paginate=simple&loader=channelPage', 'page':1, 'icon':self.DEFAULT_ICON_URL},
                        {'category':'listItems', 'title': 'Kinder & Familie', 'url':self.MAIN_URL+'api/v1/channel/top-kids-liste?channelType=channel&restriction=&paginate=simple&loader=channelPage&order=popularity:desc', 'page':1, 'icon':self.DEFAULT_ICON_URL},
                        {'category':'listItems', 'title': 'Top', 'url':self.MAIN_URL+'api/v1/channel/top-rated-movies?channelType=channel&restriction=&paginate=simple&loader=channelPage', 'page':1, 'icon':self.DEFAULT_ICON_URL},
                        {'category':'listItems', 'title': 'Filme', 'url':self.MAIN_URL+'api/v1/channel/345?returnContentOnly=true&restriction=&paginate=simple&perPage=50&query=&order=popularity:desc', 'page':1, 'icon':self.DEFAULT_ICON_URL},
                        {'category':'listItems', 'title': 'Serien', 'url':self.MAIN_URL+'api/v1/channel/352?returnContentOnly=true&restriction=paginate=simple&perPage=50&query=', 'page':1, 'icon':self.DEFAULT_ICON_URL},
                        {'category':'search','title': _('Search'), 'url':self.MAIN_URL+ 'api/v1/search/%s?query=%s&limit=8', 'search_item':True, 'icon':self.DEFAULT_ICON_URL},
                        {'category': 'search_history', 'title': _('Search history'), },
                    ]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        url = cItem['url']
        page = cItem.get('page', None)

        if page:
            url += f'&page={page}'

        sts, data = self.getPage(url)
        if not sts:
            return []

        jsonData = json.loads(data)
        pagination = jsonData.get('pagination', None)
        if pagination:
            data_ = pagination.get('data', [])
            nextPage = pagination.get('nextPage', None)
        else:
            data_ = jsonData.get('channel', {}).get('content', {}).get('data', [])
            nextPage = jsonData.get('channel', {}).get('content', {}).get('nextPage', None)

        for elem in data_:
            if not elem.get('primary_video', None):
                continue
            icon = elem.get('poster', '').replace('original', 'w300')
            title = elem['name']
            desc = elem['description']
            _id = elem['id']
            releaseDate = elem['release_date'][:4]
            param = cItem | { 'title' : f'{title} ({releaseDate})', 'id': _id, 'icon': icon,  'desc': desc}
            if elem.get('is_series', None):
                self.addDir(param | {'category':'listSeasons', 'is_series':True, 'with_mini_cover':True})
            else:
                self.addVideo(param | {'url':f'{_id}'})

        if nextPage:
            param = cItem | {'page': nextPage}
            self.addNext(param)

    def listSeasons(self, cItem):
        _id = cItem['id']
        sts, data = self.getPage(self.HOSTER % _id)
        if not sts:
            return []

        jsonData = json.loads(data)
        for season in jsonData.get('seasons', {}).get('data', []):
            number = season['number']
            icon = season['poster'].replace('original', 'w300')
            addParam = dict(cItem)
            self.addDir(addParam | {'category':'listEpisodes', 'title': f"Staffel {number}", 'season':number, 'icon':icon, 'good_for_fav': True, 'with_mini_cover':True})


    def listEpisodes(self, cItem):
        _id = cItem['id']
        season = cItem['season']
        url = self.MAIN_URL + f'api/v1/titles/{_id}/seasons/{season}/episodes?perPage=30&excludeDescription=true&query=&orderBy=episode_number&orderDir=asc&page=1'

        self.defaultParams['header']['Referer'] = url
        sts, data = self.getPage(url)
        if not sts:
            return []

        jsonData = json.loads(data)
        pagination = jsonData.get('pagination', None)
        for elem in pagination.get('data', []):
            if not elem.get('primary_video', None):
                continue
            icon = elem.get('poster', '').replace('original', 'w300')
            title = elem['name']
            desc = elem['description']
            releaseDate = elem['release_date'][:4]
            episode = elem['episode_number']

            param = cItem | { 'title' : f'{episode} {title} ({releaseDate})', 'url':f'{_id}{season}{episode}',  'episode':episode, 'icon': icon,  'desc': desc}
            self.addVideo(param)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("moflix-stream.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.MAIN_URL + 'api/v1/search/%s?query=%s&limit=8' % (quote(searchPattern), quote_plus(searchPattern))

        sts, data = self.getPage(url)
        if not sts:
            return []

        jsonData = json.loads(data)
        searchResults = jsonData.get('results', [])
        for elem in searchResults:
            icon = elem.get('poster', '').replace('original', 'w300')
            title = elem['name']
            desc = elem.get('description', '')
            _id = elem['id']
            releaseDate = elem.get('release_date', '')
            if releaseDate:
                releaseDate = releaseDate[:4]
            param = cItem | { 'title' : f'{title} ({releaseDate})', 'id': _id, 'icon': icon,  'desc': desc}
            if elem.get('is_series', None):
                self.addDir(param | {'category':'listSeasons', 'is_series':True, 'with_mini_cover':True})
            else:
                self.addVideo(param | {'url':f'{_id}'})

    def getCustomArticleContent(self, cItem) -> List:
        return super().getCustomArticleContent(cItem)

    def getCustomLinksForVideo(self, cItem):
        printDBG("moflix-stream.getCustomLinksForVideo cItem [%s]" % cItem)
        urlTab = []
        _id = cItem['id']

        isSeries = cItem.get('is_series', False)

        if isSeries:
            season = cItem['season']
            episode = cItem['episode']
            url = f'{self.MAIN_URL}api/v1/titles/{_id}/seasons/{season}/episodes/{episode}?loader=episodePage'
        else:
            url =self.HOSTER % _id

        sts, data = self.getPage(url)
        if not sts:
            return []

        jsonData = json.loads(data)

        videos = jsonData['episode']['videos'] if isSeries else jsonData['title']['videos']
        for video in videos:
            quality = '' if not video['quality'] else f"({video['quality']})"
            name = f"{self.up.getHostName(video['src'])} {quality}"
            if 'youtube' in name:
                name += '(Trailer)'
            urlTab.append({'name':name, 'url':video['src'], 'need_resolve': 1})

        return urlTab

    def getCustomVideoLinks(self, url):
        return self.up.getVideoLinkExt(url)
