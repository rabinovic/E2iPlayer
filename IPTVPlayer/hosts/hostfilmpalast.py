# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from typing import Dict
from Components.config import config, ConfigText
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _, SetIPTVPlayerLastHostError
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, DisplayList
from Plugins.Extensions.IPTVPlayer.libs.pCommon import isValidUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta

from Plugins.Extensions.IPTVPlayer.libs import ph
###################################################

###################################################
# FOREIGN import
###################################################
import re
from urllib.parse import quote
import json
###################################################

def gettytul():
    return 'http://filmpalast.to/'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'filmpalast.to', 'cookie': 'filmpalast.to.cookie'})
        self.AJAX_HEADER = dict(self.HEADER)
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})

        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'

        self.MAIN_URL = 'http://filmpalast.to/'
        self.HTTP_HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html', 'Accept-Encoding': 'gzip, deflate', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}
        self.defaultParams = {'header': self.HTTP_HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True,
                              'cookiefile': self.COOKIE_FILE}

        self.DEFAULT_ICON_URL = 'https://filmpalast.to/themes/downloadarchive/images/logo.png'
        self.cacheSeries = {}
        self.cacheSeasons = {}
        self.cacheLinks = {}

    def mainMenu(self, cItem: Dict):
        MAIN_CAT_TAB = [{'category': 'listItems', 'title': _("Main"), 'url': self.getMainUrl()},
                             {'category': 'movies', 'title': _("Movies")},
                             {'category': 'series', 'title': _("Series"), },

                             {'category': 'search', 'title': _('Search'), 'search_item': True, },
                             {'category': 'search_history', 'title': _('Search history'), },
                             ]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def series(self, cItem: Dict):
        SERIES_CAT_TAB = [
            {'category': 'listItems', 'title': _("--All Episodes--"), 'url': self.getFullUrl('/serien/view')},
            {'category': 'listSeriesABC', 'title': _("Alphabetically"), 'url': self.getFullUrl('/serien/view')},
            ]
        self.listsTab(SERIES_CAT_TAB, cItem)


    def movies(self, cItem: Dict):
        MOVIES_CAT_TAB = [{'category': 'listItems', 'title': _("New"), 'url': self.getFullUrl('/movies/new')},
                               {'category': 'listItems', 'title': _("Top"), 'url': self.getFullUrl('/movies/top')},
                               {'category': 'listCats', 'title': _("Categories"), 'url': self.getFullUrl('/movies/new') , 'm1':'id="genre"', 'm2':'</ul>'},
                               {'category': 'listCats', 'title': _("Alphabetically"), 'url': self.getFullUrl('/movies/new'), 'm1':'id="movietitle"', 'm2':'</ul>'},
                               ]
        self.listsTab(MOVIES_CAT_TAB, cItem)


    def getPage(self, baseUrl, addParams:Dict=None, post_data=None):
        if addParams is None:
            addParams = dict(self.defaultParams)

        baseUrl = self.cm.iriToUri(baseUrl)
        sts, data = self.cm.getPageCFProtection(baseUrl, addParams, post_data)
        userAgent = self.defaultParams.get('header', {}).get('User-Agent', '')
        if data.meta.get('cf_user', self.USER_AGENT) != userAgent:
            self.defaultParams['header']['User-Agent'] = data.meta['cf_user']

        return sts, data

    def getFullIconUrl(self, url):
        printDBG((f"getFullIconUrl filmpalast {url}"))
        url = self.getFullUrl(url)
        if url == '': return ''
        cookieHeader = self.cm.getCookieHeader(self.COOKIE_FILE)
        cf_header =  {'Cookie': cookieHeader, 'User-Agent': self.USER_AGENT}
        printDBG((f"Filmpalast getFullIconUrl cf_header: {cf_header}"))
        return strwithmeta(url, cf_header)

    def _listLinks(self, cItem, m1, m2):
        sts, data = self.getPage(cItem['url'])
        if not sts: return []

        retTab = []
        data = ph.getDataBetweenMarkers(data, m1, m2)[1]
        data = ph.getAllItemsBetweenMarkers(data, '<a', '</a>')
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
            title = ph.cleanHtml(item)
            retTab.append({'title': title, 'url': url})
        return retTab

    def listSeriesABC(self, cItem):
        printDBG("FilmPalastTo.listSeriesABC |%s|" % cItem)
        self.cacheSeries = {'letters': []}
        tab = self._listLinks(cItem, 'id="serien"', '</ul>')
        for item in tab:
            letter = item['title'][0]
            if not letter.isalpha(): letter = '#'
            if letter not in self.cacheSeries:
                self.cacheSeries['letters'].append(letter)
                self.cacheSeries[letter] = []
                params = dict(cItem)
                params.update({'category': 'listSeriesByLetter', 'title': letter, 'f_letter': letter})
                self.addDir(params)
            self.cacheSeries[letter].append(item)

        params = dict(cItem)
        params.update({'category': 'listSeriesByLetter', 'title': _('--All--'), 'f_letter': ''})
        self.currList.insert(0, params)

    def listSeriesByLetter(self, cItem):
        printDBG("FilmPalastTo.listSeriesByLetter |%s|" % cItem)

        if '' == cItem.get('f_letter', ''):
            letters = self.cacheSeries['letters']
        else:
            letters = [cItem['f_letter']]

        for letter in letters:
            for item in self.cacheSeries[letter]:
                params = dict(cItem)
                params.update(item)
                params['icon'] = self.getFullIconUrl('/files/movies/450/%s.jpg' % item['url'].split('/')[-1])
                params['category'] = 'exploreItem'
                self.addDir(params|{'with_mini_cover':True})

    def listCats(self, cItem):
        printDBG("FilmPalastTo.listCats |%s|" % cItem)

        m1 = cItem['m1']
        m2 = cItem['m2']
        tab = self._listLinks(cItem, m1, m2)
        for item in tab:
            params = dict(cItem)
            params.update(item)
            params['category'] = 'listItems'
            self.addDir(params)

    def listItems(self, cItem):
        printDBG("FilmPalastTo.listItems |%s|" % cItem)

        url = cItem['url']
        page = cItem.get('page', 1)

        if '?' not in url:
            if page > 1:
                if not url.endswith('/'): url += '/'
                if '/search/' not in url:
                    url += 'page/'
                url += str(page)

        sts, data = self.getPage(url)
        if not sts:
            return

        nextPage = 'vorw&auml;rts&nbsp;+' in data

        veroeffentlicht = re.search('(Ver&ouml;ffentlicht:.*)', data)
        veroeffentlicht = veroeffentlicht[1] if veroeffentlicht else ''

        spielzeit = re.search('(Spielzeit:.*)', data)
        spielzeit = spielzeit[1] if spielzeit else ''

        data = ph.getAllItemsBetweenMarkers(data, '<article', '</article>')
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
            icon = self.getFullIconUrl(ph.getSearchGroups(item, '''src=['"]([^'^"]+?\.jpe?g)['"]''')[0])
            title = ph.cleanHtml(ph.getSearchGroups(item, '''title=['"]([^'^"]+)['"]''')[0])

            sts, tmp = ph.getDataBetweenMarkers(item, '<ul class="clearfix">', '</ul>')
            if not sts:
                rating = item.count('/star_on.png')
                desc = ph.cleanHtml(ph.getDataBetweenMarkers(item, '<small', '</small>')[1])
                desc = f'{rating}/10 {desc}'
            else:
                desc = []
                tmp = ph.getAllItemsBetweenMarkers(tmp, '<li', '</li>')
                for t in tmp:
                    t = t.split('</span>')
                    for t1 in t:
                        t1 = ph.cleanHtml(t1)
                        if t1 != '':
                            desc.append(t1)
                desc = ' | '.join(desc)
            jahr = ''
            match_jahr = re.search('Jahr:\s*(\d+)', desc)
            if match_jahr:
                jahr = f'({match_jahr[1]})'
            params = dict(cItem)
            params.update({'good_for_fav': True, 'category':'exploreItem',  'title': f"{title} {jahr}" , 'url': url, 'icon': icon,
                           'desc': desc, 'with_mini_cover':True})
            self.addDir(params)

        if nextPage:
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': _('Next page'), 'page': page + 1})
            self.addNext(params)

    def listEpisodes(self, cItem):
        printDBG("FilmPalastTo.listEpisodes")
        seasonId = cItem.get('f_season', '')
        tab = self.cacheSeasons.get(seasonId, [])
        for item in tab:
            params = dict(cItem)
            params.update(item)
            # params['icon'] = self.getFullIconUrl('/files/movies/450/%s.jpg' % item['url'].split('/')[-1])
            self.addVideo(params)

    def exploreItem(self, cItem):
        printDBG("FilmPalastTo.exploreItem")

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts: return

        tmp = ph.getDataBetweenMarkers(data, '<ul class="staffelNav', '</ul>')[1]

        if 'data-id="staffId' not in data:
            params = dict(cItem)
            self.addVideo(params|{'with_mini_cover':False})
            return

        if '' != ph.getSearchGroups(cItem['title'] + ' ', '''\s([Ss][0-9]+[Ee][0-9]+)\s''')[0]:
            params = dict(cItem)
            self.addVideo(params | {'with_mini_cover': False})

        self.cacheSeasons = {}
        seasonTitles = {}

        tmp = ph.getDataBetweenMarkers(data, '<ul class="staffelNav', '</ul>')[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, '<li', '</li>')
        for item in tmp:
            seasonId = ph.getSearchGroups(item, '''data-id=['"]([^"^']+?)['"]''')[0]
            title = ph.cleanHtml(item)
            seasonTitles[seasonId] = title

        sp = '<div class="staffelWrapperLoop'
        tmp = ph.getDataBetweenMarkers(data, sp, '<a name="comments_view">', False)[1]
        tmp = tmp.split(sp)
        for item in tmp:
            seasonId = ph.getSearchGroups(item, '''id=['"]([^"^']+?)['"]''')[0]
            item = ph.getAllItemsBetweenMarkers(item, '<a', '</a>')
            for it in item:
                url = self.getFullUrl(ph.getSearchGroups(it, '''href=['"]([^'^"]+?)['"]''')[0])
                title = ph.cleanHtml(it)
                if seasonId not in self.cacheSeasons:
                    self.cacheSeasons[seasonId] = []
                    params = dict(cItem)
                    params.update(
                        {'good_for_fav': False, 'category': 'listEpisodes', 'title': seasonTitles.get(seasonId, seasonId),
                         'f_season': seasonId, 'with_mini_cover':False})
                    self.addDir(params)
                self.cacheSeasons[seasonId].append({'good_for_fav': True, 'title': title, 'url': url, 'with_mini_cover':False})

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("FilmPalastTo.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (
        cItem, searchPattern, searchType))
        cItem = dict(cItem)
        cItem['url'] = self.getFullUrl('/search/title/%s' % quote(searchPattern))
        self.listItems(cItem )

    def getCustomLinksForVideo(self, cItem):
        printDBG("FilmPalastTo.getCustomLinksForVideo [%s]" % cItem)
        linksTab = []

        if linksTab := self.cacheLinks.get(cItem['url'], []):
            return linksTab

        sts, data = self.getPage(cItem['url'], self.defaultParams)
        if not sts:
            return []

        items = ph.findall(data, ('<ul', '>', 'currentStreamLinks'), '</ul>', flags=0)
        for item in items:
            # printDBG(item)
            title = ph.cleanHtml(ph.find(item, ('<p', '>'), '</p>', flags=0)[1])
            if not title:
                title = ph.cleanHtml(item)

            url = re.findall("data-player-url=\"(.*?)\"", item)
            if url:
                url = url[0]
            else:
                url = re.findall("href=\"(.*?)\"", item)
                if url:
                    url = url[0]
                else:
                    continue

            linksTab.append({'name': title, 'url': url, 'need_resolve': 1})

        if len(linksTab):
            self.cacheLinks[cItem['url']] = linksTab

        return linksTab

    def getCustomVideoLinks(self, videoUrl):
        printDBG("FilmPalastTo.getCustomVideoLinks [%s]" % videoUrl)

        videoUrl = strwithmeta(videoUrl)

        key = videoUrl.meta.get('links_key', '')

        data_id = videoUrl.meta.get('data_id', '')
        data_stamp = videoUrl.meta.get('data_stamp', '')

        if data_id and data_stamp:
            # sts, data = self.getPage(key, self.defaultParams)
            # if not sts: return []

            url = self.getFullUrl('/stream/%s/%s' % (data_id, data_stamp))
            urlParams = dict(self.defaultParams)
            urlParams['header'] = dict(self.AJAX_HEADER)
            urlParams['header']['Referer'] = key
            sts, data = self.getPage(url, urlParams, {'streamID': data_id})
            if not sts: return []

            try:
                data = json.loads(data)
                url = data.get('url', '')
                if isValidUrl(url):
                    return self.up.getVideoLinkExt(url)
                SetIPTVPlayerLastHostError(data['msg'])
            except Exception:
                printExc()
        else:
            return self.up.getVideoLinkExt(videoUrl)

    def getCustomArticleContent(self, cItem):
        printDBG("FilmPalastTo.getCustomArticleContent [%s]" % cItem)
        retTab = []

        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts: return []

        data = ph.getDataBetweenMarkers(data, '<div id="content" role="main">', '</ul>')[1]

        title = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<h2', '</h2>')[1])
        desc = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<span class="hidden', '</span>')[1])
        icon = self.getFullIconUrl(ph.getSearchGroups(data, '''src=['"]([^"^']+\.jpe?g)['"]''')[0])

        tmpTab = []
        tmp = ph.getDataBetweenMarkers(data, 'enre</p>', '</li>', False)[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, '<a', '</a>')
        for t in tmp: tmpTab.append(ph.cleanHtml(t))
        if len(tmpTab): otherInfo['genre'] = ', '.join(tmpTab)

        tmpTab = []
        tmp = ph.getDataBetweenMarkers(data, 'chauspieler</p>', '</li>', False)[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, '<a', '</a>')
        for t in tmp: tmpTab.append(ph.cleanHtml(t))
        if len(tmpTab): otherInfo['actors'] = ', '.join(tmpTab)

        tmpTab = []
        tmp = ph.getDataBetweenMarkers(data, 'chöpfer</p>', '</li>', False)[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, '<a', '</a>')
        for t in tmp: tmpTab.append(ph.cleanHtml(t))
        if len(tmpTab): otherInfo['creators'] = ', '.join(tmpTab)

        tmpTab = []
        tmp = ph.getDataBetweenMarkers(data, 'egie</p>', '</li>', False)[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, '<a', '</a>')
        for t in tmp: tmpTab.append(ph.cleanHtml(t))
        if len(tmpTab): otherInfo['director'] = ', '.join(tmpTab)

        tmp = ph.cleanHtml(ph.getDataBetweenMarkers(data, 'hortinfos</p>', '</strong>', False)[1])
        if tmp != '': otherInfo['views'] = tmp

        tmp = ph.cleanHtml(ph.getDataBetweenMarkers(data, 'Ver&ouml;ffentlicht:', '<', False)[1])
        if tmp != '': otherInfo['released'] = tmp

        tmp = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<em>', '</em>', False)[1])
        if tmp != '': otherInfo['duration'] = tmp

        tmp = ph.cleanHtml(ph.getDataBetweenMarkers(data, 'Imdb:', '<', False)[1])
        if tmp != '': otherInfo['imdb_rating'] = tmp

        tmp = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<span class="average">', '<span', False)[1])
        if tmp != '': otherInfo['rating'] = tmp

        if title == '': title = cItem['title']
        if desc == '':  desc = cItem.get('desc', '')
        if icon == '':  icon = cItem.get('icon', self.DEFAULT_ICON_URL)

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc),
                 'images': [{'title': '', 'url': self.getFullUrl(icon)}], 'other_info': otherInfo}]


    def withArticleContent(self, cItem):
        if 'video' == cItem.get('type', '') or 'exploreItem' == cItem.get('category', ''):
            return True
        return False
