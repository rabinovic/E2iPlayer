# -*- coding: utf-8 -*-

import importlib
try:
    import simplejson as json
except Exception:
    import json
from binascii import hexlify
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, DisplayItem, DisplayItemType, RetHost, UrlItem, FavItem, RetStatus
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, GetLogoDir, GetFavouritesDir, mkdirs, rm, touch
from Plugins.Extensions.IPTVPlayer.tools.iptvfavourites import IPTVFavourites
from Plugins.Extensions.IPTVPlayer.components.iptvchoicebox import IPTVChoiceBoxItem
from Plugins.Extensions.IPTVPlayer.libs.crypto.hash.md5Hash import MD5
###################################################

###################################################
# FOREIGN import
###################################################
from Tools.Directories import fileExists
from Components.config import config, ConfigYesNo, getConfigListEntry
###################################################

###################################################
# Config options for HOST
###################################################
config.plugins.iptvplayer.favourites_use_watched_flag = ConfigYesNo(default=True)


def GetConfigList():
    optionList = []
    optionList.append(getConfigListEntry(_("Allow watched flag to be set"), config.plugins.iptvplayer.favourites_use_watched_flag))
    if config.plugins.iptvplayer.favourites_use_watched_flag.value:
        optionList.append(getConfigListEntry(_("The color of the viewed item"), config.plugins.iptvplayer.watched_item_color))
    return optionList
###################################################


def gettytul():
    return _('Favourites')

class IPTVHost(HostBase):

    def __init__(self):
        printDBG("Favourites.__init__")
        super().__init__( withSearchHistrory=False)
        self.helper = IPTVFavourites(GetFavouritesDir())
        self.host = None
        self.guestMode = False # main or guest
        self.DEFAULT_ICON_URL = 'https://www.iconninja.com/files/637/891/649/512-favorites-icon.png'
        self.cachedRet = None
        self.useWatchedFlag = config.plugins.iptvplayer.favourites_use_watched_flag.value
        self.refreshAfterWatchedFlagChange = False

    def getHost(self, hostName):
        if self.host and self.host.hostName == hostName:
            return self.host
        try:
            _temp = importlib.import_module(f'Plugins.Extensions.IPTVPlayer.hosts.host{hostName}')
            host = _temp.IPTVHost()
            if isinstance(host, HostBase):
                self.host = host
                self.host.hostName = hostName # field hostName created dynamically.
                return self.host
        except Exception:
            printExc()
        return None

    def getHostNameFromItem(self, index):
        return self.currList[index]['host']

    def isGuestMode(self):
        return self.guestMode

    def clearGuestMode(self):
        self.guestMode = False

    def mainMenu(self, cItem):
        printDBG("Favourites.listGroups")
        sts = self.helper.load()
        if not sts:
            return
        data = self.helper.getGroups()
        self.listsTab(data, {'category': 'listFavourites'})

    def listFavourites(self, cItem):
        printDBG("Favourites.listFavourites")
        sts, data = self.helper.getGroupItems(cItem['group_id'])
        if not sts:
            return

        typesMap = {DisplayItemType.VIDEO: self.addVideo,
                    DisplayItemType.AUDIO: self.addAudio,
                    DisplayItemType.PICTURE: self.addPicture,
                    DisplayItemType.ARTICLE: self.addArticle,
                    DisplayItemType.CATEGORY: self.addDir}

        for idx, item in enumerate(data):
            desc = f'{_("Source")}: {item.hostName}\n{item.description}'
            params = {'name': 'item', 'title': item.title, 'category':'listFavouriteItems', 'host': item.hostName, 'icon': item.iconimage, 'desc': desc, 'group_id': cItem['group_id'], 'item_idx': idx, 'with_mini_cover':True}

            if addFun := typesMap.get(item.type, None):
                addFun(params)

    def _getLinksForVideo(self, cItem):
        printDBG(f"Favourites.getCustomLinksForVideo idx[{cItem}]")
        ret = RetHost(RetStatus.ERROR, value=[])
        sts, data = self.helper.getGroupItems(cItem['group_id'])
        if not sts:
            return ret
        item = data[cItem['item_idx']]

        printDBG(f">>>>>>>>>>>>>>>>>>>>>>>>>>>> [{item.resolver}]" )

        if FavItem.RESOLVER_URLLPARSER == item.resolver:
            self.host = None
            retlist = []
            urlList = self.up.getVideoLinkExt(item.data)
            for item in urlList:
                name = ph.cleanHtml(item["name"])
                url = item["url"]
                retlist.append(UrlItem(name, url, 0))
            ret = RetHost(RetStatus.OK, value=retlist)
        elif FavItem.RESOLVER_DIRECT_LINK == item.resolver:
            self.host = None
            retlist = []
            retlist.append(UrlItem('direct link', item.data, 0))
            ret = RetHost(RetStatus.OK, value=retlist)
        else:
            if host := self.getHost(item.resolver):
                ret = host.getLinksForFavourite(item)
        return ret

    def _getResolvedURL(self, url):
        try:
            return self.host.getResolvedURL(url)
        except Exception:
            return RetHost(RetStatus.ERROR, value=[])

    def listFavouriteItems(self, cItem):
        sts, data = self.helper.getGroupItems(self.currItem['group_id'])
        if sts:
            item = data[self.currItem['item_idx']]
            if host := self.getHost(self.currItem['host']):
                ret = host.setInitFavouriteItem(item)
                if RetStatus.OK == ret.status:
                    self.guestMode = True

    def prepareGuestHostItem(self, index):
        ret = False
        try:
            cItem = self.currList[index]
            sts, data = self.helper.getGroupItems(cItem['group_id'])
            if sts:
                item = data[cItem['item_idx']]
                if host := self.getHost(cItem['host']):
                    ret = host.setInitFavouriteItem(item)
                    if RetStatus.OK == ret.status:
                        ret = True
        except Exception:
            printExc()
        return ret

    def getCurrentGuestHost(self):
        return self.host

    def getCurrentGuestHostName(self):
        return self.host.hostName

    def getItemHashData(self, index, displayItem:DisplayItem):
        if self.isGuestMode():
            hostName = str(self.getCurrentGuestHostName())
        else:
            hostName = str(self.getHostNameFromItem(index))

        ret = None
        if hostName:
            # prepare item hash
            hashAlg = MD5()
            hashData = f'{displayItem.title}_{displayItem.type}_{hostName}'
            if displayItem.urlItems:
                hashData =  f'{hashData}_{displayItem.urlItems[0].url}'
            hashData = hexlify(hashAlg(hashData))
            return (hostName, hashData)
        return ret

    def isItemWatched(self, index, displayItem):
        ret = self.getItemHashData(index, displayItem)
        if ret is not None:
            return fileExists(GetFavouritesDir(self.GetWatchedHashFilePath(ret)))
        return False

    def fixWatchedFlag(self, ret):
        if self.useWatchedFlag:
            # check watched flag from hash
            for idx, elm in enumerate(ret.value):
                if elm.type in [DisplayItemType.VIDEO, DisplayItemType.AUDIO] and not elm.isWatched:
                    elm.isWatched = self.isItemWatched(idx, elm)
            self.cachedRet = ret
        return ret

    def _createViewedFile(self, hashData):
        if hashData is not None and mkdirs(GetFavouritesDir('IPTVWatched') + f'/{hashData[0]}/'):
            flagFilePath = GetFavouritesDir(self.GetWatchedHashFilePath(hashData))
            if touch(flagFilePath):
                return True
        return False

    def markItemAsViewed(self, Index=0):
        retCode = RetStatus.ERROR
        retlist = []
        if self.useWatchedFlag:
            ret = self.cachedRet
            if not ret.value[Index].isWatched and ret.value[Index].type in [DisplayItemType.VIDEO, DisplayItemType.AUDIO]:
                hashData = self.getItemHashData(Index, ret.value[Index])
                if self._createViewedFile(hashData):
                    self.cachedRet.value[Index].isWatched = True
                    retCode = RetStatus.OK
                    retlist = ['refresh']
                    self.refreshAfterWatchedFlagChange = True
        return RetHost(retCode, value=retlist)

    def getCustomActions(self, Index=0):
        retCode = RetStatus.ERROR
        retlist = []
        if self.useWatchedFlag:
            ret = self.cachedRet
            if ret.value[Index].type in [DisplayItemType.VIDEO, DisplayItemType.AUDIO]:
                if tmp:= self.getItemHashData(Index, ret.value[Index]):
                    if self.cachedRet.value[Index].isWatched:
                        params = IPTVChoiceBoxItem(_('Unset watched'), "", {'action': 'unset_watched_flag', 'item_index': Index, 'hash_data': tmp})
                    else:
                        params = IPTVChoiceBoxItem(_('Set watched'), "", {'action': 'set_watched_flag', 'item_index': Index, 'hash_data': tmp})
                    retlist.append(params)
                retCode = RetStatus.OK
        return RetHost(retCode, value=retlist)

    def performCustomAction(self, privateData):
        retCode = RetStatus.ERROR
        retlist = []
        if self.useWatchedFlag:
            hashData = privateData['hash_data']
            Index = privateData['item_index']
            if privateData['action'] == 'unset_watched_flag':
                flagFilePath = GetFavouritesDir(self.GetWatchedHashFilePath(hashData))
                if rm(flagFilePath):
                    self.cachedRet.value[Index].isWatched = False
                    retCode = RetStatus.OK
            elif privateData['action'] == 'set_watched_flag':
                if self._createViewedFile(hashData):
                    self.cachedRet.value[Index].isWatched = True
                    retCode = RetStatus.OK

            if retCode == RetStatus.OK:
                self.refreshAfterWatchedFlagChange = True
                retlist = ['refresh']

        return RetHost(retCode, value=retlist)

    def GetWatchedHashFilePath(self, hashData):
        return f'IPTVWatched/{hashData[0]}/.{hashData[1].decode()}.iptvhash'


    def getLinksForVideo(self, Index=0, item=None):
        if self.isGuestMode():
            return self.getCurrentGuestHost().getLinksForVideo(Index)

        listLen = len(self.currList)
        if listLen < Index and listLen > 0:
            printDBG(f"ERROR getCustomLinksForVideo - current list is to short len: {listLen}, Index: {Index}")
            return RetHost(RetStatus.ERROR, value=[])

        if self.currList[Index]["type"] not in ['audio', 'video', 'picture']:
            printDBG("ERROR getCustomLinksForVideo - current item has wrong type")
            return RetHost(RetStatus.ERROR, value=[])
        return self._getLinksForVideo(self.currList[Index])

    def getResolvedURL(self, url):
        if self.isGuestMode():
            return self.getCurrentGuestHost().getResolvedURL(url)
        return self._getResolvedURL(url)

    def getListForItem(self, Index=0, refresh=0, selItem=None):
        guestIndex = Index
        ret = RetHost(RetStatus.ERROR, value=[])
        if not self.isGuestMode():
            ret = super().getListForItem(Index, refresh)
            guestIndex = 0
        if self.isGuestMode():
            ret = self.getCurrentGuestHost().getListForItem(guestIndex, refresh)
            for elm in ret.value:
                elm.isGoodForFavourites = False

        self.fixWatchedFlag(ret)
        return ret

    def getPrevList(self, refresh=0):
        ret = RetHost(RetStatus.ERROR, value=[])
        if not self.isGuestMode() or len(self.getCurrentGuestHost().listOfprevList) <= 1:
            if self.isGuestMode():
                self.clearGuestMode()
            ret = super().getPrevList(refresh)
        else:
            ret = self.getCurrentGuestHost().getPrevList(refresh)
            for elm in ret.value:
                elm.isGoodForFavourites = False
        self.fixWatchedFlag(ret)
        return ret

    def getCurrentList(self, refresh=0):
        if refresh == 1 and self.refreshAfterWatchedFlagChange and self.cachedRet is not None:
            ret = self.cachedRet
        else:
            ret = RetHost(RetStatus.ERROR, value=[])
            if self.isGuestMode():
                ret = self.getCurrentGuestHost().getCurrentList(refresh)
                for elm in ret.value:
                    elm.isGoodForFavourites = False
            else:
                ret = super().getCurrentList(refresh)
            self.fixWatchedFlag(ret)

        self.refreshAfterWatchedFlagChange = False
        return ret

    def getMoreForItem(self, Index=0):
        ret = RetHost(RetStatus.ERROR, value=[])
        if self.isGuestMode():
            ret = self.getCurrentGuestHost().getMoreForItem(Index)
            for elm in ret.value:
                elm.isGoodForFavourites = False
        else:
            ret = super().getMoreForItem(Index)

        self.fixWatchedFlag(ret)
        return ret

    def getCustomArticleContent(self, Index=0):
        retCode = RetStatus.ERROR
        retlist = []
        guestIndex = Index
        callQuestHost = True
        if not self.isGuestMode():
            callQuestHost = self.prepareGuestHostItem(Index)
            guestIndex = 0

        if callQuestHost:
            return self.getCurrentGuestHost().getCustomArticleContent(guestIndex)
        return RetHost(retCode, value=retlist)

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)

    def getCustomLinksForVideo(self, cItem):
        return super().getCustomLinksForVideo(cItem)

    def listSearchResult(self, cItem, searchPattern, searchType):
        return super().listSearchResult(cItem, searchPattern, searchType)