# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, DisplayItem, DisplayItemType, RetHost, UrlItem, ArticleContent, RetStatus, handleServiceDecorator
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, GetLogoDir
###################################################

###################################################
# FOREIGN import
###################################################
import re
import urllib.request
import urllib.parse
import urllib.error
try:
    import json
except Exception:
    import simplejson as json
###################################################


def gettytul():
    return 'https://www.filmahd.cc/'


class IPTVHost(HostBase):
    MAIN_URL = 'https://www.filmahd.cc/'
    SRCH_URL = MAIN_URL + '?s='
    DEFAULT_ICON_URL = 'https://www.filmahd.cc/wp-content/uploads/2019/12/filmahdfinallogo.png'

    MAIN_TV_SERIES_URL = 'https://seriale.filmahd.cc/'
    DEFAULT_TV_SERIES_ICON_URL = 'http://seriale.filmahd.cc/wp-content/uploads/2019/12/serieshdlogos.png'

    MAIN_CAT_TAB = [{'category': 'movies', 'title': _('Movies'), 'url': MAIN_URL, 'icon': DEFAULT_ICON_URL},
                    {'category': 'series', 'title': _('TV Series'), 'url': MAIN_TV_SERIES_URL, 'icon': DEFAULT_TV_SERIES_ICON_URL},
                    {'category': 'search', 'title': _('Search'), 'search_item': True, 'icon': DEFAULT_ICON_URL},
                    {'category': 'search_history', 'title': _('Search history'), 'icon': DEFAULT_ICON_URL}
                   ]

    def __init__(self):
        super().__init__( {'history': 'Filma24hdCom', 'cookie': 'Filma24hdCom.cookie'}, True, [DisplayItemType.VIDEO, DisplayItemType.AUDIO])
        self.seriesSubCategoryCache = []

    def _getFullUrl(self, url, series=False):
        if not series:
            mainUrl = self.MAIN_URL
        else:
            mainUrl = self.S_MAIN_URL
        if url.startswith('/'):
            url = url[1:]
        if 0 < len(url) and not url.startswith('http'):
            url = mainUrl + url
        if not mainUrl.startswith('https://'):
            url = url.replace('https://', 'http://')
        m1 = 'www.seriale.'
        if m1 in url:
            url = url.replace(m1, 'seriale.')
        return url

    def listMoviesCategory(self, cItem, nextCategory):
        printDBG("Filma24hdCom.listMoviesCategory")
        sts, data = self.cm.getPage(cItem['url'])
        if not sts:
            return

        data = ph.getDataBetweenMarkers(data, '<!-- Menu Starts -->', '<!-- Menu Ends -->', False)[1]
        data = ph.getAllItemsBetweenMarkers(data, '<a ', '</a>')
        for item in data:
            title = ph.cleanHtml(item)
            url = self._getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)["']''', 1, True)[0])
            if url == '' or 'seriale' in url:
                continue
            params = dict(cItem)
            params.update({'category': nextCategory, 'url': self._getFullUrl(url), 'title': title})
            self.addDir(params)

    def listSeriesCategory(self, cItem, nextCategory):
        printDBG("Filma24hdCom.listSeriesCategory")
        self.seriesSubCategoryCache = []
        sts, data = self.cm.getPage(cItem['url'])
        if not sts:
            return

        marker = '<ul class="sub-menu">'
        data = ph.getDataBetweenMarkers(data, '<div id="main-nav">', '<!-- end #main-nav -->', False)[1]
        data = data.split('</ul>')
        for item in data:
            if marker not in item:
                continue
            tmp = item.split(marker)
            subCategoryTitle = ph.rgetDataBetweenMarkers(tmp[0], '<a', '</a>')[1]
            subCategoryUrl = ph.getSearchGroups(subCategoryTitle, '''href=['"]([^"^']+?)["']''', 1, True)[0]
            subCategoryUrl = self._getFullUrl(subCategoryUrl)
            subCategoryTitle = ph.cleanHtml(subCategoryTitle)

            subItemsTab = []
            subItems = ph.getAllItemsBetweenMarkers(tmp[1], '<a ', '</a>')
            for subItem in subItems:
                title = ph.cleanHtml(subItem)
                url = ph.getSearchGroups(subItem, '''href=['"]([^"^']+?)["']''', 1, True)[0]
                subItemsTab.append({'title': title, 'url': self._getFullUrl(url)})

            if len(subItemsTab):
                params = dict(cItem)
                params.update({'category': nextCategory, 'sub_idx': len(self.seriesSubCategoryCache), 'url': self._getFullUrl(subCategoryUrl), 'title': subCategoryTitle})
                self.addDir(params)
                self.seriesSubCategoryCache.append(subItemsTab)

    def listsTab(self, tab, cItem, type='dir'):
        printDBG("Filma24hdCom.listsTab")
        for item in tab:
            params = dict(cItem)
            params.update(item)
            params['name'] = 'category'
            if type == 'dir':
                self.addDir(params)
            else:
                self.addVideo(params)

    def listItems(self, cItem, category=''):
        printDBG("Filma24hdCom.listItems")
        url = cItem['url']
        if '?' in url:
            post = url.split('?')
            url = post[0]
            post = post[1]
        else:
            post = ''
        page = cItem.get('page', 1)
        if page > 1:
            url += '/page/%d/' % page
        if post != '':
            url += '?' + post

        sts, data = self.cm.getPage(url)
        if not sts:
            return

        nextPage = False
        if ("/page/%d/" % (page + 1)) in data:
            nextPage = True

        if 'seriale.' in url:
            data = ph.getAllItemsBetweenMarkers(data, '<div id="post-', '<!-- end')
            serieItem = True
        else:
            data = ph.getAllItemsBetweenMarkers(data, '<!-- Post Starts -->', '<!-- Post Ends -->')
            serieItem = False
        for item in data:
            if serieItem:
                item = item.split('<!-- end')[0]

            url = ph.getSearchGroups(item, 'href="([^"]+?)"')[0]
            if url == '':
                continue
            icon = ph.getSearchGroups(item, 'src="([^"]+?)"')[0]
            title = ph.getDataBetweenMarkers(item, '<h2', '</h2>')[1]
            desc = ph.cleanHtml(item.split('<p class="entry-meta">')[-1])

            params = dict(cItem)
            params.update({'title': ph.cleanHtml(title), 'url': self._getFullUrl(url), 'desc': desc, 'icon': self._getFullUrl(icon)})
            self.addVideo(params)

        if nextPage:
            params = dict(cItem)
            params.update({'title': _('Next page'), 'page': page + 1})
            self.addDir(params)

    def listSeasons(self, cItem, nextCategory):
        printDBG("Filma24hdCom.listSeasons [%s]" % cItem)
        idx = cItem.get('sub_idx', -1)
        if idx < 0 or idx >= len(self.seriesSubCategoryCache):
            return

        tab = self.seriesSubCategoryCache[idx]
        params = dict(cItem)
        params['category'] = nextCategory
        self.listsTab(tab, params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        searchPattern = urllib.parse.quote_plus(searchPattern)
        cItem = dict(cItem)
        cItem['url'] = self.SRCH_URL + urllib.parse.quote_plus(searchPattern)
        self.listItems(cItem)

    def getCustomLinksForVideo(self, cItem):
        printDBG("Filma24hdCom.getCustomLinksForVideo [%s]" % cItem)
        urlTab = []
        url = cItem['url']

        sts, data = self.cm.getPage(url)
        if not sts:
            return []

        videoUrl = ph.getSearchGroups(data, '''<iframe[^>]+?src=['"](http[^"^']+?)['"]''', 1, True)[0]
        if 1 == self.up.checkHostSupport(videoUrl):
            urlTab.append({'name': self.up.getHostName(videoUrl), 'url': videoUrl, 'need_resolve': 1})

        videoUrlTab = ph.getDataBetweenMarkers(data, '<video ', '</video>', caseSensitive=False)[1]
        videoUrlTab = re.compile('''<source[^>]*?src=['"](http[^"]+?)['"][^>]*?mp4[^>]*?''', re.IGNORECASE).findall(videoUrlTab)
        for idx in range(len(videoUrlTab)):
            urlTab.append({'name': 'direct %d' % (idx + 1), 'url': videoUrlTab[idx], 'need_resolve': 0})

        videoUrlTab = ph.getDataBetweenMarkers(data, '<tbody>', '</tbody>', caseSensitive=False)[1]
        videoUrlTab += ph.getDataBetweenMarkers(data, '<map ', '</map>', caseSensitive=False)[1]
        videoUrlTab = re.compile('''<a[^>]*?href=['"](http[^"]+?)['"]''', re.IGNORECASE).findall(videoUrlTab)
        for item in videoUrlTab:
            if 1 != self.up.checkHostSupport(item):
                continue
            urlTab.append({'name': self.up.getHostName(item), 'url': item, 'need_resolve': 1})
        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        printDBG("Filma24hdCom.getCustomVideoLinks [%s]" % videoUrl)
        return self.up.getVideoLinkExt(videoUrl)

    def getFavouriteData(self, cItem):
        return cItem['url']

    def getCustomLinksForFavourite(self, fav_data):
        return self.getCustomLinksForVideo({'url': fav_data})

    def getCustomArticleContent(self, cItem):
        printDBG("Filma24hdCom.getCustomArticleContent [%s]" % cItem)
        retTab = []

        sts, data = self.cm.getPage(cItem['url'])
        if not sts:
            return retTab

        m1 = '<span style="color: #00'
        if m1 not in data:
            m1 = '<div class="entry-content'
        m2 = '<p>&nbsp;</p>'
        if m2 not in data:
            m2 = '<tbody>'
        desc = ph.getDataBetweenMarkers(data, m1, m2)[1]
        desc = ph.cleanHtml(desc)

        icon = cItem.get('icon', '')
        title = cItem.get('title', '')
        otherInfo = {}

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc), 'images': [{'title': '', 'url': self._getFullUrl(icon)}], 'other_info': otherInfo}]

    @handleServiceDecorator#
    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))

    #MAIN MENU
        if name is None:
            self.listsTab(self.MAIN_CAT_TAB, {'name': 'category'})
    #MOVIES
        elif category == 'movies':
            self.listMoviesCategory(self.currItem, 'list_items')
        elif category == 'list_items':
            self.listItems(self.currItem)
    #TVSERIES
        elif category == 'series':
            params = dict(self.currItem)
            params.update({'category': 'list_items', 'title': _('--All--')})
            self.addDir(params)
            self.listSeriesCategory(self.currItem, 'list_seasons')
        elif category == 'list_seasons':
            self.listSeasons(self.currItem, 'list_items')
    #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

    def getLogoPath(self):
        return RetHost(RetStatus.OK, value=[GetLogoDir('filma24hdcomlogo.png')])

    def getCustomLinksForVideo(self, Index=0, selItem=None):
        retCode = RetStatus.ERROR
        retlist = []
        if not self.isValidIndex(Index):
            return RetHost(retCode, value=retlist)

        urlList = self.getCustomLinksForVideo(self.currList[Index])
        for item in urlList:
            retlist.append(UrlItem(item["name"], item["url"], item['need_resolve']))

        return RetHost(RetStatus.OK, value=retlist)
    # end getCustomLinksForVideo

    def getResolvedURL(self, url):
        # resolve url to get direct url to video file
        retlist = []
        urlList = self.getCustomVideoLinks(url)
        for item in urlList:
            need_resolve = 0
            retlist.append(UrlItem(item["name"], item["url"], need_resolve))

        return RetHost(RetStatus.OK, value=retlist)

    def converItem(self, cItem):
        searchTypesOptions = [] # ustawione alfabetycznie

        hostLinks = []
        type = DisplayItemType.UNKNOWN
        possibleTypesOfSearch = None

        if 'category' == cItem['type']:
            if cItem.get('search_item', False):
                type = DisplayItemType.SEARCH
                possibleTypesOfSearch = searchTypesOptions
            else:
                type = DisplayItemType.CATEGORY
        elif cItem['type'] == 'video':
            type = DisplayItemType.VIDEO
        elif 'more' == cItem['type']:
            type = DisplayItemType.MORE
        elif 'audio' == cItem['type']:
            type = DisplayItemType.AUDIO

        if type in [DisplayItemType.AUDIO, DisplayItemType.VIDEO]:
            url = cItem.get('url', '')
            if '' != url:
                hostLinks.append(UrlItem("Link", url, 1))

        title = cItem.get('title', '')
        description = cItem.get('desc', '')
        icon = cItem.get('icon', '')

        return DisplayItem(title=title,
                                    description=description,
                                    type=type,
                                    urlItems=hostLinks,
                                    urlSeparateRequest=1,
                                    iconimage=icon,
                                    possibleTypesOfSearch=possibleTypesOfSearch)
    # end converItem

    def setSearchPattern(self):
        try:
            list = self.getCurrList()
            if 'history' == list[self.currIndex]['name']:
                pattern = list[self.currIndex]['title']
                search_type = list[self.currIndex]['search_type']
                self.history.addHistoryItem(pattern, search_type)
                self.searchPattern = pattern
                self.searchType = search_type
        except Exception:
            printDBG('setSearchPattern EXCEPTION')
            self.searchPattern = ''
            self.searchType = ''
