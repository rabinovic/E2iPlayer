# -*- coding: utf-8 -*-


from typing import Dict
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG
from Components.config import config, ConfigYesNo, getConfigListEntry

import urllib
import re

try:
    import json
except Exception:
    import simplejson as json

config.plugins.iptvplayer.netzkinopin = ConfigYesNo(default=True)
config.plugins.iptvplayer.netzkino_adult = ConfigYesNo(default=False)


def GetConfigList():
    optionList = []
    optionList.append(getConfigListEntry(_("Pin protection for plugin") + " :", config.plugins.iptvplayer.netzkinopin))
    optionList.append(getConfigListEntry(_("Show adult stuff") + " :", config.plugins.iptvplayer.netzkino_adult))
    return optionList


def gettytul():
    return 'https://www.netzkino.de/'


class IPTVHost(HostBase):
    def __init__(self):
        super().__init__( {'history': 'netzkino.de', 'cookie': 'netzkino.de.cookie'})
        self.DEFAULT_ICON_URL = 'http://pmd.bilder.netzkino.de/bilder/webseite/logo_header_small.png'

        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'
        self.HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html'}
        self.AJAX_HEADER = dict(self.HEADER)
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.MAIN_URL = 'https://www.netzkino.de/'
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True,
                              'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def mainMenu(self, cItem: Dict):

        self.MAIN_CAT_TAB = [
            {'category': 'listItems', 'title': 'neu-frontpage'},
            {'category': 'listItems', 'title': 'kinderkino'},
            {'category': 'listItems', 'title': 'themenkino-frontpage'},
            {'category':'listItems', 'title':'frontpage-exklusiv-frontpage'},
            # {'category': 'listItems', 'title': 'netzkinoplus-highlights'},
            {'category': 'listItems', 'title': 'empfehlungen_woche-frontpage'},
            {'category': 'listItems', 'title': 'highlights-frontpage'},
            {'category': 'listItems', 'title': 'beste-bewertung-frontpage'},
            {'category': 'listItems', 'title': 'meisgesehene_filme-frontpage'},
            {'category': 'listItems', 'title': 'filme_mit_auszeichnungen-frontpage'},
            {'category': 'listItems', 'title': 'letzte-chance'},
            {'category': 'listItems', 'title': 'top-20-frontpage'},
            {'category': 'listItems', 'title': 'beliebte-animes'},
            # {'category':'listItems', 'title':'frontpage-kurzfilme'},
            # {'category': 'listItems', 'title': 'starkino-frontpage'},
            {'category': 'listItems', 'title': 'hdkino'},
            {'category': 'listItems', 'title': 'animekino'},
            {'category': 'listItems', 'title': 'actionkino'},
            {'category': 'listItems', 'title': 'dramakino'},
            {'category': 'listItems', 'title': 'thrillerkino'},
            {'category': 'listItems', 'title': 'scifikino'},
            {'category': 'listItems', 'title': 'arthousekino'},
            # {'category':'listItems', 'title':'queerkino'},
            {'category': 'listItems', 'title': 'spasskino'},
            {'category': 'listItems', 'title': 'asiakino'},
            {'category': 'listItems', 'title': 'horrorkino'},
            {'category': 'search', 'title': 'Suche', 'search_item': True, },
            {'category': 'search_history', 'title': 'Search history', },
            {'category': 'listItems', 'title': 'liebesfilmkino'},
            # {'category':'listItems', 'title':'prickelkino'},
            {'category': 'listItems', 'title': 'kinoab18'}
        ]
        if not config.plugins.iptvplayer.netzkino_adult.value:
            cat = self.MAIN_CAT_TAB[:-3]
        else:
            cat = self.MAIN_CAT_TAB

        self.listsTab(cat, {'name': 'category'})

    def listItems(self, cItem):
        cat = cItem['title']
        url = "http://api.netzkino.de.simplecache.net/capi-2.0a/categories/%s.json?d=www&l=de-DE&v=v1.2.0" % cat
        if cat == 'hdkino':
            url = url.split('?')[0]
        sts, data = self.getPage(url)
        if not sts: return

        self._getVideos(data, cItem)

    def listSearchResult(self, cItem, searchPattern, searchType):
        url = "http://api.netzkino.de.simplecache.net/capi-2.0a/search?q=%s&d=www&l=de-DE&v=v1.2.0" % urllib.quote(
            searchPattern)
        sts, data = self.getPage(url)
        if not sts: return
        self._getVideos(data, cItem)

    def _stripAllTags(self, html):
        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', html.replace('\n', ''))
        return cleantext

    def _getVideos(self, data, cItem):
        json_data = json.loads(data)
        for node in json_data["posts"]:
            if 'Streaming' in node["custom_fields"]:
                Title = node["title"]
                printDBG('node["content"] %s' % node["content"])
                Inhalt = self._stripAllTags(node["content"])
                if 'thumbnail' in node:
                    Image = node["thumbnail"]
                else:
                    Image = node["custom_fields"]["Artikelbild"][0]
                Stream = node["custom_fields"]["Streaming"][0]
                IMDb = node["custom_fields"]["IMDb-Bewertung"][0]
                FSK = node["custom_fields"]["FSK"][0]
                Jahr = node["custom_fields"]["Jahr"][0]
                if 'Regisseur' in node["custom_fields"]:
                    Regie = node["custom_fields"]["Regisseur"][0]
                else:
                    Regie = "---"
                if 'Stars' in node["custom_fields"]:
                    Stars = node["custom_fields"]["Stars"][0]
                else:
                    Stars = "---"
                Descr = "Jahr: " + Jahr + "\nIMDb-Rating: " + IMDb + "\nFSK: " + FSK + "\nRegie: " + Regie + "\nStars: " + Stars
                Url = "http://pmd.netzkino-seite.netzkino.de/%s.mp4" % Stream
                params = dict(cItem)
                params.update(
                    {'good_for_fav': True, 'category': '', 'title': Title, 'url': Url, 'icon': Image, 'desc': Descr,
                     'inhalt': Inhalt, 'jahr': Jahr, 'imdb': IMDb, 'Stars': Stars, 'Regie': Regie})
                self.addVideo(params)


    def getCustomArticleContent(self, cItem):
        otherInfo = {}
        otherInfo['genre'] = cItem['category']
        otherInfo['actors'] = cItem['Stars']
        otherInfo['director'] = cItem['Regie']
        otherInfo['released'] = cItem['jahr']
        otherInfo['imdb_rating'] = cItem['imdb']

        '''
        otherInfo['duration'] = tmp
        otherInfo['creators'] = ', '.join(tmpTab)
        otherInfo['rating'] = tmp
        otherInfo['views'] = tmp
        '''

        title = cItem['title']
        desc = cItem.get('inhalt', '')
        icon = cItem.get('icon', self.DEFAULT_ICON_URL)

        return [{'title': title, 'text': desc, 'images': [{'title': '', 'url': icon}], 'other_info': otherInfo}]

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}: addParams = dict(self.defaultParams)
        return self.cm.getPage(baseUrl, addParams, post_data)

    def getCustomLinksForVideo(self, cItem):
        linksTab = []
        linksTab.append({'name': cItem['title'], 'url': cItem['url'], 'need_resolve': 0})
        return linksTab

    def isProtectedByPinCode(self):
        return config.plugins.iptvplayer.netzkinopin.value

    def withArticleContent(self, cItem):
        if 'video' == cItem.get('type', '') or 'explore_item' == cItem.get('category', ''):
            return True
        return False

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)
