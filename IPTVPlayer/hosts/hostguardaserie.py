﻿# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getBaseUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, MergeDicts
###################################################

###################################################
# FOREIGN import
###################################################
import re
import urllib.request
import urllib.parse
import urllib.error
try:
    import json
except Exception:
    import simplejson as json
###################################################


def gettytul():
    return 'https://www.guardaserie.support/'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'guardaserie.support', 'cookie': 'guardaserie.support.cookie'})

        self.USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36'
        self.MAIN_URL = 'https://www.guardaserie.support/'

        self.HEADER = {'User-Agent': self.USER_AGENT, 'Accept': 'text/html', 'Accept-Encoding': 'gzip', 'Referer': self.MAIN_URL}
        self.AJAX_HEADER = MergeDicts(self.HEADER, {'X-Requested-With': 'XMLHttpRequest', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'})

        self.DEFAULT_ICON_URL = 'https://cdnimg.guardaserie.support/wp-content/themes/guardaserie/images/logogd.png'

        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPageCF(self, baseUrl, params={}, post_data=None):
        if params == {}:
            params = self.defaultParams
        params['cloudflare_params'] = {'domain': 'guardaserie.review', 'cookie_file': self.COOKIE_FILE, 'User-Agent': self.USER_AGENT}
        return self.cm.getPageCFProtection(baseUrl, params, post_data)

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)
        return self.cm.getPage(baseUrl, addParams, post_data)

    def listMainMenu(self, cItem):
        printDBG("GuardaSerieClick.listMainMenu")
        params = MergeDicts(self.defaultParams, {'user-agent': self.USER_AGENT, 'referer': self.MAIN_URL, "accept-encoding": "gzip", "accept": "text/html"})

        sts, data = self.getPageCF(self.getMainUrl(), params)
        if not sts:
            return
        self.setMainUrl(self.cm.meta['url'])

        #item = ph.getDataBetweenNodes(data, ('<a', '>', 'lista-serie'), ('</a', '>'))[1]
        #title = ph.cleanHtml(item)
        #url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
        params = dict(cItem)
        #params.update({'name':'category', 'category':'sections', 'title':title, 'url':url})
        params.update({'name': 'category', 'category': 'sections', 'title': 'LISTA SERIE', 'url': self.getFullUrl('/lista-serie-tv')})

        self.addDir(params)

        data = ph.getDataBetweenNodes(data, ('<a', '>', 'dropdown'), ('</ul', '>'))[1].split('<ul', 1)
        sTitle = ph.cleanHtml(data[0])
        subtItems = []
        data = ph.getAllItemsBetweenMarkers(data[-1], '<a', '</a>')
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = ph.cleanHtml(item)
            params = dict(cItem)
            params.update({'name': 'category', 'category': 'list_items', 'title': title, 'url': url})
            subtItems.append(params)

        params = dict(cItem)
        params.update({'name': 'category', 'category': 'sub_items', 'sub_items': subtItems, 'title': sTitle})
        self.addDir(params)

        MAIN_CAT_TAB = [{'category': 'search', 'title': _('Search'), 'search_item': True},
                        {'category': 'search_history', 'title': _('Search history')}]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def getSeriesItems(self, cItem, nextCategory, rawItems):
        printDBG("GuardaSerieClick.listItems")
        items = []
        for item in rawItems:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            icon = self.getFullIconUrl(ph.getSearchGroups(item, '''<img[^>]+?src=['"]([^"^']+?)['"]''')[0])

            title = ph.cleanHtml(ph.getDataBetweenMarkers(item, '<p', '</p>')[1])
            if title == '':
                continue
            desc = []
            tmp = ph.getAllItemsBetweenMarkers(item, '<span', '</span>')
            for t in tmp:
                t = ph.cleanHtml(t)
                if t != '' and 'star' not in t:
                    desc.append(t)
            try:
                item = ph.getSearchGroups(item, '''star\s*?s([0-5][^'^"]*?)['"]''')[0].split('_', 1)
                star = str(int(item[0]))
                if 'half' in item[-1]:
                    star += '.5'
                else:
                    star += '.0'
                desc.append(star)
            except Exception:
                printExc()
            params = dict(cItem)
            params.update({'name': 'category', 'category': nextCategory, 'good_for_fav': True, 'title': title, 'url': url, 'icon': icon, 'desc': ' | '.join(desc)})
            items.append(params)
        return items

    def listSections(self, cItem, nextCategory):
        printDBG("GuardaSerieClick.listItems")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        self.setMainUrl(self.cm.meta['url'])

        marker = 'container-fluid greybg followMeBar containerTopBarTitle'
        data = ph.getDataBetweenMarkers(data, marker, 'container-foote', False)[1].split(marker)
        for sData in data:
            sTitle = ph.cleanHtml(ph.getDataBetweenMarkers(sData, '<h2', '</h2>')[1])
            subtItems = self.getSeriesItems(cItem, nextCategory, ph.getAllItemsBetweenMarkers(sData, '<a', '</a>'))
            if len(subtItems):
                params = dict(cItem)
                params.update({'name': 'category', 'category': 'sub_items', 'sub_items': subtItems, 'title': sTitle})
                self.addDir(params)

    def listItems(self, cItem, nextCategory):
        printDBG("GuardaSerieClick.listItems")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        self.setMainUrl(self.cm.meta['url'])

        data = ph.getDataBetweenMarkers(data, 'box-container', 'container-foote', False)[1]
        self.currList = self.getSeriesItems(cItem, nextCategory, ph.getAllItemsBetweenMarkers(data, '<a', '</a>'))

    def exploreItem(self, cItem):
        printDBG("GuardaSerieClick.exploreItem")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        self.setMainUrl(self.cm.meta['url'])

        cItem = dict(cItem)
        cItem['prev_url'] = cItem['url']

        reObj = re.compile('''meta\-embed[0-9]*?=['"]([^'^"]+?)['"]''')
        marker = 'stagioni row-stagione-'
        data = ph.getDataBetweenNodes(data, ('<div', '>', marker), ('<div', '>', 'container'), False)[1].split(marker)
        for sData in data:
            sTitle = ph.cleanHtml(ph.getDataBetweenMarkers(sData, '<h2', '</h2>')[1])
            subtItems = []
            data = ph.getAllItemsBetweenMarkers(sData, '<a', '</a>')
            for item in data:
                title = '%s - %s' % (cItem['title'], ph.cleanHtml(item.split('<p', 1)[0]))
                icon = ph.getSearchGroups(item, '''<img[^>]+?src=['"]([^"^']+?)['"]''')[0]
                if icon == '':
                    icon = ph.getSearchGroups(item, '''<img[^>]+?data\-original=['"]([^"^']+?)['"]''')[0]

                desc = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<p', '>', 'desc'), ('</p', '>'))[1])
                season = ph.getSearchGroups(item, '''meta\-stag=['"]([^"^']+?)['"]''')[0]
                episode = ph.getSearchGroups(item, '''meta\-ep=['"]([^"^']+?)['"]''')[0]
                params = dict(cItem)
                params.update({'type': 'video', 'good_for_fav': False, 'title': title, 'urls': reObj.findall(item), 'url': cItem['url'] + '?s={0}&e={1}'.format(season, episode), 'desc': desc, 'icon': self.getFullIconUrl(icon)})
                subtItems.append(params)

            params = dict(cItem)
            params.update({'name': 'category', 'category': 'sub_items', 'sub_items': subtItems, 'good_for_fav': False, 'title': sTitle})
            self.addDir(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("GuardaSerieClick.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)
        cItem['url'] = self.getFullUrl('/?s=') + urllib.parse.quote_plus(searchPattern)
        cItem['category'] = 'list_items'
        self.listItems(cItem, 'explore_item')

    def getCustomLinksForVideo(self, cItem):
        printDBG("GuardaSerieClick.getCustomLinksForVideo [%s]" % cItem)
        urlsTab = []
        for item in cItem['urls']:
            urlsTab.append({'name': getBaseUrl(item, True), 'url': self.getFullUrl(item), 'need_resolve': 1})
        return urlsTab

    def getCustomVideoLinks(self, videoUrl):
        printDBG("GuardaSerieClick.getCustomVideoLinks [%s]" % videoUrl)
        if 0 == self.up.checkHostSupport(videoUrl):
            url = ''
            sts, data = self.cm.getPage(videoUrl)
            if sts:
                url = self.getFullUrl(ph.getSearchGroups(data, '''<iframe[^>]+?src=['"]([^"^']+?)['"]''', 1, True)[0])
            if url == '':
                videoUrl = 'http://www.safersurf.org/browse.php?u={0}&b=8&f=norefer'.format(urllib.parse.quote_plus(videoUrl, ''))
                params = dict(self.defaultParams)
                params['header'] = MergeDicts(params['header'], {'Referer': videoUrl})
                sts, data = self.cm.getPage(videoUrl, params)
                if sts:
                    printDBG(data)
                    url = self.getFullUrl(ph.getSearchGroups(data, '''<iframe[^>]+?src=['"]([^"^']+?)['"]''', 1, True)[0])
                    url = urllib.parse.unquote(ph.getDataBetweenMarkers(url, '?u=', '&', False)[1])

            if url != '':
                videoUrl = url
        return self.up.getVideoLinkExt(videoUrl)

    def getCustomArticleContent(self, cItem):
        printDBG("GuardaSerieClick.getCustomVideoLinks [%s]" % cItem)
        retTab = []
        itemsList = []

        if 'prev_url' in cItem:
            url = cItem['prev_url']
        else:
            url = cItem['url']

        sts, data = self.cm.getPage(url)
        if not sts:
            return

        data = ph.getDataBetweenNodes(data, ('<div', '>', 'container-title-single'), ('<input', '>'), False)[1]
        icon = self.getFullUrl(ph.getSearchGroups(data, '''<img[^>]+?src=['"]([^'^"]+?)['"]''')[0])
        if icon != '':
            icon = icon + "|cf"

        title = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<h', '>'), ('</h', '>'), False)[1])
        desc = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<span', '>', 'desc'), ('</span', '>'), False)[1])

        try:
            tmp = ph.getSearchGroups(data, '''star\s*?s([0-5][^'^"]*?)['"]''')[0].split('_', 1)
            star = str(int(tmp[0]))
            if 'half' in tmp[-1]:
                star += '.5'
            else:
                star += '.0'
            if star != '':
                itemsList.append((_('RATING'), star))
        except Exception:
            printExc()

        tmp = ph.getAllItemsBetweenNodes(data, ('<p', '>', 'details'), ('</p', '>'), False)
        for item in tmp:
            item = item.split('</b>', 1)
            if len(item) < 2:
                continue
            key = ph.cleanHtml(item[0])
            val = ph.cleanHtml(item[1])
            if key == '' or val == '':
                continue
            itemsList.append((key, val))

        if title == '':
            title = cItem['title']
        if icon == '':
            icon = cItem.get('icon', self.DEFAULT_ICON_URL)
        if desc == '':
            desc = cItem.get('desc', '')

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc), 'images': [{'title': '', 'url': self.getFullUrl(icon)}], 'other_info': {'custom_items_list': itemsList}}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        HostBase.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        mode = self.currItem.get("mode", '')

        printDBG("handleService: || name[%s], category[%s] " % (name, category))
        self.currList = []
        self.currItem = dict(self.currItem)
        self.currItem.pop('good_for_fav', None)

    #MAIN MENU
        if name is None:
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'list_items':
            self.listItems(self.currItem, 'explore_item')
        elif category == 'sub_items':
            self.currList = self.currItem.get('sub_items', [])
        elif category == 'sections':
            self.listSections(self.currItem, 'explore_item')

        elif category == 'explore_item':
            self.exploreItem(self.currItem)
        elif category == 'list_abc':
            self.listABC(self.currItem, 'list_abc_items')
        elif category == 'list_abc_items':
            self.listABCItems(self.currItem, 'explore_item')
    #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        HostBase.endHandleService(self, refresh)

    def withArticleContent(self, cItem):
        if 'prev_url' in cItem or cItem.get('category', '') == 'explore_item':
            return True
        return False
