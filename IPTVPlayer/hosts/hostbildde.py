# -*- coding: utf-8 -*-

from collections import defaultdict
import re

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs import ph

from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import getDirectM3U8Playlist
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta

from Components.config import config, getConfigListEntry, ConfigYesNo


config.plugins.iptvplayer.Bildde_pin = ConfigYesNo(default=True)
config.plugins.iptvplayer.Bildde_choosequality = ConfigYesNo(default=True)

def GetConfigList():
    optionList = [
            getConfigListEntry(_("Choose Quality manualy")+ " :", config.plugins.iptvplayer.Bildde_choosequality),
            getConfigListEntry(_("Pin protection for plugin") + " :", config.plugins.iptvplayer.Bildde_pin)
    ]
    return optionList

def gettytul():
    return 'https://www.bild.de'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__({'history': 'bild.de'})

        self.DEFAULT_ICON = 'https://bilder.bild.de/fotos/bild-logo-35166394/Bild/45.bild.png'
        self.MAIN_URL = 'https://www.bild.de/video/mediathek/video/bild-live-71144736.bild.html'

        self.cached_categories = defaultdict(list)

    def mainMenu(self, cItem):
        sts, data = self.cm.getPage(self.MAIN_URL, self.defaultParams)
        if not sts:
            return

        sections = re.findall('<section class="block block--.+?">(<h2.+?)</section>', data, re.S)
        for section in sections:
            if title := re.search('<h2.*?><a.*?>(.*?)</span>', section, re.S)[1]:
                title = ph.cleanHtml(title.strip())

            if data1 := re.findall('<div class="article-gallery-item article-gallery-item--slider">(.+?)</article>', section, re.S):
                for elm in data1:
                    if 'teaser__image__premium' in elm:
                        continue
                    link = re.search('a href="([^"]+?)"', elm, re.S)[1]
                    img = re.search('src="([^"]+?)"',elm , re.S)
                    img = img[1] if img else ''
                    name = ph.cleanHtml(ph.getDataBetweenMarkers(elm, ('<span', '>', 'class="teaser__title__headline"'), '</span>', False)[1])
                    desc = re.search('<span class="teaser__title__kicker">(.*?)</span>', elm, re.S)
                    desc = desc[1] if desc else ''

                    self.cached_categories[title].append({'title': name, 'icon': img, 'url': link, 'desc':desc})
                self.addDir({'category': 'listItems', 'title': title, 'icon': self.DEFAULT_ICON})

    def listItems(self, cItem):
        printDBG("bildde.listItems citem [%s]" % cItem)

        if cached := self.cached_categories.get(cItem.get('title', ''), None):
            for elm in cached:
                self.addVideo(elm|{ 'need_resolve': 1, 'good_for_fav': True})

    def getCustomLinksForVideo(self, cItem):
        printDBG("bildde.getCustomLinksForVideo cItem [%s]" % cItem)
        urlTab = []

        url = cItem['url']

        sts, data = self.cm.getPage(self.getFullUrl(url), self.defaultParams)
        if not sts:
            return

        if m3u8 := re.search('hls:"([^"]+?)"', data, re.S):
            m3u8 = m3u8[1].replace('\\u002F', '/')
            url = strwithmeta(m3u8, {'Referer': self.MAIN_URL, 'User-Agent': self.USER_AGENT})
            if config.plugins.iptvplayer.Bildde_choosequality.value:
                urlTab.extend(getDirectM3U8Playlist(url, sortWithMaxBitrate=99999999))
            else:
                urlTab.append({'name': 'm3u8', 'url': url, 'need_resolve': 0})

        if mp4:= re.search('progressive":\[\{"url":"([^"]+?)"', data, re.S):
            mp4 = mp4[1].replace('\\u002F', '/')
            urlTab.append({'name': 'mp4', 'url': mp4, 'need_resolve': 0})

        return urlTab


    def isProtectedByPinCode(self):
        return config.plugins.iptvplayer.Bildde_pin.value

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)

    def listSearchResult(self, cItem, searchPattern, searchType):
        return super().listSearchResult(cItem, searchPattern, searchType)
