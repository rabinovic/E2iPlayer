# -*- coding: utf-8 -*-

import re

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.components.recaptcha_v2helper import CaptchaHelper

from Components.config import config, ConfigText
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import GetIPTVSleep

def gettytul():
    return 'Akwam'

class IPTVHost(HostBase, CaptchaHelper):

    def __init__(self):
        super().__init__( {'history': 'akowam', 'cookie': 'akwam.cookie'})
        self.MAIN_URL = 'https://ak.sv/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/0qgtD2Z/akwam.png'
        self.HEADER = {'User-Agent': self.USER_AGENT, 'DNT':'1', 'Accept': 'text/html', 'Accept-Encoding':'gzip, deflate','Referer':self.getMainUrl(), 'Origin':self.getMainUrl()}

        self.defaultParams = {'header':self.HEADER,'no_redirection':True,'with_metadata':True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams = {}, post_data = None):
        baseUrl=ph.std_url(baseUrl)
        if addParams == {}: addParams = dict(self.defaultParams)
        sts,data = self.cm.getPage(baseUrl, addParams, post_data)
        #printDBG(str(data.meta))
        code = data.meta.get('status_code','')
        while ((code == 302) or (code == 301)):
            new_url = data.meta.get('location','')
            if not new_url.startswith('http'):
                new_url = self.MAIN_URL + new_url
            new_url=ph.std_url(new_url)
            sts,data = self.cm.getPage(new_url, addParams, post_data)
            code = data.meta.get('status_code','')
            printDBG(str(data.meta))
        return sts, data


    def mainMenu(self, cItem):
        printDBG("Akwam.listMainMenu")

        MAIN_CAT_TAB = [
            {'category': 'listCatItems','cat':'movie', 'title': 'أفلام', 'icon': self.DEFAULT_ICON_URL, 'url': self.MAIN_URL + '/movies'},
            {'category': 'listCatItems','cat':'serie', 'title': 'مسلسلات', 'icon': self.DEFAULT_ICON_URL, 'url': self.MAIN_URL + '/series'},
            {'category': 'listCatItems', 'cat':'tvshow','title': 'تلفزيون', 'icon': self.DEFAULT_ICON_URL, 'url': self.MAIN_URL + '/shows'},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history')}, ]
        self.listsTab(MAIN_CAT_TAB, cItem)


    def listCatItems(self, cItem):
        printDBG(f"Akwam.listCatItems cItem[{cItem}]")
        category = self.currItem.get("cat", '')

        if category == 'movie':
            NEW_CAT_TAB = [
                {'category': 'showitms', 'title': _('أفلام أجنبية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=30')},
                {'category': 'showitms', 'title': _('أفلام عربية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=29')},
                {'category': 'showitms', 'title': _('أفلام أسيوية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=33')},
                {'category': 'showitms', 'title': _('أفلام تركية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=32')},
                {'category': 'showitms', 'title': _('أفلام هندية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=31')},
                {'category': 'showitms', 'title': _('أفلام مدبلجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=0&category=71&rating=0&year=0&language=0&formats=0&quality=0')},
                {'category': 'showitms', 'title': _('أفلام كرتون'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?category=30')},
                {'category': 'showitms', 'title': _('أفلام وثائقية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?category=28')}]
        elif category == 'serie':
            NEW_CAT_TAB = [
                {'category': 'showitms', 'title': _('رمــضـــان'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=0&category=87&rating=0&year=0&language=0&formats=0&quality=0')},
                {'category': 'showitms', 'title': _('مسلسلات أجنبية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=30')},
                {'category': 'showitms', 'title': _('مسلسلات عربية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=29')},
                {'category': 'showitms', 'title': _('مسلسلات أسيوية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=33')},
                {'category': 'showitms', 'title': _('مسلسلات تركية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=32')},
                {'category': 'showitms', 'title': _('مسلسلات هندية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=31')},
                {'category': 'showitms', 'title': _('مسلسلات انمي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?category=30')},
                {'category': 'showitms', 'title': _('مسلسلات مدبلجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=30&category=71&rating=0&year=0&language=0&formats=0&quality=0')},
                {'category': 'showitms', 'title': _('مسلسلات تركية مدبلجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=32&category=71&rating=0&year=0&language=0&formats=0&quality=0')},
                {'category': 'showitms', 'title': _('مسلسلات هندية مدبلجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=31&category=71&rating=0&year=0&language=0&formats=0&quality=0')},
                {'category': 'showitms', 'title': _('مسلسلات وثائقية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/shows?section=46&category=0&rating=0&year=0&formats=0&quality=0')},]
        elif category == 'tvshow':
            NEW_CAT_TAB = [
                {'category': 'showitms', 'title': _('رمضان'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=0&category=87&rating=0&year=0&language=0&formats=0&quality=0')},
                {'category': 'showitms', 'title': _('برامج تلفزيونية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/shows?section=42')},
                {'category': 'showitms', 'title': _('مصارعة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/shows?section=43&category=0&rating=0&year=0&formats=0&quality=0')},
                {'category': 'showitms', 'title': _('مسرحيات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/shows?section=45')}]

        NEW_CAT_TAB.append({'category': 'showitms', 'title': _('الكل'), 'url': cItem['url'], 'desc': '', 'icon': cItem['icon']})
        NEW_CAT_TAB.append({'category': 'showfilter', 'title': 'By Filter', 'url': cItem['url'], 'desc': '', 'icon': cItem['icon']})
        self.listsTab(NEW_CAT_TAB, cItem)


    def showmenu1(self, cItem):
        cItem=dict(cItem)
        title=cItem['title']
        if title == 'مسلسلات':
            self.addDir({'category': 'showitms', 'title': 'Ramadan 2023',
                         'url': self.MAIN_URL + '/series?section=0&category=87&rating=0&year=2023&language=1&formats=0&quality=0',
                         'icon': cItem['icon'], 'type': 'category', 'desc': ''})
        self.addDir({'category': 'showitms', 'title': _('الكل'), 'url': cItem['url'], 'desc': '', 'icon': cItem['icon']})
        self.addDir({'category': 'showfilter', 'title': 'By Filter', 'url': cItem['url'], 'desc': '', 'icon': cItem['icon']})


    def showfilter(self, cItem):
        count = cItem.get('count', 0)
        data = cItem.get('data', [])
        total = cItem.get('total', 0)
        url = cItem.get('url', '')
        car = '&'
        if count == 0:
            car = '?'
            sts, data0 = self.getPage(ph.std_url(url))
            if sts:
                films_list = re.findall('<select.*?name="(.*?)".*?value="(.*?)".*?>(.*?)<(.*?)</select>', data0, re.S)
                if films_list:
                    data = films_list
                    total = len(data)
        if data != []:
            name = data[count][0]
            title = data[count][2]
            elms = data[count][3]
            self.addMarker({'title':title, 'icon':self.DEFAULT_ICON_URL})
            op_list = re.findall('<option(.*?)>(.*?)</option>', elms, re.S)
            cat = 'showfilter'
            printDBG('count=' + str(count))
            printDBG('total=' + str(total))
            if count + 1 == total:
                cat = 'showitms'
            self.addDir(
                {'count': count + 1, 'data': data, 'category': cat, 'url': url + car + name + '=0', 'title': _('الكل'),
                 'desc': '', 'icon': cItem['icon'], 'hst': 'tshost', 'total': total})
            for (val, option) in op_list:
                val_list = re.findall('"(.*?)"', val, re.S)
                if val_list:
                    valeur = val_list[0]
                else:
                    valeur = option
                urlo = url + car + name + '=' + valeur
                self.addDir(
                    {'count': count + 1, 'data': data, 'category': cat, 'url': urlo, 'title': option, 'desc': '',
                     'icon': cItem['icon'], 'hst': 'tshost', 'total': total})

    def showmenu2(self, cItem):
        url0 = cItem['url']
        title0 = cItem['title']
        self.addDir(
            {'category': 'showitms', 'title': ' الكل - ' + title0, 'url': url0, 'desc': '', 'icon': cItem['icon']})

        sts, data = self.getPage(ph.std_url(url0))
        if sts:
            lst_data = re.findall('لأقسام الفرعية</span>(.*?)</ul>', data, re.S)
            if lst_data:
                lst_data1 = re.findall('href="(.*?)">(.*?)<', lst_data[0], re.S)
                for (url, title) in lst_data1:
                    self.addDir(
                        {'import': cItem['import'], 'category': 'showitms', 'title': title, 'url': url, 'desc': '', 'icon': cItem['icon'], 'with_mini_cover':True})

    def showitms(self, cItem):
        # printDBG('citem='+str(cItem))
        page = cItem.get('page', 1)
        if page == 1:
            Url = cItem['url']
        else:
            if '?' in cItem['url']:
                Url = cItem['url'] + '&page=' + str(page)
            else:
                Url = cItem['url'] + '?page=' + str(page)
        if Url.startswith('/'):
            Url = self.MAIN_URL + Url
        sts, data = self.getPage(ph.std_url(Url))
        if sts:
            lst_data = re.findall('class="entry-box.*?>(.*?)-src="(.*?)".*?href="(.*?)".*?<h3.*?>(.*?)</h3>', data, re.S)
            count = 0
            for (desc, image, url, title) in lst_data:
                rating = ''
                quality = ''
                lst_inf = re.findall('rating">(.*?)</span>', desc, re.S)
                if lst_inf:
                    rating = ph.cleanHtml(lst_inf[0])
                lst_inf = re.findall('quality">(.*?)</span>', desc, re.S)
                if lst_inf:
                    quality = ph.cleanHtml(lst_inf[0])
                desc = 'Rating: ' + rating + '\n'
                desc = desc + 'Quality: ' + quality + '\n'
                title = ph.cleanHtml(title)
                self.addDir({'category': 'showelms', 'title': title, 'url': url, 'desc': desc, 'icon': image, 'good_for_fav': True, 'with_mini_cover':True})
                count = count + 1
            if count > 1:
                self.addNext({'category': 'showitms', 'title': _('Next page'), 'url': cItem['url'], 'page': page + 1})

    def showelms(self, cItem):
        url0 = cItem['url']
        sts, data = self.getPage(ph.std_url(url0))
        if sts:
            trailer_data = re.findall('youtube.com/watch.v=(.*?)["\']', data, re.S)
            if trailer_data:
                self.addVideo({'title': 'TRAILER', 'url': 'https://www.youtube.com/watch?v=' + trailer_data[0], 'desc': '', 'icon': cItem['icon']})
            if 'id="downloads">' in data:
                self.addVideo({'title': cItem['title'].replace('[New Site] ', ''), 'url': cItem['url'], 'desc': cItem['desc'], 'icon': cItem['icon'], 'good_for_fav': True})
            else:
                lst_data = re.findall('class="bg-primary2.*?href="(.*?)".*?>(.*?)</a>(.*?)<img.*?"(.*?)"', data, re.S)
                if lst_data:
                    for (url, title1, inf, image) in lst_data:
                        self.addVideo({'title': title1, 'url': url, 'desc': ph.cleanHtml(inf), 'icon': image,
                                       'good_for_fav': True})

    def listSearchResult(self, cItem, searchPattern, searchType):
        page = cItem.get('page', None)
        searchPattern = cItem.get('searchPattern', searchPattern)
        url_ = self.MAIN_URL + '/search?q=' + searchPattern + (f'&page={page}' if page else '')
        sts, data = self.getPage(ph.std_url(url_))
        if sts:
            page = re.findall('''<a class="page-link" href="https://akwam.io/search\?q=.*&amp;page=(.*)" rel="next"''',  data, re.S)
            if page:
                page = page[0]
            lst_data = re.findall('class="entry-box.*?>(.*?)data-src="(.*?)".*?href="(.*?)".*?<h3.*?>(.*?)</h3>', data, re.S)
            for (desc, image, url, title) in lst_data:
                rating = ''
                quality = ''
                lst_inf = re.findall('rating">(.*?)</span>', desc, re.S)
                if lst_inf:
                    rating = ph.cleanHtml(lst_inf[0])
                lst_inf = re.findall('quality">(.*?)</span>', desc, re.S)
                if lst_inf:
                    quality = ph.cleanHtml(lst_inf[0])
                desc = 'Rating: ' + rating + '\n'
                desc = desc + 'Quality: ' + quality + '\n'
                title = ph.cleanHtml(title)
                elm = {'category': 'showelms', 'title': title, 'url': url, 'desc': desc, 'icon': image,'good_for_fav': True, 'with_mini_cover':True}
                self.addDir(elm)
            if page:
                elm = {'category': 'search_next_page', 'title':_('Next page'),'page':page, 'searchPattern':searchPattern}
                self.addDir(elm)

    def getCustomLinksForVideo(self, cItem):
        urlTab = []

        URL = cItem['url']
        if cItem['title'].lower() =='trailer':
            return self.up.getVideoLinkExt(URL)

        urlTab = self.cacheLinks.get(URL, [])
        if urlTab == []:
            sts, data = self.getPage(ph.std_url(URL))
            if sts:
                url_dat_1 = re.findall('<ul class="header-tabs(.*?)</ul>', data, re.S)
                if url_dat_1:
                    url_dat_1 = re.findall('<li.*?>(.*?)</li>', url_dat_1[0], re.S)
                url_dat_2 = re.findall('class="tab-content.*?href="(.*?)".*?href="(.*?)"', data, re.S)
                title = '[ '
                for x in url_dat_1:
                    title = title + ph.cleanHtml(x) + ' \ '
                title = title[:-2] + ']'
                if url_dat_2:
                    urlTab.append({'name': '|Watch Server| ' + title, 'url': url_dat_2[0][0], 'need_resolve': 1})
                    i = 0
                    for elm in url_dat_2:
                        title = ph.cleanHtml(url_dat_1[i])
                        urlTab.append({'name': '|Downl Server| ' + title, 'url': elm[1], 'need_resolve': 1})
                        i = i + 1
            self.cacheLinks[str(cItem['url'])] = urlTab
        return urlTab

    def extract_links(self, data, videoUrl):
        urlTab = []
        url_dat = re.findall('<source.*?src=["\'](.*?)["\'].*?size=["\'](.*?)["\']', data, re.S)
        if url_dat:
            for url, size in url_dat:
                URL_ = strwithmeta(url, {'Referer': videoUrl})
                urlTab.append({'name': size + 'P' , 'url': URL_})
        else:
            url_dat = re.findall('class="btn-loader">.*?href="(.*?)"', data, re.S)
            if url_dat:
                URL_ = strwithmeta(url_dat[0], {'Referer': videoUrl})
                urlTab.append({'url':URL_, 'name':''})
        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        urlTab = []
        if True:
            sts, data = self.getPage(videoUrl)
            if sts:
                urlTab = self.extract_links(data,videoUrl)
                if urlTab == []:
                    url_dat=re.findall('<div class="content.*?href="(.*?)"(.*?)(?:<center>|</body>)', data, re.S)
                    if url_dat:
                        url1 = url_dat[0][0]
                        data2 = url_dat[0][1]
                        url_dat2=re.findall('<a href="(.*?)"', data2, re.S)
                        if url_dat2:
                            url1 = url_dat2[-1]
                    if not url_dat:
                        url_dat=re.findall('<h2.*?href="(.*?)"', data, re.S)
                        url1 = url_dat[0]
                    if url_dat:
                        sts, data = self.getPage(url1)
                        if sts:
                            #printDBG('data='+data)
                            try:
                                sitekey = ph.getSearchGroups(data, '''data\-sitekey=['"]([^'^"]+?)['"]''')[0]
                            except:
                                sitekey = ''
                            if sitekey != '':
                                printDBG('sitekey='+sitekey)
                                token, errorMsgTab = self.processCaptcha(sitekey, self.cm.meta['url'])
                                post_data = {}
                                if token != '':
                                    post_data['g-recaptcha-response'] = token
                                    actionUrl = self.MAIN_URL+'/verify'
                                    sts0, data0 = self.getPage(actionUrl, post_data=post_data)
                                    GetIPTVSleep().sleep(1)
                                    sts, data = self.getPage(url1)
                            if sts:
                                urlTab = self.extract_links(data,videoUrl)
        return urlTab

    def getArticle(self, cItem):
        printDBG("Akwam.getCustomVideoLinks [%s]" % cItem)
        otherInfo1 = {}
        title = cItem['title']
        icon = cItem.get('icon', '')
        desc = cItem.get('desc', '')
        sts, data = self.getPage(ph.std_url(cItem['url']))
        if sts:
            lst_dat = re.findall('class="font-size-16 text-white mt-2">(.*?)</span>', data, re.S)
            for elm in lst_dat:
                elm = ph.cleanHtml(elm)
                if (':' in elm) and ('مدة' in elm):
                    otherInfo1['duration'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('اللغة' in elm):
                    otherInfo1['language'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('ترجمة' in elm):
                    otherInfo1['translation'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('جودة' in elm):
                    otherInfo1['quality'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('انتاج' in elm):
                    otherInfo1['production'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('سنة' in elm):
                    otherInfo1['year'] = elm.split(':', 1)[1].strip()

            lst_dat0 = re.findall('class="font-size-16 d-flex align-items-center mt-3">(.*?)</div>', data, re.S)
            if lst_dat0:
                otherInfo1['genre'] = ph.cleanHtml(lst_dat0[0])

            lst_dat0 = re.findall('header-link text-white">.*?<h2>(.*?)</div>', data, re.S)
            if lst_dat0:
                desc = ph.cleanHtml(lst_dat0[0])

        return [{'title': title, 'text': desc, 'images': [{'title': '', 'url': icon}], 'other_info': otherInfo1}]

    def withArticleContent(self, cItem):
        return cItem.get('priv_has_art', False)

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)
