# -*- coding: utf-8 -*-

import urllib.request
import urllib.parse
import urllib.error
import json
import re

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, handleServiceDecorator
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultHeader
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, MergeDicts
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.libs import ph

from Components.config import config, ConfigSelection, getConfigListEntry


def gettytul():
    return 'https://cima-club.io'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'cimaclub.com', 'cookie': 'cima-club.io.cookie'})
        self.USER_AGENT = getDefaultHeader()['User-Agent']
        self.MAIN_URL = 'https://cima-club.io'
        self.DEFAULT_ICON_URL = 'https://i.pinimg.com/originals/f2/67/05/f267052cb0ba96d70dd21e41a20a522e.jpg'
        self.HTTP_HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html', 'Accept-Encoding': 'gzip, deflate', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}
        self.AJAX_HEADER = dict(self.HTTP_HEADER)
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest', 'Accept-Encoding': 'gzip, deflate', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Accept': 'application/json, text/javascript, */*; q=0.01'})

        self.cacheLinks = {}
        self.defaultParams = {'header': self.HTTP_HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        self.MAIN_CAT_TAB = [
                             {'category': 'search', 'title': _('Search'), 'search_item': True},
                             {'category': 'search_history', 'title': _('Search history')},
                            ]
        self.cacheSubSections = {}
        self.cacheMainMenu = []
        self.cacheFilters = {}
        self.cacheFiltersKeys = []
        self.cacheEpisodes = []

    def getPage(self, baseUrl, addParams:dict=None, post_data=None):
        addParams= {} if addParams is None else addParams

        if addParams == {}:
            addParams = dict(self.defaultParams)
        sts, data = self.cm.getPageCFProtection(baseUrl, addParams, post_data)
        userAgent = self.defaultParams.get('header', {}).get('User-Agent', '')
        if data.meta.get('cf_user', self.USER_AGENT) != userAgent:
            self.defaultParams['header']['User-Agent'] = data.meta['cf_user']
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("CimaClubCom.listMainMenu")

        sts, data = self.getPage(self.getMainUrl())
        if not sts:
            return
        self.setMainUrl(data.meta['url'])

        findImage = re.search('<a.*?data-src="([^"]+?)"', data)
        if findImage:
            imageUrl = findImage[1]
            sts, image = self.getPage(imageUrl)
            if not sts:
                return
        self.cacheSubSections = {'sub_1': []}

        # main menu
        tmp = ph.getDataBetweenNodes(data, ('<div', '>', 'main-menu'), ('</div', '>'))[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, '<a', '</a>')
        for item in tmp:
            url = ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0]
            title = ph.cleanHtml(item)
            self.cacheMainMenu.append({'title': title, 'url': url})
        self.addDir({'name': 'category', 'type': 'category', 'category': 'list_sub_main', 'title': _('Main menu')})

        mapUrls = {'toprating': '/top-imdb/', 'featured': '/most-views/'}
        tmp = ph.getAllItemsBetweenNodes(data, ('<li', '>', 'data-filter'), ('</li', '>'))
        for item in tmp:
            url = ph.getSearchGroups(item, '''data\-filter=['"]([^'^"]+?)['"]''')[0]
            url = mapUrls.get(url, f'/wp-content/themes/Cimaclub/filter/{url}.php')
            self.cacheSubSections['sub_1'].append({'title': ph.cleanHtml(item), 'url': self.getFullUrl(url)})

        tmp = ph.getDataBetweenNodes(data, ('<div', '>', 'dataFilter'), ('</ul', '>'))[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, '<li', '</li>')
        self._addSubSection(cItem, 'الاقسام الفرعية', 'sub_1', tmp)

        tmp = ph.getAllItemsBetweenNodes(data, ('<a', '>', '/release-year/'), ('</a', '>'))
        self._addSubSection(cItem, _('By year'), 'by_year', tmp, True)

        self.fillCacheFilters(cItem, data)
        if self.cacheFiltersKeys:
            self.addDir({'name': 'category', 'type': 'category', 'category': 'list_filters', 'title': _('Filters'), 'url': self.getMainUrl()})

        self.listsTab(self.MAIN_CAT_TAB, cItem)

    def _addSubSection(self, cItem, sectionTitle, key, data, revert=False):
        self.cacheSubSections[key] = self.cacheSubSections.get(key, [])
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
            title = ph.cleanHtml(item)
            params = {'title': title, 'url': url}
            if revert:
                self.cacheSubSections[key].insert(0, params)
            else:
                self.cacheSubSections[key].append(params)

        if len(self.cacheSubSections[key]):
            params = dict(cItem)
            params.update({'title': sectionTitle, 'category': 'list_sub_section', 'f_sub_key': key})
            self.addDir(params)

    def listSubMenu(self, cItem, nextCategory):
        printDBG("CimaClubCom.listSubMenu [%s]" % cItem)

        for item in self.cacheMainMenu:
            if item['url'] == '#':
                self.addMarker({'title': item['title']})
                continue
            params = {'name': 'category', 'type': 'category', 'category': 'list_items'}
            params.update(item)
            self.addDir(params)

    def listSubSections(self, cItem, nextCategory):
        printDBG("CimaClubCom.listSubSections [%s]" % cItem)
        key = cItem.get('f_sub_key', '')
        tab = self.cacheSubSections.get(key, [])
        self.listsTab(tab, {'name': 'category', 'type': 'category', 'category': 'list_items'})

    def fillCacheFilters(self, cItem, data):
        printDBG("CimaClubCom.listCategories")
        self.cacheFilters = {}
        self.cacheFiltersKeys = []

        def addFilter(data, marker, baseKey, addAll=True, titleBase=''):
            key = 'f_' + baseKey
            self.cacheFilters[key] = []
            for item in data:
                value = ph.getSearchGroups(item, marker + '''=['"]([^"^']+?)['"]''')[0]
                title = ph.cleanHtml(item)
                if title.lower() in ['all', 'default', 'any']:
                    addAll = False
                self.cacheFilters[key].append({'title': title.title(), key: value})

            if len(self.cacheFilters[key]):
                if addAll:
                    self.cacheFilters[key].insert(0, {'title': _('All')})
                self.cacheFiltersKeys.append(key)

        data0 = ph.getDataBetweenNodes(data, ('<form', '>'), ('</form', '>'))[1]
        self.cacheFilters['operators'] = []
        tmp = ph.getAllItemsBetweenMarkers(data0, '<input', '</li>')
        for item in tmp:
            name = ph.getSearchGroups(item, '''name=['"]([^"^']+?)['"]''')[0]
            value = ph.getSearchGroups(item, '''value=['"]([^"^']+?)['"]''')[0]
            self.cacheFilters['operators'].append({name: value})

        data1 = ph.getAllItemsBetweenNodes(data0, ('<li', '>'), ('</li', '>'))
        for tmp in data1:
            key = ph.getSearchGroups(tmp, '''name=['"]([^"^']+?)['"]''')[0]
            tmp = ph.getAllItemsBetweenMarkers(tmp, '<option', '</option>')
            addFilter(tmp, 'value', key, False)

        printDBG(self.cacheFilters)

    def listFilters(self, cItem, nextCategory):
        printDBG("CimaClubCom.listFilters")
        cItem = dict(cItem)

        f_idx = cItem.get('f_idx', 0)
        if f_idx >= len(self.cacheFiltersKeys):
            return

        filter = self.cacheFiltersKeys[f_idx]
        f_idx += 1
        cItem['f_idx'] = f_idx
        if f_idx == len(self.cacheFiltersKeys):
            cItem['category'] = nextCategory
        self.listsTab(self.cacheFilters.get(filter, []), cItem)

    def listItems(self, cItem, nextCategory):
        printDBG("CimaClubCom.listItems [%s]" % cItem)

        url = self.getFullUrl(cItem['url'].split(self.up.getDomain(cItem['url']))[1])
        sts, data = self.getPage(ph.std_url(url))
        if not sts:
            return

        lst_data=re.findall('class="content-box">.*?href="(.*?)".*?src="(.*?)"(.*?)<h3>(.*?)</h3>', data, re.S)
        for (url1,image,desc0,name_eng) in lst_data:
            desc, name_eng = ph.uniform_title(name_eng)
            cookieHeader = self.cm.getCookieHeader(self.COOKIE_FILE)
            image = strwithmeta(ph.std_url(image), {'Cookie': cookieHeader} | self.HTTP_HEADER )
            params = {'good_for_fav': True, 'category': nextCategory, 'title': name_eng, 'url': ph.std_url(url1), 'icon': image, 'desc': desc, 'with_mini_cover':True}
            self.addDir(params)

        pagination=re.search('<ul class="pagination".*?</ul>', data, re.S)
        if pagination:
            pages=re.findall('<li.*?>.*?<a.*?>.*?</a>.*?</li>', pagination[0], re.S)
            if pages:
                found=False
                for page in pages:
                    if 'active' in page:
                        found=True
                        continue
                    if found:
                        nextPage=re.search('href="([^"]+?)"', page)
                        if nextPage:
                            self.addNext(cItem | {'title': _("Next page"), 'url': nextPage[1]})
                            break


    def exploreItem(self, cItem):
        printDBG("CimaClubCom.exploreItem [%s]" % cItem)
        url0=cItem['url']
        titre=cItem['title']
        sts, data = self.getPage(url0)
        if not sts:
            return

        tr_data=re.findall('TrailerPopup">.*?src="(.*?)"', data, re.S)
        if tr_data:
            if tr_data[0].strip() != 'https://www.youtube.com/embed/':
                params = {'import':cItem['import'],'good_for_fav':True,'category' : 'video','url': tr_data[0],'title':'Trailer','desc':'','icon':cItem['icon'],'hst':'none'}
                self.addVideo(params)
        cat_data=re.findall('(?:<ul class="Seasons|<ul class="Episodes).*?<h2>(.*?)</h2>(.*?)</ul>', data, re.S)
        if cat_data:
            for (titre_,data_) in cat_data:
                self.addMarker({'title':titre_,'icon':cItem['icon'],'desc':''})
                cat_data2=re.findall('<li.*?href="(.*?)".*?>(.*?)</li>', data_, re.S)
                for (url_,titre0_) in cat_data2:
                    if '/season/' in url_:
                        self.addDir({'good_for_fav':True,'category':'host2', 'url':url_,'data_post':'', 'title':ph.cleanHtml(titre0_), 'desc':'', 'icon':cItem['icon'], 'mode':'31','EPG':True,'hst':'tshost', 'with_mini_cover':True} )
                    else:
                        params = {'good_for_fav':True,'category' : 'video','url': url_,'title':ph.cleanHtml(titre0_),'desc':'','icon':cItem['icon'],'hst':'tshost'}
                        self.addVideo(params)
        else:
            cat_data=re.findall('holder-block">.*?<h2(.*?)</h2>(.*?)</div>', data, re.S)
            if cat_data:
                for (titre_,data_) in cat_data:
                    self.addMarker({'title':ph.cleanHtml('<'+titre_),'icon':cItem['icon'],'desc':''})
                    cat_data2=re.findall('href="(.*?)".*?>(.*?)</a>', data_, re.S)
                    for (url_,titre0_) in cat_data2:
                        if '/season/' in url_:
                            self.addDir({'good_for_fav':True,'category':'host2', 'url':url_,'data_post':'', 'title':ph.cleanHtml(titre0_), 'desc':'', 'icon':cItem['icon'], 'mode':'31','EPG':True,'hst':'tshost', 'with_mini_cover':True} )
                        else:
                            params = {'good_for_fav':True,'category' : 'video','url': url_,'title':ph.cleanHtml(titre0_),'desc':'','icon':cItem['icon'],'hst':'tshost'}
                            self.addVideo(params)
            else:
                params = {'good_for_fav':True,'category' : 'video','url': url0,'title':titre,'desc':'','icon':cItem['icon'],'desc':cItem['desc'],'hst':'tshost'}
                self.addVideo(params)


    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("CimaClubCom.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)
        if 1 == cItem.get('page', 1):
            cItem['category'] = 'list_items'
            cItem['url'] = self.getFullUrl('/search?s=') + urllib.parse.quote_plus(searchPattern)
        self.listItems(cItem, 'explore_item')

    def getCustomLinksForVideo(self, cItem):
        printDBG(f"CimaClubCom.getCustomLinksForVideo [{cItem}]")

        urlTab = []
        URL=cItem['url'].replace('/post/','/watch/').replace('/film/','/watch/').replace('/episode/','/watch/')
        sts, data = self.getPage(ph.std_url(URL))
        if sts:
            server_id_data = re.findall('_post_id=(.*?)"', data, re.S)
            if server_id_data:
                server_id = server_id_data[0]
                server_data = re.findall('class="servers-tabs">(.*?)</ul>', data, re.S)
                if server_data:
                    server_data = re.findall('<li.*?data-embedd="(.*?)".*?>(.*?)</li>', server_data[0], re.S)
                    for (id, titre) in server_data:
                        url_ = self.MAIN_URL + '/ajaxCenter?_action=getserver&_post_id=' + server_id + '&serverid=' + id
                        local = ''
                        if 'سيرفر سيما' in titre:
                            titre = 'CimaClub'
                            local = 'local'
                        urlTab.append({'name':ph.cleanHtml(titre), 'url':url_+'|'+cItem['url'], 'need_resolve':1,'type':local})
        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        printDBG("CimaClubCom.getCustomVideoLinks [%s]" % videoUrl)
        urlTab = []
        URL,referer=videoUrl.split('|')
        addParams = {'header':self.AJAX_HEADER, 'with_metadata':True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        sts, data = self.getPage(URL,addParams)
        if sts:
            urlTab = self.up.getVideoLinkExt(data.strip())
        return urlTab

    def getCustomArticleContent(self, cItem, data=None):
        printDBG("CimaClubCom.getCustomArticleContent [%s]" % cItem)

        retTab = []

        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return []

        cUrl = data.meta['url']
        self.setMainUrl(cUrl)

        data = ph.getDataBetweenNodes(data, ('<div', '>', 'CoverIntroMovie'), ('<script', '>'))[1]

        desc = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<div', '>', 'contentFilm'), ('</div', '>'))[1])
        title = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<h1', '>', 'entry-title'), ('</h1', '>'))[1])
        icon = self.getFullIconUrl(ph.getSearchGroups(data, '''\ssrc=['"]([^'^"]+?(:?\.jpe?g|\.png)(:?\?[^'^"]*?)?)['"]''')[0])

        item = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<div', '>', 'views'), ('</div', '>'))[1])
        if item != '':
            otherInfo['rating'] = item

        keysMap = {'quality': 'quality',
                   'category': 'category',
                   'genre': 'genre',
                   'year': 'year',
                   'runtime': 'duration',
                   'datePublished': 'released', }
        data = ph.getAllItemsBetweenNodes(data, ('<span', '>', 'class='), ('</span', '>'))
        printDBG(data)
        for item in data:
            marker = ph.getSearchGroups(item, '''\sitemprop=['"]([^'^"]+?)['"]''')[0]
            if marker == '':
                marker = ph.getSearchGroups(item, '''\sclass=['"]([^'^"]+?)['"]''')[0]
            printDBG(">>> %s" % marker)
            if marker not in keysMap:
                continue
            value = ph.cleanHtml(item)
            printDBG(">>>>> %s" % value)
            if value == '':
                continue
            if marker == 'genre' and '' != ph.getSearchGroups(value, '''([0-9]{4})''')[0]:
                marker = 'year'
            otherInfo[keysMap[marker]] = value

        if title == '':
            title = cItem['title']
        if desc == '':
            desc = cItem.get('desc', '')
        if icon == '':
            icon = cItem.get('icon', self.DEFAULT_ICON_URL)

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc), 'images': [{'title': '', 'url': self.getFullUrl(icon)}], 'other_info': otherInfo}]

    @handleServiceDecorator
    def handleService(self, index, refresh=0, searchPattern='', searchType=''):


        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        mode = self.currItem.get("mode", '')

        printDBG("handleService: |||| name[%s], category[%s] " % (name, category))
        self.cacheLinks = {}

    #MAIN MENU
        if name is None:
            self.listMainMenu({'name': 'category'})
        elif category == 'list_sub_main':
            self.listSubMenu(self.currItem, 'list_items')
        elif category == 'list_sub_section':
            self.listSubSections(self.currItem, 'list_items')
        elif category == 'list_filters':
            self.listFilters(self.currItem, 'list_items')
        elif category == 'list_items':
            self.listItems(self.currItem, 'explore_item')
        elif category == 'explore_item':
            self.exploreItem(self.currItem)
    #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

    def withArticleContent(self, cItem):
        if 'video' == cItem.get('type', '') or 'explore_item' == cItem.get('category', ''):
            return True
        return False
