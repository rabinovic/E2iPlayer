# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, DisplayItem, DisplayItemType
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
###################################################

###################################################
# FOREIGN import
###################################################
import re
import urllib.request
import urllib.parse
import urllib.error
try:
    import json
except Exception:
    import simplejson as json
###################################################


def gettytul():
    return 'http://movienight.ws/'


class IPTVHost(HostBase):
    MAIN_URL = 'http://movienight.ws/'
    SRCH_URL = MAIN_URL + '?s='
    DEFAULT_ICON_URL = 'http://movienight.ws/wp-content/uploads/2017/07/movineight-logo-125-2.png'

    MAIN_CAT_TAB = [{'category': 'list_items', 'title': _('Latest movies'), 'url': MAIN_URL, 'icon': DEFAULT_ICON_URL},
                    {'category': 'movies_genres', 'title': _('Movies genres'), 'filter': 'genres', 'url': MAIN_URL, 'icon': DEFAULT_ICON_URL},
                    {'category': 'movies_genres', 'title': _('Movies by year'), 'filter': 'years', 'url': MAIN_URL, 'icon': DEFAULT_ICON_URL},
                    {'category': 'list_items', 'title': _('TV Series'), 'url': MAIN_URL + 'tvshows/', 'icon': DEFAULT_ICON_URL},
                    {'category': 'search', 'title': _('Search'), 'search_item': True},
                    {'category': 'search_history', 'title': _('Search history')}
                   ]

    def __init__(self):
        super().__init__( {'history': 'MoviesNight', 'cookie': 'MoviesNight.cookie'}, True, [DisplayItemType.VIDEO, DisplayItemType.AUDIO])
        self.movieFiltersCache = {'genres': [], 'years': []}
        self.episodesCache = {}

    def _getFullUrl(self, url, series=False):
        if not series:
            mainUrl = self.MAIN_URL
        else:
            mainUrl = self.S_MAIN_URL
        if url.startswith('/'):
            url = url[1:]
        if 0 < len(url) and not url.startswith('http'):
            url = mainUrl + url
        if not mainUrl.startswith('https://'):
            url = url.replace('https://', 'http://')
        return url

    def _fillFilters(self, url):
        printDBG("MoviesNight._fillFilters")
        cache = {'genres': [], 'years': []}
        sts, data = self.cm.getPage(url)
        if not sts:
            return {}

        tmp = ph.getDataBetweenMarkers(data, '<ul class="scrolling years">', '</ul>', False)[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, '<a ', '</a>')
        for item in tmp:
            title = ph.cleanHtml(item)
            url = self._getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)["']''', 1, True)[0])
            if url.startswith('http'):
                cache['years'].append({'title': title, 'url': url})

        data = ph.getDataBetweenMarkers(data, 'Genre', '</ul>', False)[1]
        data = data.split('</li>')
        for item in data:
            title = ph.cleanHtml(item)
            url = self._getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)["']''', 1, True)[0])
            if url.startswith('http'):
                cache['genres'].append({'title': title, 'url': url})
        return cache

    def fillMovieFilters(self, url):
        printDBG("MoviesNight.fillMovieFilters")
        self.movieFiltersCache = self._fillFilters(url)
        return

    def listMoviesGenres(self, cItem, category):
        printDBG("MoviesNight.listMoviesGenres")
        filter = cItem.get('filter', '')
        if 0 == len(self.movieFiltersCache.get(filter, [])):
            self.fillMovieFilters(cItem['url'])

        cItem = dict(cItem)
        cItem['category'] = category
        #params = dict(cItem)
        #params.update({'title':_('All')})
        #self.addDir(params)
        self.listsTab(self.movieFiltersCache.get(filter, []), cItem)

    def listItems(self, cItem, category='list_seasons'):
        printDBG("MoviesNight.listMovies")
        url = cItem['url']
        if '?' in url:
            post = url.split('?')
            url = post[0]
            post = post[1]
        else:
            post = ''
        page = cItem.get('page', 1)
        if page > 1:
            url += '/page/%d/' % page
        if post != '':
            url += '?' + post

        sts, data = self.cm.getPage(url)
        if not sts:
            return

        nextPage = False
        if ('/page/%d/' % (page + 1)) in data:
            nextPage = True

        m1 = '<div class="movie">'
        data = ph.getDataBetweenMarkers(data, m1, '<div class="footer', False)[1]
        data = data.split(m1)
        if len(data):
            data[-1] = data[-1].split('<div id="paginador">')[0]

        for item in data:
            url = ph.getSearchGroups(item, 'href="([^"]+?)"')[0]
            if url == '':
                continue
            icon = ph.getSearchGroups(item, 'src="([^"]+?)"')[0]
            title = ph.getDataBetweenMarkers(item, '<h2>', '</h2>', False)[1]
            desc = ph.cleanHtml(item)

            params = dict(cItem)
            params.update({'title': ph.cleanHtml(title), 'url': self._getFullUrl(url), 'desc': desc, 'icon': self._getFullUrl(icon), 'good_for_fav': True})
            if '/tvshows/' in url:
                params['category'] = category
                self.addDir(params)
            else:
                self.addVideo(params)

        if nextPage:
            params = dict(cItem)
            params.update({'title': _('Next page'), 'page': page + 1})
            self.addDir(params)

    def listSeasons(self, cItem, category):
        printDBG("MoviesNight.listSeasons [%s]" % cItem)
        self.episodesCache = {}

        sts, data = self.cm.getPage(cItem['url'])
        if not sts:
            return

        data = ph.getDataBetweenReMarkers(data, re.compile('''<div id=['"]cssmenu['"]>'''), re.compile('</div>'), False)[1]
        data = re.split('''<li class=['"]has-sub['"]>''', data)
        if len(data):
            del data[0]

        for item in data:
            if 'No episodes' in item:
                continue
            seasonTitle = ph.cleanHtml(ph.getDataBetweenMarkers(item, '<a ', '</a>')[1])
            printDBG(">>>>>>>>>>>>>>>>>>>>>>>>>>>> " + seasonTitle)

            episodesTab = []
            episodesData = ph.getAllItemsBetweenMarkers(item, '<li', '</li>')
            for eItem in episodesData:
                eTmp = re.split('''<span class=['"]datix['"]>''', eItem)
                title = ph.cleanHtml(eTmp[0])
                desc = ph.cleanHtml(eTmp[-1])
                url = self._getFullUrl(ph.getSearchGroups(eItem, '''href=['"]([^"^']+?)["']''', 1, True)[0])
                if url.startswith('http'):
                    episodesTab.append({'title': '{0} - {1}: {2}'.format(cItem['title'], seasonTitle, title), 'url': url, 'desc': desc})

            if len(episodesTab):
                self.episodesCache[seasonTitle] = episodesTab
                params = dict(cItem)
                params.update({'category': category, 'title': seasonTitle, 'season_key': seasonTitle, 'good_for_fav': False})
                self.addDir(params)

    def listEpisodes(self, cItem):
        printDBG("MoviesNight.listEpisodes [%s]" % cItem)
        seasonKey = cItem.get('season_key', '')
        if '' == seasonKey:
            return
        cItem = dict(cItem)
        self.listsTab(self.episodesCache.get(seasonKey, []), cItem, 'video')

    def listSearchResult(self, cItem, searchPattern, searchType):
        searchPattern = urllib.parse.quote_plus(searchPattern)
        cItem = dict(cItem)
        cItem['url'] = self.SRCH_URL + urllib.parse.quote_plus(searchPattern)
        self.listItems(cItem)

    def getCustomLinksForVideo(self, cItem):
        printDBG("MoviesNight.getCustomLinksForVideo [%s]" % cItem)
        urlTab = []
        url = cItem['url']

        sts, data = self.cm.getPage(url)
        if not sts:
            return []

        data = re.compile('<iframe[^>]+?src="(https?://[^"]+?)"', re.I).findall(data)
        for videoUrl in data:
            urlTab.extend(self.up.getVideoLinkExt(videoUrl))

        return urlTab

    def getFavouriteData(self, cItem):
        printDBG('MoviesNight.getFavouriteData')
        params = {'type': cItem['type'], 'category': cItem.get('category', ''), 'title': cItem['title'], 'url': cItem['url'], 'desc': cItem.get('desc', ''), 'icon': cItem.get('icon', '')}
        return json.dumps(params)

    def getCustomLinksForFavourite(self, fav_data):
        printDBG('MoviesNight.getCustomLinksForFavourite')
        if fav_data.startswith('http://') or fav_data.startswith('https://'):
            return self.getCustomLinksForVideo({'url': fav_data})
        links = []
        try:
            cItem = json.loads(fav_data)
            links = self.getCustomLinksForVideo(cItem)
        except Exception:
            printExc()
        return links

    def setInitListFromFavouriteItem(self, fav_data):
        printDBG('MoviesNight.setInitListFromFavouriteItem')
        try:
            params = json.loads(fav_data)
        except Exception:
            params = {}
            printExc()
        self.addDir(params)
        return True

    def getCustomArticleContent(self, cItem):
        printDBG("MoviesNight.getCustomArticleContent [%s]" % cItem)
        retTab = []

        otherInfo = {}

        url = cItem.get('url', '')

        sts, data = self.cm.getPage(url)
        if not sts:
            return retTab

        m2 = '<div class="tsll">'
        if m2 not in data:
            m2 = ' id="player'

        data = ph.getDataBetweenMarkers(data, '<div class="post">', m2)[1]
        title = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<h1', '</h1>')[1])
        icon = self.getFullIconUrl(ph.getSearchGroups(data, '''<img[^>]+?src=['"]([^"^']+?\.jpe?g[^"^']*?)["']''')[0])
        desc = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<div id="dato-2"', '</p>')[1].split('</h2>')[-1])

        if '/episode/' in url:
            data = ph.getAllItemsBetweenMarkers(data, '<p', '</p>')
            if len(data) > 1:
                title += ' - ' + ph.cleanHtml(data[0])
                desc = ph.cleanHtml(data[1])
        else:
            tmp = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<span class="original">', '</span>')[1])
            if tmp != '':
                otherInfo['alternate_title'] = tmp

            tmp = ph.getSearchGroups(data, '>\s*([12][0-9]{3})\s*<')[0]
            if tmp != '':
                otherInfo['year'] = tmp

            tmp = ph.getSearchGroups(data, '>\s*([0-9]+\s*min)\s*<')[0]
            if tmp != '':
                otherInfo['duration'] = tmp

            tmp = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<div class="imdbdatos">', '</div>')[1])
            if tmp != '':
                otherInfo['rating'] = tmp

            tmp = []
            tmp2 = re.compile('<a[^>]+?rel="category[^>]+?>([^>]+?)<').findall(data)
            for t in tmp2:
                t = ph.cleanHtml(t)
                if t != '':
                    tmp.append(t)
            if len(tmp):
                otherInfo['genres'] = ', '.join(tmp)

        if title == '':
            title = cItem['title']
        if desc == '':
            desc = cItem.get('desc', '')
        if icon == '':
            icon = cItem.get('icon', self.DEFAULT_ICON_URL)

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc), 'images': [{'title': '', 'url': self.getFullUrl(icon)}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        HostBase.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    #MAIN MENU
        if name is None:
            self.listsTab(self.MAIN_CAT_TAB, {'name': 'category'})
    #MOVIES
        elif category == 'movies_genres':
            self.listMoviesGenres(self.currItem, 'list_items')
        elif category == 'list_items':
            self.listItems(self.currItem)
    #TVSERIES
        elif category == 'list_seasons':
            self.listSeasons(self.currItem, 'list_episodes')
        elif category == 'list_episodes':
            self.listEpisodes(self.currItem)
    #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        HostBase.endHandleService(self, refresh)

    def withArticleContent(self, cItem):
        if (cItem['type'] == 'video') or cItem.get('category', '') in ['list_seasons', 'list_episodes']:
            return True
        return False

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)
