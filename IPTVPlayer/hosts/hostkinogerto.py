# -*- coding: utf-8 -*-

from typing import List
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Components.config import ConfigText, config, ConfigYesNo, getConfigListEntry

from Plugins.Extensions.IPTVPlayer.libs import ph

import re
from urllib.parse import quote as urllib_quote

config.plugins.iptvplayer.Kinogerto_pin = ConfigYesNo(default=True)

DEFAULT_ICON = 'https://kinoger.to/templates/kinoger/images/logo.png'


def GetConfigList():
    optionList = []
    optionList.append(
        getConfigListEntry(_("Pin protection for plugin") + " :", config.plugins.iptvplayer.Kinogerto_pin))
    return optionList


def gettytul():
    return 'https://kinoger.to/'


class IPTVHost(HostBase):

    def __init__(self):

        super().__init__({'history': 'kinoger.to', 'cookie': 'kinoger.to.cookie'})

        self.MAIN_URL = 'https://kinoger.to'

        self.USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.79 Safari/537.36'
        self.HTTP_HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html', 'Accept-Encoding': 'gzip, deflate', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}

        self.defaultParams = {'header': self.HTTP_HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)
        baseUrl = self.cm.iriToUri(baseUrl)
        sts, data = self.cm.getPageCFProtection(baseUrl, addParams, post_data)
        userAgent = self.defaultParams.get('header', {}).get('User-Agent', '')
        if data.meta.get('cf_user', self.USER_AGENT) != userAgent:
            self.defaultParams['header']['User-Agent'] = data.meta['cf_user']
        return sts, data

    def getFullIconUrl(self, url):
        printDBG((f"getFullIconUrl filmpalast {url}"))
        url = self.getFullUrl(url)
        if url == '': return ''
        cookieHeader = self.cm.getCookieHeader(self.COOKIE_FILE)
        cf_header =  {'Cookie': cookieHeader, 'User-Agent': self.USER_AGENT}
        printDBG((f"Filmpalast getFullIconUrl cf_header: {cf_header}"))
        return strwithmeta(url, cf_header)

    def mainMenu(self, cItem):
        sts, data = self.getPage(self.MAIN_URL)
        if not sts:
            return
        printDBG("kinogerto.buildMainCatTab [data] %s" % data)

        cats = [{'icon':  self.getFullIconUrl(DEFAULT_ICON), 'category': 'search', 'title': _('Search'), 'search_item': True},
                     {'icon':  self.getFullIconUrl(DEFAULT_ICON), 'category': 'search_history', 'title': _('Search history')}]

        items = ph.getAllItemsBetweenMarkers(data, ('div', 'class="sidelinks"'), '/div>', False)
        for i in items:
            for k in ph.getAllItemsBetweenMarkers(i, ('<li', 'class="links"'), '/li>', False):
                href = ph.getSearchGroups(k, 'href="([^"]+?)"')[0]
                name = ph.getSearchGroups(k, '>([^>]+?)</[ba]')[0].strip()
                cats.append(
                    {'category': 'listItems', 'url': self.MAIN_URL + href, 'title': name, 'icon': DEFAULT_ICON})

        self.listsTab(cats ,cItem)

    def listItems(self, cItem):
        printDBG("kinogerto.listItems [citem] %s" % cItem)

        url = cItem['url']
        page = cItem.get('page', 1)

        if page > 1:
            url = url.replace("main", "stream")
            url += '/page/' + str(page) + '/'

        sts, data = self.getPage(url)
        if not sts:
            return
        printDBG("kinogerto.listItems [data] %s" % data)

        nextPage = ph.getSearchGroups(data, '''<a.*>(vorwärts \+).*?</a>''')[0]

        nodes = ph.getAllItemsBetweenNodes(data, ('<div', '>', 'short'), ('</div></div></div', '>'))

        if nodes:
            for node in nodes:
                title = ph.cleanHtml(ph.getSearchGroups(node, '''<div class="title">\W.*<a.*?>(.*?)</a>''')[0])
                link = ph.getSearchGroups(node, '''<div class=\"begin\">.*href="(.*?)"''')[0]
                icon =  self.getFullIconUrl(ph.getSearchGroups(node, '''<img src=\"([^\"]+?\.jpg)\"''')[0])
                beschreibung = ph.cleanHtml(ph.getDataBetweenNodes(node, ("<div", '>', 'text-align:right;'), ('<div', '>', 'footercontrol'), withNodes=False)[1])

                type = ph.getSearchGroups(node, '''<div style="text-align:right;">(<b>)*(.*?)<''', grupsNum=2)[1]
                nextCategory = ''

                regexp = re.compile(r'S\d+')
                if regexp.search(type):
                    nextCategory = 'listSeasons'

                params = dict(cItem)
                params.update({'category': nextCategory, 'url': link, 'title': title, 'icon': icon, 'good_for_fav': True, 'desc': beschreibung, 'cat': 'movie', 'referer': cItem['url'], 'with_mini_cover':True})

                if regexp.search(type):
                    self.addDir(params)
                else:
                    self.addVideo(params)

            if nextPage:
                params = dict(cItem)
                params.update({'good_for_fav': False, 'title': _("Next page"), 'page': page + 1})
                self.addNext(params)

    def listSeasons(self, cItem):
        printDBG("kinogerto.listSeasons [citem] %s" % cItem)

        url = cItem['url']

        sts, data = self.getPage(url)
        if not sts: return

        seasons = {}
        all_seasons_episodes = re.compile("\.show\(\d+,\[\['(.*)'\]\]\)").findall(data)

        for hoster in all_seasons_episodes:
            s = 1
            for season in hoster.split('],['):
                if s not in seasons:
                    seasons[s] = {}
                e = 1
                for episod in season.split("','"):
                    if e not in seasons[s]:
                        seasons[s][e] = []
                    seasons[s][e].append(episod.strip('"').strip())
                    e += 1
                s += 1
        # {1:{1:[a,b,c], 2:[a,b,c]},}

        i = 1
        for ep in seasons.values():
            params = dict(cItem)
            params.update({'category': 'listEpisodes', 'url': ep, 'title': ('Staffel %s' % i), 'icon': cItem['icon'], 'good_for_fav': True, 'cat': 'serie', 'referer': url, 'with_mini_cover':True})
            i += 1
            self.addDir(params)

    def listEpisodes(self, cItem):
        printDBG("kinogerto.listEpisodes [citem] %s" % cItem)

        nextCategory = ''

        url = cItem['url']
        referer = cItem['referer']
        if isinstance(url, dict):
            printDBG("kinogerto.listEpisodes [url] %s" % url)
            j = 1
            for epis in url:
                params = dict(cItem)

                params.update({'category': nextCategory,'url':"".join(url[epis]), 'url_':  url[epis], 'title': ('Folge %s' % j), 'icon': cItem['icon'], 'good_for_fav': True, 'referer': referer})
                self.addVideo(params)
                j += 1

    def getCustomLinksForVideo(self, cItem):
        printDBG("kinogerto.getCustomLinksForVideo [%s]" % cItem)
        urlTab = []

        if cItem['cat'] == 'serie':
            url = cItem['referer']
            items = cItem['url_']
            for item in items:
                if item:
                    urlParams = {'referer': url, 'need_resolve': 1}
                    if cItem['cat'] == 'serie':
                        urlParams['url'] = strwithmeta(item.strip("'"), {'Referer': url})
                        urlParams['name'] = self.up.getDomain(item)
                        urlTab.append(urlParams)
        else:
            url = cItem['url']
            sts, data = self.getPage(url)
            if not sts: return

            hosters = re.compile(r"\.show.*'([^']+?)'\]").findall(data)
            for hoster in hosters:
                urlParams = {'referer': url, 'need_resolve': 1}
                urlParams['url'] = strwithmeta(hoster, {'Referer': url})
                urlParams['name'] = self.up.getDomain(hoster)
                urlTab.append(urlParams)

        return urlTab

    def getCustomVideoLinks(self, url):
        printDBG(f"kinogerto.getCustomVideoLinks [{url}]")
        url = strwithmeta(url)
        return self.up.getVideoLinkExt(url)

    def getCustomArticleContent(self, cItem) -> List:
        return super().getCustomArticleContent(cItem)

    def listSearchResult(self, cItem, searchPattern, searchType):

        page=cItem.get('page', 1)

        url = f'https://kinoger.to/?do=search&subaction=search&search_start={page}&titleonly=3&story={urllib_quote(searchPattern)}&x=1&y=10&submit=submit'
        sts, data = self.getPage(url)
        if not sts:
            return

        results = re.findall('<div class="titlecontrol">.*?<a href="([^"]+?)">(.*?)<.*?img src="([^"]+?)".*?<div class="footercontrol">', data, re.S)
        for r in results:
            url = r[0]
            title = r[1]
            icon = r[2]
            desc = ""

            serie = 'Serie' in r
            params = dict(cItem)

            params.update({'category': 'listSeasons','url': url, 'title': title, 'icon': icon, 'good_for_fav':True, 'desc': desc, 'cat': 'movie', 'with_mini_cover':True})

            if serie:
                self.addDir(params)
            else:
                self.addVideo(params)

        nextPage = re.search('''<a.*>(vorwärts \+).*?</a>''', data, re.S)
        if nextPage:
            page += 1
            params = dict(cItem)
            params.update({'category': 'search_next_page', 'page':page})
            self.addNext(params)

    def isProtectedByPinCode(self):
        return config.plugins.iptvplayer.Kinogerto_pin.value

    def withArticleContent(self, cItem):
        if 'video' == cItem.get('type', '') or 'explore_item' == cItem.get('category', ''):
            return True
        return False
