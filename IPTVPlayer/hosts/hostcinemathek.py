# -*- coding: utf-8 -*-

import re
import json

from urllib.parse import quote, quote_plus
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase

from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def gettytul():
    return 'Cinemathek.net'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__({'history': 'Cinemathek.net','cookie': 'Cinemathek.cookie'})

        self.DEFAULT_ICON = 'https://cinemathek.net/wp-content/uploads/2023/05/logo_production_neu.png'
        self.MAIN_URL = 'https://Cinemathek.net/'

        self.USER_AGENT = getDefaultUserAgent()
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Referer':self.MAIN_URL}
        self.defaultParams = {'header':self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams=None, post_data = None):
        if addParams is None:
            addParams = dict(self.defaultParams)

        return self.cm.getPage(baseUrl, addParams, post_data)

    def mainMenu(self, cItem):
        printDBG(f"cinemathek.listMainMenu cItem [{cItem}]")

        MAIN_CAT_TAB = [
                        {'category':'listItems', 'title': 'Filme', 'url':self.MAIN_URL+'/filme', 'icon':self.DEFAULT_ICON_URL},
                        {'category':'listItems', 'title': 'Serien', 'url':self.MAIN_URL+'/serien', 'icon':self.DEFAULT_ICON_URL},
                        {'category':'listItems', 'title': 'Episoden', 'url':self.MAIN_URL+'/episoden', 'icon':self.DEFAULT_ICON_URL},
                        {'category':'search','title': _('Search'), 'search_item':True, 'icon':self.DEFAULT_ICON_URL},
                        {'category': 'search_history', 'title': _('Search history'), },
                    ]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        url = cItem['url']
        sts, data = self.getPage(url)
        if sts:
            pass

        if maches := re.findall('''<article.*?<img src="([^"]+?)" alt="([^"]+?)".*?<a href="([^"]+?)".*?<span>(.*?)</span>.*?class="imdb">(.*?)<.*?class="texto">(.*?)<.*?</article>''', data, re.S):
            for match in maches:
                icon  = match[0]
                title = match[1]
                url = match[2]
                jahr = match[3]
                imdb = match[4]
                desc = jahr + ' '+ imdb + ' ' + match[5]
                param = cItem | { 'title' : title, 'url': url, 'icon': icon,  'desc': desc}
                self.addVideo(param)

        if match := re.search('''<link rel="next" href="([^"]+?)" />''', data, re.S):
            nextPage = match[1]

            param = cItem | {'page': nextPage}
            self.addNext(param)

    def listSeasons(self, cItem):
        page = cItem['page']
        url = cItem['url'] + str(page)
        sts, data = self.getPage(url)
        if sts:
            pass

        json_data = json.loads(data)

        seasons = json_data['seasons']['data']

        for s in seasons:
            id = s['title_id'] # ID ändert sich !!!
            seasonNr = str(s['number'])
            icon = s['poster']
            desc = s['release_date']
            title = f'Staffel {seasonNr}'
            addParam = dict(cItem)
            self.addDir(addParam | {'category':'listEpisodes', 'title':title, 'icon':icon, 'desc':desc, 'id': id,'seasonNr': seasonNr, 'good_for_fav': True, 'with_mini_cover':True})


    def listEpisodes(self, cItem):
        id = cItem['id']
        seasonNr = cItem['seasonNr']
        url = f'{self.MAIN_URL}/api/v1/titles/{id}/seasons/{seasonNr}?load=episodes,primaryVideo'
        sts, data = self.getPage(url)
        if sts:
            pass

        json_data = json.loads(data)
        episodes = json_data['episodes']['data']

        for e in episodes:
            episode_number = str(e['episode_number']) # Episoden Nummer
            icon = e['poster'] # Episoden Poster

            title = f"Folge {episode_number} - {e['name']}" # Episoden Titel
            url = self.MAIN_URL + 'api/v1/titles/%s/seasons/%s/episodes/%s?load=videos,compactCredits,primaryVideo' % (id, seasonNr, episode_number)
            params = dict(cItem)
            self.addVideo(params | {'title':title, 'icon':icon, 'url':url})

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("cineclix.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)
        cItem['url'] = self.MAIN_URL + '?s=' + quote(searchPattern)

        self.listItems(cItem)

    def getCustomLinksForVideo(self, cItem):
        printDBG("cinemathek.getCustomLinksForVideo cItem [%s]" % cItem)
        url = cItem['url']

        if urlTab := self.cacheLinks.get(url,[]):
            return self.cacheLinks[url]

        sts, data = self.getPage(url)
        if sts:
            pass

        quality = '720'
        if qualityMatch := re.search(r">Release:.*?\s*(\d\d\d+)p", data, re.S):
            Quality = qualityMatch[1]

        if match := re.findall(r'''player-option-\d.*?type=['"]([^"']+).*?post=['"](\d+).*?nume=['"](\d).*?starten!\s([^<]+)''', data, re.S):
            for sType, sID, sLink, sName in match:
                sUrl = 'https://cinemathek.net/wp-json/dooplayer/v2/%s/%s/%s' % (sID, sType, sLink)
                urlTab.append({'name':sName, 'url':sUrl, 'need_resolve': 1})

        if urlTab:
            self.cacheLinks[cItem['url']]= urlTab

        return urlTab

    def getCustomVideoLinks(self, url):
        printDBG("cinemathek.getCustomVideoLinks [%s]" % url)
        sts, data = self.getPage(url)
        if sts:
            pass
        json_data = json.loads(data)
        return self.up.getVideoLinkExt(json_data['embed_url'])

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)
