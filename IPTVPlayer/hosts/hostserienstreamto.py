# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
import json
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _, SetIPTVPlayerLastHostError
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.components.recaptcha_v2helper import CaptchaHelper
from Plugins.Extensions.IPTVPlayer.libs.pCommon import clearCookie, isValidUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, GetCookieDir, MergeDicts, ReadTextFile, WriteTextFile, GetTmpDir, rm
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.libs import ph

from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import hex_md5
###################################################

###################################################
# FOREIGN import
###################################################
import re
from copy import deepcopy
from Components.config import config, ConfigSelection, ConfigText, getConfigListEntry
from functools import cmp_to_key
###################################################


###################################################
# E2 GUI COMMPONENTS
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvmultipleinputbox import IPTVMultipleInputBox
from Screens.MessageBox import MessageBox
###################################################

###################################################
# Config options for HOST
###################################################
config.plugins.iptvplayer.serienstreamto_langpreference = ConfigSelection(default="de,de_sub,en", choices=[("de,de_sub,en", "de,sub,en"),
                                                                                                               ("de,en,de_sub", "de,en,sub"),
                                                                                                               ("de_sub,de,en", "sub,de,en"),
                                                                                                               ("de_sub,en,de", "sub,en,de"),
                                                                                                               ("en,de_sub,de", "en,sub,de"),
                                                                                                               ("en,de,de_sub", "en,de,sub")])
config.plugins.iptvplayer.serienstreamto_login = ConfigText(default="", fixed_size=False)
config.plugins.iptvplayer.serienstreamto_password = ConfigText(default="", fixed_size=False)


def GetConfigList():
    optionList = []
    optionList.append(getConfigListEntry(_("Your language preference:"), config.plugins.iptvplayer.serienstreamto_langpreference))

    optionList.append(getConfigListEntry(_("e-mail") + ":", config.plugins.iptvplayer.serienstreamto_login))
    optionList.append(getConfigListEntry(_("password") + ":", config.plugins.iptvplayer.serienstreamto_password))
    return optionList
###################################################


def gettytul():
    return 'Serienstream.to'


class IPTVHost(HostBase, CaptchaHelper):

    def __init__(self):
        super().__init__( {'history': 'serienstream.io', 'cookie': 'serienstreamto.cookie'})
        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36'
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Accept': 'text/html'}
        self.AJAX_HEADER = dict(self.HEADER)
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})

        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        self.MAIN_URL = 'https://serienstream.to/'
        self.DEFAULT_ICON_URL = self.MAIN_URL + 'public/img/facebook.jpg'

        self.cacheFilters = {}
        self.cookieHeader = ''
        self.login = ''
        self.password = ''
        self.loggedIn = None
        self.allCache = {'genres_list': [], 'genres_keys': {}, 'letters_list': [], 'letters_keys': {}}

    def mainMenu(self, cItem):
        MAIN_CAT_TAB = [{'category': 'listAllSeries', 'title': 'Alle Serien', 'url': self.getFullUrl('/serien-alphabet')},
                             {'category': 'listFilter', 'title': _('A-Z'), 'url': self.MAIN_URL, 'filter':'abc'},
                             {'category': 'listFilter', 'title': _('Genres'), 'url': self.MAIN_URL, 'filter':'genres'},
                             {'category': 'list_items', 'title': _('New'), 'url': self.getFullUrl('/neu')},
                             {'category': 'list_items', 'title': _('Popular'), 'url': self.getFullUrl('/beliebte-serien')},
                             {'category': 'search', 'title': _('Search'), 'search_item': True, },
                             {'category': 'search_history', 'title': _('Search history'), }
                            ]
        self.listsTab(MAIN_CAT_TAB, cItem)


    def listAllSeries(self, cItem):

        ALL_SERIES_TAB = [{'category': 'listsAllLetters', 'title': 'Alphabet', 'url': self.getFullUrl('/serien-alphabet')},
                               {'category': 'listsAllGenres', 'title': 'Genres', 'url': self.getFullUrl('/serien-genres')}, ]

        self.listsTab(ALL_SERIES_TAB, cItem)


    def getPage(self, baseUrl, params={}, post_data=None):
        if params == {}:
            params = dict(self.defaultParams)
        params['cloudflare_params'] = {'cookie_file': self.COOKIE_FILE, 'User-Agent': self.USER_AGENT}
        return self.cm.getPageCFProtection(baseUrl, params, post_data)

    def refreshCookieHeader(self):
        self.cookieHeader = self.cm.getCookieHeader(self.COOKIE_FILE)

    def getIconUrl(self, url, refreshCookieHeader=True):
        url = self.getFullUrl(url)
        if url == '':
            return ''
        if refreshCookieHeader:
            self.refreshCookieHeader()
        return strwithmeta(url, {'Cookie': self.cookieHeader, 'User-Agent': self.USER_AGENT})

    def fillFilters(self, url):
        printDBG("SerienStreamTo.listABC")

        self.cacheFilters = {'abc': [], 'genres': []}

        sts, data = self.getPage(url)
        if not sts:
            return

        for filter in [('abc', '<ul class="catalogNav"', '<li class="'), ('genres', '<ul class="homeContentGenresList"', '</ul>')]:
            tmp = ph.getDataBetweenMarkers(data, filter[1], filter[2], withMarkers=False)[1]
            tmp = ph.getAllItemsBetweenMarkers(tmp, '<a ', '</a>', withMarkers=True)
            for item in tmp:
                url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
                title = ph.cleanHtml(item)
                if title == '':
                    title = ph.cleanHtml(ph.getSearchGroups(item, '''title=['"]([^'^"]+?)['"]''')[0])
                params = {'good_for_fav': True, 'title': title, 'url': url}
                self.cacheFilters[filter[0]].append(params)

    def listFilter(self, cItem):
        printDBG("SerienStreamTo.listFilter")

        filter = cItem.get('filter', [])
        tab = self.cacheFilters.get(filter, [])

        if 0 == len(tab):
            self.fillFilters(cItem['url'])
            tab = self.cacheFilters.get(filter, [])

        params = dict(cItem)
        params['category'] = 'listItems'
        self.listsTab(tab, params)

    def listsAllLetters(self, cItem):
        printDBG("SerienStreamTo.listsAllLetters")
        if 0 == len(self.allCache['letters_list']):
            sts, data = self.getPage(cItem['url'])
            if not sts:
                return
            self.setMainUrl(data.meta['url'])
            data = ph.getDataBetweenMarkers(data, 'seriesGenreList', '</ul>', False)[1]
            data = ph.getAllItemsBetweenMarkers(data, '<li', '</li>')
            for item in data:
                url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
                title = ph.cleanHtml(item)
                letter = title[0].upper()
                if not letter.isalpha():
                    letter = '#'
                if letter not in self.allCache['letters_list']:
                    self.allCache['letters_list'].append(letter)
                    self.allCache['letters_keys'][letter] = []
                self.allCache['letters_keys'][letter].append({'url': url, 'title': title})

        for letter in self.allCache['letters_list']:
            params = dict(cItem)
            params.update({'category': "listsAllItems", 'title': letter, 'all_key': letter, 'all_mode': 'letters'})
            self.addDir(params)

    def listsAllGenres(self, cItem):
        printDBG("SerienStreamTo.listsAllGenres")
        if 0 == len(self.allCache['genres_list']):
            sts, data = self.getPage(cItem['url'])
            if not sts:
                return
            self.setMainUrl(data.meta['url'])
            data = ph.getAllItemsBetweenMarkers(data, 'seriesGenreList', '</ul>', False)
            for section in data:
                genre = ph.cleanHtml(ph.getDataBetweenMarkers(section, '<h3', '</h3>')[1])
                section = ph.getAllItemsBetweenMarkers(section, '<li', '</li>')
                for item in section:
                    url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
                    title = ph.cleanHtml(item)
                    if genre not in self.allCache['genres_list']:
                        self.allCache['genres_list'].append(genre)
                        self.allCache['genres_keys'][genre] = []
                    self.allCache['genres_keys'][genre].append({'url': url, 'title': title})

        for genre in self.allCache['genres_list']:
            params = dict(cItem)
            params.update({'category': "listsAllItems", 'title': genre, 'all_key': genre, 'all_mode': 'genres'})
            self.addDir(params)

    def listsAllItems(self, cItem):
        printDBG("SerienStreamTo.listsAllItems")
        key = cItem['all_key']
        mode = cItem['all_mode']
        for item in self.allCache['%s_keys' % mode][key]:
            params = dict(cItem)
            params.update(item)
            params.update({'category': "listSeasons", 'good_for_fav': True})
            self.addDir(params)

    def listItems(self, cItem):
        printDBG("SerienStreamTo.listItems")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = ph.getDataBetweenNodes(data, ('<div', '>', 'pagination'), ('</div', '>'), False)[1]
        nextPage = self.getFullUrl(ph.getSearchGroups(nextPage, '''<a[^>]*?href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        data = ph.getDataBetweenMarkers(data, '<div class="seriesListContainer', '<div class="cf">', withMarkers=True)[1]
        data = ph.getAllItemsBetweenMarkers(data, '<a ', '</a>', withMarkers=True)
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
            if not isValidUrl(url):
                continue
            icon = self.getFullUrl(ph.getSearchGroups(item, '''data-src=['"]([^'^"]+?)['"]''')[0])
            title = ph.cleanHtml(ph.getDataBetweenMarkers(item, '<h3>', '</h3>', withMarkers=False)[1])
            if title == '':
                title = ph.cleanHtml(ph.getSearchGroups(item, '''alt=['"]([^'^"]+?)['"]''')[0])
            desc = ph.cleanHtml(item)
            params = dict(cItem)
            params.update({'category': "listSeasons", 'good_for_fav': True, 'title': title, 'url': url, 'icon': icon, 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': _('Next page'), 'url': nextPage, 'page': page + 1, 'desc': ''})
            self.addNext(params)

    def listSeasons(self, cItem):
        printDBG("SerienStreamTo.listSeasons")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = ph.getDataBetweenMarkers(data, '<div class="seriesContentBox"', '<div class="series-add')[1]
        icon = self.getFullUrl(ph.getSearchGroups(tmp, '''data-src=['"]([^'^"]+?)['"]''')[0])
        if '' == icon:
            icon = cItem.get('series_title', '')
        desc = ph.cleanHtml(ph.getSearchGroups(tmp, '''description=['"]([^'^"]+?)['"]''')[0])

        trailerUrl = self.getFullUrl(ph.getSearchGroups(tmp, '''href=['"]([^'^"]+?)['"][^>]+?itemprop=['"]trailer['"]''')[0])
        if isValidUrl(trailerUrl):
            params = {'good_for_fav': True, 'title': _('Trailer'), 'url': trailerUrl, 'icon': icon, 'desc': desc}
            self.addVideo(params)

        data = ph.getDataBetweenMarkers(data, 'Staffeln:', '</ul>')[1]
        data = ph.getAllItemsBetweenMarkers(data, '<a ', '</a>')
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
            if not isValidUrl(url):
                continue
            title = ph.cleanHtml(item)
            try:
                seasonNum = str(int(title))
                title = ph.cleanHtml(ph.getSearchGroups(item, '''title=['"]([^'^"]+?)['"]''')[0])
            except Exception:
                seasonNum = ''
            params = dict(cItem)
            params.update({'category': "listEpisodes", 'good_for_fav': True, 'season_num': seasonNum, 'series_title': cItem['title'], 'title': cItem['title'] + ': ' + title, 'url': url, 'icon': icon, 'desc': desc})
            self.addDir(params)

    def listEpisodes(self, cItem):
        printDBG("SerienStreamTo.listEpisodes")

        seasonNum = cItem.get('season_num', '')
        seriesTitle = cItem.get('series_title', '')
        cItem = dict(cItem)
        cItem.pop('season_num', None)
        cItem.pop('series_title', None)

        #seriesTitle = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<div class="series-title">', '<div', withMarkers=False)[1])
        #if seriesTitle == '': seriesTitle = cItem.get('series_title', '')

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        data = ph.getDataBetweenMarkers(data, '<tbody ', '</tbody>')[1]
        data = ph.getAllItemsBetweenMarkers(data, '<tr ', '</tr>')
        for item in data:
            tmp = ph.getAllItemsBetweenMarkers(item, '<td ', '</td>')
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
            if not isValidUrl(url):
                continue
            title = ph.cleanHtml(tmp[1])
            try:
                episodeNum = str(int(ph.getSearchGroups(item, '''episode\-([0-9]+?)[^0-9]''')[0]))
            except Exception:
                episodeNum = ''
            if '' != episodeNum and '' != seasonNum:
                title = 's%se%s' % (seasonNum.zfill(2), episodeNum.zfill(2)) + ' - ' + title

            langs = re.compile('/public/img/([a-z]+?)\.png').findall(item)
            desc = '[{0}]'.format(' | '.join(langs)) + '[/br]' + cItem.get('desc', '')
            params = dict(cItem)
            params.update({'good_for_fav': True, 'title': '{0}: {1}'.format(seriesTitle, title), 'url': url, 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("SerienStreamTo.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))

        baseUrl = self.getFullUrl('ajax/search')
        post_data = {'keyword': searchPattern}
        sts, data = self.getPage(baseUrl, {}, post_data)
        if not sts:
            return

        printDBG(data)

        try:
            data = json.loads(data)
            for item in data:
                title = ph.cleanHtml(item['title'])
                desc = ph.cleanHtml(item['description'])
                url = self.getFullUrl(item['link'])
                params = {'name': 'category', 'category': 'list_seasons', 'good_for_fav': True, 'title': title, 'url': url, 'desc': desc}
                self.addDir(params)
        except Exception:
            printExc()

    def getCustomLinksForVideo(self, cItem):
        printDBG("SerienStreamTo.getCustomLinksForVideo [%s]" % cItem)
        urlTab = []

        # 1 - de, 2 - en, 3 - de_sub
        langPreference = config.plugins.iptvplayer.serienstreamto_langpreference.value.replace('de_sub', '3').replace('de', '1').replace('en', '2').split(',')
        printDBG(langPreference)

        def compare(itemX, itemY):
            x = itemX['lang_id']
            if x in langPreference:
                x = len(langPreference) - langPreference.index(x)
            else:
                x = 0
            y = itemY['lang_id']
            if y in langPreference:
                y = len(langPreference) - langPreference.index(y)
            else:
                y = 0
            return int(y) - int(x)

        if self.up.getDomain(self.MAIN_URL) in cItem['url']:
            sts, data = self.getPage(cItem['url'])
            if not sts:
                return []
            # fill data lang map
            langMap = {}
            tmp = ph.getDataBetweenMarkers(data, '<div class="changeLanguageBox"', '</div>')[1]
            tmp = ph.getAllItemsBetweenMarkers(tmp, '<img ', '>')
            for item in tmp:
                key = ph.getSearchGroups(item, '''data-lang-key=['"]([^'^"]+?)['"]''')[0]
                title = ph.getSearchGroups(item, '''title=['"]([^'^"]+?)['"]''')[0] #ph.getSearchGroups(item, '''src=['"]([^'^"]+?)['"]''')[0].split('/')[-1].repace('.png', '')
                langMap[key] = title

            data = ph.getDataBetweenMarkers(data, '<div class="changeLanguageBox"', '</ul>')[1]
            data = ph.getAllItemsBetweenMarkers(data, '<li', '</li>')
            for item in data:
                langId = ph.getSearchGroups(item, '''data-lang-key=['"]([^'^"]+?)['"]''')[0]
                title = ph.cleanHtml(ph.getDataBetweenMarkers(item, '<h4>', '</h4>', withMarkers=False)[1])
                url = strwithmeta(self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0]), {'base_url': cItem['url']})
                if url == '':
                    url = strwithmeta(self.getFullUrl(ph.getSearchGroups(item, '''data\-link\-target=['"]([^'^"]+?)['"]''')[0]), {'base_url': cItem['url']})
                urlTab.append({'name': '[{0}] {1}'.format(langMap.get(langId, _('Unknown')), title), 'lang_id': langId, 'url': url, 'need_resolve': 1})

            urlTab = sorted(urlTab, key=cmp_to_key(compare))
        else:
            urlTab = self.up.getVideoLinkExt(cItem['url'])
        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        printDBG("SerienStreamTo.getCustomVideoLinks [%s]" % videoUrl)
        urlTab = []
        videoUrl = strwithmeta(videoUrl)

        if isValidUrl(videoUrl):
            if 1 != self.up.checkHostSupport(videoUrl):
                params = dict(self.defaultParams)
                try:
                    params['max_data_size'] = 0
                    params['no_redirection'] = True
                    tries = 0
                    url = videoUrl
                    while tries < 3:
                        sts, data = self.getPage(url, params)
                        printDBG("+++++++++++")
                        printDBG(self.cm.meta)
                        printDBG("+++++++++++")
                        url = self.cm.meta.get('location', '')
                        if not isValidUrl(url):
                            break
                        if url != '':
                            videoUrl = url
                        tries += 1
                except Exception:
                    printExc()

            printDBG(">>>>>>>>>>>>>>>>>>>>>>>>||||||||||||||||||||||||")
            if 1 != self.up.checkHostSupport(videoUrl):
                sts, data = self.getPage(videoUrl)
                if sts:
                    videoUrl = self.cm.meta['url']
                printDBG("+++++++++++")
                printDBG(data)
                printDBG("+++++++++++")
                if sts and 'google.com/recaptcha/' in data and 'sitekey' in data:
                    token = ''

                    sitekey = re.findall("'sitekey': '(.*?)'", data)
                    if sitekey:
                        (token, errorMsgTab) = CaptchaHelper.processCaptcha(self, sitekey[0], videoUrl)
                        printDBG("Captcha Token: %s" % token)

                    if not token:
                        message = _('Link protected with google recaptcha v2.')
                        if True != self.loggedIn:
                            message += '\n' + _('Please fill your login and password in the host configuration (available under blue button) and try again.')
                        else:
                            message += '\n' + ph.cleanHtml(ph.getDataBetweenMarkers(data, '<small', '</small>')[1])
                            message += '\n' + _('Please retry later.')
                        SetIPTVPlayerLastHostError(message)
                    else:
                        videoUrl = videoUrl + '?token=' + token
                        sts, data = self.getPage(videoUrl)
                        if sts:
                            videoUrl = self.cm.meta['url']

            if 1 == self.up.checkHostSupport(videoUrl):
                urlTab = self.up.getVideoLinkExt(videoUrl)
        return urlTab

    def tryTologin(self):
        printDBG('tryTologin start')

        if self.login == config.plugins.iptvplayer.serienstreamto_login.value and \
           self.password == config.plugins.iptvplayer.serienstreamto_password.value:
           return

        clearCookie(self.COOKIE_FILE, ['__cfduid', 'cf_clearance'])
        self.login = config.plugins.iptvplayer.serienstreamto_login.value
        self.password = config.plugins.iptvplayer.serienstreamto_password.value

        if '' == self.login.strip() or '' == self.password.strip():
            printDBG('tryTologin wrong login data')
            self.loggedIn = None
            return

        url = self.getFullUrl('/login')

        post_data = {'email': self.login, 'password': self.password, 'autoLogin': 'on'}

        while True:
            errorMsg = ''
            httpParams = dict(self.defaultParams)
            httpParams['header'] = dict(httpParams['header'])
            httpParams['header']['Referer'] = url
            sts, data = self.getPage(url, httpParams, post_data)
            printDBG(data)
            sts, data = self.getPage(url)
            if sts and '/home/logout' in data:
                printDBG('tryTologin OK')
                self.loggedIn = True
                return
            elif sts:
                'messageAlert'

        self.sessionEx.open(MessageBox, _('Login failed.'), type=MessageBox.TYPE_ERROR, timeout=10)
        printDBG('tryTologin failed')
        self.loggedIn = False
        return

    def tryTologin(self):
        printDBG('tryTologin start')

        if None == self.loggedIn or self.login != config.plugins.iptvplayer.serienstreamto_login.value or\
            self.password != config.plugins.iptvplayer.serienstreamto_password.value:

            loginCookie = GetCookieDir('s.to.login')
            self.login = config.plugins.iptvplayer.serienstreamto_login.value
            self.password = config.plugins.iptvplayer.serienstreamto_password.value

            sts, data = self.cm.getPage(self.getMainUrl(), self.defaultParams)
            if sts:
                self.setMainUrl(self.cm.meta['url'])

            freshSession = False
            if sts and '/home/logout' in data:
                printDBG("Check hash")
                hash = hex_md5('%s@***@%s' % (self.login, self.password))
                prevHash = ReadTextFile(loginCookie)[1].strip()

                printDBG("$hash[%s] $prevHash[%s]" % (hash, prevHash))
                if hash == prevHash:
                    self.loggedIn = True
                    return
                else:
                    freshSession = True

            rm(loginCookie)
            rm(self.COOKIE_FILE)
            if freshSession:
                sts, data = self.cm.getPage(self.getMainUrl(), MergeDicts(self.defaultParams, {'use_new_session': True}))

            self.loggedIn = False
            if '' == self.login.strip() or '' == self.password.strip():
                return False

            actionUrl = self.getFullUrl('/login')
            post_data = {'email': self.login, 'password': self.password, 'autoLogin': 'on'}
            tries = 0
            while tries < 3:
                tries += 1
                errorMsg = ''
                httpParams = dict(self.defaultParams)
                httpParams['header'] = dict(httpParams['header'])
                httpParams['header']['Referer'] = actionUrl
                sts, data = self.getPage(actionUrl, httpParams, post_data)
                printDBG("+++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                printDBG(data)
                printDBG("+++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                if sts and not data.strip():
                    sts, data = self.getPage(actionUrl)
                if sts and '/home/logout' in data:
                    printDBG('tryTologin OK')
                    self.loggedIn = True
                    break
                elif sts:
                    errorMsg = ph.cleanHtml(ph.find(data, ('<div', '>', 'messageAlert'), '</div>', flags=0)[1])
                    tmp1 = ph.find(data, ('<div', '>', 'formCaptcha'), '</div>', flags=0)[1]
                    imgUrl = self.getFullUrl(ph.search(tmp1, ph.IMAGE_SRC_URI_RE)[1], self.cm.meta['url'])
                    tmp2 = ph.find(data, ('<input', '>', 'captcha'), flags=0)[1]
                    if imgUrl:
                        captchaLabel = _('Captcha')
                        captchaTitle = errorMsg
                        sendLabel = _('Send')

                        header = dict(httpParams['header'])
                        header['Accept'] = 'image/png,image/*;q=0.8,*/*;q=0.5'
                        params = dict(self.defaultParams)
                        params.update({'maintype': 'image', 'subtypes': ['jpeg', 'png'], 'check_first_bytes': [b'\xFF\xD8', b'\xFF\xD9', b'\x89\x50\x4E\x47'], 'header': header})
                        filePath = GetTmpDir('.iptvplayer_captcha.jpg')
                        rm(filePath)
                        ret = self.cm.saveWebFile(filePath, imgUrl.replace('&amp;', '&'), params)
                        if not ret.get('sts'):
                            SetIPTVPlayerLastHostError(_('Fail to get "%s".') % imgUrl)
                            return
                        params = deepcopy(IPTVMultipleInputBox.DEF_PARAMS)
                        params['accep_label'] = sendLabel
                        params['title'] = captchaLabel
                        params['status_text'] = captchaTitle
                        params['status_text_hight'] = 200
                        params['with_accept_button'] = True
                        params['list'] = []
                        item = deepcopy(IPTVMultipleInputBox.DEF_INPUT_PARAMS)
                        item['label_size'] = (660, 110)
                        item['input_size'] = (680, 25)
                        item['icon_path'] = filePath
                        item['title'] = _('Answer')
                        item['input']['text'] = ''
                        params['list'].append(item)
                        #params['vk_params'] = {'invert_letters_case':True}

                        ret = 0
                        retArg = self.sessionEx.waitForFinishOpen(IPTVMultipleInputBox, params)
                        printDBG(retArg)
                        if retArg and len(retArg) and retArg[0]:
                            printDBG(retArg[0])
                            post_data['captcha'] = retArg[0][0]
                            continue
                        else:
                            break

            if self.loggedIn:
                hash = hex_md5('%s@***@%s' % (self.login, self.password))
                WriteTextFile(loginCookie, hash)
            else:
                self.sessionEx.open(MessageBox, _('Login failed.') + '\n' + errorMsg, type=MessageBox.TYPE_ERROR, timeout=10)

        return self.loggedIn

    def getCustomArticleContent(self, cItem):
        printDBG("SerienStreamTo.getCustomArticleContent [%s]" % cItem)
        retTab = []

        sts, data = self.getPage(cItem.get('url', ''))
        if not sts:
            return retTab

        retTab = []
        itemsList = []
        icons = []
        title = ''
        desc = ''

        sts, data = self.getPage(cItem.get('url'))
        if not sts:
            return []
        cUrl = self.cm.meta['url']
        self.setMainUrl(cUrl)

        if cItem.get('type') == 'video':
            tmp = ph.find(data, ('<div', '>', 'containsSeason'), '</script>', flags=0)[1]
            s = ph.cleanHtml(ph.getattr(ph.find(tmp, ('<meta', '>', 'seasonNumber'))[1], 'content'))
            e = ph.cleanHtml(ph.getattr(ph.find(tmp, ('<meta', '>', 'episode'))[1], 'content'))
            title = ph.cleanHtml(ph.find(tmp, ('<h2', '>'), '</h2>', flags=0)[1])
            if s and e and title:
                title = 's%se%s %s' % (s.zfill(2), e.zfill(2), title)
            desc = ph.cleanHtml(ph.find(tmp, ('<p', '>'), '</p>', flags=0)[1])

        data = ph.find(data, ('<section', '>'), '</section>', flags=0)[1]

        data = ph.findall(data, ('<strong', '</strong>'), '</ul', flags=ph.START_S)
        for idx in range(1, len(data), 2):
            label = ph.cleanHtml(data[idx - 1])
            if len(label) < 3:
                continue
            value = []
            tmp = ph.findall(data[idx], ('<li', '>'), '</li>', flags=0)
            for it in tmp:
                it = ph.cleanHtml(it)
                if it:
                    value.append(it)

            if label and value:
                itemsList.append((label, ', '.join(value)))

        if not title:
            title = cItem['title']
        if not icons:
            icons.append({'title': '', 'url': cItem.get('icon', self.DEFAULT_ICON_URL)})
        if not desc:
            desc = cItem.get('desc', '')

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc), 'images': icons, 'other_info': {'custom_items_list': itemsList}}]


    def withArticleContent(self, cItem):
        return cItem.get('type') == 'video'
