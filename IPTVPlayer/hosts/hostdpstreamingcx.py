# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _, IPTVPlayerSleep
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs.pCommon import isValidUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
###################################################

###################################################
# FOREIGN import
###################################################
import datetime
import re
import urllib.request
import urllib.parse
import urllib.error
import urllib.parse
try:
    import json
except Exception:
    import simplejson as json
###################################################


def gettytul():
    return 'https://streaming-series.watch/'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'dpstreaming.cx', 'cookie': 'dpstreaming.cx.cookie'})

        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'
        self.HTTP_HEADER = {'User-Agent': self.USER_AGENT, 'Accept': 'text/html'}
        self.MAIN_URL = 'https://www.streaming-series.watch/'
        self.DEFAULT_ICON_URL = 'http://reviewme.co.za/wp-content/uploads/2013/06/lista_series_7327_622x.jpg'
        self.MAIN_CAT_TAB = [{'category': 'list_items', 'title': 'Nouveaux Films', 'url': self.getMainUrl()},
                             {'category': 'sort', 'title': 'Parcourir', 'url': self.getFullUrl('/parcourir/')},
                             {'category': 'search', 'title': _('Search'), 'search_item': True},
                             {'category': 'search_history', 'title': _('Search history'), }]

        self.defaultParams = {'header': self.HTTP_HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        def _getFullUrl(url):
            if isValidUrl(url):
                return url
            return urllib.parse.urljoin(baseUrl, url)

        addParams['cloudflare_params'] = {'domain': self.up.getDomain(baseUrl), 'cookie_file': self.COOKIE_FILE, 'User-Agent': self.USER_AGENT, 'full_url_handle': _getFullUrl}
        sts, data = self.cm.getPageCFProtection(baseUrl, addParams, post_data)
        return sts, data

    def getFullIconUrl(self, url):
        url = self.getFullUrl(url)
        if url == '':
            return ''
        cookieHeader = self.cm.getCookieHeader(self.COOKIE_FILE)
        return strwithmeta(url, {'Cookie': cookieHeader, 'User-Agent': self.USER_AGENT})

    def listSort(self, cItem, nextCategory):
        printDBG("StreamingSeriesWatch.listSort")
        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        data = ph.getDataBetweenNodes(data, ('<ul', '>', 'dropdown'), ('</ul', '>'))[1]
        data = ph.getAllItemsBetweenMarkers(data, '<a', '</a>')
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
            title = ph.cleanHtml(item)
            params = dict(cItem)
            params.update({'category': nextCategory, 'url': url, 'title': title})
            self.addDir(params)

    def listItems(self, cItem, category):
        printDBG("StreamingSeriesWatch.listItems")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.getFullUrl(ph.getSearchGroups(data, '''<a[^>]+?href=['"]([^'^"]+?)['"][^>]*?>\s*Suivante''')[0])

        data = ph.getDataBetweenNodes(data, ('<div', '>', 'space'), ('<div', '>', 'clear'), False)[1]
        data = re.compile('''<div[^>]+?video[^>]+?>''').split(data)
        if len(data):
            del data[0]
        for item in data:
            url = ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0]
            icon = ph.getSearchGroups(item, '''src=['"]([^'^"]+?)['"]''')[0]
            title = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<div', '>', 'title'), ('</div', '>'))[1])
            if title == '':
                title = ph.cleanHtml(ph.getSearchGroups(item, '''alt=['"]([^'^"]+?)['"]''')[0])
            desc = []
            tmp = ph.getAllItemsBetweenMarkers(item, '<div', '</div>')
            for t in tmp:
                t = ph.cleanHtml(t)
                if t != '':
                    desc.append(t)
            season = ph.getSearchGroups(url, 'saison-([0-9]+?)-')[0]
            params = dict(cItem)
            params.update({'good_for_fav': True, 'category': category, 'url': url, 'title': title, 'desc': '[/br]'.join(desc), 'icon': icon, 'season': season})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage})
            self.addDir(params)

    def listEpisodes(self, cItem):
        printDBG("StreamingSeriesWatch.listEpisodes")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        descData = ph.getDataBetweenNodes(data, ('<div', '>', 'video-container'), ('<div', '>', 'clear'))[1]
        desc = ph.cleanHtml(ph.getDataBetweenMarkers(descData, 'Synopsis', '</p>')[1])
        icon = ph.getSearchGroups(descData, '''src=['"]([^'^"]+?)['"]''')[0]
        titleSeason = cItem['title'].split('Saison')[0]

        data = ph.getDataBetweenMarkers(data, 'Episodes', '</div>')[1]
        data = ph.getAllItemsBetweenMarkers(data, '<a', '</a>')
        for item in data:
            url = ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0]
            title = ph.cleanHtml(item)
            params = dict(cItem)
            params.update({'good_for_fav': False, 'url': url, 'title': titleSeason + ' s%se%s' % (cItem['season'], title), 'icon': icon, 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("StreamingSeriesWatch.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)
        cItem['url'] = self.MAIN_URL + '?s=' + urllib.parse.quote(searchPattern)
        self.listItems(cItem, 'episodes')

    def getCustomLinksForVideo(self, cItem):
        printDBG("StreamingSeriesWatch.getCustomLinksForVideo [%s]" % cItem)
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return []

        data = ph.getDataBetweenNodes(data, ('<div', '>', 'video-container'), ('</div', '>'))[1]
        data = data.split('</iframe>')
        if len(data):
            del data[-1]

        lang = ''
        for item in data:
            tmp = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<span', '>', '"lg"'), ('</span', '>'), False)[1])
            if tmp != '':
                lang = tmp
            name = ph.getDataBetweenMarkers(item, '<b', '</b>', False)[1]
            if lang != '':
                name = '%s: %s' % (lang, name)
            url = ph.getSearchGroups(item, '''<iframe[^>]+?src=['"]([^'^"]+?)['"]''')[0]
            if isValidUrl(url):
                urlTab.append({'name': name, 'url': url, 'need_resolve': 1})
        return urlTab

    def getCustomVideoLinks(self, url):
        printDBG("StreamingSeriesWatch.getCustomVideoLinks [%s]" % url)
        urlTab = []

        if 'protect-stream.com' not in url:
            return []

        sts, data = self.getPage(url, self.defaultParams)
        if not sts:
            return []
        cUrl = self.cm.meta['url']

        k = ph.getSearchGroups(data, 'var\s+?k[^"]*?=[^"]*?"([^"]+?)"')[0]
        secure = ph.getSearchGroups(data, '''['"/](secur[^\.]*?)\.js''')[0]

        try:
            sts, tmp = self.getPage('https://www.protect-stream.com/%s.js' % secure, self.defaultParams)
            count = ph.getSearchGroups(tmp, 'var\s+?count\s*?=\s*?([0-9]+?);')[0]
            if int(count) < 15:
                IPTVPlayerSleep().sleep(int(count))
        except Exception:
            printExc()
            return []

        header = dict(self.defaultParams['header'])
        params = dict(self.defaultParams)
        header['Referer'] = cUrl
        header['Content-Type'] = "application/x-www-form-urlencoded"
        header['Accept-Encoding'] = 'gzip, deflate'
        params['header'] = header
        params['use_cookie'] = False

        sts, data = self.getPage('https://www.protect-stream.com/%s.php' % secure, params, {'k': k})
        if not sts:
            return []
        printDBG('==========================================')
        printDBG(data)
        printDBG('==========================================')

        tmp = re.compile('''<iframe[^>]+?src=['"]([^'^"]+?)['"]''').findall(data)
        tmp.extend(re.compile('''<a[^>]+?href=['"]([^'^"]+?)['"]''').findall(data))
        for videoUrl in tmp:
            videoUrl = self.getFullUrl(videoUrl)
            urlTab.extend(self.up.getVideoLinkExt(videoUrl))
        return urlTab

    def getFavouriteData(self, cItem):
        printDBG('StreamingSeriesWatch.getFavouriteData')
        return json.dumps(cItem)

    def getCustomLinksForFavourite(self, fav_data):
        printDBG('StreamingSeriesWatch.getCustomLinksForFavourite')
        links = []
        try:
            cItem = json.loads(fav_data)
            links = self.getCustomLinksForVideo(cItem)
        except Exception:
            printExc()
        return links

    def setInitListFromFavouriteItem(self, fav_data):
        printDBG('StreamingSeriesWatch.setInitListFromFavouriteItem')
        try:
            params = json.loads(fav_data)
        except Exception:
            params = {}
            printExc()
        self.addDir(params)
        return True

    def getCustomArticleContent(self, cItem):
        printDBG("MoviesNight.getCustomArticleContent [%s]" % cItem)
        retTab = []

        if 'resource_uri' not in cItem:
            return []

        if 0 == len(self.loginData['api_key']) and 0 == len(self.loginData['username']):
            self.requestLoginData()

        url = cItem['resource_uri']
        url += '?api_key=%s&username=%s' % (self.loginData['api_key'], self.loginData['username'])
        url = self.getFullUrl(url)

        sts, data = self.getPage(url)
        if not sts:
            return []

        title = cItem['title']
        desc = cItem.get('desc', '')
        icon = cItem.get('icon', '')
        otherInfo = {}
        try:
            data = json.loads(data)
            icon = self._viaProxy(self.getFullUrl(data['poster']))
            title = data['title']
            desc = data['overview']
            otherInfo['actors'] = data['actors']
            otherInfo['director'] = data['director']
            genres = []
            for item in data['genre']:
                genres.append(item['name'])
            otherInfo['genre'] = ', '.join(genres)
            otherInfo['rating'] = data['imdb_rating']
            otherInfo['year'] = data['year']
            otherInfo['duration'] = str(datetime.timedelta(seconds=data['runtime']))
        except Exception:
            printExc()

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc), 'images': [{'title': '', 'url': icon}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        HostBase.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        mode = self.currItem.get("mode", '')

        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    #MAIN MENU
        if name is None:
            self.listsTab(self.MAIN_CAT_TAB, {'name': 'category'})
        elif category == 'sort':
            self.listSort(self.currItem, 'list_items')
        elif category == 'list_items':
            self.listItems(self.currItem, 'episodes')
        elif category == 'episodes':
            self.listEpisodes(self.currItem)
    #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        HostBase.endHandleService(self, refresh)

