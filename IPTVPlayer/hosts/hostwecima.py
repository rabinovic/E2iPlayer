# -*- coding: utf-8 -*-

import re

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG
from Plugins.Extensions.IPTVPlayer.libs import ph

def gettytul():
    return "WeCima"


class IPTVHost(HostBase):
    def __init__(self):
        super().__init__( {'history': 'wecima', 'cookie': 'mycima.cookie'})

        self.USER_AGENT = getDefaultUserAgent()
        self.HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html'}
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True,
                              'cookiefile': self.COOKIE_FILE, 'with_metadata':True}

        self.getPage('https://wecima.online/', self.defaultParams)
        self.MAIN_URL = self.cm.meta['url']

        self.DEFAULT_ICON_URL = self.MAIN_URL + 'wp-content/uploads/2023/02/wecima-favicon-1.png'
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams=None, post_data=None):
        if addParams is None:
            addParams = dict(self.defaultParams)

        baseUrl = self.cm.iriToUri(baseUrl)
        sts, data = self.cm.getPageCFProtection(baseUrl, addParams, post_data)
        userAgent = self.defaultParams.get('header', {}).get('User-Agent', '')
        if data.meta.get('cf_user', self.USER_AGENT) != userAgent:
            self.defaultParams['header']['User-Agent'] = data.meta['cf_user']

        return sts, data

    def mainMenu(self, cItem):
        printDBG('mycima.listMainMenu')
        MAIN_CAT_TAB = [{'category': 'showmenu1', 'title': 'أفلام', 'index':0 },
                        {'category': 'showmenu1', 'title': 'مسلسلات', 'index':1},
                        {'category': 'showmenu1', 'title': 'أنمي','index':2},
                        {'category': 'showmenu1', 'title': 'المزيد', 'index':3},
                        {'category': 'search', 'title': _('Search'), 'search_item': True, },
                        {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def showmenu1(self,cItem):
        cItem = dict(cItem)
        index = cItem.get('index', 0)

        sts, data = self.getPage(self.MAIN_URL)
        if sts:
            data0=re.findall('<ul class="sub-menu">(.*?)</ul', data, re.S)
            if data0:
                data1=re.findall('<li.*?href="(.*?)".*?>(.*?)</li>', data0[index], re.S)
                if data1:
                    for elm in data1:
                        url   = self.getFullUrl(elm[0])
                        title = ph.cleanHtml(elm[1])
                        if  'افلام' in title:
                            cItem['index']=0
                        self.addDir(cItem| {'title': title, 'category':'showitms','url':url, 'icon':self.DEFAULT_ICON_URL})

    def showitms(self,cItem):
        cItem = dict(cItem)
        sts, data = self.getPage(cItem['url'])
        if sts:
            data0=re.findall('<div class="Grid--WecimaPosts".*?>(.*?)(?:class="pagination">|class="RightUI">)', data, re.S)
            if data0:
                data1=re.findall('class="GridItem".*?href="(.*?)".*?image:url\((.*?)\).*?<strong.*?>(.*?)<span class="year">\((.*?)\)</span>(.*?)</ul>', data0[0], re.S)
                if data1:
                    for elm in data1:
                        url   = re.sub('https://.*?/', self.MAIN_URL, elm[0])
                        title = ph.cleanHtml(elm[2])
                        if elm[1] !=-1:
                            image = ph.std_url(elm[1])
                            image = re.sub('https://.*?/', self.MAIN_URL, image)
                        desc = _("year") + ": " + elm[3]
                        params = cItem | {'good_for_fav':True, 'title': title, 'category':'showelms','url':url, 'icon':image, 'desc':desc, 'with_mini_cover':True}
                        if cItem['index']==0:
                            self.addVideo(params)
                        else:
                            self.addDir(params)

                next_ = re.findall('class="next page.*?href="(.*?)"', data, re.S)
                if next_:
                    self.addNext(cItem| {'category':'showitms', 'url':next_[0]})

    def showelms(self,cItem):
        cItem = dict(cItem)
        self.addVideo({'good_for_fav':True, 'title': cItem['title'],'url':cItem['url'],'icon':cItem['icon']})

        sts, data = self.getPage(cItem['url'])

        if sts:
            data0=re.findall('class="List--Seasons--Episodes">(.*?)</div>', data, re.S)
            if data0:
                data1=re.findall('href="(.*?)".*?>(.*?)<', data0[0], re.S)
                if data1:
                    self.addMarker({'title': _('Seasons'),'icon':cItem['icon']})
                    for elm in data1:
                        url   = elm[0]
                        title = ph.cleanHtml(elm[1])
                        self.addDir({'good_for_fav':True,'category':'showelms', 'title': title,'url':url,'icon':cItem['icon'], 'with_mini_cover':True})


            data0=re.findall('class="Episodes--Seasons--Episodes(.*?)</singlesection', data, re.S)
            if data0:
                data1=re.findall('href="(.*?)".*?>(.*?)</episodeTitle>', data0[0], re.S)
                if data1:
                    self.addMarker({'title': _('Episodes'),'icon':cItem['icon']})
                    for elm in data1:
                        url   = elm[0]
                        title = ph.cleanHtml(elm[1])
                        self.addVideo({'good_for_fav':True, 'title': title,'url':url,'icon':cItem['icon']})

    def getCustomLinksForVideo(self,cItem):
        urlTab = []
        url = cItem['url']

        if linksTab := self.cacheLinks.get(url, []):
            return linksTab

        sts, data = self.getPage(url)
        if sts:
            data0 = re.findall('WatchServersList">(.*?)</ul', data, re.S)
            if 	data0:
                data1 = re.findall('<li.*?data-url="(.*?)".*?>(.*?)</li>', data0[0], re.S)
                if data1:
                    for elm in data1:
                        url = elm[0]
                        title = ph.cleanHtml(elm[1]).strip()
                        urlTab.append({'name':'|Watch Server| '+title, 'url':url, 'need_resolve':1})
        if urlTab:
            self.cacheLinks[cItem['url']] = urlTab

        return urlTab

    def getCustomVideoLinks(self,videoUrl):
        urlTab = []

        videoUrl = videoUrl.replace('\r','').replace('\n','')
        urlTab = self.up.getVideoLinkExt(videoUrl)
        return urlTab

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("Cinemanow.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)

        page= cItem.get('page',1)
        searchPattern= cItem.get('searchPattern',searchPattern)

        url = self.MAIN_URL+f'/search/{ph.std_url(searchPattern)}/page/{page}'

        sts, data = self.getPage(url)
        if sts:
            data0=re.findall('<div class="Grid--MycimaPosts".*?>(.*?)(?:class="pagination">|class="RightUI">)', data, re.S)
            if data0:
                data1=re.findall('class="GridItem".*?href="(.*?)".*?image:url\((.*?)\).*?</span>(.*?)</strong>(.*?)</ul>', data0[0], re.S)
                if data1:
                    for elm in data1:
                        url   = self.getFullUrl(elm[0])
                        title = ph.cleanHtml(elm[2])
                        if elm[1] !=-1:
                            image = ph.std_url(elm[1])
                        desc0  = elm[3]
                        self.addDir({'title': title, 'category':'showelms','url':url, 'icon':image, 'desc':desc0, 'with_mini_cover':True})

    def getArticle(self,cItem):
        Desc = [('Quality','fa-play"></i>الجودة.*?<a>(.*?)</a>','',''),('Time','fa-clock">.*?<a>(.*?)</a>','',''),
                ('Story','fa-info-circle">(.*?)</li>','\n','')]
        if desc =='':
            desc = cItem.get('desc','')
        return [{'title':cItem['title'], 'text': desc, 'images':[{'title':'', 'url':cItem.get('icon','')}], 'other_info':{}}]

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)