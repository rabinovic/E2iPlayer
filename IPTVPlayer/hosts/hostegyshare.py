# -*- coding: utf-8 -*-


import json
import re
from urllib.parse import quote
from Components.config import config, ConfigText
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import SetIPTVPlayerLastHostError
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _


def gettytul():
    return 'Egy Share'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'egyShare', 'cookie': 'egyshare.cookie'})
        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'

        self.MAIN_URL = 'https://egyshare.club/'
        self.DEFAULT_ICON_URL = self.MAIN_URL + 'wp-content/uploads/2019/05/logo-egy01.png'

        self.HTTP_HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html', 'Accept-Encoding': 'gzip, deflate', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}

        self.defaultParams = {'header':self.HTTP_HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        self.MAIN_CAT_TAB = [
            {'category': 'show_sub_menu', 'title':'الافلام', 'icon':self.DEFAULT_ICON_URL, 'mod':0},
            {'category': 'show_sub_menu', 'title':'المسلسلات', 'icon':self.DEFAULT_ICON_URL, 'mod':1},
            {'category': 'show_items', 'title':'مصارعة', 'url': self.MAIN_URL + 'genre/%d9%85%d8%b5%d8%a7%d8%b1%d8%b9%d8%a9/','icon':self.DEFAULT_ICON_URL},
            {'category': 'show_items', 'title':'منوعات', 'url': self.MAIN_URL + 'genre/%d9%85%d9%86%d9%88%d8%b9%d8%a7%d8%aa/','icon':self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True, 'icon': self.DEFAULT_ICON_URL },
            {'category': 'search_history', 'title': _('Search history')     , 'icon': self.DEFAULT_ICON_URL }
        ]

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)
        baseUrl = self.cm.iriToUri(baseUrl)
        sts, data = self.cm.getPageCFProtection(baseUrl, addParams, post_data)
        userAgent = self.defaultParams.get('header', {}).get('User-Agent', '')
        if data.meta.get('cf_user', self.USER_AGENT) != userAgent:
            self.defaultParams['header']['User-Agent'] = data.meta['cf_user']

        return sts, data

    def getFullIconUrl(self, url):
        url = self.getFullUrl(url)
        if url == '':
            return ''
        cookieHeader = self.cm.getCookieHeader(self.COOKIE_FILE, ['cf_clearance'])
        return strwithmeta(url, {'Cookie': cookieHeader, 'User-Agent': self.USER_AGENT})

    def mainMenu(self, cItem):
        printDBG(f"egyshare.mainMenu cItem [{cItem}]")
        self.listsTab(self.MAIN_CAT_TAB, cItem)

    def show_sub_menu(self, cItem):
        printDBG(f"egyshare.show_sub_menu cItem [{cItem}]")

        mod = cItem['mod']

        if mod==0:
            self.listsTab([
                {'category': 'show_items', 'title':'افلام عربي', 'url': self.MAIN_URL + 'genre/%d8%a7%d9%81%d9%84%d8%a7%d9%85-%d8%b9%d8%b1%d8%a8%d9%8a/','icon':self.getFullIconUrl(self.DEFAULT_ICON_URL)},
                {'category': 'show_items', 'title':'افلام اجنبي', 'url': self.MAIN_URL + 'genre/%d8%a7%d9%81%d9%84%d8%a7%d9%85-%d8%a7%d8%ac%d9%86%d8%a8%d9%8a-1/','icon':self.getFullIconUrl(self.DEFAULT_ICON_URL)},
                {'category': 'show_items', 'title':'افلام هندي', 'url': self.MAIN_URL + 'genre/%d8%a7%d9%81%d9%84%d8%a7%d9%85-%d9%87%d9%86%d8%af%d9%8a/','icon':self.getFullIconUrl(self.DEFAULT_ICON_URL)},
                {'category': 'show_items', 'title':'افلام انمي', 'url': self.MAIN_URL + 'genre/%d8%a7%d9%81%d9%84%d8%a7%d9%85-%d8%a7%d9%86%d9%85%d9%8a/','icon':self.getFullIconUrl(self.DEFAULT_ICON_URL)},
                {'category': 'show_items', 'title':'افلام للكبار فقط', 'url': self.MAIN_URL + 'genre/%d8%a7%d9%81%d9%84%d8%a7%d9%85-%d9%84%d9%84%d9%83%d8%a8%d8%a7%d8%b1-%d9%81%d9%82%d8%b7/','icon':self.getFullIconUrl(self.DEFAULT_ICON_URL)}
            ],
            cItem)
        else:
            self.listsTab([
                {'category': 'show_items', 'title':'مسلسلات اجنبي', 'url': self.MAIN_URL + 'genre/%d9%85%d8%b3%d9%84%d8%b3%d9%84%d8%a7%d8%aa-%d8%a7%d8%ac%d9%86%d8%a8%d9%8a/','icon':self.getFullIconUrl(self.DEFAULT_ICON_URL)},
                {'category': 'show_items', 'title':'مسلسلات Netflix', 'url': self.MAIN_URL + 'network/netflix/','icon':self.getFullIconUrl(self.DEFAULT_ICON_URL)},
            ],
            cItem)


    def show_items(self, cItem):
        printDBG(f"egyshare.show_items cItem [{cItem}]")
        url = cItem['url']
        mod = cItem.get('mod', None)
        sts, data = self.getPage(url, self.defaultParams)
        if not sts:
            return

        if data0:=re.search(r'<div class="items full".*?<div class="sidebar', data, re.S):
            if data1:=re.findall(r'<article.*?</article>', data0[0], re.S):
                for article in data1:
                    if data2:=re.search(r'data-src="([^"]+)" alt="([^"]+)".*?href="([^"]+)"', article, re.S):

                        icon = data2[1]
                        desc, title = ph.uniform_title(data2[2])
                        url = data2[3]

                        if 'Quality' not in desc:
                            if data3:=re.search(r'quality">(.*?)<', article, re.S):
                                desc += f"Quality: {data3[1]}"

                        param = dict(cItem)
                        param |= {'title':title, 'url': url, 'icon':self.getFullIconUrl(ph.std_url(icon)), 'good_for_fav': True, 'with_mini_cover':True, 'desc':desc}

                    if not mod or mod == 0:
                        self.addVideo(param)
                    elif mod == 1:
                        param |= {'category': 'show_episodes'}
                        self.addDir(param)
            data3 = re.search("""href="([^"]+?)"><i id=["']nextpagination["'] class=["']fas fa-caret-right["']>""", data, re.S)
            if data3:
                param = dict(cItem)
                param |= {'url': data3[1]}
                self.addNext(param)


    def show_episodes(self, cItem):
        printDBG(f"egyshare.show_items cItem [{cItem}]")
        url = cItem['url']
        sts, data = self.getPage(url, self.defaultParams)
        if not sts:
            return

        data0 =re.search(r"""<div id=['"]seasons['"]>(.*?)class=['"]sbox""", data, re.S)
        if data0:
            seasons = data0[1].rsplit("""class='title'""")
            for season in seasons:
                title = re.search(r'Season \d', season, re.S)
                if title:
                    self.addMarker({'title':title[0]})
                data1=re.findall('''class=['"]mark-.*?['"](.*?)</li>''', season, re.S)
                if data1:
                    for ep in data1:
                        data2=re.search('''img src=['"]([^'"]+?)['"].*?href=['"]([^'"]+?)['"]>(.*?)<''', ep, re.S)
                        if data2:
                            icon=data2[1]
                            url=data2[2]
                            desc, title = ph.uniform_title(data2[3])

                            param = dict(cItem)
                            param |= {'icon':self.getFullIconUrl(ph.std_url(icon)), 'title':title, 'url': url, 'good_for_fav': True, 'desc':desc}
                            self.addVideo(param)


    def getCustomLinksForVideo(self, cItem):
        printDBG(f"egyshare.getCustomVideoLinks cItem [{cItem}]")
        linksTab = []
        url = cItem['url']
        sts, data = self.getPage(url, self.defaultParams)
        if not sts:
            return
        data0 = re.findall('''<li id=['"]player-option.*?data-post=['"](.*?)['"].*?data-nume=['"](.*?)['"].*?['"]title['"]>(.*?)<''', data, re.S)
        if data0:
            for data_post,data_num, data_name in data0:
                linksTab.append({'name': data_name, 'url': strwithmeta(url+data_post+data_num, {'post_data':data_post, 'data_num':data_num}), 'need_resolve': 1})

        return linksTab


    def getCustomVideoLinks(self, VideoUrl):
        printDBG(f"egyshare.getCustomVideoLinks VideoUrl [{VideoUrl}]")

        url = strwithmeta(VideoUrl)
        post_data = url.meta.get('post_data')
        data_num = url.meta.get('data_num')

        url = f"{self.MAIN_URL}wp-json/dooplayer/v2/{post_data}/movie/{data_num}"
        sts, data = self.getPage(url, self.defaultParams)
        if not sts:
            return
        try:
            js = json.loads(data)
            videoUrl = js.get('embed_url', '')
            if videoUrl:
                return self.up.getVideoLinkExt(videoUrl)
        except Exception as e:
            SetIPTVPlayerLastHostError(e)

    def list_search_result(self, cItem, searchPattern, searchType):
        printDBG(f"egyshare.list_search_result VideoUrl [{cItem}]")

        url = f"{self.MAIN_URL}?s={quote(searchPattern)}"
        sts, data = self.getPage(url, self.defaultParams)
        if not sts:
            return

        data0 = re.findall('<div class="result-item"><article>(.*?)</article>', data, re.S)
        if data0:
            for article in data0:
                data1 = re.search('href="([^"]+?)".*?img src="([^"]+?)" alt="([^"]+?)"', article, re.S)
                if data1:
                    url=data1[1]
                    icon=data1[2]
                    desc, title = ph.uniform_title(data1[3])

                    param = dict(cItem)
                    param |= {'icon':ph.std_url(icon), 'title':title, 'url': url, 'desc':desc}
                    self.addVideo(param)


    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)

    def listSearchResult(self, cItem, searchPattern, searchType):
        return super().listSearchResult(cItem, searchPattern, searchType)