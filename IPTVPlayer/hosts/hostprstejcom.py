# -*- coding: utf-8 -*-

import re
from datetime import datetime

###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG
from Plugins.Extensions.IPTVPlayer.libs import ph
###################################################

def gettytul():
    return 'prstej.com'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'prstej.com', 'cookie': 'prstej.com.cookie'})
        self.reIMG = re.compile(r'''<img[^>]+?src=(['"])([^>]*?)(?:\1)''', re.I)
        self.defaultParams = {
                                'header': {
                                            'User-Agent': getDefaultUserAgent("firefox"),
                                }
        }

        self.DEFAULT_ICON_URL = 'https://ba.brstej.com/22.png'

        self.DEFAULT_MAIN_URL = 'https://ba.brstej.com'
        self.MAIN_URL = 'https://ba.brstej.com'

        self.filterYear = [f"{y}" for y in range(datetime.now().year, 1989, -1)]
        self.filterGenre = ["Action", "Abenteuer", "Animation", "Biographie", "Komödie", "Krimi", "Dokumentation", "Drama", "Familie", "Fantasy", "Geschichte", "Horror", "Musik", "Mystery", "Romantik", "Reality-TV", "Sport", "Thriller", "Krieg", "Western"]

        self.cacheFilters = {}
        self.cacheFiltersKeys = []
        self.cacheLinks = {}

    def getPage(self, url, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)
        # addParams['cloudflare_params'] = {'cookie_file': self.COOKIE_FILE, 'User-Agent': self.HTTP_HEADER['User-Agent']}
        # return self.cm.getPageCFProtection(url, addParams, post_data)
        return self.cm.getPage(url, addParams, post_data)

    def mainMenu(self, cItem):
        printDBG("PrstejCom.listMain")
        self.cacheFilters = {}

        MAIN_CAT_TAB = [
            {'category': 'listItems', 'title': "أخر الاضافات", 'url':self.MAIN_URL + '/newvideos.php', "icon": self.DEFAULT_ICON_URL},
            {'category': 'listItems', 'title': "افلام",'url':self.MAIN_URL + '/category102.php?cat=movies2023', "icon": self.DEFAULT_ICON_URL},
            {'category': 'listSeries', 'title': "مسلسلات", "icon": self.DEFAULT_ICON_URL},
            {'category': 'listItems', 'title': "برامج تلفزيونية",'url':self.MAIN_URL + '/category102.php?cat=tv2023', "icon": self.DEFAULT_ICON_URL},

            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]

        self.listsTab(MAIN_CAT_TAB, cItem)

    def listMovies(self, cItem):
        MoviesTAB = [
            {'category': 'listItems', 'title': "x", 'url': 'y', "icon": self.DEFAULT_ICON_URL}
            ]
        self.listsTab(MoviesTAB, cItem)


    def listSeries(self, cItem):
        MoviesTAB = [
            {'category': 'listItems', 'title': "مسلسلات شامية", 'url': self.MAIN_URL + '/category102.php?cat=sy-2023', "icon": self.DEFAULT_ICON_URL},
            {'category': 'listItems', 'title': "مسلسلات عربية", 'url': self.MAIN_URL + '/category102.php?cat=arab2023', "icon": self.DEFAULT_ICON_URL},
            {'category': 'listItems', 'title': "مسلسلات مصرية", 'url': self.MAIN_URL + '/category102.php?cat=eg2024', "icon": self.DEFAULT_ICON_URL},
            {'category': 'listItems', 'title': "رمضان 2023", 'url': self.MAIN_URL + '/category102.php?cat=ramdan2023', "icon": self.DEFAULT_ICON_URL},
            ]
        self.listsTab(MoviesTAB, cItem)


    def listItems(self, cItem):
        printDBG("PrstejCom.listItems")
        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        elements = re.findall('<li class="col-xs-6 col-sm-[46] col-md-3">(.*?)</li>', data, re.S)
        for elem in elements:
            caption = ph.getDataBetweenMarkers(elem, ('<div', '>', 'class="caption'), '</div>')[1]
            link = ph.getattr(caption, "href")
            desc, title = ph.uniform_title(ph.getattr(caption, "title"))

            img = ph.getDataBetweenMarkers(elem, '<img', '>')[1]
            icon = ph.getattr(img, "data-echo").split("?")[0]

            self.addDir(cItem | {'category': 'listElems', 'title': title, 'icon': icon, 'url': link, 'desc':desc, 'with_mini_cover':True})

        data0 = re.findall('<ul class="pagination pagination-sm pagination-arrows">(.*?)</ul>', data, re.S)
        if data0:
            data1 = re.findall('<a href="([^"]+?)">(.*?)</a>', data0[0], re.S)
            if data1:
                for elm in data1:
                    if elm[1] == '&raquo;':
                        link = elm[0]
                        self.addNext(cItem | {'url': self.getFullUrl(link)})
                        break


    def listElems(self, cItem):
        printDBG("PrstejCom.listElems")

        url = cItem['url']

        self.addVideo(cItem | {'good_for_fav': True})

        sts, data = self.getPage(url)
        if not sts:
            return

        if data0 := re.search('<div class="SeasonsEpisodes".*?>(.*?)</div>', data, re.S):
            if data1 := re.findall('<a class href="([^"]+?)".*?><em>(.*?)</em><span>(.*?)</span></a>', data0[1]):
                self.addMarker({'title': _('Episodes'),'icon': cItem['icon']})
                for elem in data1:
                    link = elem[0]
                    title = elem[1] + elem[2]
                    if link.startswith('./'):
                        link = link.replace('./', '/')
                    self.addVideo(cItem | {'good_for_fav': True, 'title': title, 'url': self.getFullUrl(link)})

    def getCustomLinksForVideo(self, cItem):
        printDBG(f"PrstejCom.getCustomVideoLinks [{cItem}]")
        linksTab = []

        url = cItem["url"]

        url = url.replace("watch", "view")
        sts, data = self.getPage(url)
        if not sts:
            return

        watchServers = re.search("""<div.*?id="WatchServers"(.*?</div>)""", data, re.S)[1]

        servers = re.findall('''data-embed-url=['"]([^'"]+?)['"]''', watchServers, re.S)
        printDBG(servers)

        for server in servers:
            linksTab.append({'name': f"{self.up.getHostName(server)}", 'url': server, 'need_resolve': 1})

        return linksTab

    def listSearchResult(self, cItem, searchPattern=None, searchType=None):

        url = self.getFullUrl(f"search.php?keywords={searchPattern}&video-id=")

        params = dict(cItem)
        params.update({'url': ph.std_url(url)})
        self.listItems(params)

    def getCustomVideoLinks(self, videoUrl):
        printDBG(f"PrstejCom.getCustomVideoLinks [{videoUrl}]")
        return self.up.getVideoLinkExt(videoUrl)

    def getCustomArticleContent(self, cItem, data=None):
        printDBG("PrstejCom.getCustomArticleContent [%s]" % cItem)
        retTab = []
        itemsList = []

        if not data:
            url = cItem.get('prev_url', cItem['url'])
            sts, data = self.getPage(url)
            if not sts:
                return []
            data = re.sub("<!--[\s\S]*?-->", "", data)

        data = ph.find(data, ('<div', '>', 'content'), '<style', flags=0)[1]

        title = ph.cleanHtml(ph.find(data, ('<h', '>'), '</h', flags=0)[1])
        icon = ''
        desc = ph.cleanHtml(ph.find(data, ('<p', '>'), '</p>', flags=0)[1])

        data = ph.findall(data, ('<div', '>', 'extras'), '</div>', flags=0)
        for item in data:
            item = item.split(':', 1)
            label = ph.cleanHtml(item[0])
            value = ph.cleanHtml(item[-1])
            if label and value:
                itemsList.append((label + ':', value))

        if title == '':
            title = cItem['title']
        if icon == '':
            icon = cItem.get('icon', self.DEFAULT_ICON_URL)
        if desc == '':
            desc = cItem.get('desc', '')

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc), 'images': [{'title': '', 'url': self.getFullUrl(icon)}], 'other_info': {'custom_items_list': itemsList}}]

    def withArticleContent(self, cItem):
        return 'prev_url' in cItem
