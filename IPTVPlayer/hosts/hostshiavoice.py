# -*- coding: utf-8 -*-

from urllib.parse import quote
import re
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, handleServiceDecorator
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.pCommon import common, getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def gettytul():
    return 'https://shiavoice.com/'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'shiavoice.com', 'cookie': 'shiavoice.com.cookie'})

        self.MAIN_URL = gettytul()

        self.DEFAULT_ICON_URL = 'https://shiavoice.com/assets/img/shiavoice.png'

        self.defaultParams = {
            'header': {
                'User-Agent': getDefaultUserAgent(),
                'DNT': '1',
                'Accept': 'text/html',
                'Accept-Encoding': 'gzip, deflate',
                'Referer': self.getMainUrl(),
                'Origin': self.getMainUrl()
                },
            'use_cookie': True,
            'load_cookie': True,
            'save_cookie': True,
            'cookiefile': self.COOKIE_FILE
        }

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)
        return self.cm.getPage(baseUrl, addParams, post_data)

    def listMainMenu(self, cItem, nextCategory):
        sts, html = self.getPage(self.getMainUrl())
        if not sts:
            return

        m0 = re.search("data:{(t):'([^']+?)',(tok):'([^']+?)'}", html, re.S|re.I)
        if m0:
            self.post_data = {m0[1]:m0[2], m0[3]:m0[4]}

        self.recentlyAddedAlbums = 'last_added_albums'
        self.recentlyAddedSongs = 'last_added_songs'

        MAIN_CAT_TAB = [
            {'category': self.recentlyAddedAlbums, 'title': 'آخر الإصدارات المضافة', 'icon': self.DEFAULT_ICON_URL},
            {'category': self.recentlyAddedSongs, 'title': 'آخر المقاطع المضافة', 'icon': self.DEFAULT_ICON_URL}]
        self.listsTab(MAIN_CAT_TAB, cItem)

        # Categories
        data = ph.getDataBetweenNodes(html, ('<ul', '>', 'dropdown-menu'), ('</ul', '>'))[1]
        data = ph.getAllItemsBetweenMarkers(data, '<li', '</li>')
        for item in data:
            url = self.getFullUrl(re.search('href="([^"]+?)"', item)[1])
            title = re.search('<b>(.*?)</b>', item, re.I)[1]
            self.addDir(cItem|{'good_for_fav': False, 'category': nextCategory, 'url': url, 'title': title})

        self.cacheLinks = {}

        # last albums added
        self.cacheLinks[self.recentlyAddedAlbums] = []
        data = ph.getAllItemsBetweenMarkers(html, '<div class="col-lg-2 col-sm-4 p-1 release-card">', '</div></div>')
        for item in data:
            m = re.search('<a href=(.*?)name="([^"]+?)".*src=\.\.(.*?) ', item, re.S)
            if m:
                url = self.getFullUrl(m[1])
                title = m[2]
                icon = self.getFullUrl(quote(m[3]))
                self.cacheLinks[self.recentlyAddedAlbums].append(cItem | {'good_for_fav': False, 'category': nextCategory, 'url': self.getFullUrl(url), 'icon': icon, 'title': title})

        # last added songs
        self.cacheLinks[self.recentlyAddedSongs] = []
        m1 = re.search( 'آخر المقاطع المضافة'+'.*?</ul>',  html, re.S)[0]
        if m1:
            m2 = re.findall( '<li.*?</span>', m1, re.S)
            if m2:
                for item in m2:
                    url = self.getFullUrl(quote(re.search('data-track-url=(.*?) ', item, re.S)[1]))
                    title = re.search('name="(.*?)" ', item, re.S)[1]
                    icon = self.getFullUrl(quote(re.search('src=\.\.(.*?) ', item, re.S)[1]))
                    self.cacheLinks[self.recentlyAddedSongs].append(cItem | {'good_for_fav': False, 'category': nextCategory, 'url': url, 'icon': icon, 'title': title})

        MAIN_CAT_TAB = [
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history')}]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listSubMenu(self, cItem, search_pattern=None):
        url = cItem['url']

        post_data=None
        if search_pattern:
            post_data = self.post_data | {'qr': search_pattern, 'pg': '1'}

        sts, data = self.getPage(url,post_data=post_data)
        if not sts:
            return

        m0 = re.search('<ul class="list-unstyled filterItems.*?</ul>', data, re.S)
        if m0:
            m1= re.findall('<li.*?</li>',m0[0],re.S)
            if m1:
                for item in m1:
                    url = self.getFullUrl(quote(re.search('data-track-url="(.*?)" ', item, re.S)[1]))
                    title = re.search('title="([^"]+?)"', item, re.S)[1].replace('\r','')
                    self.addAudio(cItem | {'title':title, 'url':url})
        else:
            m0 = re.findall( 'div class="media border-bottom.*?</div>', data, re.S)
            if m0:
                for item in m0:
                    url = self.getFullUrl(re.search('href="(.*?)"', item)[1])
                    icon = self.getFullUrl(re.search('src="\.\.(.*?)"', item)[1])
                    title = re.search('<h5.*?>(.*?)</h5>', item,re.I|re.S)[1].strip()
                    self.addDir(cItem | {'good_for_fav': False, 'url': url, 'icon': icon, 'title': title,
                                'category': 'sub_menu'})


    def listRecentlyAddedAlbums(self, cItem):
        for item in self.cacheLinks[self.recentlyAddedAlbums]:
            params = dict(cItem)
            params.update(item)
            self.addDir(params)

    def listRecentlyAddedSongs(self, cItem):
        for item in self.cacheLinks[self.recentlyAddedSongs]:
            params = dict(cItem)
            params.update(item)
            self.addAudio(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        searchPattern = quote(searchPattern)

        cItem = dict(cItem)
        cItem['url'] = self.getFullUrl('/assets/php/get.php')
        self.listSubMenu(cItem, searchPattern)

    def getCustomLinksForVideo(self, cItem):

        url = cItem['url']
        if not url.endswith(".mp3"):
            sts, data = self.getPage(cItem['url'])
            if not sts:
                return

            url = ph.getSearchGroups(data, '<source src="(.*?)"')[0]
            if not url:
                url = ph.getSearchGroups(data, '<video.*?src="(.*?)"')[0]

            url = self.getFullUrl(url)

        return [{'name': 'direct', 'url': url, 'need_resolve': 0}]

    @handleServiceDecorator
    def handleService(self, index, refresh=0, searchPattern='', searchType=''):

        name = self.currItem.get('name', '')
        category = self.currItem.get('category', '')
        mode = self.currItem.get('mode', '')

        if name is None:
            self.listMainMenu({'name': 'category'}, 'sub_menu')
        elif category == 'sub_menu':
            self.listSubMenu(self.currItem)
        elif category == self.recentlyAddedAlbums:
            self.listRecentlyAddedAlbums(self.currItem)
        elif category == self.recentlyAddedSongs:
            self.listRecentlyAddedSongs(self.currItem)
        elif category in ('search', 'search_next_page'):
            cItem = dict(self.currItem)
            cItem.update({'search_item': False,
                          'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
        elif category == 'search_history':
            self.listsHistory({'name': 'history',
                               'category': 'search'}, 'desc', _('Type: '))
        else:
            printExc()

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)
