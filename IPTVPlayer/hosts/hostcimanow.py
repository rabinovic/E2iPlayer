# -*- coding: utf-8 -*-

import re
import base64
from urllib.parse import quote

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def gettytul():
    return 'cimanow'


class IPTVHost(HostBase):
    def __init__(self):
        super().__init__( {'history': 'cimanow', 'cookie': 'cimanow.cookie'})
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/F5GycyM/logo.png'

        self.USER_AGENT = getDefaultUserAgent()
        self.HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html'}
        self.MAIN_URL = 'https://bs.cimanow.cc/'
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True,
                              'cookiefile': self.COOKIE_FILE, 'with_metadata': True}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams=None, post_data=None):
        baseUrl = ph.std_url(baseUrl)
        if addParams is None:
            addParams = dict(self.defaultParams)
        sts, data = self.cm.getPage(baseUrl, addParams, post_data)
        if not sts:
            return sts, data
        if 'adilbo' in data:
            script_matches = re.findall('''<script.*?;.*?['"](.*?);''', data, re.DOTALL)
            int_matches = re.findall('/g.....(.*?)\)', data, re.DOTALL)
            if script_matches and int_matches:
                script = script_matches[0].replace("'", '').replace("+", '').replace("\n", '')
                split_script = script.split('.')
                decoded_page = ''

                for item in split_script:
                    decoded_element = ph.ensure_str(base64.b64decode(f'{item}=='))
                    if digit_matches := re.findall('\d+',decoded_element, re.DOTALL):
                        nb = int(digit_matches[0]) + int(int_matches[0])
                        decoded_page += chr(nb)
            data = ph.ensure_str(decoded_page, "iso-8859-1")


        return sts, data

    def mainMenu(self, cItem):

        self.listsTab([
            {'title': 'أفــلام', 'category': 'showmenu_aflam', 'icon': self.DEFAULT_ICON_URL},
            {'title': 'مسلسلات', 'category': 'showmenu_series', 'icon': self.DEFAULT_ICON_URL},
            {'title': 'وثائقي', 'category': 'showitms', 'url': self.MAIN_URL+'/?s=%D9%88%D8%AB%D8%A7%D8%A6%D9%82%D9%8A', 'icon': self.DEFAULT_ICON_URL, 'sub':0},
            {'title': 'رمضان', 'category': 'showmenu_ramadan', 'icon': self.DEFAULT_ICON_URL},
            {'title': 'الاحدث', 'category': 'showmenu_new', 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True, },
            {'category': 'search_history', 'title': _('Search history'), }],cItem)

    def showmenu_aflam(self, cItem):
        self.addDir({'title': ' أفــلام أجنبــية', 'category': 'showitms', 'url': self.MAIN_URL+'/category/افلام-اجنبية/', 'icon': self.DEFAULT_ICON_URL, 'sub':0})
        self.addDir({'title': 'أفلام عربية', 'category': 'showitms', 'url': self.MAIN_URL+'/category/افلام-عربية/', 'icon': self.DEFAULT_ICON_URL, 'sub':0})
        self.addDir({'title': 'أفلام تركية', 'category': 'showitms', 'url': self.MAIN_URL+'/category/افلام-تركية/', 'icon': self.DEFAULT_ICON_URL, 'sub':0})
        self.addDir({'title': 'أفلام هندية', 'category': 'showitms', 'url': self.MAIN_URL+'/category/افلام-هندية/', 'icon': self.DEFAULT_ICON_URL, 'sub':0})
        self.addDir({'title': 'أفلام أنيميشن', 'category': 'showitms', 'url': self.MAIN_URL+'/category/افلام-انيميشن/', 'icon': self.DEFAULT_ICON_URL, 'sub':0})

    def showmenu_series(self, cItem):
        self.addDir({'title': 'مسلسلات عربية', 'category': 'showitms', 'url': self.MAIN_URL+'/category/مسلسلات-عربية/', 'icon': self.DEFAULT_ICON_URL, 'sub':1})
        self.addDir({'title': 'مسلسلات أجنبية', 'category': 'showitms', 'url': self.MAIN_URL+'/category/مسلسلات-اجنبية/', 'icon': self.DEFAULT_ICON_URL, 'sub':1})
        self.addDir({'title': 'مسلسلات تركية', 'category': 'showitms', 'url': self.MAIN_URL+'/category/مسلسلات-تركية/', 'icon': self.DEFAULT_ICON_URL, 'sub':1})
        self.addDir({'title': 'مسلسلات أنيميشن', 'category': 'showitms', 'url': self.MAIN_URL+'/category/مسلسلات-انيميشن/', 'icon': self.DEFAULT_ICON_URL, 'sub':1})

    def showmenu_new(self, cItem):
        eelm = {'category': 'showitms', 'url': cItem['url'], 'icon': self.DEFAULT_ICON_URL}
        self.addDir(eelm)

    def showmenu_ramadan(self, cItem):
        eelm = { 'category': 'showitms', 'url': cItem['url'], 'icon': self.DEFAULT_ICON_URL}
        self.addDir(eelm)

    def showmenu1(self, cItem):
        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        data1 = re.findall('<section>.*?<span>(.*?)<.*?href="(.*?)"', data, re.S)
        if data1:
            for elm in data1:
                url = self.getFullUrl(elm[1])
                title = ph.cleanHtml(elm[0])
                if not any(word in title for word in ['قريبا','أختر وجهتك المفضلة','الاكثر مشاهدة','الاكثر اعجا','احدث الحفلات']):
                    eelm = {'title': title, 'category': 'showitms', 'url': url, 'icon': self.DEFAULT_ICON_URL}
                    self.addDir(eelm)

    def showitms(self, cItem):

        url = cItem['url']
        page = cItem.get('page', 1)

        if page > 1:
            url = url+'/page/'+str(page)+'/'
            url = url.replace('//page', '/page')

        sts, data = self.getPage(url)
        if not sts:
            return

        data1 = re.findall('<article .*?href="(.*?)"(.*?)title">(.*?)(<em>.*?)*?</li>.*?-src="(.*?)"', data, re.S)
        if data1:
            for elm in data1:
                link = self.getFullUrl(elm[0])
                title = ph.cleanHtml(elm[2])
                image = ph.std_url(elm[4])
                desc0 = elm[1] + elm[3]

                quality = re.findall('ribbon">(.*?)</li>', desc0, re.S)
                if quality:
                    quality = ph.cleanHtml(quality[0]).strip()

                year = re.findall('year">(.*?)</li>', desc0, re.S)
                if year:
                    year = ph.cleanHtml(year[0]).strip()

                genre = re.findall('<em>(.*?)</em>', desc0, re.S)
                if genre:
                    genre = ph.cleanHtml(genre[0]).strip()

                desc = f'{year} {quality} {genre}'
                if title == '':
                    continue
                eelm = {'good_for_fav': True, 'title': title, 'category': 'showelms', 'url': link, 'icon':image, 'desc':desc, 'with_mini_cover':True}
                if cItem['sub']==0:
                    self.addVideo(eelm)
                else:
                    self.addDir(eelm)

            if len(data1) == 18:
                eelm = cItem|{'title': _("Next page"), 'category': 'showitms', 'url': cItem['url'], 'icon':self.DEFAULT_ICON_URL, 'page': page+1}
                self.addNext(eelm)

    def showelms(self, cItem):
        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return
        data0 = re.findall('<ul class.{,30}?id="eps">(.*?)</ul>', data, re.S)
        if data0:
            data1 = re.findall('<li.*?href="(.*?)".*?(?:alt="logo"|</h3>).*?src="(.*?)".*?alt="(.*?)"', data0[0], re.S)
            if data1:
                for elm in data1:
                    url = self.getFullUrl(elm[0])
                    title = ph.cleanHtml(elm[2])
                    image = ph.std_url(elm[1])
                    if title == '':
                        continue
                    desc, title = ph.uniform_title(title)
                    eelm = {'good_for_fav': True, 'title': title, 'url': url, 'icon': image, 'desc':desc}
                    self.addVideo(eelm)

    def getCustomLinksForVideo(self, cItem):
        urlTab = []

        if linksTab := self.cacheLinks.get(cItem['url'], []):
            return linksTab

        url = cItem['url'] + 'watching'
        sts, data = self.getPage(url)
        if sts:
            #printDBG("data: [%s]" % data)
            # watch
            data0 = re.findall('''<li .*?data-index="(.*?)".*?data-id="(.*?)".*?>(.*?)</li>''', data, re.S)
            if data0:
                for idx, id_, name in data0:
                    urlTab.append({'name': '|Watch Server| ' + name, 'url': idx + '|' + id_, 'need_resolve': 1})

            Liste_els = re.findall('id="download">(.*?)</ul>', data, re.S)
            for elm in Liste_els:
                Tag    = '|Down|'
                L_els = re.findall('href="(.*?)".*?</i>(.*?)</a>', elm, re.S)
                for (url0,titre) in L_els:
                    resolve = 1
                    if url0.endswith('.mp4'):
                        url0 = strwithmeta(url0, {'Referer':url})
                        resolve = 0
                    urlTab.append({'name':Tag+ph.cleanHtml(titre), 'url':url0, 'need_resolve': resolve})

        if urlTab:
            self.cacheLinks[cItem['url']] = urlTab

        return urlTab

    def getCustomVideoLinks(self, url):
        urlTab=[]
        split = url.split('|')
        if split:
            url = self.MAIN_URL+'/wp-content/themes/Cima%20Now%20New/core.php?action=switch&index='+split[0]+'&id='+split[1]
            addParams = dict(self.defaultParams)
            header = dict(addParams['header'])
            header['Referer'] = self.MAIN_URL
            addParams.update({'header':header})
            sts, data = self.getPage(url,addParams)
            if sts:
                Liste_els_3 = re.findall('src="(.+?)"', data, re.S|re.IGNORECASE)
                if Liste_els_3:
                    URL = Liste_els_3[0]
                    if URL.startswith('//'): URL='http:'+ URL
                    if 'cimanow' in URL or 'newcima' in URL:
                        host = 'https://' + URL.split('/')[2]
                        addParams['header']['Referer'] = self.MAIN_URL+'/'
                        sts, data = self.getPage(URL,addParams)
                        if sts:
                            Liste_els = re.findall('source.*?src="(.*?)".*?size="(.*?)"', data, re.S|re.IGNORECASE)
                            for elm in Liste_els:
                                url_ = elm[0]
                                if not(url_.startswith('http')): url_ = host + quote(url_)
                                URL_= strwithmeta(url_, {'Referer':host})
                                urlTab.append({'url':URL_,'name':elm[1]})
                    else:
                        urlTab = self.up.getVideoLinkExt(URL)
        else:
            urlTab = self.up.getVideoLinkExt(url)

        return urlTab

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("Cinemanow.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)

        page = cItem.get('page', 1)
        searchPattern = cItem.get('searchPattern', searchPattern)

        url = self.MAIN_URL+'/page/'+str(page)+'/?s='+ph.std_url(searchPattern)

        sts, data = self.getPage(url)
        if not sts:
            return

        data0 = re.findall('<article .*?href="(.*?)"(.*?)title">(.*?)(<em>.*?)*?</li>.*?data-src="(.*?)"', data, re.S)
        if data0:
            for elm in data0:
                link = self.getFullUrl(elm[0])
                title = ph.cleanHtml(elm[2])
                image = ph.std_url(elm[4])
                desc0 = elm[1] + elm[3]

                quality = re.findall('ribbon">(.*?)</li>', desc0, re.S)
                if quality:
                    quality = ph.cleanHtml(quality[0]).strip()

                year = re.findall('year">(.*?)</li>', desc0, re.S)
                if year:
                    year = ph.cleanHtml(year[0]).strip()

                genre = re.findall('<em>(.*?)</em>', desc0, re.S)
                if genre:
                    genre = ph.cleanHtml(genre[0]).strip()

                desc = f'{year} {quality} {genre}'
                if title == '':
                    continue
                eelm = {'good_for_fav': True, 'title': title, 'category': 'showelms', 'url': link, 'icon': image, 'desc':desc}
                self.addDir(eelm)

            if len(data0) == 18:
                eelm = {'title': _("Next page"), 'category': 'search_next_page', 'icon':self.DEFAULT_ICON_URL, 'page': page+1, 'searchPattern':searchPattern}
                self.addNext(eelm)


    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)
