# -*- coding: utf-8 -*-

import re
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc

import base64
def gettytul():
    return 'TvFun'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__({'history': 'TvFun.co'})

        self.MAIN_URL = 'https://www.tv-f.com/'
        self.DEFAULT_ICON = self.MAIN_URL + '/uploads/custom-logo.png'

        self.seasonEpsCache = {}

    def getPage(self, baseUrl, addParams=None, post_data=None):
        baseUrl = ph.std_url(baseUrl)
        if addParams is None:
            addParams = dict(self.defaultParams)
        return self.cm.getPage(baseUrl, addParams, post_data)

    def mainMenu(self, cItem):
        MAIN_CAT_TAB = [
            {'category': 'show_series', 'title': 'مسلسلات عربية', 'url': self.MAIN_URL + 'cat/mosalsalat-3arabia,3/', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'مسلسلات أجنبية', 'url': self.MAIN_URL + 'cat/mosalsalat-latinia/', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'مسلسلات تركية', 'url': self.MAIN_URL + 'cat/mosalsalat-torkia,3/', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2023', 'url': self.MAIN_URL + 'ts/mosalsalat-ramadan-2023/', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2022', 'url': self.MAIN_URL + 'ts/mosalsalat-ramadan-2022/', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2021', 'url': self.MAIN_URL + 'ts/mosalsalat-ramadan-2021/', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'الكاميرا الخفية', 'url': self.MAIN_URL + 'ts/hidden-camera/', 'icon':self.DEFAULT_ICON_URL},

            {'category': 'search', 'title': _('Search'), 'search_item': True, 'icon': self.DEFAULT_ICON_URL },
            {'category': 'search_history', 'title': _('Search history')     , 'icon': self.DEFAULT_ICON_URL }
        ]

        self.listsTab(MAIN_CAT_TAB, cItem)


    def show_series(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        result = re.findall('<div class="serie-thumb">.*?<a href="([^"]+?)" title="([^"]+?)".*?<img.*?src="([^"]+?)".*?</div></div>', data, re.S)
        for e in result:
            link = e[0]
            title = e[1].replace("مشاهدة وتحميل","").replace("اون لاين","").replace("مترجمة","").replace("مترجم","")
            icon = ph.std_url(e[2])

            year = ''

            self.addDir(cItem | {'good_for_fav':True, 'title': title, 'category':'show_eps', 'url':self.fixLink(link),'desc':year, 'icon':icon, 'with_mini_cover':True })

        pagination = re.search('<ul class="pagination".*?</ul>', data, re.S)
        if pagination:
            pagination = pagination[0]
            currentPage = re.search('<ul class="pagination".*?class="current"', pagination, re.S)
            if currentPage:
                nextPage = re.search('<ul class="pagination".*?class="current".*?</a>.*?<a href="([^"]+?)"', pagination, re.S)
                if nextPage:
                    nextPage = nextPage[1]
            else:
                nextPage = url +'/page,2/'
        if nextPage:
            self.addNext(cItem | {'url': self.fixLink(nextPage)})

    def show_eps(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        #data0 = re.search('class="headline">(.+?)class="head-title"> أخر الحلقات </h3>', data, re.S)
        #if data0:
        data1 = re.findall('<div class="video-thumb">.*?<a href="([^"]+?)".*?title="([^"]+?)">.*?<img.*?src="([^"]+?)"', data, re.S)
        if data1:
            for elem in data1:
                title = elem[1]
                link = elem[0]
                self.addVideo(cItem | {'title': title, 'url':self.fixLink(link)})

        pagination = re.search('<ul class="pagination".*?</ul>', data, re.S)
        if pagination:
            pagination = pagination[0]
            currentPage = re.search('<ul class="pagination".*?class="current"', pagination, re.S)
            if currentPage:
                nextPage = re.search('<ul class="pagination".*?class="current".*?</a>.*?<a href="([^"]+?)"', pagination, re.S)
                if nextPage:
                    nextPage = nextPage[1]
            else:
                nextPage = url +'/page,2/'
        if nextPage:
            self.addNext(cItem | {'url': self.fixLink(nextPage)})

    def fixLink(self, link):
        result = link
        if result.startswith("//"):
            result = 'https:' + result
        elif result.startswith("/"):
            result = self.MAIN_URL + result
        return result

    def getCustomLinksForVideo(self,cItem):
        urlTab = []

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        data0 = re.findall("PGlmcmFt(.+?)'", data, re.S)
        for e in data0:

            m3url = "PGlmcmFt" + e
            decoded = base64.b64decode(m3url).decode()
            m = re.search('title="(.+?)" src="(.+?)".+?allowfullscreen', decoded, re.S)
            if m:
                title = m[1]
                link = m[2]

                if link.startswith('//'):
                    link = 'https:' + link
                link = link.replace("https://dai.ly/","https://www.dailymotion.com/video/")
                urlTab.append({'name': title, 'url': link, 'need_resolve':1 })
        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        urlTab = []
        urlTab = self.up.getVideoLinkExt(videoUrl)
        return urlTab


    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("TvFun.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)

        url = cItem.get('url', None)
        url = url if url else f'{self.MAIN_URL}/q/{ph.std_url(searchPattern)}'

        self.show_series(cItem |  {'url': url})


    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)