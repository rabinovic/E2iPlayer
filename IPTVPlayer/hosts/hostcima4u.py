# -*- coding: utf-8 -*-

import re

from Components.config import config, ConfigText

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.libs import ph

def gettytul():
    return 'Cima4u'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'cima4u', 'cookie': 'cima4u.cookie'})

        self.MAIN_URL = 'https://www.cima4u.pro/' # alternative 'https://cima4u.film'
        self.MAIN_URL2  = 'http://cima4u.mx'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/4FCCKvf/cima4u.png'

        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'
        self.HTTP_HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html', 'Accept-Encoding': 'gzip, deflate', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}
        self.defaultParams = {'header':self.HTTP_HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        self.cacheLinks = {}

    def mainMenu(self, cItem):
        printDBG("Cima4u.listMainMenu")

        MAIN_CAT_TAB = [
            {'category': 'showmenu1', 'title': 'أفلام', 'icon': self.DEFAULT_ICON_URL, 'sub-mode':0},
            {'category': 'showmenu1', 'title': 'مسلسلات', 'icon': self.DEFAULT_ICON_URL, 'sub-mode':1},
            {'category':'showitms','title': 'مصارعة حرة','url':self.MAIN_URL+'/category/%d9%85%d8%b5%d8%a7%d8%b1%d8%b9%d8%a9-%d8%ad%d8%b1%d8%a9-wwe/'},
            {'category':'showitms','title': 'برامج تلفزيونية','url':self.MAIN_URL+'/category/مسلسلات-series/برامج-تليفزيونية-tv-shows/'},
            {'category':'showitms','title': 'افلام و مسلسلات Netflix','url':self.MAIN_URL+'/netflix/'},
            {'category':'showitms','title': 'افلام النجوم','url':self.MAIN_URL+'/actors/','sub-mode':1},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history')}, ]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def getPage(self, baseUrl, addParams=None, post_data=None):
        if not addParams or addParams == {}:
            addParams = dict(self.defaultParams)
        baseUrl = self.cm.iriToUri(baseUrl)
        sts, data = self.cm.getPageCFProtection(baseUrl, addParams, post_data)
        userAgent = self.defaultParams.get('header', {}).get('User-Agent', '')
        if data.meta.get('cf_user', self.USER_AGENT) != userAgent:
            self.defaultParams['header']['User-Agent'] = data.meta['cf_user']

        return sts, data

    def showmenu1(self, cItem):
        gnr=cItem['sub-mode']
        sts, data = self.getPage(ph.std_url(self.MAIN_URL))
        if sts:
            cat_film_data=re.findall('<ul class="sub-menu">(.*?)</ul>', data, re.S)
            if cat_film_data:
                data2=re.findall('<li.*?href="(.*?)">(.*?)<', cat_film_data[gnr], re.S)
                for (url,titre) in data2:
                    if not url.startswith('http'):
                        url=self.MAIN_URL+url
                    Params= cItem | {'category' : 'showitms','url': url,'title':titre}
                    self.addDir(Params)

    def showitms(self, cItem):
        url = cItem['url']
        sts, data = self.getPage(ph.std_url(url))
        if sts:
            if '/actors/' in url:
                if Liste_els:= re.search('class="ActorsItems">(.*?)(?:</ul|<ul)', data, re.S):
                    Liste_els = re.findall('<li.*?href="(.*?)".*?title="(.*?)".*?url\((.*?)\).*?>(.*?)</a>'	, Liste_els[1], re.S)
                    for (url,titre,image,desc) in Liste_els:
                        titre = ph.cleanHtml(titre)
                        desc = ph.cleanHtml(desc.replace('</div>','\\n').replace('</i>','\\n')).strip()
                        self.addDir({'good_for_fav':True,'category' : 'host2','url': url,'title':titre,'desc':desc,'icon':image, 'with_mini_cover':True})
            else:
                if page_content:= re.search('<div class="PageContent">.*', data, re.S):
                    if films_list:= re.findall('class="MovieBlock">.*?href="([^"]+?)".*?background-image:url\(([^\()]+?)\)(.*?)</li>', page_content[0], re.S):
                        for (url,image,data0) in films_list:

                            if titre_:= re.search('BoxTitleInfo">.*?</div>(.*?)</a>', data0, re.S):
                                desc, titre =ph.uniform_title(ph.cleanHtml(titre_[1]))
                            else:
                                titre = '!!'

                            if genre_:= re.search('class="Genres">(.*?)</div>', data0, re.S):
                                genre = ph.cleanHtml(genre_[1])
                            else:
                                genre = '!!'

                            if cat_:= re.search('class="Category">(.*?)</div>', data0, re.S):
                                cat = ph.cleanHtml(cat_[1])
                            else:
                                cat = '!!'

                            if desc.strip()!='':
                                desc = 'Info: '+desc

                            self.addDir(cItem | {'good_for_fav':True,'category' : 'showelms','url': url,'title':titre,'desc':desc,'icon':image,'EPG':True, 'with_mini_cover':True})

            data1=re.search('<a class="next page-numbers" href="([^"]+?)"', data, re.S)
            if data1:
                self.addNext(cItem | {'title':_('Next Page'),'url':data1[1]} )

    def showelms(self, cItem):
        url = cItem['url']
        sts, data0 = self.getPage(ph.std_url(url))
        if sts:
            Liste_els = re.search('class="StatsMain">.*?</ul>.*?href="(http.*?)".*?WatchIcon">', data0, re.S)
            if Liste_els:
                URL = Liste_els[1]
                sts, data = self.getPage(ph.std_url(URL))
                if sts:
                    if ('/tag/' in URL) or ('/packs/' in URL):
                        pat = 'class="MovieBlock">.*?href="(.*?)".*?image:url\((.*?)\)(.*?)</li>'
                        films_list = re.findall(pat, data, re.S)
                        if films_list:
                            for (url,image,data0) in films_list:
                                if titre_:= re.search('BoxTitleInfo">.*?</div>(.*?)</a>', data0, re.S):
                                    desc, titre =ph.uniform_title(ph.cleanHtml(titre_[1]))
                                else: titre = '!!'

                                if genre_:= re.search('class="Genres">(.*?)</div>', data0, re.S):
                                    genre = ph.cleanHtml(genre_[1])
                                else:
                                    genre = '!!'

                                cat_ = re.search('class="Category">(.*?)</div>', data0, re.S)
                                if cat_:
                                    cat = ph.cleanHtml(cat_[1])
                                else: cat = '!!'

                                self.addDir(cItem | {'good_for_fav':True,'url': url,'title':titre,'desc':desc,'icon':image,'EPG':True, 'with_mini_cover':True})
                    else:
                        Liste_tr = re.search('<iframe.*?src="(.*?)"', data0, re.S)
                        if Liste_tr:
                            self.addVideo(cItem | {'url': Liste_tr[1],'title':'Trailer'})
                        if '/episode/' in URL.lower():
                            films_list = re.findall('EpisodeItem.*?href="(.*?)".*?>(.*?)</a>', data, re.S)
                            if films_list:
                                for (url,titre) in films_list:
                                    titre = ph.cleanHtml(titre)
                                    self.addVideo(cItem | {'good_for_fav':True,'url': url,'title':titre})
                        elif  '/video/' in URL.lower():
                            self.addVideo(cItem | {'good_for_fav':True,'good_for_fav':True,'url': URL})

    def listSearchResult(self, cItem, searchPattern, searchType):
        elms = []
        url_='http://cima4u.io'+'/search/'+searchPattern+'/page/'+str(page)+'/'
        sts, data = self.getPage(url_)
        if sts:
            pat = 'class="MovieBlock">.*?href="(.*?)".*?image:url\((.*?)\)(.*?)</li>'
            films_list = re.findall(pat, data, re.S)
            if films_list:
                for (url,image,data0) in films_list:
                    if titre_:= re.search('BoxTitleInfo">.*?</div>(.*?)</a>', data0, re.S):
                        titre = ph.cleanHtml(titre_[1])
                    else:
                        titre = '!!'

                    if genre_:= re.search('class="Genres">(.*?)</div>', data0, re.S):
                        genre = ph.cleanHtml(genre_[1])
                    else:
                        genre = '!!'

                    if cat_:= re.search('class="Category">(.*?)</div>', data0, re.S):
                        cat = ph.cleanHtml(cat_[1])
                    else:
                        cat = '!!'

                    desc = 'Genre: ' +ph.cleanHtml(genre)+'\n'+'Cat: '+ph.cleanHtml(cat)
                    desc0,titre = ph.uniform_title(titre,1)
                    desc = desc0 + desc
                    elm = {'good_for_fav':True,'url': url,'title':titre,'desc':desc,'icon':image,'EPG':True, 'with_mini_cover':True}
                    elms.append(elm)
                    self.addDir(elm)
        return elms

    def getCustomLinksForVideo(self, cItem):
        urlTab = []
        url=cItem['url']
        if cItem['title']=='Trailer':
            return self.up.getVideoLinkExt(url)
        else:
            #URL = urllib.quote(URL).replace('%3A//','://')
            sts, data = self.getPage(ph.std_url(url))
            if sts:
                Liste_els =  re.findall('data-link="(.*?)".*?>(.*?)</a>', data, re.S)
                for (code,host_) in Liste_els:
                    host_ = ph.cleanHtml(host_).strip()
                    if 'thevids'   in host_.lower(): host_= 'thevideobee'
                    if 'up-stream' in host_.lower(): host_= 'uptostream'
                    urlTab.append({'name':host_, 'url': code, 'need_resolve':1})
        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        urlTab = []
        sUrl = self.MAIN_URL2+'/structure/server.php?id='+videoUrl
        post_data = {'id':videoUrl}
        sts, data = self.getPage(ph.std_url(sUrl), post_data=post_data)
        if not sts:
            return urlTab

        if Liste_els_3:= re.search('src="(.*?)"', data, re.S):
            urlTab = self.up.getVideoLinkExt(Liste_els_3[1].replace('\r',''))

        return urlTab

    def getArticle(self, cItem):
        printDBG("Cima4u.getCustomVideoLinks [%s]" % cItem)
        otherInfo1 = {}
        title = cItem['title']
        icon = cItem.get('icon', '')
        desc = cItem.get('desc', '')
        sts, data = self.getPage(ph.std_url(cItem['url']))
        if sts:
            lst_dat = re.findall('class="font-size-16 text-white mt-2">(.*?)</span>', data, re.S)
            for elm in lst_dat:
                elm = ph.cleanHtml(elm)
                if (':' in elm) and ('مدة' in elm):
                    otherInfo1['duration'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('اللغة' in elm):
                    otherInfo1['language'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('ترجمة' in elm):
                    otherInfo1['translation'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('جودة' in elm):
                    otherInfo1['quality'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('انتاج' in elm):
                    otherInfo1['production'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('سنة' in elm):
                    otherInfo1['year'] = elm.split(':', 1)[1].strip()

            lst_dat0 = re.search('class="font-size-16 d-flex align-items-center mt-3">(.*?)</div>', data, re.S)
            if lst_dat0:
                otherInfo1['genre'] = ph.cleanHtml(lst_dat0[1])

            lst_dat0 = re.search('header-link text-white">.*?<h2>(.*?)</div>', data, re.S)
            if lst_dat0:
                desc = ph.cleanHtml(lst_dat0[1])

        return [{'title': title, 'text': desc, 'images': [{'title': '', 'url': icon}], 'other_info': otherInfo1}]


    def withArticleContent(self, cItem):
        return cItem.get('priv_has_art', False)

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)
