# -*- coding: utf-8 -*-

import re
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import getDirectM3U8Playlist
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent

def gettytul():
    return 'Al3arab'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__({'history': 'alarab.com', 'cookie': 'alarab.cookie'})

        self.MAIN_URL = 'https://vod2.alarab.com/'

        self.USER_AGENT = getDefaultUserAgent()
        self.HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html'}
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True,
                              'cookiefile': self.COOKIE_FILE, 'with_metadata': True}

    def getPage(self, baseUrl, addParams=None, post_data=None):
        baseUrl = ph.std_url(baseUrl)
        if addParams is None:
            addParams = dict(self.defaultParams)
        return self.cm.getPage(baseUrl, addParams, post_data)

    def mainMenu(self, cItem):
        MAIN_CAT_TAB = [
            {'category': 'show_movies', 'title':'افلام عربية', 'url': self.MAIN_URL + '/view-1/%D8%A7%D9%81%D9%84%D8%A7%D9%85-%D8%B9%D8%B1%D8%A8%D9%8A%D8%A9', 'icon':self.DEFAULT_ICON_URL, 'page':2},
            {'category': 'show_movies', 'title':'افلام مصرية قديمة', 'url': self.MAIN_URL + '/view-6181/%D8%A7%D9%81%D9%84%D8%A7%D9%85-%D9%85%D8%B5%D8%B1%D9%8A%D8%A9-%D9%82%D8%AF%D9%8A%D9%85%D8%A9', 'icon':self.DEFAULT_ICON_URL, 'page':2},
            {'category': 'show_movies', 'title':'افلام اجنبية', 'url': self.MAIN_URL + '/view-5553/%D8%A7%D9%81%D9%84%D8%A7%D9%85-%D8%A7%D8%AC%D9%86%D8%A8%D9%8A%D8%A9', 'icon':self.DEFAULT_ICON_URL, 'page':2},

            {'category': 'show_series', 'title': 'رمضان 2024', 'url': self.MAIN_URL + '/view-8/%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA-%D8%B1%D9%85%D8%B6%D8%A7%D9%86-2024', 'icon':self.DEFAULT_ICON_URL, 'page':2},
            {'category': 'show_series', 'title': 'رمضان 2023', 'url': self.MAIN_URL + '/view-8/%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA-%D8%B1%D9%85%D8%B6%D8%A7%D9%86-2023', 'icon':self.DEFAULT_ICON_URL, 'page':2},
            {'category': 'show_series', 'title': 'مسلسلات أجنبية', 'url': self.MAIN_URL + '/view-1951/%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA-%D8%A7%D8%AC%D9%86%D8%A8%D9%8A%D8%A9', 'icon':self.DEFAULT_ICON_URL, 'page':2},
        ]

        self.listsTab(MAIN_CAT_TAB, cItem)

    def show_movies(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        page = cItem['page']

        sts, data = self.getPage(url)
        if not sts:
            return

        result = re.findall('description-box.*?a href="([^"]+?)".*?img.*?data-src="([^"]+?)".*?<h5><a.*?>(.*?)</a>', data, re.S)
        for e in result:
            link = self.MAIN_URL + ph.std_url(e[0])
            icon = ph.std_url(e[1])
            title = e[2].replace("مشاهدة","").replace("HD رمضان 2022","").replace("HD رمضان 2023","").replace("HD رمضان 2024","").replace("حلقات كاملة","").replace("مترجمة","").replace("مترجم","").replace("فيلم","").replace("برنامج","").replace("اون لاين","").replace("WEB-DL","").replace("BRRip","").replace("720p","").replace("HD-TC","").replace("HDRip","").replace("HD-CAM","").replace("DVDRip","").replace("BluRay","").replace("1080p","").replace("WEBRip","").replace("WEB-dl","").replace("مباشرة","").replace("HD","").replace("انتاج ","").replace("مشاهدة","").replace("مسلسل","").replace("انمي","").replace("مترجمة","").replace("مترجم","").replace("فيلم","").replace("والأخيرة","").replace("مدبلج للعربية","مدبلج").replace("والاخيرة","").replace("كاملة","").replace("حلقات كاملة","").replace("اونلاين","").replace("مباشرة","").replace("انتاج ","").replace("جودة عالية","").replace("كامل","").replace("HD","").replace("السلسلة الوثائقية","").replace("الفيلم الوثائقي","").replace("اون لاين","").replace("الحلقة "," E").replace("حلقة "," E")
            self.addVideo({'good_for_fav':True, 'title': title, 'url':link,'icon':icon, 'with_mini_cover':True})

        pagesMatch = re.findall('<a class="tsc_3d_button .*?" href="([^"]+?)".*?>(.*?)</a>', data, re.S)
        if pagesMatch:
            pagesDict = {int(y):x for (x,y) in  pagesMatch}

            nextPage = pagesDict.get(page, None)

            if nextPage:
                self.addNext(cItem| {'url': self.MAIN_URL + nextPage, 'page': page+1})

    def show_series(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        page = cItem['page']

        sts, data = self.getPage(url)
        if not sts:
            return

        result = re.findall('description-box.*?a href="([^"]+?)".*?img.*?data-src="([^"]+?)".*?<h2><a.*?>(.*?)</a></h2>', data, re.S)
        for e in result:
            link = self.MAIN_URL + e[0]
            icon = ph.std_url(e[1])
            title = e[2].replace("مشاهدة","").replace("HD رمضان 2022","").replace("HD رمضان 2023","").replace("حلقات كاملة","").replace("مترجمة","").replace("مترجم","").replace("فيلم","").replace("برنامج","").replace("اون لاين","").replace("WEB-DL","").replace("BRRip","").replace("720p","").replace("HD-TC","").replace("HDRip","").replace("HD-CAM","").replace("DVDRip","").replace("BluRay","").replace("1080p","").replace("WEBRip","").replace("WEB-dl","").replace("مباشرة","").replace("HD","").replace("انتاج ","").replace("مشاهدة","").replace("مسلسل","").replace("انمي","").replace("مترجمة","").replace("مترجم","").replace("فيلم","").replace("والأخيرة","").replace("مدبلج للعربية","مدبلج").replace("والاخيرة","").replace("كاملة","").replace("حلقات كاملة","").replace("اونلاين","").replace("مباشرة","").replace("انتاج ","").replace("جودة عالية","").replace("كامل","").replace("HD","").replace("السلسلة الوثائقية","").replace("الفيلم الوثائقي","").replace("اون لاين","").replace("الحلقة "," E").replace("حلقة "," E")
            self.addDir({'good_for_fav':True, 'title': title, 'category':'show_eps', 'url':link,'icon':icon, 'with_mini_cover':True, 'page':2})

        pagesMatch = re.findall('<a class="tsc_3d_button .*?" href="([^"]+?)".*?>(.*?)</a>', data, re.S)
        if pagesMatch:
            pagesDict = {int(y):x for (x,y) in  pagesMatch}

            nextPage = pagesDict.get(page, None)

            if nextPage:
                self.addNext(cItem| {'url': self.MAIN_URL + nextPage, 'page': page+1})


    def show_eps(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        page = cItem['page']

        sts, data = self.getPage(ph.std_url(url))
        if not sts:
            return

        result = re.findall('description-box.*?a href="([^"]+?)".*?img.*?data-src="([^"]+?)".*?<h5><a.*?>(.*?)</a>', data, re.S)
        for e in result:
            link = self.MAIN_URL + ph.std_url(e[0])
            icon = ph.std_url(e[1])
            title = e[2].replace("مشاهدة","").replace("HD رمضان 2022","").replace("HD رمضان 2023","").replace("حلقات كاملة","").replace("مترجمة","").replace("مترجم","").replace("فيلم","").replace("برنامج","").replace("اون لاين","").replace("WEB-DL","").replace("BRRip","").replace("720p","").replace("HD-TC","").replace("HDRip","").replace("HD-CAM","").replace("DVDRip","").replace("BluRay","").replace("1080p","").replace("WEBRip","").replace("WEB-dl","").replace("مباشرة","").replace("HD","").replace("انتاج ","").replace("مشاهدة","").replace("مسلسل","").replace("انمي","").replace("مترجمة","").replace("مترجم","").replace("فيلم","").replace("والأخيرة","").replace("مدبلج للعربية","مدبلج").replace("والاخيرة","").replace("كاملة","").replace("حلقات كاملة","").replace("اونلاين","").replace("مباشرة","").replace("انتاج ","").replace("جودة عالية","").replace("كامل","").replace("HD","").replace("السلسلة الوثائقية","").replace("الفيلم الوثائقي","").replace("اون لاين","").replace("الحلقة "," E").replace("حلقة "," E")
            self.addVideo({'good_for_fav':True, 'title': title, 'url':link,'icon':icon, 'with_mini_cover':True})

        pagesMatch = re.findall('<a class="tsc_3d_button .*?" href="([^"]+?)".*?>(.*?)</a>', data, re.S)
        if pagesMatch:
            pagesDict = {int(y):x for (x,y) in  pagesMatch}

            nextPage = pagesDict.get(page, None)

            if nextPage:
                self.addNext(cItem| {'url': self.MAIN_URL + nextPage, 'page': page+1})

    def getCustomLinksForVideo(self,cItem):
        urlTab = []

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        match = re.search('<iframe class="resp-iframe" src="(?P<src>[^"]+?)"', data, re.S)
        if not match:
            match = re.search('<script.*?>.*"(src|embedUrl)":.*?"(?P<src>[^"]+?)".*?</script>', data, re.S)
        if match:
            sts, data = self.getPage(match.group('src'))
            if not sts:
                return

        if m3u8 := re.search('<script.*?>.*"(src|embedUrl)":.*?"([^"]+?)".*?</script>', data, re.S):
            hlsLinksTab = getDirectM3U8Playlist(m3u8[2], checkExt=True, checkContent=True, sortWithMaxBitrate=999999999)
            urlTab.extend(hlsLinksTab)

        return urlTab


    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)

    def listSearchResult(self, cItem, searchPattern, searchType):
        return super().listSearchResult(cItem, searchPattern, searchType)