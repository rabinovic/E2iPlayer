# -*- coding: utf-8 -*-

import re
import urllib.parse
import urllib.error
import base64
from copy import deepcopy

###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, RetHost, ArticleContent, handleServiceDecorator
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.libs import ph
###################################################

def gettytul():
    return 'https://www3.xcine.io/'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'XCine.me', 'cookie': 'XCineMe.cookie'})
        self.USER_AGENT = getDefaultUserAgent()
        self.HEADER = {'User-Agent': self.USER_AGENT}
        self.defaultParams = {'header': self.HEADER,'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.filtersCache = {'genre': [], 'country': [], 'sort': []}

        self.DEFAULT_ICON = "https://www3.xcine.io/themes/hdfilmev4/desktop/assets/img/logo-top.png"
        self.MAIN_URL = 'https://www3.xcine.io/'

        self.SEARCH_URL = self.MAIN_URL + 'search?'

        self.MAIN_CAT_TAB = [{'icon': self.DEFAULT_ICON, 'category': 'list_filters', 'filter': 'genre', 'title': _('Movies'), 'url':self.MAIN_URL + 'filme1?'},
                        {'icon': self.DEFAULT_ICON, 'category': 'list_filters', 'filter': 'genre', 'title': _('Series'), 'url': self.MAIN_URL + 'serien1?'},
                        {'icon': self.DEFAULT_ICON, 'category': 'list_filters', 'filter': 'genre', 'title': _('Trailers'), 'url': self.MAIN_URL + 'trailer?'},
                        {'icon': self.DEFAULT_ICON, 'category': 'search', 'title': _('Search'), 'search_item': True},
                        {'icon': self.DEFAULT_ICON, 'category': 'search_history', 'title': _('Search history')}]

    def selectDomain(self):
        return self.MAIN_URL

    def getPage(self, baseUrl, params=None, post_data=None):
        if params is None:
            params = self.defaultParams
        return self.cm.getPage(baseUrl, params, post_data)

    def getPageCF(self, baseUrl, params=None, post_data=None):
        if params is None:
            params = self.defaultParams
        params['cloudflare_params'] = {'domain': 'xcine.me', 'cookie_file': self.COOKIE_FILE, 'User-Agent': self.USER_AGENT, 'full_url_handle': self.getFullUrl}
        return self.cm.getPageCFProtection(baseUrl, params, post_data)


    def fillFiltersCache(self, cItem):
        printDBG("XCineMe.fillFiltersCache")
        self.filtersCache = {'genre': [], 'country': [], 'sort': []}

        params = dict(self.defaultParams)
        params['header']=dict(self.HEADER)
        params['header'].update({'Referer': self.MAIN_URL, "accept-encoding": "gzip", "accept": "text/html"})

        sts, data = self.getPageCF(cItem['url'], params)
        if not sts:
            return

        for filter in [{'m': 'name="category"', 'key': 'genre'}, {'m': 'name="country"', 'key': 'country'}, {'m': 'name="sort"', 'key': 'sort'}]:
            filterData = ph.findall(data, '<select class="orderby" ' + filter['m'] + '>', '</select>')
            optionsData = ph.findall(filterData[0], ('<option', '>'), '</option>')
            optionsData.sort()

            old_value = ""
            for item in optionsData:
                title = ph.cleanHtml(item)
                value = ph.getSearchGroups(item, '''value=['"]([^'^"]+?)['"]''')[0]
                if value == '':
                    continue

                if value != old_value:
                    old_value = value
                    self.filtersCache[filter['key']].append({'title': title, filter['key']: value})

            if len(self.filtersCache[filter['key']]) and filter['key'] != 'sort':
                self.filtersCache[filter['key']].insert(0, {'title': _('--All--'), filter['key']: ''})

        orderLen = len(self.filtersCache['sort'])
        for idx in range(orderLen):
            item = deepcopy(self.filtersCache['sort'][idx])
            # desc
            self.filtersCache['sort'][idx].update({'title': '\u2193 ' + self.filtersCache['sort'][idx]['title'], 'sort_type': 'desc'})
            # asc
            item.update({'title': '\u2191 ' + item['title'], 'sort_type': 'asc'})
            self.filtersCache['sort'].append(item)


    def listFilters(self, cItem, nextCategory, nextFilter):
        filter = cItem.get('filter', '')
        printDBG("XCineMe.listFilters filter[%s] nextFilter[%s]" % (filter, nextFilter))
        tab = self.filtersCache.get(filter, [])
        if len(tab) == 0:
            self.fillFiltersCache(cItem)
        tab = self.filtersCache.get(filter, [])
        params = dict(cItem)
        params['category'] = nextCategory
        params['filter'] = nextFilter
        self.listsTab(tab, params)

    def listItems(self, cItem, nextCategory):
        printDBG("XCineMe.listItems")

        page = cItem.get('page', 1)
        url = cItem['url']

        params = dict(self.defaultParams)
        params['header']=dict(self.HEADER)
        params['header'].update({"Origin": self.MAIN_URL, "Referer": url})

        query = {}
        if 'search_pattern' in cItem:
            query = {'key': cItem['search_pattern'], 'page': page}
        else:
            query = {'page': page, 'category': cItem['genre'], 'country': cItem['country'], 'sort': cItem['sort'], 'sort_type': cItem['sort_type']}

        url += urllib.parse.urlencode(query)
        sts, data = self.getPageCF(url, post_data={'load': 'full-page'})
        if not sts:
            return

        data0=re.findall('<a href="([^"]+?)" title="([^"]+?)" class="film-small">.+?data-src="([^"]+?)".+?</a>', data, re.S)
        if data0:
            for elem in data0:
                url=elem[0]
                title = elem[1]
                img=elem[2]
                params = dict(cItem)
                params.update({'good_for_fav': True, 'category': nextCategory, 'title': title, 'url': self.getFullUrl(url), 'icon': self.getFullUrl(img), 'with_mini_cover':True})
                self.addDir(params)

        if page:
            params = dict(cItem)
            params.update({'page': page+1})
            self.addNext(params)

    def exploreItem(self, cItem):
        printDBG("XCineMe.exploreItem")

        addParams = dict(self.defaultParams)
        addParams['header']=dict(self.HEADER)
        addParams['header'].update({ 'Origin': self.MAIN_URL, 'Referer':cItem['url']})
        sts, data = self.getPageCF(cItem['url'], addParams)
        if not sts:
            return

        movieId = ph.getSearchGroups(data, '''data-movie-id=['"]([^'^"]+?)['"]''')[0]
        episodeId = ph.getSearchGroups(data, '''data-episode-id=['"]([^'^"]+?)['"]''')[0]

        trailerUrl = ''
        linksPageUrl = ''

        links = ph.getAllItemsBetweenMarkers(data, ('<a', '>', 'class="play-'),'</a>')
        for l in links:
            url = ph.getSearchGroups(l, '''href=['"]([^'^"]+?)['"]''')[0]
            if 'play-trailer' in l:
                trailerUrl = url
            elif 'play-film' in l:
                linksPageUrl = url

        # trailer section
        if trailerUrl:
            trailerUrl = self.getFullUrl(trailerUrl)
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': '%s [%s]' % (cItem['title'], _('Trailer')), 'url': strwithmeta(trailerUrl.replace('&amp;', '&'), {'trailer': 1})})
            self.addVideo(params)

        sts, linkspage_data = self.getPageCF(linksPageUrl, addParams)
        if not sts:
            return

        episodesTab = []
        episodesLinks = {}

        data = []
        parts = ph.getAllItemsBetweenMarkers(linkspage_data, '<section class="box">', '</section>')
        for part in parts:
            data_part = ph.getAllItemsBetweenMarkers(part, '<i class="fa fa-fw fa-chevron-right">', '</ul>') #'<ul class="list-inline list-film"'
            if data_part:
                data.extend(data_part)

        for server in data:
            serverData = ph.findall(server, '<li>', '</li>')
            for link in serverData:
                episodeName = ph.cleanHtml(link)
                episodeUrl = self.getFullUrl(ph.getSearchGroups(link, '''href=['"]([^'^"]+?)['"]''')[0])
                episodeId = ph.getSearchGroups(link, '''data-episode-id=['"]([^'^"]+?)['"]''')[0]

                if not episodeUrl.startswith('http'):
                    continue
                if episodeName not in episodesTab:
                    episodesTab.append(episodeName)

                episodesLinks[episodeName] = strwithmeta(episodeUrl.replace('&amp;', '&'), {'episodeId': episodeId, 'movieId': movieId})

        baseTitleReObj = re.compile('''staffel\s*[0-9]+?$''', flags=re.IGNORECASE)
        baseTitle = cItem['title']
        season = ph.getSearchGroups(cItem['url'], '''staf[f]+?el-([0-9]+?)-''')[0]
        if season == '':
            season = ph.getSearchGroups(baseTitle, '''staffel\s*([0-9]+?)$''', ignoreCase=True)[0]
            if season != '':
                baseTitle = baseTitleReObj.sub('', baseTitle, 1).strip()

        try:
            episodesTab.sort(key=lambda item: int(item))
        except Exception:
            printExc()
        for episode in episodesTab:
            title = baseTitle
            if season != '':
                title += ': ' + 's%se%s' % (season.zfill(2), episode.zfill(2))
            elif len(episodesTab) > 1:
                title += ': ' + 'e%s' % (episode.zfill(2))
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': title, 'url': episodesLinks[episode]})
            self.addVideo(params)

    def getCustomLinksForVideo(self, cItem):
        printDBG("XCineMe.getCustomLinksForVideo [%s]" % cItem)
        urlTab = []
        url = cItem.get('url', '')

        if isinstance(url, strwithmeta):
            if 'trailer' in url.meta:
                return self.up.getVideoLinkExt(url)

            if 'episodeId' in url.meta:
                episode_id = url.meta['episodeId']
            if 'movieId' in url.meta:
                movie_id = url.meta['movieId']
            else:
                movie_id = ''

        params = dict(self.defaultParams)
        params['header']=dict(self.HEADER)
        params['header'].update({
            'Upgrade-Insecure-Requests': '1',
            })

        sts, data = self.getPageCF(url, params)
        if not sts:
            return []

        cookies, token = self.get_data(data)

        if len(url) > 1:

            if movie_id == '':
                url = self.getFullUrl(url + episode_id )
            else:
                url = self.getFullUrl(f"https://xcine.me/movie/load-stream/{movie_id}/{episode_id}?")

            params = dict(self.defaultParams)
            params['header']=dict(self.HEADER)
            params['header'].update({
                    "Origin": self.MAIN_URL,
                    'x-requested-with': 'XMLHttpRequest',
                    'Referer': cItem['url'],
                    }
            )

            if cookies and token:
                url += urllib.parse.urlencode(token)
                for c in cookies:
                    self.cm.addCookieItem(params['cookiefile'], {'name': c.split("=")[0], 'value': c.split("=")[1]})


            sts, data = self.getPage(url, params)
            if not sts: return

            data0 = re.search('vip_source .*?;', data, re.S|re.I)
            if data0:
                data1 = re.findall('file":"([^"]+).*?label":"([^"]+)', data0[0])
                data1.sort(key=lambda y: int(re.split('(\d+)', y[1])[1]), reverse=True)
                for elem in data1:
                    url = elem[0].replace('\\/', '/')
                    label=elem[1]
                    urlTab.append({'name': label, 'url': strwithmeta(url, {'Referer':'https://xcine.me/'}), 'need_resolve': 0})

        return urlTab

    def get_data(self, sHtmlContent):
        sContainer = re.findall( '}</script><script>.*?<div class="footer1">',sHtmlContent, re.DOTALL)
        if sContainer:
            coo = re.findall( '"(_[^;]+);path',sContainer[0], re.DOTALL)
            if not coo and sContainer:
                aResult = re.findall( '(?:window|atob).*?"([^"]+)',sContainer[0], re.DOTALL)
                if aResult:
                    aResult = aResult[0]
                    if not aResult.endswith('='):
                        aResult = aResult + '=='
                    aResult = base64.b64decode(aResult).decode()
                    coo = re.findall( '"(_[^;]+);path',aResult, re.DOTALL)
            if coo:
                sHtmlContainer = re.findall("'server=3';.*?function",sHtmlContent, re.DOTALL)
                if sHtmlContainer:
                    K1 = re.findall('loadStreamSV,.*?([a0-z9]+)',sHtmlContainer[0], re.DOTALL)
                    if K1:
                        K2 = re.findall('var[^>]_.*?;',sHtmlContainer[0], re.DOTALL)
                        if K2:
                            K2 = K2[0].replace(',', '').replace('];', '')
                            K2 = re.findall( '"([^"]+)',K2, re.DOTALL)
                        if K2:
                            L = int(K2[int(len(K2)) - 2])
                            if len(str(L)) == 1 and str(L).isdigit():
                                K2 = str(K2[L].replace('\\', '').replace('x', ''))
                                K2 = bytearray.fromhex(K2).decode()
                                sig = {K1[0]: K2}
                                return coo, sig
        return '', {}

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("XCineMe.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)
        cItem['search_pattern'] = urllib.parse.quote(searchPattern)
        cItem['url'] = self.SEARCH_URL
        self.listItems(cItem, 'explore_item')

    def getCustomArticleContent(self, cItem):
        printDBG("XCineMe.getCustomArticleContent [%s]" % cItem)
        retTab = []

        sts, data = self.getPage(cItem['url'], self.defaultParams)
        if not sts:
            return retTab

        data = ph.getDataBetweenMarkers(data, '<div id="main">', '<div class="row">')[1]

        icon = self.getFullUrl(ph.getSearchGroups(data, '''src=['"]([^'^"]+?)['"]''')[0])
        if icon == '':
            icon = cItem.get('icon', '')

        title = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<b class="text-blue title-film">', '</b>', False)[1])
        if title == '':
            title = cItem['title']

        desc = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<div class="caption">', '</div>', False)[1])

        descData = ph.getDataBetweenMarkers(data, '<div class="movie-info pull-left">', '</div>', False)[1]
        descData = ph.getAllItemsBetweenMarkers(descData, '<p', '</p>', caseSensitive=False)
        descTabMap = {"Genre": "genre",
                      "IMDB": "rating",
                      "Bewertung": "rated",
                      "Veröffentlichungsjahr": "year",
                      "Regisseur": "director",
                      "Schauspieler": "actors",
                      "Staat": "country",
                      "Zeit": "duration",
                      }

        otherInfo = {}
        for item in descData:
            item = item.split('</span>')
            if len(item) < 2:
                continue
            key = ph.cleanHtml(item[0]).replace(':', '').strip()
            val = ph.cleanHtml(item[1])
            for dKey, dvalue in descTabMap.items():
                if dKey in key:
                    if dvalue == 'rating':
                        val += ' IMDB'
                    otherInfo[dvalue] = val
                    break

        views = ph.getSearchGroups(data, '''Aufrufe[^>]*?([0-9]+?)[^0-9]''')[0]
        if views != '':
            otherInfo['views'] = views

        return [{'title': ph.cleanHtml(title), 'text': desc, 'images': [{'title': '', 'url': self.getIconUrl(icon)}], 'other_info': otherInfo}]

    @handleServiceDecorator
    def handleService(self, index, refresh=0, searchPattern='', searchType=''):

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        mode = self.currItem.get("mode", '')
        filter = self.currItem.get("filter", '')

        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))

    #MAIN MENU
        if name is None:
            self.listsTab(self.MAIN_CAT_TAB, {'name': 'category'})
        elif category == 'list_filters':
            if filter == 'genre':
                nextFilter = 'country'
                nextCategory = 'list_filters'
            elif filter == 'country':
                nextFilter = 'sort'
                nextCategory = 'list_filters'
            else:
                nextFilter = ''
                nextCategory = 'list_items'
            self.listFilters(self.currItem, nextCategory, nextFilter)
        elif category == 'list_items':
            self.listItems(self.currItem, 'explore_item')
        elif category == 'explore_item':
            self.exploreItem(self.currItem)
    #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)
