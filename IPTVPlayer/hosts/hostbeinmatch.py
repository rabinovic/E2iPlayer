﻿# -*- coding: utf-8 -*-

###################################################
# LOCAL import
###################################################
from dataclasses import replace
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultHeader, getBaseUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, MergeDicts
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import getDirectM3U8Playlist
from Plugins.Extensions.IPTVPlayer.tools.e2ijs import js_execute

from Plugins.Extensions.IPTVPlayer.libs import ph
###################################################

###################################################
# E2 GUI COMMPONENTS
###################################################
from Screens.MessageBox import MessageBox
###################################################

import re
import urllib.request
import urllib.parse
import urllib.error


def gettytul():
    return 'http://www.beinmatch.com/'


class IPTVHost(HostBase):

    def __init__(self):
        HostBase.__init__(self)
        self.MAIN_URL = 'http://www.beinmatch.com/'
        self.DEFAULT_ICON_URL = self.getFullIconUrl('/assets/images/bim/logo.png')
        self.HTTP_HEADER = MergeDicts(getDefaultHeader(browser='chrome'), {'Referer': self.getMainUrl()})
        self.http_params = {'header': self.HTTP_HEADER}
        self.getLinkJS = ''


    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.http_params)
        origBaseUrl = baseUrl
        baseUrl = ph.std_url(baseUrl)
        return self.cm.getPage(baseUrl, addParams, post_data)

    def getList(self, cItem):
        printDBG("BeinmatchApi.getChannelsList")

        url = cItem.get('url', self.MAIN_URL)

        sts, data = self.getPage(url, self.http_params)
        if not sts:
            return

        self.setMainUrl(self.cm.meta['url'])

        tmp = ph.findall(data, ('<script', '>', ph.check(ph.none, ('src=',))), '</script>', flags=0)
        for item in tmp:
            if 'goToMatch' in item:
                self.getLinkJS = item
                break

        if not self.getLinkJS:
            self.sessionEx.waitForFinishOpen(MessageBox, _('Data for link generation could not be found.'), type=MessageBox.TYPE_ERROR, timeout=10)

        data0 = ph.find(data, ('<table', '>', 'tabIndex'), ('<div', '>', 'Side'))[1]
        data1 = ph.rfindall(data0, '</tr>', ('<table', '>', 'tabIndex'))
        for item in data1:
            icon = self.getFullIconUrl(ph.find(item, 'url(', ')', flags=0)[1].strip())
            teams_ar = ph.findall(item, ('<td', '>', 'tdTeam'), '</td>', flags=0)
            title = ph.cleanHtml(' vs '.join(teams_ar))
            url = ph.getattr(item, 'onclick')
            desc = ph.cleanHtml(ph.find(item, ('<td', '>', 'compStl'), '</td>', flags=0)[1])
            self.addVideo(cItem | {'title': title, 'url': url, 'icon': icon, 'desc': desc})


        nextPage= re.search('<a href="([^"]+?)" data-ci-pagination-page="[^"]+?" rel="next">', data)
        if nextPage:
            self.addDir(cItem | {'category':'getList', 'title': _("Next Page"), 'url': nextPage[1]})

    def getCustomLinksForVideo(self, cItem):
        printDBG("BeinmatchApi.getCustomLinksForVideo")
        urlTab = []

        url = cItem['url']
        m = re.search("goToMatch\((.*?),'(.*?)'\);",url)
        if m:
            id = m[1]
            match = m[2]

            m1 = re.search('window\.location\.assign\((.*?)\)',self.getLinkJS)
            if m1:
                url = m1[1].replace('idMatch', id).replace('linkM', match).replace('+','').replace('"','')

                if url:
                    sts, data = self.getPage(url, self.http_params)
                    if sts:
                        cUrl = self.cm.meta['url']
                        m2 = re.search('<iframe.*?src="([^"]+?)"', data)
                        if m2:
                            urlTab.append({'name': 'video', 'url': strwithmeta(m2[1], {'Referer': getBaseUrl(cUrl)[:-1], 'User-Agent': self.HTTP_HEADER['User-Agent']}), 'need_resolve': 1})
        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        printDBG("BeinmatchApi.getVideoLink")
        urlsTab = []
        baseUrl = strwithmeta(videoUrl)
        self.http_params['header']['Referer'] = baseUrl.meta.get('Referer', self.getMainUrl())
        sts, data = self.getPage(videoUrl, self.http_params)
        if sts:
            m = re.search('<div class="media-container".*?src="([^"]+?)"', data, re.S)
            if m:
                link = m[1]
                if link.startswith('//'):
                    link = 'https:' + link.replace("&amp;","&")
                    return[{'name': 'mp4', 'url': link}]

        cUrl = self.cm.meta['url']
        printDBG(data)
        url = self.getFullUrl(ph.search(data, '''['"]([^'^"]+?\.m3u8(?:\?[^'^"]*?)?)['"]''')[0], cUrl)
        if url:
            url = strwithmeta(url, {'Referer': cUrl, 'Origin': getBaseUrl(cUrl)[:-1], 'User-Agent': self.HTTP_HEADER['User-Agent']})
            return getDirectM3U8Playlist(url, checkContent=True, sortWithMaxBitrate=999999999)
        data = ph.find(data, ('<div', '>', 'video-container'), '</div>', flags=0)[1]
        url = self.getFullUrl(ph.search(data, ph.IFRAME)[1])
        if 0 == self.up.checkHostSupport(url):
            sts, data = self.getPage(url, self.http_params)
            if not sts:
                return urlsTab
            url = self.getFullUrl(ph.search(data, ph.IFRAME)[1])
        return self.up.getVideoLinkExt(url)

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        HostBase.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        mode = self.currItem.get("mode", '')

        printDBG("handleService: >> name[%s], category[%s] " % (name, category))
        self.currList = []

    #MAIN MENU
        if name is None or category == 'getList':
            self.getList(self.currItem | {'name': 'category'})
        else:
            printExc()

        HostBase.endHandleService(self, refresh)

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)
