# -*- coding: utf-8 -*-

import re
from urllib.parse import quote_plus

from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.urlparser import urlparser
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultHeader, getDefaultUserAgent, isValidUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def gettytul():
    return 'ArabSeed'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'araabseed', 'cookie': 'arabseed.cookie'})

        self.MAIN_URL = 'https://m.asd.homes/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/WtH2Wp2/arabseed.png'

        self.HEADER = getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(ph.std_url(baseUrl), addParams, post_data)
        return sts, data

    def mainMenu(self, cItem):
        printDBG("ArabSeed.listMainMenu")
        MAIN_CAT_TAB = [
            {'category':'listCatItems', 'cat': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies/')},
            {'category':'listCatItems', 'cat': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series/')},
            {'category':'listCatItems', 'cat': 'ramadan', 'title': _('مســلـســلات رمـضـان'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-رمضان/')},
            {'category':'listCatItems', 'cat': 'netfilx', 'title': _('Netfilx'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/netfilx/')},
            {'category':'listCatItems', 'cat': 'wwe', 'title': _('المــصـارعــة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/wwe-shows/')},
            {'category':'listCatItems', 'cat': 'search', 'title': _('Search'), 'search_item': True},
            {'category':'listCatItems', 'cat': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("ArabSeed.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("cat", '')

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        if category == 'movei':
            sStart = '>افلام<'
        elif category == 'serie':
            sStart = '>مسلسلات<'
        elif category == 'netfilx':
            sStart = '>Netfilx<'
        elif category == 'ramadan':
            sStart = '>مسلسلات رمضان<'

        tmp = ph.getDataBetweenMarkers(data, sStart, ('</ul', '>'), True)[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, ('<li', 'menu-item'), ('</a', '>'))
        for item in tmp:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = ph.cleanHtml(ph.getDataBetweenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
            self.addDir(params)

    def listItems(self, cItem):
        printDBG("ArabSeed.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = ph.getDataBetweenMarkers(data, ('<div', '>', 'pagination'), ('</ul', '>'), True)[1]
        nextPage = self.getFullUrl(ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        tmp = ph.getDataBetweenMarkers(data, ('<ul', '>', 'Blocks-UL'), ('<script', '>', 'text/javascript'), True)[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, ('<div', '>', 'MovieBlock'), ('</a', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(ph.getSearchGroups(item, '''data-src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<h4', '>'), ('</h4', '>'), False)[1])
            desc = "ph.extract_desc"

            #info = ph.cleanHtml(title)
            #if title != '':
            #    title = info.get('title_display')
            #desc = info.get('desc')

            params = dict(cItem)
            params.update({'category': 'exploreItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': ph.std_url(icon), 'desc': desc, 'with_mini_cover':True})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addNext(params)

    def exploreItems(self, cItem):
        printDBG("ArabSeed.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = ph.cleanHtml(ph.getSearchGroups(data, '''descrip['"][>]([^>]+?)[$<]''')[0])

        Season = ph.getDataBetweenMarkers(data, ('<div', '>', 'SeasonsListHolder'), ('</ul', '>'), True)[1]
        if Season:
            self.addMarker({'title': '{}مـــواســم'.format('\c0000??00'), 'icon': cItem['icon'], 'desc': ''})
            tmp = ph.getAllItemsBetweenMarkers(Season, ('<li', '>'), ('</li', '>'))
            for item in tmp:
                sId = ph.cleanHtml(ph.getSearchGroups(item, '''data-id=['"]([^"^']+?)['"]''')[0])
                sSeason = ph.cleanHtml(ph.getSearchGroups(item, '''data-season=['"]([^"^']+?)['"]''')[0])
                title = ph.cleanHtml(ph.getDataBetweenNodes(item, ('fa-folder', '>'), ('</span', '>'), False)[1])

                #info = ph.cleanHtml(title)
                #if title != '':
                #    title = info.get('title_display')
                #otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'category': 'listSeason', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': cItem['url'], 'icon': cItem['icon'], 'sSeason': sSeason, 'sId': sId, 'with_mini_cover':True})
                self.addDir(params)

        Episod = ph.getDataBetweenMarkers(data, ('<div', '>', 'ContainerEpisodesList'), ('</div', '>'), True)[1]
        if Episod:
            tmp = ph.getAllItemsBetweenMarkers(Episod, '<a', ('</a', '>'))
            for item in tmp:
                url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = ph.cleanHtml(ph.getDataBetweenNodes(item, ('href', '>'), ('</em', '>'), False)[1])

                #info = ph.cleanHtml(title)
                #if title != '':
                #    title = info.get('title_display')
                #otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon']})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSeason(self, cItem):
        printDBG("ArabSeed.listSeason cItem[%s]" % (cItem))

        tmpUrl = self.getFullUrl('/wp-content/themes/Elshaikh2021/Ajaxat/Single/Episodes.php')
        sts, data = self.getPage(tmpUrl, post_data={'post_id': cItem['sId'], 'season': cItem['sSeason']})
        if not sts:
            return

        tmp = ph.getAllItemsBetweenMarkers(data, '<a', ('</a', '>'))
        for item in tmp:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = ph.cleanHtml(ph.getDataBetweenNodes(item, ('href', '>'), ('</em', '>'), False)[1])

            # info = ph.cleanHtml(title)
            # if title != '':
            #     title = info.get('title_display')
            # desc = info.get('desc')

            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon']})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("ArabSeed.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/find/?find={}'.format(quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url, 'with_mini_cover':True}
        self.listItems(params)

    def getCustomLinksForVideo(self, cItem):
        printDBG("ArabSeed.getLinksForVideo [%s]" % cItem)
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        sPost = ph.cleanHtml(ph.getSearchGroups(data, '''data-post=['"]([^"^']+?)['"]''')[0])
        cUrl = ph.cleanHtml(ph.getSearchGroups(data, '''HomeURL = ['"]([^"^']+?)['"]''')[0])
        self.setMainUrl(cUrl)

        params = dict(self.defaultParams)
        params['header'] = dict(self.AJAX_HEADER)
        params['header']['Origin'] = self.up.getDomain(self.cm.meta['url'], False)
        params['header']['Referer'] = cUrl

        tmpUrl = self.getFullUrl('/wp-content/themes/Elshaikh2021/Ajaxat/Single/Server.php')

        for server in ('0', '1', '2', '3', '4', '5'):
            if server == '0':
                sName = 'Arabseed'
            if server == '1':
                sName = 'Server 1'
            if server == '2':
                sName = 'Server 2'
            if server == '3':
                sName = 'Server 3'
            if server == '4':
                sName = 'Server 4'
            if server == '5':
                sName = 'Server 5'

            for sQual in ('1080', '720', '480'):
                post_data = {'post_id': sPost, 'server': server, 'qu': sQual}
                sts, sHtmlContent = self.getPage(tmpUrl, params, post_data)
                if not sts:
                    return

                url = self.getFullUrl(ph.getSearchGroups(sHtmlContent, '''src=['"]([^"^']+?)['"]''', ignoreCase=True)[0])
                title = ('{} - {}p - {}'.format(sName, sQual,  self.up.getHostName(url, True)))

                if url != '':
                    urlTab.append({'name': title, 'url': url, 'need_resolve': 1})
        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        printDBG("ArabSeed.getVideoLinks [%s]" % videoUrl)

        if isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getCustomArticleContent(self, cItem):
        printDBG("ArabSeed.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = ph.getDataBetweenMarkers(data, ('<div', '>', 'MetaTermsInfo'), ('<div', '>', 'topBarSingle'), True)[1]

        Info = ph.cleanHtml(ph.getDataBetweenNodes(data, ('RatingImdb', 'IMDB'), ('</em', '>'), False)[1])
        if Info != '':
            otherInfo['imdb_rating'] = Info

        Info = ph.cleanHtml(ph.getDataBetweenNodes(tmp, ('مدة العرض', '>'), ('</li', '>'), False)[1])
        if Info != '':
            otherInfo['duration'] = Info

        Info = ph.cleanHtml(ph.getDataBetweenNodes(tmp, ('Quality', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        Info = ph.cleanHtml(ph.getDataBetweenNodes(tmp, ('release-year', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = ph.cleanHtml(ph.getDataBetweenNodes(tmp, ('النوع', '>'), ('</li', '>'), False)[1])
        if Info != '':
            otherInfo['genre'] = Info

        Info = ph.cleanHtml(ph.getDataBetweenNodes(tmp, ('language', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['language'] = Info

        Info = ph.cleanHtml(ph.getDataBetweenNodes(tmp, ('nation', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['country'] = Info

        Info = ph.cleanHtml(ph.getDataBetweenNodes(tmp, ('category', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['category'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': otherInfo}]



    def withArticleContent(self, cItem):
        if cItem.get('type', 'video') != 'video' and cItem.get('category', 'unk') != 'showitms':
            return False
        return True

