# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs.pCommon import isValidUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
###################################################

###################################################
# FOREIGN import
###################################################
import urllib.parse
import re
import urllib.request
import urllib.parse
import urllib.error
try:
    import json
except Exception:
    import simplejson as json
###################################################


def gettytul():
    return 'https://ororo.tv/'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'ororo.tv', 'cookie': 'ororo.tv.cookie'})
        self.DEFAULT_ICON_URL = 'http://www.yourkodi.com/wp-content/uploads/2016/02/ororo.png'
        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'
        self.MAIN_URL = 'https://ororo.tv/'
        self.HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html', 'Accept-Encoding': 'gzip, deflate', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}
        self.AJAX_HEADER = dict(self.HEADER)
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest', 'Accept-Encoding': 'gzip, deflate', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Accept': 'application/json, text/javascript, */*; q=0.01'})

        self.cacheFilters = {}
        self.cacheLinks = {}
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        self.MAIN_CAT_TAB = [
                             {'category': 'list_channels', 'title': _('Channels'), 'url': self.getFullUrl('/channels')},
                             {'category': 'search', 'title': _('Search'), 'search_item': True, },
                             {'category': 'search_history', 'title': _('Search history'), }
                            ]

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        baseUrl = self.cm.iriToUri(baseUrl)

        def _getFullUrl(url):
            if isValidUrl(url):
                return url
            else:
                return urllib.parse.urljoin(baseUrl, url)

        addParams['cloudflare_params'] = {'domain': self.up.getDomain(baseUrl), 'cookie_file': self.COOKIE_FILE, 'User-Agent': self.USER_AGENT, 'full_url_handle': _getFullUrl}
        return self.cm.getPageCFProtection(baseUrl, addParams, post_data)

    def listChannels(self, cItem, nextCategory):
        printDBG("OroroTV.listChannels")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        data = ph.getDataBetweenMarkers(data, '"items":', '};', False)[1]
        try:
            data = json.loads(data)
            for item in data:
                url = self.getFullUrl(item.get('url', ''))
                icon = self.getFullUrl(item.get('banner', ''))
                if icon == '':
                    icon = self.getFullUrl(item.get('image', ''))
                title = ph.cleanHtml(item.get('title', ''))

                desc = ' | '.join(item.get('parsed_tags', []))
                if desc != '':
                    desc += ' [/br]'
                desc += ph.cleanHtml(item.get('description', ''))

                params = dict(cItem)
                params.update({'good_for_fav': True, 'category': nextCategory, 'title': title, 'desc': desc, 'url': url, 'icon': icon})
                self.addDir(params)
        except Exception:
            printExc()

    def listItems(self, cItem):
        printDBG("OroroTV.listItems [%s]" % cItem)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        #desc = []
        #tmp = ph.getDataBetweenNodes(data, ('<section', '>', 'show-desc'), ('</section', '>'))[1]
        #tmpTab = ph.getAllItemsBetweenMarkers(tmp, '<li', '</li>')
        #for t in tmpTab:
        #    t = ph.cleanHtml(t)
        #    if t == '': continue
        #    desc.append(t)
        #desc  = '|'.join( desc )
        #if desc != '': desc + '[/br]'
        #desc += ph.cleanHtml( ph.getDataBetweenNodes(tmp, ('<p', '>', 'show-desc-text'), ('</p', '>'))[1] )

        sp = re.compile('''<div[^>]+?js\-watched\-mark[^>]+?>''')
        data = ph.getDataBetweenNodes(data, ('<div', '>', 'show-content'), ('<div', '>', 'site-footer'))[1]
        data = sp.split(data)
        if len(data):
            del data[0]
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''data\-href=['"]([^'^"]+?)['"]''')[0])
            icon = self.getFullUrl(ph.getSearchGroups(item, '''original=['"]([^'^"]+?)['"]''')[0])
            title = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<a', '>', 'series-card-title'), ('</a', '>'))[1])

            desc = []
            tmp = ph.getAllItemsBetweenMarkers(item, '<i', '</i>')
            for t in tmp:
                if 'inactive' in t:
                    continue
                t = ph.getSearchGroups(t, '''class=['"]([^'^"]*?flag[^'^"]*?)['"]''')[0].replace('flag', '').replace('active-sub', '')
                t = ph.cleanHtml(t)
                if t == '':
                    continue
                desc.append(t)
            desc = ' | '.join(desc)
            if desc != '':
                desc += '[/br]'

            tmp = ph.getAllItemsBetweenMarkers(item, '<p', '</p>')
            tmpTab = []
            for t in tmp:
                t = ph.cleanHtml(t)
                if t == '':
                    continue
                tmpTab.insert(0, t)
            desc += '[/br]'.join(tmpTab)

            params = dict(cItem)
            params.update({'good_for_fav': True, 'title': title, 'url': url, 'desc': desc, 'icon': icon})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("OroroTV.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))

        url = self.getFullUrl('/api/frontend/search?query=') + urllib.parse.quote_plus(searchPattern)

        sts, data = self.getPage(url)
        if not sts:
            return

        data = ph.getAllItemsBetweenMarkers(data, '<a', '</a>')
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
            icon = self.getFullUrl(ph.getSearchGroups(item, '''src=['"]([^'^"]+?)['"]''')[0])
            tmp = ph.getAllItemsBetweenMarkers(item, '<p', '</p>')
            title = ph.cleanHtml(tmp[0])
            desc = ph.cleanHtml(tmp[1])
            params = dict(cItem)
            params.update({'good_for_fav': True, 'title': title, 'url': url, 'desc': desc, 'icon': icon})
            if '/channels/' in url and '/videos/' not in url:
                params['category'] = 'list_items'
                self.addDir(params)
            else:
                self.addVideo(params)

    def getCustomLinksForVideo(self, cItem):
        printDBG("OroroTV.getCustomLinksForVideo [%s]" % cItem)
        retTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return []

        data = ph.getDataBetweenMarkers(data, '<video', '</video>')[1]

        tmp = ph.getDataBetweenMarkers(data, '<source', '>')[1]
        videoUrl = ph.getSearchGroups(tmp, '''src=['"](https?://[^'^"]+?)['"]''')[0]
        type = ph.getSearchGroups(tmp, '''type=['"]([^'^"]+?)['"]''')[0]

        retTab = self.up.getVideoLinkExt(videoUrl)
        if 0 == len(retTab):
            return []

        subTracks = []
        tmp = ph.getAllItemsBetweenMarkers(data, '<track', '>')
        for item in tmp:
            if 'subtitles' not in item:
                continue

            url = ph.getSearchGroups(item, '''src=['"](https?://[^'^"]+?)['"]''')[0]
            lang = ph.getSearchGroups(item, '''label=['"]([^'^"]+?)['"]''')[0]
            title = ph.getSearchGroups(item, '''title=['"]([^'^"]+?)['"]''')[0]

            if url != '':
                subTracks.append({'title': title, 'url': url, 'lang': lang, 'format': url[-3:]})

        if len(subTracks):
            for idx in range(len(retTab)):
                tmp = list(subTracks)
                tmp.extend(retTab[idx]['url'].meta.get('external_sub_tracks', []))
                retTab[idx]['url'] = strwithmeta(retTab[idx]['url'], {'external_sub_tracks': list(tmp)})

        return retTab

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        HostBase.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        mode = self.currItem.get("mode", '')

        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    #MAIN MENU
        if name is None:
            self.listsTab(self.MAIN_CAT_TAB, {'name': 'category'})
        elif category == 'list_channels':
            self.listChannels(self.currItem, 'list_items')
        elif category == 'list_items':
            self.listItems(self.currItem)
    #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        HostBase.endHandleService(self, refresh)

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)
