# -*- coding: utf-8 -*-

import re
import json

from urllib.parse import quote, quote_plus
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase

from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def gettytul():
    return 'CineClix.de'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__({'history': 'CineClix.de','cookie': 'arabseed.cookie'})

        self.DEFAULT_ICON = 'https://cineclix.de/storage/branding_media/46a55c77-7f35-40fc-a66f-f466eaba0b46.png'
        self.MAIN_URL = 'https://cineclix.de/'

        self.USER_AGENT = getDefaultUserAgent()
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Referer':self.MAIN_URL}
        self.defaultParams = {'header':self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        self.URL_MOVIES = self.MAIN_URL + 'api/v1/channel/movies?channelType=channel&restriction=&paginate=simple'
        self.URL_TOP_MOVIES = self.MAIN_URL + 'api/v1/channel/top-10-filme-diese-woche?channelType=channel&restriction=&paginate=simple'
        self.URL_NEW_MOVIES = self.MAIN_URL + 'api/v1/channel/neu-hinzugefuegt?channelType=channel&restriction=&paginate=simple'

        self.URL_SERIES = self.MAIN_URL + 'api/v1/channel/series?channelType=channel&restriction=&paginate=simple'
        self.URL_TOP_SERIES = self.MAIN_URL + '/api/v1/channel/top-10-serien-diese-woche?channelType=channel&restriction=&paginate=simple'
        self.URL_NEW_SERIES = self.MAIN_URL + '/api/v1/channel/neue-serien?channelType=channel&restriction=&paginate=simple'
        self.URL_SEARCH = self.MAIN_URL + 'api/v1/search/%s?query=%s&limit=8'

        # Genre
        self.URL_ACTION = self.MAIN_URL + 'api/v1/channel/action-filme?channelType=channel&restriction=&paginate=simple'
        self.URL_ANIMATION = self.MAIN_URL + 'api/v1/channel/animations-filme?channelType=channel&restriction=&paginate=simple'
        self.URL_HORROR = self.MAIN_URL + 'api/v1/channel/horror-filme?channelType=channel&restriction=&paginate=simple'
        self.URL_KOMOEDIE = self.MAIN_URL + 'api/v1/channel/komoedien-filme?channelType=channel&restriction=&paginate=simple'
        self.URL_LOVE = self.MAIN_URL + 'api/v1/channel/gefuehlskino-herzklopfen-inklusive?channelType=channel&restriction=&paginate=simple'
        self.URL_MUSIC = self.MAIN_URL + 'api/v1/channel/musik?channelType=channel&restriction=&paginate=simple'
        self.URL_SCIFI = self.MAIN_URL + 'api/v1/channel/kosmische-erzaehlungen?channelType=channel&restriction=&paginate=simple'


        # Hoster
        self.URL_HOSTER = self.MAIN_URL + 'api/v1/titles/%s?load=images,genres,productionCountries,keywords,videos,primaryVideo,seasons,compactCredits'


    def getPage(self, baseUrl, addParams=None, post_data = None):
        if addParams is None:
            addParams = dict(self.defaultParams)

        return self.cm.getPage(baseUrl, addParams, post_data)

    def mainMenu(self, cItem):
        printDBG(f"cineclix.listMainMenu cItem [{cItem}]")

        MAIN_CAT_TAB = [
                        {'category':'showMovies', 'title': 'Filme',  'icon':self.DEFAULT_ICON_URL},
                        {'category':'showSeries', 'title': 'Serien', 'icon':self.DEFAULT_ICON_URL},
                        {'category':'showGenres', 'title': 'Genre', 'icon':self.DEFAULT_ICON_URL},
                        {'category':'search','title': _('Search'), 'search_item':True,'page':1, 'icon':self.DEFAULT_ICON_URL},
                        {'category': 'search_history', 'title': _('Search history'), },
                    ]
        self.listsTab(MAIN_CAT_TAB, cItem)


    def showMovies(self, cItem):
        MAIN_CAT_TAB = [
                        {'category':'listItems', 'title': _('New'), 'url':self.URL_NEW_MOVIES +'&page=','page':1, 'icon':self.DEFAULT_ICON_URL},
                        {'category':'listItems', 'title': 'TOP', 'url':self.URL_TOP_MOVIES +'&page=','page':1, 'icon':self.DEFAULT_ICON_URL},
                        {'category':'listItems', 'title': _('All'), 'url':self.URL_MOVIES+ '&page=','page':1, 'icon':self.DEFAULT_ICON_URL},

                    ]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def showSeries(self, cItem):
        MAIN_CAT_TAB = [
                {'category':'listItems', 'title': _('New'), 'url':self.URL_NEW_SERIES +'&page=','page':1, 'icon':self.DEFAULT_ICON_URL},
                {'category':'listItems', 'title': 'TOP', 'url':self.URL_TOP_SERIES +'&page=','page':1, 'icon':self.DEFAULT_ICON_URL},
                {'category':'listItems', 'title': _('All'), 'url':self.URL_SERIES+ '&page=','page':1, 'icon':self.DEFAULT_ICON_URL},

            ]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def showGenres(self, cItem):
        MAIN_CAT_TAB = [
                {'category':'listItems', 'title': 'Action', 'url':self.URL_ACTION+ '&page=','page':1, 'icon':self.DEFAULT_ICON_URL},
                {'category':'listItems', 'title': 'Animation', 'url':self.URL_ANIMATION +'&page=','page':1, 'icon':self.DEFAULT_ICON_URL},
                {'category':'listItems', 'title': 'Horror', 'url':self.URL_HORROR +'&page=','page':1, 'icon':self.DEFAULT_ICON_URL},
                {'category':'listItems', 'title': 'Komödien', 'url':self.URL_KOMOEDIE +'&page=','page':1, 'icon':self.DEFAULT_ICON_URL},
                {'category':'listItems', 'title': 'Liebe', 'url':self.URL_LOVE +'&page=','page':1, 'icon':self.DEFAULT_ICON_URL},
                {'category':'listItems', 'title': _('Music'), 'url':self.URL_MUSIC +'&page=','page':1, 'icon':self.DEFAULT_ICON_URL},
                {'category':'listItems', 'title': _('Sci-Fi'), 'url':self.URL_SCIFI +'&page=','page':1, 'icon':self.DEFAULT_ICON_URL},

            ]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        page = cItem.get('page', '')
        url = cItem['url'] + str(page)
        sts, data = self.getPage(url)
        if sts:
            pass

        json_data = json.loads(data)

        elems = json_data.get('pagination', {}).get('data', [])
        if not elems:
            elems = json_data.get('channel', {}).get('content', {}).get('data', [])
        if not elems:
            elems = json_data['results']  # search results

        for elem in elems:
            if 'person' in elem['model_type']: continue
            icon = elem['poster'].replace('original', 'w300')
            title = elem['name']
            id = elem['id']
            desc = elem['description']
            release_date = elem['release_date'].split('T')[0]
            title += f" ({release_date.split('-')[0]})"
            addParam = dict(cItem)
            if elem['is_series']:
                self.addDir(addParam | {'category':'listSeasons', 'title':title, 'icon':icon, 'desc':desc, 'url': self.URL_HOSTER % id,'good_for_fav': True, 'with_mini_cover':True})
            else:
                self.addVideo(addParam | {'title':title, 'icon':icon, 'desc':desc, 'url': self.URL_HOSTER % id,'good_for_fav': True, 'with_mini_cover':True})
        nextPage = json_data.get('pagination', {}).get('nextPage', None)
        if not nextPage:
            nextPage = json_data.get('channel', {}).get('content', {}).get('next_page', None)

        if nextPage:
            addParam = dict(cItem)
            self.addNext(addParam | {'title':_('Next page'), 'page':nextPage})

    def listSeasons(self, cItem):
        page = cItem['page']
        url = cItem['url'] + str(page)
        sts, data = self.getPage(url)
        if sts:
            pass

        json_data = json.loads(data)

        seasons = json_data['seasons']['data']

        for s in seasons:
            id = s['title_id'] # ID ändert sich !!!
            seasonNr = str(s['number'])
            icon = s['poster'].replace('original', 'w300')
            desc = s['release_date']
            title = f'Staffel {seasonNr}'
            addParam = dict(cItem)
            self.addDir(addParam | {'category':'listEpisodes', 'title':title, 'icon':icon, 'desc':desc, 'id': id,'seasonNr': seasonNr, 'good_for_fav': True, 'with_mini_cover':True})


    def listEpisodes(self, cItem):
        id = cItem['id']
        seasonNr = cItem['seasonNr']
        url = f'{self.MAIN_URL}/api/v1/titles/{id}/seasons/{seasonNr}?load=episodes,primaryVideo'
        sts, data = self.getPage(url)
        if sts:
            pass

        json_data = json.loads(data)
        episodes = json_data['episodes']['data']

        for e in episodes:
            episode_number = str(e['episode_number']) # Episoden Nummer
            icon = e['poster'].replace('original', 'w300') # Episoden Poster

            title = f"Folge {episode_number} - {e['name']}" # Episoden Titel
            url = self.MAIN_URL + 'api/v1/titles/%s/seasons/%s/episodes/%s?load=videos,compactCredits,primaryVideo' % (id, seasonNr, episode_number)
            params = dict(cItem)
            self.addVideo(params | {'title':title, 'icon':icon if icon else params['icon'], 'url':url})

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("cineclix.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)
        cItem['url'] = self.MAIN_URL + 'api/v1/search/%s?query=%s&limit=8' % (quote(searchPattern), quote_plus(searchPattern))

        self.listItems(cItem)

    def getCustomLinksForVideo(self, cItem):
        printDBG("cineclix.getCustomLinksForVideo cItem [%s]" % cItem)
        urlTab = []
        url = cItem['url']

        sts, data = self.getPage(url)
        if sts:
            pass

        json_data = json.loads(data)

        try:
            videos = json_data['title']['videos']
        except:
            videos = json_data['episode']['videos']

        for video in videos:
            name = video['name']
            src = video['src']
            urlTab.append({'name':name, 'url':src, 'need_resolve': 1})
        return urlTab

    def getCustomVideoLinks(self, url):
        printDBG("cineclix.getCustomVideoLinks [%s]" % url)
        return self.up.getVideoLinkExt(url)

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)
