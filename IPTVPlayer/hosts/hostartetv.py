# -*- coding: utf-8 -*-

###################################################
# LOCAL import
###################################################
###################################################
# FOREIGN import
###################################################
import json
import re
from typing import List
import urllib
from urllib.parse import quote
from datetime import datetime, timedelta

###################################################
# Config options for HOST
###################################################
from Components.config import (ConfigSelection, ConfigYesNo, config, getConfigListEntry)
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph

from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (GetDefaultLang,
                                                           MergeDicts,
                                                           printDBG, printExc)

config.plugins.iptvplayer.artetv_quality = ConfigYesNo(default=True)
config.plugins.iptvplayer.artetv_audio = ConfigYesNo(default=False)
config.plugins.iptvplayer.artetv_language = ConfigSelection(default="de", choices=[
        ("de", 'Deutsch ( DE )'),
        ("fr", 'Français ( FR )'),
        ("en", 'English ( EN )'),
        ("es", 'Español ( ES )'),
        ("pl", 'Polski ( PL )')
    ])

def GetConfigList():
    optionList = [
        getConfigListEntry(_("Show only best quality of streams:"), config.plugins.iptvplayer.artetv_quality),
        getConfigListEntry(_("Show only audio in selected language:"), config.plugins.iptvplayer.artetv_audio),
        getConfigListEntry(_("Select language:"), config.plugins.iptvplayer.artetv_language)
    ]
    return optionList

def gettytul():
    return 'https://www.arte.tv/'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'arte.tv', 'cookie': 'arte.tv.cookie'})
        self.MAIN_URL = 'https://www.arte.tv/'
        self.DEFAULT_ICON_URL = 'https://i.pinimg.com/originals/3c/e6/54/3ce6543cf583480fa6d0e233384f336e.jpg'

        # from hbbtv
        self.API_URL = 'http://www.arte.tv/hbbtvv2/services/web/index.php'
        self.API_ENDPOINTS = {
            'categories': '/EMAC/teasers/{type}/v2/{lang}',
            'category': '/EMAC/teasers/category/v2/{code}/{lang}',
            'subcategory': '/OPA/v3/videos/subcategory/{sub_category_code}/page/1/limit/100/{lang}',
            'magazines': '/OPA/v3/magazines/{lang}',
            'collection': '/EMAC/teasers/collection/v2/{code}/{lang}',
            # program details
            'video': '/OPA/v3/videos/{code}/{lang}',
            # program streams
            'streams': '/OPA/v3/streams/{code}/{kind}/{lang}',
            'daily': '/OPA/v3/programs/{date}/{lang}',
            #search
            'search':"https://www.arte.tv/guide/api/emac/v3/{lang}/web/data/SEARCH_LISTING/?query={searchpattern}&page={page}&limit=22"
        }

        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Accept': 'text/html'}
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True,
                              'cookiefile': self.COOKIE_FILE}


    def parseDate(self, datestr):
        # remove weekday & timezone
        datestr = ' '.join(datestr.split(None)[1:5])

        # replace months with numbers - there will be problems with localization
        months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

        for i in range(12):
            datestr = datestr.replace(months[i], str(i + 1))

        date = None
        try:
            date = datetime.strptime(datestr, '%d %m %Y %H:%M:%S')
        except Exception as e:
            printExc()

        return date

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        return self.cm.getPage(baseUrl, addParams, post_data)

    def mapVideo(self, item):

        label = item.get('title')
        subtitle = item.get('subtitle')
        if subtitle:
            label += " - " + subtitle

        desc = []

        duration = int(item.get('duration') or 0) * 60 or item.get('durationSeconds')

        desc1 = ""
        if duration > 0:
            desc1 = _('Duration') + ": %s" % str(timedelta(seconds=duration))
        try:
            airdate = item.get('broadcastBegin')
            if airdate is not None:
                desc1 = desc1 + " | " + (_("Broadcast begins at %s") % datetime.strftime(self.parseDate(airdate), '%d %B %Y %H:%M:%S'))
        except Exception as e:
            printExc()

        if desc1:
            desc.append(desc1)

        if item.get('fullDescription', ''):
            desc.append(item.get('fullDescription', ''))
        elif item.get('shortDescription', ''):
            desc.append(item.get('shortDescription', ''))

        if item.get('genrePresse'):
            desc.append(item.get('genrePresse'))

        desc = ' | '.join(desc)

        return {
            'kind': item.get('kind'),
            'programId': item.get('programId'),
            'title': label,
            'icon': item.get('imageUrl'),
            'desc': desc,
            'good_for_fav': True
        }

    def mapPlaylist(self, item):
        label = item.get('title')
        subtitle = item.get('subtitle')
        if subtitle:
            label += " - " + subtitle

        desc = item.get('teaserText', '')
        if not desc:
            desc = item.get('desc', '')

        return {
            'title': label,
            'icon': item.get('imageUrl'),
            'desc': desc
        }

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("ArteTV.listSearchResult [%s]" % cItem)

        lang = config.plugins.iptvplayer.artetv_language.value

        page = cItem.get('page','1')

        url = self.API_ENDPOINTS['search'].format(lang=lang, searchpattern=quote(searchPattern), page=page)

        sts, data = self.getPage(url)
        if not sts:
            return

        printDBG("ArteTV.listSearchResult data[%s]" % data)

        response = json.loads(data)

        nextPage = response.get('nextPage')

        data = response.get('data','')
        for i in data:
            isCollection = i['kind']['isCollection']
            code = i['kind']['code']
            programId=i['programId']
            title=i['title']
            desc=i['shortDescription']
            icon = i['images']['landscape']['resolutions'][3]['url']

            category = 'collection' if isCollection else 'category'
            cat_url = self.API_URL + self.API_ENDPOINTS[category].format(code=programId, lang=lang)
            params = dict(cItem)
            params.update({'category': category, 'title': title, 'code': code, 'url': cat_url, 'icon':icon, 'desc':desc})
            self.addDir(params)

        if nextPage:
            page = re.search("page=(\d+?)&",nextPage)[1]
            params = dict(cItem)
            params.update({'category': 'search_next_page', 'title': _('Next page'), 'page': page})
            self.addNext(params)

    def mainMenu(self, cItem):
        printDBG("ArteTV.listLang [%s]" % cItem)

        lang = config.plugins.iptvplayer.artetv_language.value

        # home categories
        home_url = self.API_URL + self.API_ENDPOINTS['categories'].format(type='home', lang=lang)

        CAT_TAB = [
            {'category': 'home_cat', 'key': 'mostRecent', 'title': _('Most recent'), 'f_lang': lang, 'url': home_url},
            {'category': 'home_cat', 'key': 'mostViewed', 'title': _('Most viewed'), 'f_lang': lang, 'url': home_url},
            {'category': 'home_cat', 'key': 'lastChance', 'title': _('Last chance'), 'f_lang': lang, 'url': home_url},
        ]

        self.listsTab(CAT_TAB, cItem)

        # categories
        url = self.API_URL + self.API_ENDPOINTS['categories'].format(type='categories', lang=lang)

        sts, data = self.getPage(url)

        if not sts:
            return

        try:
            response = json.loads(data)

            for c in response.get("categories", []):
                title = c.get("title", "")
                code = c.get("code", "")

                if title and code:
                    cat_url = self.API_URL + self.API_ENDPOINTS['category'].format(code=code, lang=lang)

                    params = dict(cItem)
                    params.update({'category': 'category', 'title': title, 'code': code, 'url': cat_url})
                    self.addDir(params)
        except:
            printExc()

        CAT_TAB = [
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history')},
        ]
        self.listsTab(CAT_TAB, cItem)

    def collection(self, cItem):
        printDBG("ArteTV.listItems [%s]" % cItem)

        url = cItem.get('url', '')

        sts, data = self.getPage(url)
        if not sts:
            return

        response = json.loads(data)
        videos = response.get("videos", [])
        if len(videos) == 0:
            subcollections = response.get("subCollections", [])
            for subcollection in subcollections:
                videos.extend(subcollection.get("videos", []))
        for v in videos:
            videoInfo = self.mapVideo(v)
            params = dict(cItem)
            params.update(videoInfo)
            params.update({'url':videoInfo.get('programId')})
            self.addVideo(params)

    def home_cat(self, cItem):
        printDBG("ArteTV.listItems [%s]" % cItem)

        url = cItem.get('url', '')
        key = cItem.get("key", "")

        sts, data = self.getPage(url)
        if not sts:
            return

        response = json.loads(data)
        teasers = response.get("teasers", {})
        # read key
        if key:
            for v in teasers.get(key, []):
                videoInfo = self.mapVideo(v)
                params = dict(cItem)
                params.update(videoInfo)
                params.update({'url':videoInfo.get('programId')})
                self.addVideo(params)

    def category(self, cItem):
        printDBG("ArteTV.listItems [%s]" % cItem)

        url = cItem.get('url', '')
        lang = config.plugins.iptvplayer.artetv_language.value

        sts, data = self.getPage(url)
        if not sts:
            return

        response = json.loads(data)

        # read key category
        for c in response["category"]:
            title = c.get("title", "")
            code = c.get("code", "")
            category_type = c.get("type", "")

            if title:

                params = dict(cItem)

                if category_type == "category":
                    cat_url = self.API_URL + self.API_ENDPOINTS['category'].format(code=code, lang=lang)
                    params.update({'category': category_type, 'title': title, 'code': code, 'url': cat_url})
                    self.addDir(params)

                else:# category_type is not None:# or any(item in ["highlight", "collection", "listing" , "content"] for item in category_type.split('_')):
                    # read sub items
                    subItems = []

                    for cc in c.get("teasers", []):
                        videoInfo = self.mapVideo(cc)
                        subItems.append(videoInfo)

                    if subItems:
                        params.update({'category': "listItems", 'title': title, 'subitems': subItems})
                        self.addDir(params)

    def listItems(self, cItem):
        printDBG("ArteTV.listItems [%s]" % cItem)

        lang = config.plugins.iptvplayer.artetv_language.value

        for c in cItem.get("subitems", []):
            kind = c.get("kind", "")
            programId = c.get("programId", "")

            if kind == "SHOW":
                params = dict(cItem)
                params.update(c)
                params.update({'url':programId})
                self.addVideo(params)

            if kind in ["TOPIC", "TV_SERIES", "MAGAZINE"]:
                playlistInfo = self.mapPlaylist(c)
                params = dict(cItem)
                params.update(playlistInfo)
                params.update({'category': 'collection', 'url': self.API_URL + self.API_ENDPOINTS["collection"].format(code=programId, lang=lang)})
                self.addDir(params)

    def getCustomLinksForVideo(self, cItem):
        printDBG("ArteTV.getCustomLinksForVideo [%s]" % cItem)

        linksTab = []

        programId = cItem.get("programId", "")
        kind = cItem.get("kind", "")

        lang = config.plugins.iptvplayer.artetv_language.value

        if kind in  ('SHOW', 'BONUS'):
            url = self.API_URL + self.API_ENDPOINTS["streams"].format(code=programId, kind=kind, lang=lang)
            sts, data = self.getPage(url)

            if not sts:
                return

            try:
                response = json.loads(data)

                videostreams = response.get("videoStreams", [])

                for v in videostreams:
                    videoUrl = v.get('url', '')
                    if videoUrl:
                        slot = v.get('audioSlot', 0)
                        quality = v.get('quality', '')
                        height = int(v.get('height', 0))
                        width = int(v.get('width', 0))
                        label = v.get('audioLabel', 'audio')

                        if config.plugins.iptvplayer.artetv_quality.value and (height < 700):
                            continue
                        if config.plugins.iptvplayer.artetv_audio.value and (slot > 1):
                            continue

                        linksTab.append({'name': label + " %dx%d" % (width, height), 'slot': slot, 'url': videoUrl})
            except:
                printExc()

            linksTab = sorted(linksTab, key=lambda k: k['slot'])
        return linksTab

    def getCustomVideoLinks(self, url) -> List:
        return super().getCustomVideoLinks(url)

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)
