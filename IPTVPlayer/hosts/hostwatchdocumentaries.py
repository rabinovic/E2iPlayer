# -*- coding: utf-8 -*-


import urllib
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, handleServiceDecorator
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def gettytul():
    return 'watchdocumentaries'


class IPTVHost(HostBase):
    def __init__(self):
        super().__init__( {'history': 'watchdocumentaries.com', 'cookie': 'watchdocumentaries.com.cookie'})

        self.MAIN_URL = 'https://www.watchdocumentaries.com/'
        self.DEFAULT_ICON_URL = 'https://watchdocumentaries.com/wp-content/uploads/social-logo.jpg'

        self.cachedMainSectionsItems={}

    def listMainMenu(self, cItem, nextCategory):
        self.listsTab([
            {'category': 'mainContents', 'title': _('Main')},
            {'category': 'categories', 'title': _('Category')},
            {'category': 'search', 'title': _('Search'), 'search_item': True, },
            {'category': 'search_history', 'title': _('Search history'), }]
            , cItem)

    def listMainContents(self, cItem):
        sts, data = self.cm.getPage(self.getMainUrl())
        if not sts: return

        sections = ph.getAllItemsBetweenMarkers(data, ('<section', '>', 'class="elementor-section'), '</section', withMarkers=False)

        for section in sections:
            name = ph.getDataBetweenMarkers(section, ('<h2', '>'), '</h2>', withMarkers=False)[1]
            if not name or name == 'About': continue

            articles=ph.getAllItemsBetweenMarkers(section, ('<article', '>'), '</article>', withMarkers=False)
            items = []
            for article in articles:
                a = ph.getDataBetweenMarkers(article, ('<a', '>', 'data-post-id'), '</a>', withMarkers=True)[1]
                url = ph.getattr(a, 'href')
                title = ph.cleanHtml(ph.getattr(a, 'title'))
                icon = ph.getattr(ph.getDataBetweenMarkers(a, '<img','>', withMarkers=True )[1], 'data-src')

                params = dict(cItem)
                params.update({'url':url, 'title':title, 'icon':icon})
                items.append(params)

            self.cachedMainSectionsItems[name]=items

            params = dict(cItem)
            params.update({'title':name, 'category':'listMainSubContents'})
            self.addDir(params)


    def listMainSubContents(self, cItem):
        title = cItem['title']
        items = self.cachedMainSectionsItems.get(title, [])

        for item in items:
            params = dict(cItem)
            params.update(item)
            self.addVideo(params)


    def listCategories(self, cItem, nextCategory):
        sts, data = self.cm.getPage(self.getMainUrl())
        if not sts: return

        categories = ph.getAllItemsBetweenMarkers(data, ('<li', '>', 'menu-item-object-category'), '</li>', withMarkers=False)
        for cat in categories:
            url = ph.getattr(cat, 'href')
            name= ph.cleanHtml(ph.getDataBetweenMarkers(cat, ('<a', '>'), '</a>', withMarkers=False)[1])

            params = dict(cItem)
            params.update({"category":nextCategory, 'title':name, 'url':url})
            self.addDir(params)


    def listItems(self, cItem):
        url = cItem['url']
        sts, data = self.cm.getPage(url)
        if not sts: return

        articles = ph.getAllItemsBetweenMarkers(data, ("<article", '>'), '</article')

        for article in articles:
            imageContainer = ph.getDataBetweenMarkers(article, ('<a', '>', 'class="blog-img"'), '</a>',withMarkers=True)[1]
            url = ph.getattr(imageContainer, 'href')
            title = ph.cleanHtml(ph.getattr(imageContainer, 'title'))
            icon = ph.getattr(ph.getDataBetweenMarkers(imageContainer, '<img', '>', withMarkers=True)[1], 'data-src')
            publication = ph.getDataBetweenMarkers(article, ('<time', '>'), '</time>', withMarkers=False)[1]
            desc = ph.getDataBetweenMarkers(article, ('<div', '>', 'class="entry-content'), '</div>', withMarkers=False)[1]
            desc = publication + "\n" + desc

            params = dict(cItem)
            params.update({ 'title':title, 'url':url, 'icon':icon, 'desc':desc, 'good_for_fav':True})
            self.addVideo(params)

        pageNavigation = ph.getDataBetweenMarkers(data, ('<div', '>', "class='wp-pagenavi'"), '</div>', withMarkers=False)[1]
        nextPage = ph.getattr(ph.getDataBetweenMarkers(pageNavigation, ('<a','>', 'class="page larger"'),'</a>', withMarkers=True)[1], 'href')
        if nextPage:
            params = dict(cItem)
            params.update({ 'title':_("Next page"), 'url':nextPage})
            self.addNext(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        cItem = dict(cItem)
        cItem.update({'category': 'listItems', 'url': ('%s?s=%s') % (self.MAIN_URL, urllib.quote(searchPattern))})
        self.listItems(cItem)

    def getCustomLinksForVideo(self, cItem):
        urlTab=[]
        url = cItem['url']
        sts, data = self.cm.getPage(url)
        if not sts: return

        printDBG(data)
        link = ph.getattr(ph.getDataBetweenMarkers(data, ('<iframe', '>'), '</iframe>', withMarkers=True)[1], 'src')
        if not link:
            link = ph.getSearchGroups(data, '"embedUrl":"([^"]+?)"', grupsNum=1)[0].replace('\\', '')
        if not link:
            link = ph.getSearchGroups(data, '"single_video_url":"([^"]+?)"', grupsNum=1)[0].replace('\\', '')

        tmpTab = self.up.getVideoLinkExt(link)
        for item in tmpTab:
            item['need_resolve'] = 0
            urlTab.append(item)

        return urlTab

    @handleServiceDecorator
    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        category = self.currItem.get("category")

        # MAIN MENU
        if category is None:
            self.listMainMenu({'name': 'category', 'type': 'category'}, 'listItems')
        if category == 'mainContents':
            self.listMainContents(self.currItem)
        if category == 'listMainSubContents':
            self.listMainSubContents(self.currItem)
        if category == 'categories':
            self.listCategories(self.currItem,'listItems')
        if category == 'listItems':
            self.listItems(self.currItem)
        # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
        # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)
