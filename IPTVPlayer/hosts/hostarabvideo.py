# -*- coding: utf-8 -*-

import re
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc

def gettytul():
    return 'ArabVideo'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__({'history': 'ArabVideo.co'})

        self.MAIN_URL = 'https://r.3rb.cam/'
        self.DEFAULT_ICON = self.MAIN_URL + '/uploads/custom-logo.png'

        self.seasonEpsCache = {}

    def getPage(self, baseUrl, addParams=None, post_data=None):
        baseUrl = ph.std_url(baseUrl)
        if addParams is None:
            addParams = dict(self.defaultParams)
        return self.cm.getPage(baseUrl, addParams, post_data)

    def mainMenu(self, cItem):
        MAIN_CAT_TAB = [
            {'category': 'show_movies', 'title':'افلام عربية', 'url': self.MAIN_URL + '/category.php?cat=moviearabic', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_movies', 'title':'افلام اجنبية', 'url': self.MAIN_URL + '/category.php?cat=movieenglish', 'icon':self.DEFAULT_ICON_URL},

            {'category': 'show_series', 'title': 'مسلسلات عربية', 'url': self.MAIN_URL + '/category.php?cat=mosalsalatarabia', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'مسلسلات أجنبية', 'url': self.MAIN_URL + '/category.php?cat=mosalsalatenglish', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'مسلسلات تركية', 'url': self.MAIN_URL + '/category.php?cat=turkish-series', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2023', 'url': self.MAIN_URL + '/category.php?cat=ramdan2023', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2022', 'url': self.MAIN_URL + '/category.php?cat=ramdan2022', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2021', 'url': self.MAIN_URL + '/category.php?cat=ramdan2021', 'icon':self.DEFAULT_ICON_URL},

            {'category': 'search', 'title': _('Search'), 'search_item': True, 'icon': self.DEFAULT_ICON_URL },
            {'category': 'search_history', 'title': _('Search history')     , 'icon': self.DEFAULT_ICON_URL }
        ]

        self.listsTab(MAIN_CAT_TAB, cItem)

    def show_movies(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        result = re.findall('</span>.*?<a href="([^"]+)" title="([^"]+?)">.+?data-echo="([^"]+)" class="img-responsive">', data, re.S)
        for e in result:
            if "مسلسل"  in e[1]:
                continue

            if "حلقة"  in e[1]:
                continue

            link = e[0].replace("watch.php","play.php")
            title = e[1].replace("مشاهدة","").replace("مشاهده","").replace("مترجم","").replace("فيلم","").replace("اونلاين","").replace("اون لاين","").replace("برنامج","").replace("WEB-DL","").replace("BRRip","").replace("720p","").replace("HD-TC","").replace("HDRip","").replace("HD-CAM","").replace("DVDRip","").replace("BluRay","").replace("1080p","").replace("WEBRip","").replace("WEB-dl","").replace("4K","").replace("All","").replace("BDRip","").replace("HDCAM","").replace("HDTC","").replace("HDTV","").replace("HD","").replace("720","").replace("HDCam","").replace("Full HD","").replace("1080","").replace("HC","").replace("Web-dl","").replace("انمي","")

            icon = ph.std_url(e[2])
            m = re.search('([0-9]{4})', title)
            if m:
                sYear = str(m.group(0))
                title = title.replace(sYear,'')
            year = sYear
            self.addVideo({'good_for_fav':True, 'title': title, 'url':link, 'icon':icon, 'desc': year, 'with_mini_cover':True})

        if nextPage := re.search('<a href="([^"]+?)">&raquo;</a>', data, re.S):
            self.addNext(cItem| {'url': self.MAIN_URL + nextPage[1]})

    def show_series(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        result = re.findall('</span>.*?<a href="([^"]+)" title="([^"]+?)">.+?data-echo="([^"]+)" class="img-responsive">', data, re.S)
        for e in result:
            link = e[0].replace("watch.php","play.php")
            title = e[1].replace("مشاهدة","").replace("مسلسل","").replace("انمي","").replace("مترجمة","").replace("مترجم","").replace("مشاهده","").replace("برنامج","").replace("مترجمة","").replace("فيلم","").replace("اون لاين","").replace("WEB-DL","").replace("BRRip","").replace("720p","").replace("HD-TC","").replace("HDRip","").replace("HD-CAM","").replace("DVDRip","").replace("BluRay","").replace("1080p","").replace("WEBRip","").replace("WEB-dl","").replace("مترجم ","").replace("مشاهدة وتحميل","").replace("اون لاين","")

            eps = ''
            m = re.search('([0-9]+)', title)
            if m:
                eps = m[1]
            title = title.split('موسم')[0].split('الحلقة')[0] + eps
            icon = ph.std_url(e[2])
            year = ''
            self.addDir(cItem | {'good_for_fav':True, 'title': title, 'category':'show_seasons', 'url':link,'desc':year, 'icon':icon, 'with_mini_cover':True })

        if nextPage := re.search('<a href="([^"]+?)">&raquo;</a>', data, re.S):
            self.addNext(cItem| {'url': self.MAIN_URL + nextPage[1]})

    def show_seasons(self, cItem):
        self.seasonEpsCache = {}

        cItem = dict(cItem)

        url = cItem['url']
        sts, html = self.getPage(url)
        if not sts:
            return

        if data2 := re.findall('<div class="SeasonsEpisodes" style="display:none;" data-serie="(.+?)">(.+?)</div>', html, re.S):
            for elem in data2:
                season = elem[0]
                epsData = elem[1]
                if data3 := re.findall('<a class=.*?href="([^"]+?)" title="[^"]+?"><em>([0-9]+)</em><span>حلقة</span></a>', epsData, re.S):
                    self.seasonEpsCache[season]=data3

            if not self.seasonEpsCache:
                return

            if len(data2) > 1:
                self.addDir(cItem | {'good_for_fav':True, 'title': season, 'category':'show_eps'})
            else:
                self.show_eps(cItem | {'good_for_fav':True, 'title': list(self.seasonEpsCache.keys())[0]})

    def show_eps(self, cItem):
        for elem in self.seasonEpsCache.get(cItem['title'],[]):
            self.addVideo(cItem | {'good_for_fav':True, 'title': elem[1], 'url':self.MAIN_URL + elem[0].replace("watch.php","play.php"), 'with_mini_cover':True})

    def getCustomLinksForVideo(self,cItem):
        urlTab = []

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        data0 = re.findall("""data-embed='([^']+?)'.*?</span>([^<]+?)<""", data, re.S)
        for e in data0:
            link = e[0]
            name = e[1]
            if link.startswith('//'):
                link = 'https:' + link
            urlTab.append({'name': name, 'url': link, 'need_resolve':1 })
        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        urlTab = []
        urlTab = self.up.getVideoLinkExt(videoUrl)
        return urlTab


    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("ArabVideo.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)

        url = cItem.get('url', None)
        url = url if url else f'{self.MAIN_URL}/search.php?keywords={ph.std_url(searchPattern)}'

        if 'movie' == searchType:
            self.show_movies(cItem |  {'url': url})
        elif 'serie' == searchType:
            self.show_series(cItem |  {'url': url})

    def getSearchTypes(self):
        searchTypesOptions = []
        searchTypesOptions.append(("فيلم", "movie"))
        searchTypesOptions.append(("مسلسل", "serie"))
        return searchTypesOptions

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)
