# -*- coding: utf-8 -*-

import re
import urllib.parse
import urllib.request
import urllib.error

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs.pCommon import isValidUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG


def gettytul():
    return 'kinox'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'kinox.to', 'cookie': 'kinox.to.cookie'})
        self.DEFAULT_ICON_URL = 'https://www.medienrecht-urheberrecht.de/images/Urteil_streaming-plattform.PNG'
        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'
        self.HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html'}
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE, 'cookie_items': {'ListMode': 'cover', 'CinemaMode': 'cover'}}
        self.MAIN_URL = 'https://kinox.today/'

    def mainMenu(self, cItem):
        MAIN_CAT_TAB = [
            {'category': 'listItems', 'title': _('Movies'), 'url': self.getFullUrl('/kinofilme-online'), },
            {'category': 'listItems', 'title': _('Cinema movies'), 'url': self.getFullUrl('aktuelle-kinofilme-im-kino/')},
            {'category': 'listItems', 'title': _('Series'), 'url': self.getFullUrl('serienstream-deutsch/')},
            {'category': 'listGenres', 'title': _('Genres')},

            {'category': 'search', 'title': _('Search'), 'search_item': True, },
            {'category': 'search_history', 'title': _('Search history'), }
        ]
        self.listsTab(MAIN_CAT_TAB, cItem)


    def listGenres(self, cItem):
        printDBG("Kinox.listGenres")
        GENRES_TAB =[
            {'category': 'listItems','url': self.MAIN_URL+'/action/', 'title': 'Action'},
            {'category': 'listItems','url': self.MAIN_URL+'/animation/', 'title': 'Animation'},
            {'category': 'listItems','url': self.MAIN_URL+'/komodie/', 'title': 'Komödie'},
            {'category': 'listItems','url': self.MAIN_URL+'/dokumentation/', 'title': 'Dokumentation'},
            {'category': 'listItems','url': self.MAIN_URL+'/familie/', 'title': 'Familie'},
            {'category': 'listItems','url': self.MAIN_URL+'/historien/', 'title': 'Historienfilme'},
            {'category': 'listItems','url': self.MAIN_URL+'/musikfilme/', 'title': 'Musikfilme'},
            {'category': 'listItems','url': self.MAIN_URL+'/romantik/', 'title': 'Romantik'},
            {'category': 'listItems','url': self.MAIN_URL+'/sport/', 'title': 'Sport'},
            {'category': 'listItems','url': self.MAIN_URL+'/krieg/', 'title': 'Krieg'},
            {'category': 'listItems','url': self.MAIN_URL+'/abenteuer/', 'title': 'Abenteuer'},
            {'category': 'listItems','url': self.MAIN_URL+'/biographie/', 'title': 'Biographie'},
            {'category': 'listItems','url': self.MAIN_URL+'/krimi/', 'title': 'Krimi'},
            {'category': 'listItems','url': self.MAIN_URL+'/drama/', 'title': 'Drama'},
            {'category': 'listItems','url': self.MAIN_URL+'/fantasy/', 'title': 'Fantasy'},
            {'category': 'listItems','url': self.MAIN_URL+'/horror/', 'title': 'Horror'},
            {'category': 'listItems','url': self.MAIN_URL+'/mystery/', 'title': 'Mystery'},
            {'category': 'listItems','url': self.MAIN_URL+'/sci-fi/', 'title': 'Science-Fiction'},
            {'category': 'listItems','url': self.MAIN_URL+'/thriller/', 'title': 'Thriller'},
            {'category': 'listItems','url': self.MAIN_URL+'/western/', 'title': 'Western'},
            {'category': 'listItems','url': self.MAIN_URL+'/erotikfilme/', 'title': 'Erotikfilme'},
        ]

        self.listsTab(GENRES_TAB, cItem)

    def listItems(self, cItem):
        printDBG("Kinox.listItems")
        url = cItem['url']
        if cItem.get('title','') == 'Search':
            url += "&search_start=" + str(cItem['search_start'])

        sts, data = self.cm.getPage(url, self.defaultParams)
        if not sts:
            return

        if match0 := re.findall('<div class="short-entry">(.*?)</div>\s*</div>', data, re.S):
            for e in match0:
                if match1 := re.search('<a href="([^"]+?)".*?img src="([^"]+?)".*>(.*?)</a>', e, re.S):
                    url = match1[1]
                    icon = match1[2]
                    title = match1[3]
                    if 'serie-num' in e:
                        self.addDir(cItem | {'category': 'listEpisodes', 'icon': icon, 'title': title, 'url': url, 'with_mini_cover':True})
                    else:
                        self.addVideo(cItem | {'icon': icon, 'title': title, 'url': url})

        if match1 := re.search('>weiter.*?<a href="([^"]+?)"', data, re.S):
            if cItem.get('title','') == 'Search':
                nextPage = cItem['url']
                cItem['search_start'] += 1
                cItem['category']='listItems'
            else:
                nextPage = match1[1]
            self.addNext(cItem | {'url': nextPage})


    def listEpisodes(self, cItem):
        printDBG("Kinox.listEpisodes")

        url = cItem['url']

        sts, data = self.cm.getPage(url, self.defaultParams)
        if not sts:
            return

        if match0 := re.search('<ul class="dropdown-menu series-select-menu ep-menu">(.*?)<div class="series-select dropdown">', data, re.S):
            if match1 := re.findall('<li id="serie.*?>.*?</li>.*?</li>', match0[1], re.S):
                for m in match1:
                    title=re.search('<a href="#">(.*?)</a>', m)[1]
                    links = re.findall('<a href="#" id=.*?data-link="([^"]+?)".*?alt="([^"]+?)"', m, re.S)

                    self.addVideo(cItem | {'title': title, 'url': url+title, 'links':links})

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("Kinox.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)
        cItem['url'] = self.getFullUrl('/index.php?do=search&subaction=search&full_search=0&result_from=0&story=' + urllib.parse.quote_plus(searchPattern))
        cItem['search_start']=0
        cItem['category']='listItems'
        self.listItems(cItem)

    def getCustomLinksForVideo(self, cItem):
        printDBG("Kinox.getCustomLinksForVideo [%s]" % cItem)
        retTab = []

        if links := cItem.get('links', []):
            for e in links:
                retTab.append({'name': e[1], 'url': e[0], 'need_resolve': 1})

            return retTab

        url = cItem['url']
        sts, data = self.cm.getPage(url, self.defaultParams)
        if not sts:
            return retTab

        if match := re.search('<ul id="HosterList.*?>(.*?)</ul>', data, re.S):
            if match2 := re.findall('<li.?data-link="([^"]+?)".*?Named">(.*?)<.*?</li>', match[1], re.S):
                for url, name in match2:
                    retTab.append({'name': name, 'url': url, 'need_resolve': 1})

        return retTab

    def getCustomVideoLinks(self, videoUrl):
        printDBG("Kinox.getCustomVideoLinks [%s]" % videoUrl)
        urlTab = []

        if videoUrl.startswith('//'):
            videoUrl = 'https:' + videoUrl

        if isValidUrl(videoUrl):
            urlTab = self.up.getVideoLinkExt(videoUrl)

        return urlTab

    def getCustomArticleContent(self, cItem):
        printDBG("Kinox.getCustomArticleContent [%s]" % cItem)
        retTab = []

        url = cItem.get('url', '')

        sts, data = self.cm.getPage(url, self.defaultParams)
        if not sts:
            return retTab

        if match := re.search('<div class="Descriptore">(.*?)<', data, re.S):
            desc = match[1]

        otherInfo = {}
        if matchDuration := re.search('<li class="DetailDat" title="Runtime"><span class="Runtime"></span>(.*?)</li>', data, re.S):
            otherInfo['duration'] = matchDuration[1]

        if matchGenres:= re.search('<li class="DetailDat" title="Genre"><span class="Genre"></span>(.*?)</li>', data, re.S):
            if matchAllGenres := re.findall('<a.*?>(.*?)</a>', matchGenres[1], re.S):
                otherInfo['genre'] = ",".join(matchAllGenres)

        icon = cItem.get('icon', self.DEFAULT_ICON_URL)

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': self.getFullUrl(icon)}], 'other_info': otherInfo}]


    def withArticleContent(self, cItem):
        if cItem['type'] == 'video' or cItem.get('category') in ['listEpisodes',]:
            return True
        return False
