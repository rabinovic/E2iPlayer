# -*- coding: utf-8 -*-

import json
from typing import List

from urllib.parse import quote, quote_plus
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase

from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc

###################################################
# Config options for HOST
###################################################
from Components.config import ConfigSelection, ConfigYesNo, config,getConfigListEntry
config.plugins.iptvplayer.uniqueNameForPin = ConfigYesNo(default=True)
config.plugins.iptvplayer.uniqueNameForOptions = ConfigSelection(default="de", choices=[
        ("de", 'Deutsch ( DE )'),
        ("fr", 'Français ( FR )')
    ])
def GetConfigList():
    optionList = [
        getConfigListEntry(_("Pin protection for plugin") + " :", config.plugins.iptvplayer.uniqueNameForPin),
        getConfigListEntry(_("Select language:"), config.plugins.iptvplayer.uniqueNameForOptions)]
    return optionList
###################################################

def gettytul():
    return 'XXXXXX'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__({'history': 'XXXXXX','cookie': 'XXXXX.cookie'}, True)

        self.DEFAULT_ICON = 'https://XXXXXXXXX.XYZ/YYYYYYYYYY.png'
        self.MAIN_URL = 'https://XXXXXXXXX.xyz/'

        self.USER_AGENT = getDefaultUserAgent()
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Referer':self.MAIN_URL, 'Accept': 'application/json'}
        self.defaultParams = {'header':self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def isProtectedByPinCode(self):
        return config.plugins.iptvplayer.uniqueNameForPin.value

    def withArticleContent(self, cItem):
        if 'video' == cItem.get('type', '') or 'explore_item' == cItem.get('category', ''):
            return True
        return False

    def getPage(self, baseUrl, addParams=None, post_data = None):
        if addParams is None:
            addParams = dict(self.defaultParams)

        return self.cm.getPage(baseUrl, addParams, post_data)

    def listMainMenu(self, cItem):
        printDBG(f"XXXXXXXXX.listMainMenu cItem [{cItem}]")

        MAIN_CAT_TAB = [
                        {'category':'listItems', 'title': 'Filme', 'url':'XXXXXXXXX.XYZ', 'icon':self.DEFAULT_ICON_URL},
                        {'category':'listItems', 'title': 'Serien', 'url':'XXXXXXXXX.XYZ', 'icon':self.DEFAULT_ICON_URL},
                        {'category':'search','title': _('Search'), 'url':'XXXXXXXXX.XYZ', 'search_item':True, 'icon':self.DEFAULT_ICON_URL},
                        {'category': 'search_history', 'title': _('Search history'), },
                    ]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        url = cItem['url']

        sts, data = self.getPage(url)
        if not sts:
            return []

        param = dict(cItem)

        param.update({ 'title' : 'XY', 'icon': 'icon', 'url': 'url',  'desc': 'desc'})

        self.addDir(param | {'category':'listSeasons', 'with_mini_cover':True})

        self.addVideo(param | {'url':'url'})


        #if nextPage:
        param.update({'page': 'nextPage'})
        self.addNext(param)


    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("XXXXXX.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.MAIN_URL + 'api/v1/search/%s?query=%s&limit=8' % (quote(searchPattern), quote_plus(searchPattern))

        sts, data = self.getPage(url)
        if not sts:
            return []

        param = dict(cItem)

        param.update({ 'title' : 'XY', 'icon': 'icon', 'url': 'url',  'desc': 'desc'})

        self.addDir(param | {'category':'listSeasons', 'with_mini_cover':True})

        self.addVideo(param | {'url':'url'})

        #if nextPage:
        param.update({'page': 'nextPage'})
        self.addNext(param)

    def getCustomLinksForVideo(self, cItem):
        printDBG("moflix-stream.getCustomLinksForVideo cItem [%s]" % cItem)
        urlTab = []

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return []

        # ...
        urlTab.append({'name':'name', 'url':'videoUrl', 'need_resolve': 1})

        return urlTab

    def getCustomVideoLinks(self, url):
        return self.up.getVideoLinkExt(url)

    def getCustomArticleContent(self, cItem) -> List:
        return super().getCustomArticleContent(cItem)