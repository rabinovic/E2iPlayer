# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getBaseUrl, isValidUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, rm
###################################################

###################################################
# FOREIGN import
###################################################
import re
import urllib.request
import urllib.parse
import urllib.error
from urllib.parse import urlparse
try:
    import json
except Exception:
    import simplejson as json
###################################################


def gettytul():
    return 'http://tainieskaiseires.tv/'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'tainieskaiseires.tv', 'cookie': 'tainieskaiseires.tv.cookie'})
        self.defaultParams = {'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        self.DEFAULT_ICON_URL = 'http://www.tainieskaiseires.tv/wp-content/uploads/2017/01/Logo-002.png'
        self.HEADER = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0', 'DNT': '1', 'Accept': 'text/html', 'Accept-Encoding': 'gzip, deflate'}
        self.AJAX_HEADER = dict(self.HEADER)
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Accept': '*/*'})
        self.MAIN_URL = None
        self.seasonsCache = {}
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, url, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)
        return self.cm.getPage(url, addParams, post_data)

    def selectDomain(self):
        domain = 'http://www.tainieskaiseires.tv/'
        addParams = dict(self.defaultParams)
        addParams['with_metadata'] = True

        sts, data = self.getPage(domain, addParams)
        if sts:
            self.MAIN_URL = getBaseUrl(data.meta['url'])
        else:
            self.MAIN_URL = domain

        self.MAIN_CAT_TAB = [{'category': 'search', 'title': _('Search'), 'search_item': True, },
                             {'category': 'search_history', 'title': _('Search history'), }]

    def listMainMenu(self, cItem):
        printDBG("TainieskaiSeiresTv.listMainMenu")
        sts, data = self.getPage(self.getMainUrl())
        if sts:
            data = ph.getDataBetweenNodes(data, ('<ul', '>', 'classic-dropdown'), ('</nav', '>'))[1]
            data = re.compile('(<li[^>]*?>|</li>|<ul[^>]*?>|</ul>)').split(data)
            if len(data) > 1:
                try:
                    cTree = self.listToDir(data[1:-1], 0)[0]
                    params = dict(cItem)
                    params['c_tree'] = cTree['list'][0]
                    params['category'] = 'list_categories'
                    self.listCategories(params, 'list_items')
                except Exception:
                    printExc()
        self.listsTab(self.MAIN_CAT_TAB, cItem)

    def listCategories(self, cItem, nextCategory):
        printDBG("TainieskaiSeiresTv.listCategories")
        try:
            cTree = cItem['c_tree']
            for item in cTree['list']:
                title = ph.cleanHtml(ph.getDataBetweenMarkers(item['dat'], '<a', '</a>')[1])
                url = self.getFullUrl(ph.getSearchGroups(item['dat'], '''href=['"]([^'^"]+?)['"]''')[0])
                if 'list' not in item:
                    if isValidUrl(url) and title != '':
                        params = dict(cItem)
                        params.pop('c_tree', None)
                        params.update({'good_for_fav': False, 'category': nextCategory, 'title': title, 'url': url})
                        self.addDir(params)
                elif len(item['list']) == 1 and title != '':
                    params = dict(cItem)
                    params.pop('c_tree', None)
                    params.update({'good_for_fav': False, 'c_tree': item['list'][0], 'title': title, 'url': url})
                    self.addDir(params)
            url = cItem.get('url', '')
            if len(self.currList) and isValidUrl(url) and '@' not in url:
                params = dict(cItem)
                params.pop('c_tree', None)
                params.update({'good_for_fav': False, 'category': nextCategory, 'title': _('--All--'), 'url': url})
                self.currList.insert(0, params)
        except Exception:
            printExc()

    def listItems(self, cItem, nextCategory1, nextCategory2):
        printDBG("TainieskaiSeiresTv.listItems")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        parsedUri = urlparse(cItem['url'])
        if parsedUri.path == '/' and parsedUri.query == '':
            data = ph.getDataBetweenMarkers(data, '<article', '</article>')[1]
            data = ph.getAllItemsBetweenNodes(data, ('<a', '>', '/category/'), ('</strong', '>'))
            printDBG(data)
            for item in data:
                url = self.getFullUrl(ph.getSearchGroups(item, '''\shref=['"]([^'^"]+?)['"]''')[0])
                if url == '':
                    continue
                icon = self.getFullIconUrl(ph.getSearchGroups(item, '''\ssrc=['"]([^'^"]+?)['"]''')[0])
                title = ph.cleanHtml(item)
                params = dict(cItem)
                params.update({'good_for_fav': False, 'title': title, 'url': url, 'icon': icon})
                self.addDir(params)
        elif '/category/' in cItem['url'] or '/?s=' in cItem['url']:
            page = cItem.get('page', 1)
            data = ph.getDataBetweenMarkers(data, '<section', '</section>')[1]
            nextPage = ph.getDataBetweenNodes(data, ('<div', '>', 'pagenavi'), ('</div', '>'))[1]
            nextPage = self.getFullUrl(ph.getSearchGroups(nextPage, '''\shref=['"]([^"^']*?/page/%s/[^"^']*?)['"]''' % (page + 1))[0])

            data = ph.rgetAllItemsBetweenNodes(data, ('<div', '>', 'clearfix'), ('<div', '>', 'video-item'))
            for item in data:
                url = self.getFullUrl(ph.getSearchGroups(item, '''\shref=['"]([^"^']+?)['"]''')[0])
                if url == '':
                    continue
                icon = self.getFullIconUrl(ph.getSearchGroups(item, '''\ssrc=['"]([^"^']+?)['"]''')[0])
                title = ph.cleanHtml(ph.getDataBetweenMarkers(item, '<h3', '</h3>')[1])
                desc = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<', '>', 'fa-eye'), ('</span', '>'))[1])
                desc += '[/br]' + ph.cleanHtml(ph.getDataBetweenMarkers(item, '<p', '</p>')[1])

                params = dict(cItem)
                params.update({'good_for_fav': True, 'category': nextCategory2, 'title': title, 'url': url, 'info_url': url, 'icon': icon, 'desc': desc})
                self.addDir(params)

            if nextPage != '':
                params = dict(cItem)
                params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
                self.addDir(params)
        else:
            data = ph.getDataBetweenMarkers(data, '<article', '</article>')[1]
            data = ph.getAllItemsBetweenNodes(data, ('<div', '>', 'smart-box '), ('<div', '>', '"clear"'))
            for section in data:
                sTitle = ph.cleanHtml(ph.getDataBetweenMarkers(section, '<h2', '</h2>')[1])
                itemsTab = []
                section = ph.getAllItemsBetweenNodes(section, ('<div', '>', 'video-item'), ('</a', '>'))
                for item in section:
                    url = self.getFullUrl(ph.getSearchGroups(item, '''\shref=['"]([^"^']+?)['"]''')[0])
                    if url == '':
                        continue
                    icon = self.getFullIconUrl(ph.getSearchGroups(item, '''\ssrc=['"]([^"^']+?)['"]''')[0])
                    title = ph.cleanHtml(ph.getSearchGroups(item, '''\stitle=['"]([^"^']+?)['"]''')[0])
                    if title == '':
                        title = ph.cleanHtml(ph.getSearchGroups(item, '''\salt=['"]([^"^']+?)['"]''')[0])
                    desc = ph.cleanHtml(item)
                    itemsTab.append({'title': title, 'url': url, 'info_url': url, 'icon': icon, 'desc': desc})
                if len(itemsTab):
                    params = dict(cItem)
                    params.update({'title': sTitle, 'category': nextCategory1, 'items_tab': itemsTab})
                    self.addDir(params)

    def listSectionItems(self, cItem, nextCategory):
        printDBG("TainieskaiSeiresTv.listSectionItems [%s]" % cItem)
        cItem = dict(cItem)
        listTab = cItem.pop('items_tab', [])
        cItem.update({'good_for_fav': True, 'category': nextCategory})
        self.listsTab(listTab, cItem)

    def exploreItem(self, cItem, nextCategory):
        printDBG("TainieskaiSeiresTv.exploreItem")
        linksTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        baseTitle = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<h1>', '</h1>', '<strong'), ('<', '>'))[1])
        if baseTitle == '':
            baseTitle = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<h', '>', 'entry-title'), ('</h', '>'))[1])
        if baseTitle == '':
            baseTitle = cItem['title']

        tmp = ph.getDataBetweenNodes(data, ('<div', '>', 'player-embed'), ('', '</div>'))[1]
        url = self.getFullUrl(ph.getSearchGroups(tmp, '''<(?:iframe|embed)[^>]+?src=['"]([^"^']+?)['"]''', 1, True)[0])
        if isValidUrl(url):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': '%s %s' % (_('[trailer]'), cItem['title']), 'url': url})
            self.addVideo(params)

        reObjSeasons = re.compile('(<strong[^>]*?>[^>]*?SEASON[^>]*?</strong>)', re.IGNORECASE)
        seasonsData = ph.getDataBetweenReMarkers(data, reObjSeasons, re.compile('<div[^>]+?item\-tax\-list[^>]+?>'))[1]
        seasonsData = reObjSeasons.split(seasonsData)
        if len(seasonsData):
            del seasonsData[0]
        if 0 == len(seasonsData):
            seasonsData = ph.getDataBetweenMarkers(data, '<table', '</table>')[1]
            seasonsData = ph.getAllItemsBetweenMarkers(seasonsData, '<td', '</td>')

        if 0 == len(seasonsData):
            data = ph.getAllItemsBetweenNodes(data, ('<a', '>', 'href'), ('</a', '>'))
            for item in data:
                name = ph.cleanHtml(item)
                url = self.getFullUrl(ph.getSearchGroups(item, '''\shref=['"]([^"^']+?)['"]''')[0])
                if 1 == self.up.checkHostSupport(url):
                    linksTab.append({'name': name, 'url': url, 'need_resolve': 1})
        elif 'SEASON' in ph.cleanHtml(seasonsData[0]).upper():
            seasonsKeys = []
            self.seasonsCache = {}
            domain = self.up.getDomain(self.getMainUrl())
            reSeasonObj = re.compile('SEASON\s+?[0-9]+', re.IGNORECASE)
            reEpisodeObj = re.compile('\sE[0-9]+(?:\-E?[0-9]+)?', re.IGNORECASE)
            for idx in range(0, len(seasonsData), 2):
                sTitle = ph.cleanHtml(seasonsData[idx]).replace("Ε", "E")
                sSubTitle = ph.cleanHtml(reSeasonObj.sub('', sTitle))
                seasonId = ph.cleanHtml(ph.getSearchGroups(sTitle, 'SEASON\s*?([0-9]+(?:\-[0-9]+)?)', 1, True)[0])
                printDBG("++ SEASON ID -> \"%s\" \"%s\"" % (seasonId, sSubTitle))
                if seasonId not in seasonsKeys:
                    seasonsKeys.append(seasonId)
                    self.seasonsCache[seasonId] = {'title': sTitle.replace(sSubTitle, ''), 'season_id': seasonId, 'episodes': []}

                tmp = ph.getAllItemsBetweenMarkers(seasonsData[idx + 1], '<a', '</a>')
                for item in tmp:
                    eTitle = ' ' + ph.cleanHtml(item).replace("Ε", "E")
                    eSubTitle = ph.cleanHtml(reEpisodeObj.sub('', eTitle))
                    episodeId = ph.cleanHtml(ph.getSearchGroups(eTitle, '\s(E[0-9]+(?:\-E?[0-9]+)?)', 1, True)[0])
                    printDBG("+++++++++++++++++ EPISODE ID \"%s\"-> \"%s\"" % (eTitle, episodeId))
                    url = self.getFullUrl(ph.getSearchGroups(item, '''\shref=['"]([^"^']+?)['"]''')[0])
                    if url == '':
                        continue

                    fakeUrl = cItem['url'] + '#season=%s&episode=%s' % (seasonId, eTitle)

                    if seasonId != '' and episodeId != '':
                        title = '%s - S%s%s %s' % (baseTitle, seasonId.zfill(2), episodeId, eSubTitle)
                    else:
                        title = ('%s - %s %s') % (baseTitle, sTitle.replace(sSubTitle, ''), eTitle)
                    self.seasonsCache[seasonId]['episodes'].append({'title': title, 'episode_id': seasonId, 'url': fakeUrl})

                    if domain not in url:
                        name = self.up.getHostName(url)
                        if sSubTitle != '':
                            name += ' ' + sSubTitle
                    else:
                        name = sSubTitle

            for seasonKey in seasonsKeys:
                season = self.seasonsCache[seasonKey]
                if 0 == len(season['episodes']):
                    continue
                params = dict(cItem)
                params.update({'good_for_fav': False, 'category': nextCategory, 'title': season['title'], 'season_id': season['season_id']})
                self.addDir(params)

            if 1 == len(self.currList):
                cItem = self.currList.pop()
                self.listEpisodes(cItem)
        else:
            for idx in range(0, len(seasonsData), 2):
                quality = ph.cleanHtml(seasonsData[idx])
                tmp = ph.getAllItemsBetweenMarkers(seasonsData[idx + 1], '<a', '</a>')
                for item in tmp:
                    name = ph.cleanHtml(item)
                    url = self.getFullUrl(ph.getSearchGroups(item, '''\shref=['"]([^"^']+?)['"]''')[0])
                    linksTab.append({'name': '%s - %s' % (quality, name), 'url': url, 'need_resolve': 1})

        if len(linksTab):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': baseTitle})
            self.addVideo(params)

    def listEpisodes(self, cItem):
        printDBG("TainieskaiSeiresTv.listEpisodes")
        listTab = self.seasonsCache[cItem['season_id']]['episodes']
        cItem = dict(cItem)
        cItem.update({'good_for_fav': False})
        self.listsTab(listTab, cItem, 'video')

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("TainieskaiSeiresTv.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)
        cItem['url'] = self.getFullUrl('/?s=') + urllib.parse.quote_plus(searchPattern)
        cItem['category'] = 'list_items'
        self.listItems(cItem, 'list_section_items', 'explore_item')

    def getCustomLinksForVideo(self, cItem):
        printDBG("TainieskaiSeiresTv.getCustomLinksForVideo [%s]" % cItem)

        if 1 == self.up.checkHostSupport(cItem['url']):
            return self.up.getVideoLinkExt(cItem['url'])

    def getCustomVideoLinks(self, videoUrl):
        printDBG("TainieskaiSeiresTv.getCustomVideoLinks [%s]" % videoUrl)

        domain = self.up.getDomain(self.getMainUrl())
        if domain in videoUrl:
            sts, data = self.getPage(videoUrl)
            if not sts:
                return

            data = ph.getDataBetweenNodes(data, ('<div', '>', 'player-embed'), ('', '</div>'))[1]
            videoUrl = self.getFullUrl(ph.getSearchGroups(data, '''<iframe[^>]+?src=['"]([^"^']+?)['"]''', 1, True)[0])

        return self.up.getVideoLinkExt(videoUrl)

    def getCustomArticleContent(self, cItem):
        printDBG("TainieskaiSeiresTv.getCustomArticleContent [%s]" % cItem)
        retTab = []

        sts, data = self.getPage(cItem.get('info_url', ''))
        if not sts:
            return retTab

        data = ph.rgetDataBetweenNodes(data, ('</table', '>'), ('<table', '>'))[1]
        tmp = ph.getAllItemsBetweenMarkers(data, '<th', '</th>')

        if len(tmp) > 2:
            desc = tmp[1].split('</span>', 1)
            title = ph.cleanHtml(desc[0])
            desc = ph.cleanHtml(desc[-1])
        icon = self.getFullIconUrl(ph.getSearchGroups(tmp[0], '''\ssrc=['"]([^'^"]+?)['"]''')[0])

        if title == '':
            title = cItem['title']
        if desc == '':
            desc = cItem.get('desc', '')
        if icon == '':
            icon = cItem.get('icon', '')

        descData = ph.getAllItemsBetweenMarkers(tmp[-1], '<span', '</span>')
        descTabMap = {"Ετος παραγωγής": "year",
                      "Βαθμολογία": "rated",
                      "Ηθοποιοί": "actors",
                      "Κατηγορία": "genres",
                      "Σκηνοθεσία": "director",
                     }

        otherInfo = {}
        for idx in range(1, len(descData), 2):
            key = ph.cleanHtml(descData[idx - 1])
            val = ph.cleanHtml(descData[idx])
            if val.endswith(','):
                val = val[:-1]
            if key in descTabMap:
                try:
                    otherInfo[descTabMap[key]] = val
                except Exception:
                    continue

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc), 'images': [{'title': '', 'url': self.getFullUrl(icon)}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        HostBase.handleService(self, index, refresh, searchPattern, searchType)
        if self.MAIN_URL is None:
            rm(self.COOKIE_FILE)
            self.selectDomain()

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        mode = self.currItem.get("mode", '')

        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    #MAIN MENU
        if name is None:
            self.listMainMenu({'name': 'category'})
        elif category == 'list_categories':
            self.listCategories(self.currItem, 'list_items')
        elif category == 'list_filters':
            self.listFilters(self.currItem, 'list_items')
        elif category == 'list_items':
            self.listItems(self.currItem, 'list_section_items', 'explore_item')
        elif category == 'list_section_items':
            self.listSectionItems(self.currItem, 'explore_item')
        elif category == 'explore_item':
            self.exploreItem(self.currItem, 'list_episodes')
        elif category == 'list_episodes':
            self.listEpisodes(self.currItem)
    #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        HostBase.endHandleService(self, refresh)

    def withArticleContent(self, cItem):
        return 'info_url' in cItem
