# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs.pCommon import isValidUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, rm
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
###################################################

###################################################
# FOREIGN import
###################################################
import urllib.parse
import re
import urllib.request
import urllib.parse
import urllib.error
try:
    import json
except Exception:
    import simplejson as json
###################################################


def gettytul():
    return 'http://filmaon.com/'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'filmaon.com', 'cookie': 'filmaon.com.cookie'})
        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'
        self.MAIN_URL = 'http://www.filmaon.com/'
        self.DEFAULT_ICON_URL = 'http://www.filmaon.com/wp-content/themes/Dooplay_/assets/img/Logomakr1.png'
        self.HTTP_HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html', 'Accept-Encoding': 'gzip, deflate', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}
        self.AJAX_HEADER = dict(self.HTTP_HEADER)
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest', 'Accept-Encoding': 'gzip, deflate', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Accept': 'application/json, text/javascript, */*; q=0.01'})

        self.defaultParams = {'header': self.HTTP_HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)
        origBaseUrl = baseUrl
        baseUrl = self.cm.iriToUri(baseUrl)

        def _getFullUrl(url):
            if isValidUrl(url):
                return url
            return urllib.parse.urljoin(baseUrl, url)
        addParams['cloudflare_params'] = {'domain': self.up.getDomain(baseUrl), 'cookie_file': self.COOKIE_FILE, 'User-Agent': self.USER_AGENT, 'full_url_handle': _getFullUrl}
        return self.cm.getPageCFProtection(baseUrl, addParams, post_data)

    def getFullIconUrl(self, url, baseUrl=None):
        url = HostBase.getFullIconUrl(self, url, baseUrl)
        if url != '':
            url = strwithmeta(url, {'Referer': self.getMainUrl()})
        return url

    def listMainMenu(self, cItem, nextCategory1, nextCategory2):
        printDBG("InteriaTv.listMainMenu")
        sts, data = self.getPage(self.getMainUrl())
        if not sts:
            return
        self.setMainUrl(data.meta['url'])

        data = ph.getAllItemsBetweenNodes(data, ('<div', '>', 'sidemenu'), ('</div', '>'), False)
        for section in data:
            sTitle = ph.cleanHtml(ph.getDataBetweenNodes(section, ('<h', '>'), ('</h', '>'), False)[1])
            section = ph.getAllItemsBetweenNodes(section, ('<li', '>'), ('</li', '>'), False)
            subItems = []
            for item in section:
                if 'icon-home' in item:
                    continue
                url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = ph.cleanHtml(item)
                params = {'name': 'category', 'type': 'category', 'category': nextCategory2, 'title': title, 'url': url}
                subItems.append(params)
                if url.endswith('/seriale/'):
                    for it in [('/seasons/', 'SEZONET E SERIALEV'), ('/episodes/', 'EPISODET E REJA')]:
                        params = {'name': 'category', 'type': 'category', 'category': nextCategory2, 'title': it[1], 'url': self.getFullUrl(it[0])}
                        subItems.append(params)

            if len(subItems):
                params = {'name': 'category', 'type': 'category', 'category': nextCategory1, 'title': sTitle, 'sub_items': subItems}
                self.addDir(params)

        MAIN_CAT_TAB = [{'category': 'top', 'title': 'TOP IMDb 50', 'url': self.getFullUrl('/top-imdb/')},
                        {'category': 'search', 'title': _('Search'), 'search_item': True},
                        {'category': 'search_history', 'title': _('Search history')}, ]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listSubItems(self, cItem):
        printDBG("FilmaonCom.listSubItems")
        self.currList = cItem['sub_items']

    def listItems(self, cItem, nextCategory, data=None):
        printDBG("InteriaTv.listItems")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        self.setMainUrl(data.meta['url'])

        nextPage = ph.getDataBetweenNodes(data, ('<div', '>', 'pagination'), ('</div', '>'))[1]
        nextPage = self.getFullUrl(ph.getSearchGroups(nextPage, '''<a[^>]+?href=['"]([^"^']+?)['"][^>]*?>\s*?{0}\s*?<'''.format(page + 1))[0])

        data = ph.getAllItemsBetweenNodes(data, ('<article', '>'), ('</article', '>'))
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            icon = self.getFullIconUrl(ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            title = ph.cleanHtml(ph.getDataBetweenMarkers(item, '<h3', '</h3>')[1])
            if title == '':
                title = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<div', '>', 'title'), ('</div', '>'))[1])
            if cItem['url'].endswith('/seasons/'):
                title = ph.cleanHtml(ph.getSearchGroups(item, '''alt=['"]([^"^']+?)['"]''')[0])
            sTitle = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<span', '>', 'serie'), ('</span', '>'))[1])
            if sTitle != '':
                title = '%s %s' % (sTitle, title)

            desc = ph.cleanHtml(ph.getDataBetweenMarkers(item, '<p', '</p>')[1])
            descTab = []
            rating = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<div', '>', 'rating'), ('</div', '>'))[1])
            item = ph.getAllItemsBetweenMarkers(item, '<span', '</span>')
            for t in item:
                t = ph.cleanHtml(t)
                if t != '':
                    descTab.append(t)
            if rating != '':
                descTab.append('%s/10' % rating)
            desc = ' | '.join(descTab) + '[/br]' + desc
            params = {'good_for_fav': True, 'priv_has_art': True, 'category': nextCategory, 'url': url, 'title': title, 'desc': desc, 'icon': icon}
            if '/episodes/' in url or '-episodi' in url:
                self.addVideo(params)
            else:
                self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _('Next page'), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def listTop(self, cItem, nextCategory1, nextCategory2):
        printDBG("InteriaTv.listTop")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        self.setMainUrl(data.meta['url'])

        reObj = re.compile('''<div[^>]+?top\-imdb\-item[^>]*>''', re.IGNORECASE)
        data = ph.getDataBetweenNodes(data, ('<div', '>', 'top-imdb-list'), ('<script', '>'), False)[1]
        data = re.compile('''<div[^>]+?top\-imdb\-list[^>]*>''', re.IGNORECASE).split(data)
        for section in data:
            sTitle = ph.cleanHtml(ph.getDataBetweenMarkers(section, '<h3', '</h3>')[1])
            section = reObj.split(section)
            if len(section):
                del section[0]
            subItems = []
            for item in section:
                url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                icon = self.getFullIconUrl(ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
                title = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<div', '>', 'title'), ('</div', '>'))[1])
                descTab = []
                number = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<div', '>', 'puesto'), ('</div', '>'))[1])
                rating = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<div', '>', 'rating'), ('</div', '>'))[1])
                descTab.append(number)
                descTab.append('%s/10' % (rating))
                params = {'good_for_fav': True, 'priv_has_art': True, 'name': 'category', 'type': 'category', 'category': nextCategory2, 'title': title, 'url': url, 'desc': ' '.join(descTab), 'icon': icon}
                subItems.append(params)

            if len(subItems):
                params = {'name': 'category', 'type': 'category', 'category': nextCategory1, 'title': sTitle, 'sub_items': subItems}
                self.addDir(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("FilmaonCom.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))

        url = self.getFullUrl('?s=') + urllib.parse.quote_plus(searchPattern)
        cItem = dict(cItem)
        cItem.update({'url': url, 'category': 'list_items'})
        self.listItems(cItem, 'explore_item')

    def getEpisodes(self, data, iTitle, iIcon):
        data = ph.getAllItemsBetweenNodes(data, ('<li', '>'), ('</li', '>'))
        episodesTab = []
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            icon = self.getFullIconUrl(ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            if icon == '':
                icon = iIcon

            numer = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<div', '>', 'numerando'), ('</div', '>'))[1])
            title = ph.getDataBetweenNodes(item, ('<div', '>', 'episodiotitle'), ('</div', '>'))[1].split('</a>', 1)
            desc = ph.cleanHtml(title[-1])
            title = ph.cleanHtml(title[0])

            params = {'good_for_fav': True, 'priv_has_art': True, 'type': 'video', 'url': url, 'title': '%s: %s %s' % (iTitle, numer, title), 'desc': desc, 'icon': icon}
            episodesTab.append(params)
        return episodesTab

    def exploreItem(self, cItem, nextCategory):
        printDBG("InteriaTv.listItems")
        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        cUrl = data.meta['url']
        self.setMainUrl(cUrl)

        tmp = ph.getDataBetweenNodes(data, ('<div', '>', 'trailer'), ('</div', '>'))[1]
        iTrailer = self.getFullUrl(ph.getSearchGroups(tmp, '''<iframe[^>]+?src=['"]([^"^']+?youtube[^"^']+?)['"]''', 1, True)[0])

        iTitle = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<h1', '</h1>')[1])
        iIcon = ph.getDataBetweenNodes(data, ('<div', '>', 'poster'), ('</div', '>'))[1]
        iIcon = self.getFullIconUrl(ph.getSearchGroups(iIcon, '''<img[^>]+?src=['"]([^"^']+?)['"]''', 1, True)[0])

        if '/filma/' in cUrl or '/episodes/' in cUrl:
            if '/links/' in data or 'play-box' in data:
                params = dict(cItem)
                self.addVideo(params)
        elif '/seriale/' in cUrl:
            data = ph.getAllItemsBetweenNodes(data, ('<div', '>', 'seasons'), ('<', '>', 'script'))
            for seasonData in data:
                seasonData = seasonData.split('</ul>')
                for season in seasonData:
                    sTitle = ph.cleanHtml(ph.getDataBetweenNodes(season, ('<div', '>', '"se-'), ('</div', '>'))[1])
                    if sTitle == '':
                        sTitle = ph.cleanHtml(ph.getDataBetweenNodes(season, ('<span', '>', 'title'), ('</span', '>'))[1])
                    episodesTab = self.getEpisodes(season, iTitle, iIcon)
                    if len(episodesTab):
                        params = {'good_for_fav': False, 'category': nextCategory, 'title': sTitle, 'sub_items': episodesTab, 'desc': '', 'icon': iIcon}
                        self.addDir(params)
        elif '/seasons/' in cUrl:
            data = ph.getDataBetweenNodes(data, ('<ul', '>', 'episodios'), ('</ul', '>'))[1]
            self.currList = self.getEpisodes(data, iTitle, iIcon)

        if iTrailer != '':
            params = {'good_for_fav': False, 'url': iTrailer, 'title': '%s - %s' % (iTitle, _('trailer')), 'icon': iIcon}
            self.addVideo(params)

    def getCustomLinksForVideo(self, cItem):
        printDBG("FilmaonCom.getCustomLinksForVideo [%s]" % cItem)

        if 1 == self.up.checkHostSupport(cItem.get('url', '')):
            videoUrl = cItem['url'].replace('youtu.be/', 'youtube.com/watch?v=')
            return self.up.getVideoLinkExt(videoUrl)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        cUrl = data.meta['url']
        self.setMainUrl(cUrl)

        retTab = []

        namesData = {}
        tmp = ph.getAllItemsBetweenNodes(data, ('<a', '>', 'options'), ('</a', '>'))
        for item in tmp:
            name = ph.cleanHtml(item)
            id = ph.getSearchGroups(item, '''href=['"]#([^"^']+?)['"]''', 1, True)[0]
            namesData[id] = name

        tmp = ph.getAllItemsBetweenNodes(data, ('<div', '>', 'play-box'), ('</div', '>'))
        for item in tmp:
            url = self.getFullUrl(ph.getSearchGroups(item, '''<iframe[^>]+?src=['"]([^"^']+?)['"]''', 1, True)[0])
            if 1 != self.up.checkHostSupport(url):
                continue
            id = ph.getSearchGroups(item, '''id=['"]([^"^']+?)['"]''', 1, True)[0]
            name = namesData.get(id, '')
            if name == '':
                name = self.up.getHostName(url)
            retTab.append({'name': name, 'url': strwithmeta(url, {'Referer': cUrl}), 'need_resolve': 1})

        data = ph.getDataBetweenNodes(data, ('<div', '>', 'links_table'), ('</table', '>'))[1].split('<tbody', 1)[-1]
        data = ph.getAllItemsBetweenNodes(data, ('<tr', '>'), ('</tr', '>'))
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])

            item = ph.getAllItemsBetweenNodes(item, ('<td', '>'), ('</td', '>'))
            if len(item) < 2:
                continue
            fakeHostUrl = 'http://%s/' % ph.cleanHtml(item[1]).lower()
            #if 1 != self.up.checkHostSupport(fakeHostUrl): continue
            title = []
            for idx in range(1, len(item)):
                title.append(ph.cleanHtml(item[idx]))
            title = ' | '.join(title)
            retTab.append({'name': title, 'url': strwithmeta(url, {'Referer': cUrl}), 'need_resolve': 1})

        return retTab

    def getCustomVideoLinks(self, baseUrl):
        printDBG("FilmaonCom.getCustomVideoLinks [%s]" % baseUrl)
        baseUrl = strwithmeta(baseUrl)
        urlTab = []

        if 1 == self.up.checkHostSupport(baseUrl):
            urlTab = self.up.getVideoLinkExt(baseUrl)
        else:
            paramsUrl = dict(self.defaultParams)
            paramsUrl['header'] = dict(self.AJAX_HEADER)
            paramsUrl['header']['Referer'] = baseUrl.meta['Referer']

            sts, data = self.getPage(baseUrl, paramsUrl)
            if not sts:
                return
            printDBG(data)
            try:
                videoUrl = self.getFullUrl(ph.getSearchGroups(data, '''location\.href=['"]([^"^']+?)['"]''', 1, True)[0])
                urlTab = self.up.getVideoLinkExt(videoUrl)
            except Exception:
                printExc()

        return urlTab

    def getCustomArticleContent(self, cItem):
        printDBG("FilmaonCom.getCustomArticleContent [%s]" % cItem)

        retTab = []

        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return []
        cUrl = data.meta['url']
        self.setMainUrl(cUrl)

        if '/episodes/' in cUrl:
            m1 = 'info'
        else:
            m1 = 'sheader'
        data = ph.getDataBetweenNodes(data, ('<div', '>', m1), ('<script', '>'), False)[1]

        title = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<h1', '</h1>')[1])
        icon = ph.getDataBetweenNodes(data, ('<div', '>', 'poster'), ('</div', '>'))[1]
        icon = self.getFullIconUrl(ph.getSearchGroups(icon, '''<img[^>]+?src=['"]([^"^']+?)['"]''', 1, True)[0])
        desc = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<div', '>', 'wp-content'), ('</div', '>'))[1])

        tmp = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<div', '>', 'starstruck-rating'), ('</div', '>'), False)[1])
        if tmp != '':
            otherInfo['rating'] = tmp
        tmp = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<b', '</b>', 'Vleresimi IMDb'), ('</div', '>'), False)[1])
        if tmp != '':
            otherInfo['imdb_rating'] = tmp
        tmp = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<b', '</b>', 'Vleresimi TMDb'), ('</div', '>'), False)[1])
        if tmp != '':
            otherInfo['tmdb_rating'] = tmp
        tmp = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<b', '</b>', 'Titulli origjinal'), ('</div', '>'), False)[1])
        if tmp != '':
            otherInfo['original_title'] = tmp
        tmp = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<b', '</b>', 'Statusi'), ('</div', '>'), False)[1])
        if tmp != '':
            otherInfo['status'] = tmp
        tmp = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<b', '</b>', 'Sezonet'), ('</div', '>'), False)[1])
        if tmp != '':
            otherInfo['seasons'] = tmp
        tmp = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<b', '</b>', 'Episodat'), ('</div', '>'), False)[1])
        if tmp != '':
            otherInfo['episodes'] = tmp
        tmp = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<span', '>', 'date'), ('</span', '>'), False)[1])
        if tmp != '':
            otherInfo['released'] = tmp
        tmp = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<span', '>', 'country'), ('</span', '>'), False)[1])
        if tmp != '':
            otherInfo['country'] = tmp
        tmp = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<span', '>', 'runtime'), ('</span', '>'), False)[1])
        if tmp != '':
            otherInfo['duration'] = tmp
        tmp = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<a', '>', '/network/'), ('</a', '>'), False)[1])
        if tmp != '':
            otherInfo['station'] = tmp

        tmp = ph.getDataBetweenNodes(data, ('<div', '>', 'sgeneros'), ('</div', '>'), False)[1]
        tmp = ph.getAllItemsBetweenNodes(tmp, ('<a', '>'), ('</a', '>'), False)
        if len(tmp):
            otherInfo['genres'] = ph.cleanHtml(', '.join(tmp))

        creators = []
        actors = []
        directors = []

        data = ph.getDataBetweenNodes(data, ('<div', '>', 'persons'), ('<div', '>', 'info'), False)[1].split('class="person"')
        for item in data:
            name = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<div', '>', 'name'), ('</div', '>'), False)[1])
            caracter = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<div', '>', 'caracter'), ('</div', '>'), False)[1]).lower()
            if caracter == 'director':
                directors.append(name)
            elif caracter == 'creator':
                creators.append(name)
            elif name != '':
                actors.append(name)

        if 1 == len(directors):
            otherInfo['director'] = ph.cleanHtml(', '.join(directors))
        elif 1 < len(directors):
            otherInfo['directors'] = ph.cleanHtml(', '.join(directors))
        if 1 == len(creators):
            otherInfo['creator'] = ph.cleanHtml(', '.join(creators))
        elif 1 < len(creators):
            otherInfo['creators'] = ph.cleanHtml(', '.join(creators))
        if len(actors):
            otherInfo['actors'] = ph.cleanHtml(', '.join(actors))

        if title == '':
            title = cItem['title']
        if icon == '':
            icon = cItem.get('icon', self.DEFAULT_ICON_URL)

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc), 'images': [{'title': '', 'url': self.getFullUrl(icon)}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        HostBase.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        mode = self.currItem.get("mode", '')

        printDBG("handleService: |||| name[%s], category[%s] " % (name, category))
        self.currList = []

    #MAIN MENU
        if name is None and category == '':
            rm(self.COOKIE_FILE)
            self.listMainMenu({'name': 'category'}, 'sub_items', 'list_items')
        elif category == 'list_items':
            self.listItems(self.currItem, 'explore_item')
        elif category == 'sub_items':
            self.listSubItems(self.currItem)
        elif category == 'top':
            self.listTop(self.currItem, 'sub_items', 'explore_item')
        elif category == 'explore_item':
            self.exploreItem(self.currItem, 'sub_items')
    #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        HostBase.endHandleService(self, refresh)

    def withArticleContent(self, cItem):
        if cItem.get('priv_has_art', False):
            return True
        return False
