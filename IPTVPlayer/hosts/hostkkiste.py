# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultHeader
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, MergeDicts
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta

from Plugins.Extensions.IPTVPlayer.libs import ph
###################################################

###################################################
# FOREIGN import
###################################################
import re
from urllib.parse import urlsplit, urlunsplit, urlparse
from datetime import datetime
import copy
from Components.config import config, ConfigSelection, ConfigText, getConfigListEntry
import json
###################################################


def gettytul():
    return 'https://kinokiste.club/'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'kinokiste.club', 'cookie': 'kinokiste.club.cookie'})
        self.reIMG = re.compile(r'''<img[^>]+?src=(['"])([^>]*?)(?:\1)''', re.I)
        self.HTTP_HEADER = getDefaultHeader(browser='chrome')
        self.defaultParams = {'with_metadata':True, 'header': self.HTTP_HEADER}

        self.DEFAULT_ICON_URL = 'https://tarnkappe.info/wp-content/uploads/kkiste-logo.jpg'

        self.DEFAULT_MAIN_URL = gettytul()
        self.MAIN_URL = None

        self.filterYear = [f"{y}" for y in range(datetime.now().year, 1989, -1)]
        self.filterGenre = ["Action", "Abenteuer", "Animation", "Biographie", "Komödie", "Krimi", "Dokumentation", "Drama", "Familie", "Fantasy", "Geschichte", "Horror", "Musik", "Mystery", "Romantik", "Reality-TV", "Sport", "Thriller", "Krieg", "Western"]

        self.cacheFilters = {}
        self.cacheFiltersKeys = []
        self.cacheLinks = {}

    def getPage(self, url, addParams=None, post_data=None):
        addParams = dict(self.defaultParams) if addParams is None else addParams
        addParams['cloudflare_params'] = {'cookie_file': self.COOKIE_FILE, 'User-Agent': self.HTTP_HEADER['User-Agent']}
        return self.cm.getPageCFProtection(url, addParams, post_data)
        #return self.cm.getPage(url, addParams, post_data)

    def mainMenu(self, cItem):
        printDBG("KinokisteClub.listMain")
        self.cacheFilters = {}

        MAIN_CAT_TAB = [
            {'category': 'listItems', 'title': _('Kinofilme'), 'url': "https://api.tmdb.club/data/featured/?lang=2&cat=movie&limit=4", "icon": self.DEFAULT_ICON_URL},
            {'category': 'listItems', 'title': _('Neu'), 'url': "https://api.tmdb.club/data/latest/?lang=2&cat=movie&limit=8", "icon": self.DEFAULT_ICON_URL},
            {'category': 'listItems', 'title': _('Trending'), 'url': "https://api.tmdb.club/data/trending/?lang=2&cat=movie&limit=16", "icon": self.DEFAULT_ICON_URL},
            {'category': 'listFilter', 'title': _('Movies'), 'cat': "movies", "icon": self.DEFAULT_ICON_URL},
            {'category': 'listFilter', 'title': _('Series'), 'cat': "tvseries", "icon": self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("KinokisteClub.listItems")

        url = cItem['url']

        sts, data = self.getPage(url)
        if not sts:
            return

        data = json.loads(data)
        for obj in data:
            title, desc, icon = self.extractInfo(obj)
            streams = obj["streams"]
            self.addVideo({'good_for_fav': True, 'title': title, 'icon':icon, 'desc':desc, 'streams':streams})

    def extractInfo(self, obj):
        title = obj["title"]
        year = obj["year"]
        storyline = obj["storyline"]
        duration = obj["runtime"]
        cast=obj["cast"]

        desc = f"{storyline}\n{year=}\n{duration=}\n{cast=}"

        if icon:=obj.get("poster_path", ""):
            icon = "https://image.tmdb.org/t/p/w300/"+icon
        else:
            icon = "https://tmdb.club/images/no_poster.jpg"
        return title,desc,icon

    def listItems2(self, cItem):
        printDBG("KinokisteClub.listItems2")

        url = cItem['url']
        page = cItem.get('page', '1')
        cat = cItem['cat']

        headers = copy.deepcopy(self.defaultParams)
        headers['header'].update({
                                'Accept': '*/*','Accept-Language': 'de,en-US;q=0.7,en;q=0.3', 'Referer': 'https://kinokiste.club/','Origin': 'https://kinokiste.club','DNT': '1' ,'Connection': 'keep-alive', 'Sec-Fetch-Dest': 'empty', 'Sec-Fetch-Mode': 'cors' , 'Sec-Fetch-Site': 'cross-site' , 'Pragma': 'no-cache' ,'Cache-Control': 'no-cache', 'TE': 'trailers'
                            })

        sts, data = self.cm.getPage(url + f"&page={page}" if page else "", addParams=headers)
        if not sts:
            return

        data = json.loads(data)

        current_page = data['pager']['currentPage']

        movies = data['movies']

        for obj in movies:
            title, desc, icon = self.extractInfo(obj)

            if cat == 'tvseries':
                id = obj['_id']
                self.addDir({'category':'exploreEpisodes', 'good_for_fav': True, 'title': title, 'icon':icon, 'desc':desc, '_id':id, 'with_mini_cover':True})
            else:
                if streams := obj.get("streams", None):
                    self.addVideo({'good_for_fav': True, 'title': title, 'icon':icon, 'streams':streams, 'desc':desc})

        if len(movies)>=20:
            params = dict(cItem)
            params.update({'category':'listItems2', 'good_for_fav': False, 'title': _("Next Page"), 'icon':self.DEFAULT_ICON_URL, 'page': current_page+1})
            self.addMore(params)

    def exploreEpisodes(self, cItem):
        printDBG("KinokisteClub.exploreEpisodes")
        #TODO
        pass

    def listFilter(self, cItem):
        printDBG("KinokisteClub.listFilter")

        genre = cItem.get("genre", "")
        year = cItem.get("year", "")

        if genre and year:
            type=cItem["cat"]
            self.listItems2(cItem | {'url':f"https://api.tmdb.club/data/browse/?lang=2&keyword=&year={year}&rating=&votes=&genre={genre}&country=&cast=&directors=&type={type}&order_by=releases", 'page':'1'})
        else:
            if not genre:
                for g in self.filterGenre:
                    self.addDir(cItem | {'good_for_fav': False, 'title': g, 'genre':g})
            elif not year:
                for y in self.filterYear:
                    self.addDir(cItem | {'good_for_fav': False, 'title': y, 'year':y})

    def getCustomLinksForVideo(self, cItem):
        printDBG(f"KinokisteClub.getCustomVideoLinks [{cItem}]")
        linksTab = []

        streams = cItem["streams"]

        for stream in streams:
            # printDBG(stream)
            link = stream["stream"]
            source = stream.get("source", "")
            relase= stream.get("release", "")
            linksTab.append({'name': f"{self.up.getHostName(link)}/{source} {relase}", 'url': link, 'need_resolve': 1})

        return linksTab


    def listSearchResult(self, cItem, searchPattern=None, searchType=None):

        url = f"https://api.tmdb.club/data/browse/?lang=2&keyword={searchPattern}&year=&rating=&votes=&genre=&country=&cast=&directors=&type=&order_by="

        params = dict(cItem)
        params.update({'url': url, 'cat':'search'})
        self.listItems2(params)

    def getCustomVideoLinks(self, videoUrl):
        printDBG(f"KinokisteClub.getCustomVideoLinks [{videoUrl}]")
        return self.up.getVideoLinkExt(videoUrl)

    def getCustomArticleContent(self, cItem, data=None):
        printDBG("KinokisteClub.getCustomArticleContent [%s]" % cItem)
        retTab = []
        itemsList = []

        if not data:
            url = cItem.get('prev_url', cItem['url'])
            sts, data = self.getPage(url)
            if not sts:
                return []
            data = re.sub("<!--[\s\S]*?-->", "", data)

        data = ph.find(data, ('<div', '>', 'content'), '<style', flags=0)[1]

        title = ph.cleanHtml(ph.find(data, ('<h', '>'), '</h', flags=0)[1])
        icon = ''
        desc = ph.cleanHtml(ph.find(data, ('<p', '>'), '</p>', flags=0)[1])

        data = ph.findall(data, ('<div', '>', 'extras'), '</div>', flags=0)
        for item in data:
            item = item.split(':', 1)
            label = ph.cleanHtml(item[0])
            value = ph.cleanHtml(item[-1])
            if label and value:
                itemsList.append((label + ':', value))

        if title == '':
            title = cItem['title']
        if icon == '':
            icon = cItem.get('icon', self.DEFAULT_ICON_URL)
        if desc == '':
            desc = cItem.get('desc', '')

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc), 'images': [{'title': '', 'url': self.getFullUrl(icon)}], 'other_info': {'custom_items_list': itemsList}}]


    def withArticleContent(self, cItem):
        return 'prev_url' in cItem
