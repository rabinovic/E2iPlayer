# -*- coding: utf-8 -*-

import json
import re

from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, handleServiceDecorator
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import getDirectM3U8Playlist, getMPDLinksWithMeta
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta

def gettytul():
    return 'https://www.tele5.de/'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'tele5.de', 'cookie': 'tele5.de.cookie'})
        self.DEFAULT_ICON_URL = "https://www.tele5.de/wp-content/uploads/2019/04/logo_tele5@3x-150x150.png"

        self.MAIN_URL = 'https://www.tele5.de/'
        self.MAIN_CAT_TAB = [{'category': 'movies', 'title': _("Movies"), 'url': self.getFullUrl("/filme/")},
                             {'category': 'series', 'title': _("Series"), 'url': self.getFullUrl("/serien/")},
                             {'category': 'series', 'title': _("Mediathek"), 'url': self.getFullUrl("/mediathek/")},
                             {'category': 'search', 'title': _('Search'), 'search_item': True, },
                             {'category': 'search_history', 'title': _('Search history'), },
                             ]

        self.cacheEpisodes = {}
        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:96.0) Gecko/20100101 Firefox/96.0'
        self.HTTP_HEADER = {
            'User-Agent': self.USER_AGENT,
            'Accept': '*/*',
            'Accept-Language': 'de,de-DE;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
            'Accept-Encoding': 'gzip, deflate, br',
            'Content-Type': 'application/json',
            'Origin': 'https://tele5.de',
            'pragma': 'no-cache',
            'Referer': 'https://tele5.de/',
            'X-Disco-Client': 'Alps:HyogaPlayer:0.0.0',
            'X-Disco-Params': 'realm=dmaxde',
            'X-Device-Info': 'STONEJS/1 (Unknown/Unknown; Unknown/Unknown; Unknown)',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'cross-site',
        }

        self.defaultParams = {'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def exploreItem(self, cItem):
        printDBG("Tele5.exploreItem cItem: %s" % cItem)

        url = cItem['url']
        addParams = dict(self.defaultParams)
        addParams.update({'header': {'User-Agent': self.USER_AGENT}})
        sts, data = self.cm.getPage(url, self.defaultParams)
        if not sts: return

        items = ph.getAllItemsBetweenMarkers(data, ('<hyoga-player', '>'), '</hyoga-player>')

        for item in items:
            videoid = ph.getattr(item, 'assetid')

            postData = {
                "deviceInfo": {
                    "adBlocker":'false',
                    "drmSupported":'false',
                    "hdrCapabilities":["SDR"],
                    "hwDecodingCapabilities":[],
                    "soundCapabilities":["STEREO"]
                },
                "videoId":f"{videoid}",
                "wisteriaProperties":{},
            }

            postData = json.dumps(postData).encode('utf-8')

            addParams = dict(self.defaultParams)
            addParams.update({'header': self.HTTP_HEADER })
            sts, data = self.cm.getPage('https://eu1-prod.disco-api.com/token/?realm=dmaxde', addParams)
            if not sts:
                return
            json_data = json.loads(data)
            token = json_data['data']['attributes']['token']

            addParams = dict(self.defaultParams)
            addParams.update({
                'header': self.HTTP_HEADER | {'Authorization': f'Bearer {token}'},
                'raw_post_data': True
            })

            sts, data = self.cm.getPage('https://eu1-prod.disco-api.com/playback/v3/videoPlaybackInfo', addParams, post_data=postData)
            if not sts:
                return

            json_data = json.loads(data)
            streams = json_data.get('data', {}).get('attributes',{}).get('streaming',[])

            for stream in streams:
                type_ = stream['type']
                #if type_ == 'dash':
                if type_ == 'hls':
                    url = stream['url']
                    params = dict(cItem)
                    params.update({'url': url})
                    self.addVideo(params)

    def getCustomLinksForVideo(self, cItem):
        printDBG("Tele5.getCustomLinksForVideo [%s]" % cItem)
        linksTab = []
        link = cItem['url']

        link = strwithmeta(link, {'iptv_proto': 'm3u8'})
        #link = strwithmeta(link, {'iptv_proto': 'mpd'})
        #linksTab.extend(getDirectM3U8Playlist(link, sortWithMaxBitrate=999999999))
        linksTab.append({'url':link,'name':'m3u8', 'need_resolve':0})

        return linksTab

    def listSeries(self, cItem, nextCategory):
        printDBG("Tele5.listSeries cItem: %s" % cItem)

        url = cItem['url']
        addParams = dict(self.defaultParams)
        sts, data = self.cm.getPage(url, addParams)
        if not sts: return

        items = ph.getAllItemsBetweenMarkers(data, ('<div','>', 'itemprop="itemListElement"'), ('</a', '</div>'))

        for item in items:
            icon = ph.getSearchGroups(item, '''data-backgroundimage="([^"]+?)"''')[0]
            link = self.getFullIconUrl(ph.getSearchGroups(item, '''href="([^"]+?)"''')[0])
            title = ph.cleanHtml(ph.getSearchGroups(item, '''title="([^"]+?)"''')[0])
            desc = ph.decodeHtml(ph.getDataBetweenMarkers(item, '<p class="card-desc">', '</p>', withMarkers=False)[1].strip())

            params = dict(cItem)
            params.update({'good_for_fav': True, 'category': nextCategory, 'title': title, 'url': link, 'icon': icon,
                           'desc': desc, 'with_mini_cover':True})
            self.addDir(params)

    def listSeasons(self, cItem, nextCategory):
        printDBG(f"Tele5.listSeasons cItem: {cItem}")

        url = cItem['url']

        sts, data = self.cm.getPage(url, self.defaultParams)
        if not sts:
            return

        episodesRegex = r"<a href=\"([^\"]+?)\".*?>.*?url\((.*?)\).*?video__title\">(.*?)</div>"

        tabsRegex = r"tab-pane.*?\"(.*?)</a>\s*</div>\s*</div>\s*</div>"
        tabMatches = re.finditer(tabsRegex, data, re.MULTILINE | re.DOTALL)

        i = 1
        for tabMatch in tabMatches:
            printDBG("Tele5.listSeasons tabMatch.group(1): %s" % tabMatch.group(1))
            episodeMatches = re.finditer(episodesRegex, tabMatch.group(1), re.MULTILINE | re.DOTALL)

            self.cacheEpisodes[str(i)] = []
            for episode in episodeMatches:
                link = self.getFullUrl(episode.group(1).strip())
                icon = episode.group(2).strip()
                title = ph.cleanHtml(episode.group(3).strip())

                self.cacheEpisodes[str(i)].append({"link": link, "icon": icon, "title": title, 'with_mini_cover':True})

            params = dict(cItem)
            params.update(
                {'good_for_fav': False, 'category': nextCategory, 'title': "Staffel %s" % str(i), 'season': str(i)})
            self.addDir(params)
            i += 1

    def listEspisodes(self, cItem, nextCategory):

        printDBG("Tele5.exploreItem self.cacheEpisodes: %s" % self.cacheEpisodes)
        season = cItem['season']
        episodes = self.cacheEpisodes[season]

        for episode in episodes:
            params = dict(cItem)
            params.update(
                {'good_for_fav': True, 'category': nextCategory, 'url': episode['link'], 'title': episode['title'],
                 'icon': episode['icon'], 'with_mini_cover':True})
            self.addDir(params)

    def listMovies(self, cItem, nextCategory):
        printDBG("Tele5.listMovies cItem: %s" % cItem)

        url = cItem['url']

        sts, data = self.cm.getPage(url, self.defaultParams)
        if not sts:
            return

        items = ph.getAllItemsBetweenMarkers(data, '<div itemprop="itemListElement"', ('</a', '</div>'))

        for item in items:
            icon = ph.getSearchGroups(item, '''data-backgroundimage="([^"]+?)"''')[0]
            link = self.getFullIconUrl(ph.getSearchGroups(item, '''href="([^"]+?)"''')[0])
            title = ph.cleanHtml(ph.getSearchGroups(item, '''title="([^"]+?)"''')[0])
            desc = ph.decodeHtml(ph.getDataBetweenMarkers(item, '<p class="card-desc">', '</p>', withMarkers=False)[1].strip())

            params = dict(cItem)
            params.update({'good_for_fav': True, 'category': nextCategory, 'title': title, 'url': link, 'icon': icon,
                           'desc': desc, 'with_mini_cover':True})
            self.addDir(params)

    @handleServiceDecorator
    def handleService(self, index, refresh=0, searchPattern='', searchType=''):

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        mode = self.currItem.get("mode", '')

        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))

        # MAIN MENU
        if name is None:
            self.listsTab(self.MAIN_CAT_TAB, {'name': 'category'})
        elif category == 'movies':
            self.listMovies(self.currItem, 'exploreItem')
        elif category == 'series':
            self.listSeries(self.currItem, 'list_seasons')
        elif category == 'list_seasons':
            self.listSeasons(self.currItem, "list_episodes")
        elif category == 'list_episodes':
            self.listEspisodes(self.currItem, "exploreItem")
        elif category == 'exploreItem':
            self.exploreItem(self.currItem)
        # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
        # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)
