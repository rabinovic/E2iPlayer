# -*- coding: utf-8 -*-

import re
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent

def gettytul():
    return 'ShoofLive'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__({'history': 'shooflive.co', 'cookie': 'shooflive.cookie'})

        self.MAIN_URL = 'https://r.shooflive.co'
        self.DEFAULT_ICON = self.MAIN_URL + '/wp-content/uploads/2022/03/TvLOGOo.png'

        self.USER_AGENT = getDefaultUserAgent('chrome')
        self.HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html'}
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True,
                              'cookiefile': self.COOKIE_FILE, 'with_metadata': True}

    def getPage(self, baseUrl, addParams=None, post_data=None):
        baseUrl = ph.std_url(baseUrl)
        if addParams is None:
            addParams = dict(self.defaultParams)
        return self.cm.getPage(baseUrl, addParams, post_data)

    def mainMenu(self, cItem):
        MAIN_CAT_TAB = [
            {'category': 'show_movies', 'title':'افلام عربية', 'url': self.MAIN_URL + '/category/movies/افلام-عربية/', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_movies', 'title':'افلام اجنبية', 'url': self.MAIN_URL + '/category/movies/افلام-اجنبية/', 'icon':self.DEFAULT_ICON_URL},

            {'category': 'show_series', 'title': 'مسلسلات عربية', 'url': self.MAIN_URL + '/category/%d9%85%d8%b3%d9%84%d8%b3%d9%84%d8%a7%d8%aa/?sercat=%d8%a7%d9%84%d9%85%d8%b3%d9%84%d8%b3%d9%84%d8%a7%d8%aa-%d8%a7%d9%84%d8%b9%d8%b1%d8%a8%d9%8a%d8%a9', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'مسلسلات أجنبية', 'url': self.MAIN_URL + '/category/%d9%85%d8%b3%d9%84%d8%b3%d9%84%d8%a7%d8%aa/?sercat=%d8%a7%d9%84%d9%85%d8%b3%d9%84%d8%b3%d9%84%d8%a7%d8%aa-%d8%a7%d9%84%d8%a7%d8%ac%d9%86%d8%a8%d9%8a%d8%a9', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'مسلسلات تركية', 'url': self.MAIN_URL + '/category/%d9%85%d8%b3%d9%84%d8%b3%d9%84%d8%a7%d8%aa/?sercat=%d8%a7%d9%84%d9%85%d8%b3%d9%84%d8%b3%d9%84%d8%a7%d8%aa-%d8%a7%d9%84%d8%aa%d8%b1%d9%83%d9%8a%d8%a9', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2023', 'url': self.MAIN_URL + '/category/%d9%85%d8%b3%d9%84%d8%b3%d9%84%d8%a7%d8%aa/?sercat=%d8%b1%d9%85%d8%b6%d8%a7%d9%86-2023', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2022', 'url': self.MAIN_URL + '/category/%d9%85%d8%b3%d9%84%d8%b3%d9%84%d8%a7%d8%aa/?sercat=%d8%b1%d9%85%d8%b6%d8%a7%d9%86-2022', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2021', 'url': self.MAIN_URL + '/category/%d9%85%d8%b3%d9%84%d8%b3%d9%84%d8%a7%d8%aa/?sercat=%d8%b1%d9%85%d8%b6%d8%a7%d9%86-2021', 'icon':self.DEFAULT_ICON_URL},

            {'category': 'search', 'title': _('Search'), 'search_item': True, 'icon': self.DEFAULT_ICON_URL },
            {'category': 'search_history', 'title': _('Search history')     , 'icon': self.DEFAULT_ICON_URL }
        ]

        self.listsTab(MAIN_CAT_TAB, cItem)

    def show_movies(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        result = re.findall('href="([^<]+)" alt="(.+?)">.+?<img src="(.+?)"><.+?/release-year/(.+?)/">', data, re.S)
        for e in result:
            if "فيلم"  not in e[1]:
                continue
            link = e[0]
            title = e[1].replace("مشاهدة","").replace("مشاهده","").replace("مترجم","").replace("فيلم","").replace("اون لاين","").replace("برنامج","").replace("WEB-DL","").replace("BRRip","").replace("720p","").replace("HD-TC","").replace("HDRip","").replace("HD-CAM","").replace("DVDRip","").replace("BluRay","").replace("1080p","").replace("WEBRip","").replace("WEB-dl","").replace("4K","").replace("All","").replace("BDRip","").replace("HDCAM","").replace("HDTC","").replace("HDTV","").replace("HD","").replace("720","").replace("HDCam","").replace("Full HD","").replace("1080","").replace("HC","").replace("Web-dl","").replace("انمي","").replace("كامل","").replace("اون لاين","")

            icon = ph.std_url(e[2])
            year = e[3]
            self.addVideo({'good_for_fav':True, 'title': title, 'url':link, 'icon':icon, 'desc': year, 'with_mini_cover':True})

        if navigation:=re.search('<div class="navigation".*?</div>', data, re.S):
            if nextPage := re.search('(?:.<a href="([^"]+?)").*&laquo;</a>', navigation[0], re.S):
                self.addNext(cItem| {'url': nextPage[1]})

    def show_series(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']

        params = dict(self.defaultParams)
        referer = cItem.get('Referer', None)

        if referer:
            params['header']['Referer'] = referer
        sts, data = self.getPage(url, params)
        if not sts:
            return

        if result := re.findall('href="([^<]+)" alt="(.+?)">.+?<img src="(.+?)"><.+?/release-year/(.+?)/">', data, re.S):
            for e in result:
                if "فيلم" in e[1]:
                    continue
                link = e[0]
                title = e[1].replace("مشاهدة","").replace("مسلسل","").replace("انمي","").replace("مترجمة","").replace("مترجم","").replace("مشاهده","").replace("برنامج","").replace("مترجمة","").replace("فيلم","").replace("اون لاين","").replace("WEB-DL","").replace("BRRip","").replace("720p","").replace("HD-TC","").replace("HDRip","").replace("HD-CAM","").replace("DVDRip","").replace("BluRay","").replace("1080p","").replace("WEBRip","").replace("WEB-dl","").replace("مترجم ","").replace("مشاهدة وتحميل","").replace("اون لاين","")
                icon = ph.std_url(e[2])
                year = e[3]
                self.addDir({'good_for_fav':True, 'title': title, 'category':'show_seasons', 'url':link,'icon':icon, 'desc': year, 'with_mini_cover':True})

            if nextPage := re.search('<a class="next page-numbers" href="([^"]+?)">&laquo;', data, re.S):
                if nextPage[1] not in url:
                    self.addNext(cItem| {'url': self.MAIN_URL + nextPage[1], 'Referer':url})

    def show_seasons(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        sts, html = self.getPage(url)
        if not sts:
            return

        if '<div class="seasons">' in html:
            if data0:=re.search('<div class="seasons">(.*?)class="allepcont"', html, re.S):
                html = data0[1]
            if data1:=re.findall('data-slug="(.+?)">(.+?)</', html, re.S):
                for elem in data1:
                    title = elem[1].replace("الموسم العاشر","S10").replace("الموسم الحادي عشر","S11").replace("الموسم الثاني عشر","S12").replace("الموسم الثالث عشر","S13").replace("الموسم الرابع عشر","S14").replace("الموسم الخامس عشر","S15").replace("الموسم السادس عشر","S16").replace("الموسم السابع عشر","S17").replace("الموسم الثامن عشر","S18").replace("الموسم التاسع عشر","S19").replace("الموسم العشرون","S20").replace("الموسم الحادي و العشرون","S21").replace("الموسم الثاني و العشرون","S22").replace("الموسم الثالث و العشرون","S23").replace("الموسم الرابع والعشرون","S24").replace("الموسم الخامس و العشرون","S25").replace("الموسم السادس والعشرون","S26").replace("الموسم السابع والعشرون","S27").replace("الموسم الثامن والعشرون","S28").replace("الموسم التاسع والعشرون","S29").replace("الموسم الثلاثون","S30").replace("الموسم الحادي و الثلاثون","S31").replace("الموسم الثاني والثلاثون","S32").replace("الموسم الأول","S1").replace("الموسم الاول","S1").replace("الموسم الثاني","S2").replace("الموسم الثالث","S3").replace("الموسم الرابع","S4").replace("الموسم الخامس","S5").replace("الموسم السادس","S6").replace("الموسم السابع","S7").replace("الموسم الثامن","S8").replace("الموسم التاسع","S9").replace("الموسم","S").replace("S ","S")
                    link = self.MAIN_URL +'/series/' + str(elem[0])
                    self.addDir(cItem | {'good_for_fav':True, 'title': title, 'category':'show_eps', 'url':link, })

        if 'class="seasons">' not in html:
            if data2 := re.search('class="episodesAside">(.*?)class="detail-section" style', html, re.S):
                html = data2[1]
            if data3 := re.findall('style="order:([^<]+)" class.+?href="(.+?)">', html, re.S):
                for elem in data3:
                    title = elem[0]
                    link = elem[1]
                    self.addVideo(cItem | {'good_for_fav':True, 'title': title, 'url':link, 'with_mini_cover':True})

    def show_eps(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        sts, html = self.getPage(url)
        if not sts:
            return

        if data2 := re.search('class="episodesAside">(.*?)class="detail-section" style', html, re.S):
            html = data2[1]
        if data3 := re.findall('style="order:([^<]+)" class.+?href="(.+?)">', html, re.S):
            for elem in data3:
                title = elem[0]
                link = elem[1]
                self.addVideo(cItem | {'good_for_fav':True, 'title': title, 'url':link, 'with_mini_cover':True})


    def getCustomLinksForVideo(self,cItem):
        urlTab = []

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        sURL_MAIN = re.search('title="الرئيسية" href="(.+?)">', data, re.S)[1]
        id_ = re.search('data-id="(.+?)">' , data, re.S)[1]

        data = {'id':id_,'action':'getpostServers'}
        sts, data = self.getPage(sURL_MAIN +'/wp-admin/admin-ajax.php', post_data=data)
        if sts:
            m3url = re.search('<a class="watchNow" href="([^<]+)" target=' , data, re.S)[1]
            if m3url.startswith('//'):
                m3url = 'https:' + m3url
                params = dict(self.defaultParams)
                params['header'] = dict(params['header'])
                params['header']['Referer'] = sURL_MAIN
                sts, data = self.getPage(m3url, params)
                if sts:
                    data0 = re.findall("""<a href="javascript:void\(0\)" data-src="([^"]+?)">(.*?)</a>""", data, re.S)
                    for e in data0:
                        link = e[0]
                        if link.startswith('//'):
                            link = 'https:' + link
                        urlTab.append({'name': e[1], 'url': link, 'need_resolve':1 })
        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        urlTab = []
        urlTab = self.up.getVideoLinkExt(videoUrl)
        return urlTab


    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("ShoofLive.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)

        url = cItem.get('url', None)
        url = url if url else f'{self.MAIN_URL}/?s={ph.std_url(searchPattern)}'

        sts, data = self.getPage(url)
        if not sts:
            return

        if result := re.findall('href="([^<]+)" alt="(.+?)">.+?<img src="(.+?)"><.+?/release-year/(.+?)/">', data, re.S):
            for e in result:
                link = e[0]
                icon = ph.std_url(e[2])
                year = e[3]
                if "فيلم"  in e[1]:
                    title = e[1].replace("مشاهدة","").replace("مشاهده","").replace("مترجم","").replace("فيلم","").replace("اون لاين","").replace("برنامج","").replace("WEB-DL","").replace("BRRip","").replace("720p","").replace("HD-TC","").replace("HDRip","").replace("HD-CAM","").replace("DVDRip","").replace("BluRay","").replace("1080p","").replace("WEBRip","").replace("WEB-dl","").replace("4K","").replace("All","").replace("BDRip","").replace("HDCAM","").replace("HDTC","").replace("HDTV","").replace("HD","").replace("720","").replace("HDCam","").replace("Full HD","").replace("1080","").replace("HC","").replace("Web-dl","").replace("انمي","").replace("كامل","").replace("اون لاين","")
                    self.addVideo({'good_for_fav':True, 'title': title, 'url':link, 'icon':icon, 'desc': year, 'with_mini_cover':True})
                else:
                    title = e[1].replace("مشاهدة","").replace("مسلسل","").replace("انمي","").replace("مترجمة","").replace("مترجم","").replace("مشاهده","").replace("برنامج","").replace("مترجمة","").replace("فيلم","").replace("اون لاين","").replace("WEB-DL","").replace("BRRip","").replace("720p","").replace("HD-TC","").replace("HDRip","").replace("HD-CAM","").replace("DVDRip","").replace("BluRay","").replace("1080p","").replace("WEBRip","").replace("WEB-dl","").replace("مترجم ","").replace("مشاهدة وتحميل","").replace("اون لاين","")
                    self.addDir({'good_for_fav':True, 'title': title, 'category':'show_seasons', 'url':link,'icon':icon, 'desc': year, 'with_mini_cover':True})

        if navigation:=re.search('<div class="navigation".*?</div>', data, re.S):
            if nextPage := re.search('(?:.<a href="([^"]+?)").*&laquo;</a>', navigation[0], re.S):
                self.addNext(cItem| {'url': nextPage[1]})


    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)