# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _, SetIPTVPlayerLastHostError
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.pCommon import isValidUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, rm, GetTmpDir, WriteTextFile, ReadTextFile
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta

###################################################

###################################################
# FOREIGN import
###################################################
import re
import urllib.request
import urllib.parse
import urllib.error
from datetime import datetime
from hashlib import md5
from copy import deepcopy
from Components.config import config, ConfigText, getConfigListEntry
import json
###################################################


###################################################
# E2 GUI COMMPONENTS
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvmultipleinputbox import IPTVMultipleInputBox
from Screens.MessageBox import MessageBox
###################################################

###################################################
# Config options for HOST
###################################################
config.plugins.iptvplayer.plusdede_login = ConfigText(default="", fixed_size=False)
config.plugins.iptvplayer.plusdede_password = ConfigText(default="", fixed_size=False)


def GetConfigList():
    optionList = []
    optionList.append(getConfigListEntry(_("login") + ":", config.plugins.iptvplayer.plusdede_login))
    optionList.append(getConfigListEntry(_("password") + ":", config.plugins.iptvplayer.plusdede_password))
    return optionList
###################################################


def gettytul():
    return 'https://megadede.com/'


class IPTVHost(HostBase):
    login = None
    password = None

    def __init__(self):
        super().__init__( {'history': 'plusdede.com', 'cookie': 'plusdede.com.cookie'})
        self.DEFAULT_ICON_URL = 'https://img15.androidappsapk.co/300/f/d/3/com.joramun.plusdede.png'
        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'
        self.MAIN_URL = 'https://www.megadede.com/'
        self.HTTP_HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html', 'Accept-Encoding': 'gzip, deflate', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}
        self.AJAX_HEADER = dict(self.HTTP_HEADER)
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest', 'Accept-Encoding': 'gzip, deflate', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Accept': 'application/json, text/javascript, */*; q=0.01'})

        self.cacheLinks = {}
        self.cacheFilters = {}
        self.cacheFiltersKeys = []
        self.cacheEpisodes = {}
        self.defaultParams = {'header': self.HTTP_HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        self.MAIN_CAT_TAB = [{'category': 'list_filters', 'title': 'Series', 'url': self.getFullUrl('/series')},
                             {'category': 'list_filters', 'title': 'Pelis', 'url': self.getFullUrl('/pelis')},
                             {'category': 'list_lists', 'title': 'Listas', 'url': self.getFullUrl('/listas')},


                             {'category': 'search', 'title': _('Search'), 'search_item': True},
                             {'category': 'search_history', 'title': _('Search history')},
                            ]
        self.loggedIn = None
        self.LOGIN_MARKER_FILE = self.COOKIE_FILE + '.mark'

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)
        origBaseUrl = baseUrl
        baseUrl = self.cm.iriToUri(baseUrl)
        addParams['cloudflare_params'] = {'cookie_file': self.COOKIE_FILE, 'User-Agent': self.USER_AGENT}
        return self.cm.getPageCFProtection(baseUrl, addParams, post_data)

    def calcLoginMarker(self, login, password):
        printDBG("PlusDEDE.calcLoginMarker")
        marker = md5(login + '<-------------->' + password).hexdigest()
        printDBG("marker[%s]" % marker)
        return marker

    def saveLoginMarker(self):
        printDBG("PlusDEDE.saveLoginMarker")
        marker = self.calcLoginMarker(PlusDEDE.login, PlusDEDE.password)
        printDBG("marker[%s]" % marker)
        return WriteTextFile(self.LOGIN_MARKER_FILE, marker)

    def readLoginMarker(self):
        printDBG("PlusDEDE.saveLoginMarker")
        sts, marker = ReadTextFile(self.LOGIN_MARKER_FILE)
        if not sts:
            marker = ''
        printDBG("marker[%s]" % marker)
        return marker

    def fillCacheFilters(self, cItem):
        printDBG("PlusDEDE.listCategories")
        self.cacheFilters = {}
        self.cacheFiltersKeys = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        data = re.sub("<!--[\s\S]*?-->", "", data)

        def addFilter(data, itemMarker, valMarker, key, allTitle=None):
            self.cacheFilters[key] = []
            for item in data:
                value = ph.getSearchGroups(item, valMarker + '''="([^"]+?)"''')[0]
                title = ph.rgetDataBetweenMarkers2(item, '</%s>' % itemMarker, '>', False)[1]
                title = ph.cleanHtml(title)
                if value == '':
                    if allTitle is None:
                        allTitle = title
                    continue
                self.cacheFilters[key].append({'title': title.title(), key: value})

            if len(self.cacheFilters[key]):
                if allTitle is not None:
                    self.cacheFilters[key].insert(0, {'title': allTitle, key: ''})
                self.cacheFiltersKeys.append(key)

        # get sub categories
        tmpTab = []
        key = 'f_sub_cats'
        tmp = ph.getDataBetweenNodes(data, ('<ul', '>', 'filters'), ('</ul', '>'), False)[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, '<li', '</li>')
        for item in tmp:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
            if not isValidUrl(url):
                continue
            title = ph.cleanHtml(item)
            title = re.sub("&[^;]+?;", "", title).strip()
            tmpTab.append({'title': title, 'url': url})
        if len(tmpTab):
            self.cacheFilters[key] = tmpTab
            self.cacheFiltersKeys.append(key)

        sp = 'filter-container'
        data = ph.getDataBetweenMarkers(data, sp, 'medialist-filtered', False)[1].split(sp)
        for idx in range(len(data)):
            key = 'f_%s' % ph.getSearchGroups(data[idx], '''[^>]+?name=['"]([^'^"]+?)['"]''')[0]
            if key in ['f_year']:
                #val = ph.getSearchGroups(data[idx], '''[^>]+?value=['"]([^'^"]+?)['"]''')[0].split(';')
                #if 2 != len(val): continue
                try:
                    start = datetime.now().year #int(val[1])
                    end = 1900 #int(val[0])
                    self.cacheFilters[key] = []
                    for val in range(start, end - 1, -1):
                        self.cacheFilters[key].append({'title': str(val), key: '%s;%s' % (val, val)})
                    if len(self.cacheFilters[key]):
                        self.cacheFilters[key].insert(0, {'title': _('--Any--'), key: '%s;%s' % (end, start)})
                        self.cacheFiltersKeys.append(key)
                except Exception:
                    printExc()
            else:
                if key in ['f_quality']:
                    tmp = ph.getAllItemsBetweenMarkers(data[idx], '<input', '</label>')
                    addFilter(tmp, 'label', 'value', key)
                else:
                    tmp = ph.getAllItemsBetweenMarkers(data[idx], '<option', '</option>')
                    addFilter(tmp, 'option', 'value', key, _('--Any--'))

            if [] != self.cacheFilters.get(key, []):
                title = ph.cleanHtml(ph.getDataBetweenMarkers(data[idx], '<label', '</label>')[1])
                if len(title):
                    self.cacheFilters[key].insert(0, {'title': title, 'type': 'marker'})

        printDBG(self.cacheFilters)

    def listFilters(self, cItem, nextCategory):
        printDBG("PlusDEDE.listFilters")
        cItem = dict(cItem)

        f_idx = cItem.get('f_idx', 0)
        if f_idx == 0:
            self.fillCacheFilters(cItem)

        if f_idx >= len(self.cacheFiltersKeys):
            return

        filter = self.cacheFiltersKeys[f_idx]
        f_idx += 1
        cItem['f_idx'] = f_idx
        if f_idx == len(self.cacheFiltersKeys):
            cItem['category'] = nextCategory
        self.listsTab(self.cacheFilters.get(filter, []), cItem)

    def listMainMenu(self, cItem, nextCategory):
        printDBG("PlusDEDE.listMainMenu")
        if not self.loggedIn:
            return
        self.listsTab(self.MAIN_CAT_TAB, cItem)

    def listLists(self, cItem, nextCategory):
        printDBG("PlusDEDE.listLists [%s]" % cItem)
        page = cItem.get('page', 0)
        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = ph.getDataBetweenNodes(data, ('<div', '>', 'load-more'), ('</div', '>'))[1]
        nextPage = self.getFullUrl(ph.getSearchGroups(nextPage, '''data\-url=['"]([^'^"]+?)['"]''')[0])
        data = ph.getAllItemsBetweenNodes(data, ('<div', '>', 'lista model'), ('<div', '>', 'media-container'))
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
            icon = self.getFullIconUrl(ph.getSearchGroups(item, '''\ssrc=['"]([^'^"]+?)['"]''')[0])
            title = ph.cleanHtml(item.split('<button', 1)[0])

            desc = []
            tmp = ph.getAllItemsBetweenNodes(item, ('<div', '>', 'lista-stat'), ('</div', '>'))
            for t in tmp:
                t = ph.cleanHtml(t)
                if t != '':
                    desc.append(t)
            desc = ' | '.join(desc)
            desc += '[/br]' + ph.cleanHtml(item.split('</h4>', 1)[-1])

            params = dict(cItem)
            params.update({'good_for_fav': True, 'category': nextCategory, 'title': title, 'url': url, 'icon': icon, 'desc': desc})
            self.addDir(params)

        if isValidUrl(nextPage):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def listItems(self, cItem, nextCategory):
        printDBG("PlusDEDE.listItems [%s]" % cItem)
        page = cItem.get('page', 0)

        url = cItem['url']
        if not url.endswith('/'):
            url += '/'

        if page == 0:
            if '/lista/' in url:
                url = url
            elif 'f_search_query' not in cItem:
                query = {}
                for key in self.cacheFiltersKeys:
                    if key in cItem:
                        query[key[2:]] = cItem[key]

                query = urllib.parse.urlencode(query)
                if '?' in url:
                    url += '&' + query
                else:
                    url += '?' + query
            else:
                url += urllib.parse.quote(cItem['f_search_query'])

        sts, data = self.getPage(url)
        if not sts:
            return

        nextPage = ph.getDataBetweenNodes(data, ('<div', '>', 'load-more'), ('</div', '>'))[1]
        nextPage = self.getFullUrl(ph.getSearchGroups(nextPage, '''data\-url=['"]([^'^"]+?)['"]''')[0])
        data = re.compile('''<div[^>]+?media\-container[^>]+?>''').split(data)
        if len(data):
            del data[0]
        reSeriesTitle = re.compile('^[0-9]+?x[0-9]+?\s')
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
            icon = self.getFullIconUrl(ph.getSearchGroups(item, '''[\s\-]src=['"]([^'^"]+?)['"]''')[0])
            title = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<div', '>', 'media-title'), ('</div', '>'), False)[1])
            year = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<div', '>', 'year'), ('</div', '>'), False)[1])
            val = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<i', '>', 'star'), ('</div', '>'), False)[1])
            desc = [year, val]
            if isValidUrl(url) and title != '':
                if '/serie/' in url:
                    desc.append(title)
                    title = reSeriesTitle.sub('', title)
                params = dict(cItem)
                params.update({'good_for_fav': True, 'category': nextCategory, 'title': title, 'url': url, 'icon': icon, 'desc': ' | '.join(desc)})
                self.addDir(params)

        if isValidUrl(nextPage):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItem(self, cItem, nextCategory):
        printDBG("PlusDEDE.exploreItem")

        self.cacheEpisodes = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = ph.getDataBetweenNodes(data, ('<button', '>', 'data-youtube'), ('</button', '>'))[1]
        url = ph.getSearchGroups(tmp, '''data\-youtube=['"]([^'^"]+?)['"]''')[0]
        if url != '':
            title = '%s - %s' % (cItem['title'], ph.cleanHtml(tmp))
            url = 'https://www.youtube.com/watch?v=' + url
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': title, 'url': url})
            self.addVideo(params)

        # movie <button class="show-close-footer btn btn-primary" data-modal-class="modal-lg" data-toggle="modal" data-target="#myModal" data-href="/aportes/4/58254">ver enlaces</button>
        if 'season-links' in data:
            seasonsTitle = {}
            tmp = ph.getDataBetweenNodes(data, ('<ul', '>', 'season-links'), ('</ul', '>'))[1]
            tmp = ph.getAllItemsBetweenMarkers(tmp, '<li', '</li>')
            for item in tmp:
                title = ph.cleanHtml(item)
                sNum = ph.getSearchGroups(item, '''data-season=['"]([^'^"]+?)['"]''')[0]
                seasonsTitle[sNum] = title
            sData = ph.getAllItemsBetweenNodes(data, ('<ul', '>', 'episode-container'), ('</ul', '>'))
            for season in sData:
                sNum = ph.getSearchGroups(season, '''data-season=['"]([^'^"]+?)['"]''')[0]
                sTitle = seasonsTitle.get(sNum, _('Season %s') % sNum)
                episodesTab = []
                eData = ph.getAllItemsBetweenNodes(season, ('<a', '>', 'episode'), ('</li', '>'))
                for item in eData:
                    url = self.getFullUrl(ph.getSearchGroups(item, '''data\-href=['"]([^'^"]+?)['"]''')[0])
                    if not isValidUrl(url):
                        continue

                    tmp = ph.getDataBetweenNodes(item, ('<div', '>', 'name'), ('</div', '>'))[1].split('</span>', 1)
                    eNum = ph.cleanHtml(tmp[0])
                    eTitle = ph.cleanHtml(tmp[-1])
                    title = ('%s - s%se%s %s' % (cItem['title'], sNum.zfill(2), eNum.zfill(2), eTitle)).strip()
                    desc = []
                    tmp = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<div', '>', 'date'), ('</div', '>'))[1])
                    if tmp != '':
                        desc.append(_('Date: %s') % tmp)
                    tmp = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<i', '>', 'wifi'), ('</div', '>'))[1])
                    if tmp != '':
                        desc.append(_('Views: %s') % tmp)
                    tmp = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<i', '>', 'download'), ('</div', '>'))[1])
                    if tmp != '':
                        desc.append(_('Downloads: %s') % tmp)
                    tmp = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<i', '>', 'comment'), ('</div', '>'))[1])
                    if tmp != '':
                        desc.append(_('Comments: %s') % tmp)
                    episodesTab.append({'title': title, 'url': url, 'desc': '[/br]'.join(desc)})
                if len(episodesTab):
                    self.cacheEpisodes[sNum] = episodesTab
                    params = dict(cItem)
                    params.update({'good_for_fav': False, 'category': nextCategory, 'title': sTitle, 's_num': sNum})
                    self.addDir(params)
        else:
            tmp = ph.getDataBetweenNodes(data, ('<button', '>', 'show-close'), ('</button', '>'))[1]
            url = self.getFullUrl(ph.getSearchGroups(tmp, '''data\-href=['"]([^'^"]+?)['"]''')[0])
            if isValidUrl(url):
                params = dict(cItem)
                params.update({'good_for_fav': True, 'url': url, 'prev_url': cItem['url']})
                self.addVideo(params)

    def listEpisodes(self, cItem):
        printDBG("PlusDEDE.listEpisodes")

        sNum = cItem.get('s_num', '')
        tab = self.cacheEpisodes.get(sNum, [])
        for item in tab:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'prev_url': cItem['url']})
            params.update(item)
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("PlusDEDE.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)
        if 0 == cItem.get('page', 0):
            cItem['f_search_query'] = searchPattern
            cItem['url'] = self.getFullUrl('/search/')
        self.listItems(cItem, 'explore_item')

    def getCustomLinksForVideo(self, cItem):
        printDBG("PlusDEDE.getCustomLinksForVideo [%s]" % cItem)
        self.tryTologin()

        retTab = []
        dwnTab = []
        if 1 == self.up.checkHostSupport(cItem.get('url', '')):
            videoUrl = cItem['url'].replace('youtu.be/', 'youtube.com/watch?v=')
            return self.up.getVideoLinkExt(videoUrl)

        cacheKey = cItem['url']
        cacheTab = self.cacheLinks.get(cacheKey, [])
        if len(cacheTab):
            return cacheTab

        self.cacheLinks = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        data = data.split('<div id="download"', 1)
        for idx in range(len(data)):
            dataItem = data[idx]
            dataItem = ph.getAllItemsBetweenNodes(dataItem, ('<a', '>', 'data-v'), ('</a', '>'))
            for item in dataItem:
                url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
                if not isValidUrl(url):
                    printDBG("No url in link item: [%s]" % item)
                    continue
                host = ph.getSearchGroups(item, '''src=['"][^'^"]*?/hosts/([^'^"^\.]+?)['"\.]''')[0]
                lang = ph.getSearchGroups(item, '''src=['"][^'^"]*?/flags/([^'^"^\.]+?)['"\.]''')[0]
                #dataV = ph.getSearchGroups(item, '''data\-v=['"]([^'^"]+?)['"]''')[0]
                #dataId = ph.getSearchGroups(item, '''data\-id=['"]([^'^"]+?)['"]''')[0]
                titleTab = [host, lang]
                tmp = ph.getAllItemsBetweenMarkers(item, '<div', '</div>')
                for t in tmp:
                    t = ph.cleanHtml(t)
                    if t != '':
                        titleTab.append(t)
                if idx == 0:
                    retTab.append({'name': '%s' % (' | '.join(titleTab)), 'url': self.getFullUrl(url), 'need_resolve': 1})
                else:
                    dwnTab.append({'name': '%s' % (' | '.join(titleTab)), 'url': self.getFullUrl(url), 'need_resolve': 1})

        #retTab.extend(dwnTab)
        if len(retTab):
            self.cacheLinks[cacheKey] = retTab
        return retTab

    def getCustomVideoLinks(self, videoUrl):
        printDBG("PlusDEDE.getCustomVideoLinks [%s]" % videoUrl)
        videoUrl = strwithmeta(videoUrl)
        urlTab = []

        # mark requested link as used one
        if len(list(self.cacheLinks.keys())):
            for key in self.cacheLinks:
                for idx in range(len(self.cacheLinks[key])):
                    if videoUrl in self.cacheLinks[key][idx]['url']:
                        if not self.cacheLinks[key][idx]['name'].startswith('*'):
                            self.cacheLinks[key][idx]['name'] = '*' + self.cacheLinks[key][idx]['name'] + '*'
                        break

        sts, data = self.getPage(videoUrl)
        if not sts:
            return []

        data = ph.getDataBetweenNodes(data, ('<div', '>', 'visit-buttons'), ('</div', '>'))[1]
        videoUrl = self.getFullUrl(ph.getSearchGroups(data, '''href=['"]([^'^"]+?)['"]''')[0])
        if isValidUrl(videoUrl):
            params = dict(self.defaultParams)
            params['max_data_size'] = 0
            self.getPage(videoUrl, params)
            videoUrl = self.cm.meta.get('url', videoUrl)
        printDBG(">> videoUrl[%s]" % videoUrl)
        urlTab = self.up.getVideoLinkExt(videoUrl)
        return urlTab

    def getCustomArticleContent(self, cItem, data=None):
        printDBG("PlusDEDE.getCustomArticleContent [%s]" % cItem)
        self.tryTologin()

        retTab = []

        otherInfo = {}

        if data is None:
            sts, data = self.getPage(cItem.get('prev_url', cItem['url']))
            if not sts:
                return []

        desc = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<div', '>', 'plot'), ('</div', '>'), False)[1])
        title = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<h1', '>', 'big-title'), ('</h1', '>'), False)[1])
        icon = ph.getDataBetweenNodes(data, ('<div', '>', 'avatar-container'), ('</div', '>'), False)[1]
        icon = self.getFullIconUrl(ph.getSearchGroups(icon, '''\ssrc=['"]([^'^"]+?)['"]''')[0])

        otherInfo['rating'] = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<div', '>', 'item-vote'), ('</div', '>'), False)[1].split('</span>', 1)[-1])
        otherInfo['released'] = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<strong', '</strong>', 'Fecha'), ('</div', '>'), False)[1])
        otherInfo['duration'] = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<strong', '</strong>', 'Duración'), ('</div', '>'), False)[1])
        tmp = ph.getDataBetweenNodes(data, ('<strong', '</strong>', 'Género'), ('</ul', '>'), False)[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, '<li', '</li>')
        tmpTab = []
        for t in tmp:
            t = ph.cleanHtml(t)
            if t != '':
                tmpTab.append(t)
        otherInfo['genres'] = ', '.join(tmpTab)

        objRe = re.compile('<div[^>]+?text\-sub[^>]+?>')
        tmp = ph.getAllItemsBetweenNodes(data, ('<li', '>', 'star-container'), ('</li', '>'), False)
        stars = []
        directors = []
        for t in tmp:
            t = objRe.split(t, 1)
            t[0] = ph.cleanHtml(t[0])
            if t[0] == '':
                continue
            if 2 == len(t):
                t[1] = ph.cleanHtml(t[1])
                if t[1] == 'Director':
                    directors.append(t[0])
                    continue
            stars.append(t[0])
        if len(directors):
            otherInfo['director'] = ', '.join(directors)
        if len(stars):
            otherInfo['stars'] = ', '.join(stars)

        if title == '':
            title = cItem['title']
        if desc == '':
            desc = cItem.get('desc', '')
        if icon == '':
            icon = cItem.get('icon', self.DEFAULT_ICON_URL)

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc), 'images': [{'title': '', 'url': self.getFullUrl(icon)}], 'other_info': otherInfo}]

    def tryTologin(self):
        printDBG('tryTologin start')

        if PlusDEDE.login is None and PlusDEDE.password is None:
            if self.readLoginMarker() == self.calcLoginMarker(config.plugins.iptvplayer.plusdede_login.value, config.plugins.iptvplayer.plusdede_password.value):
                PlusDEDE.login = config.plugins.iptvplayer.plusdede_login.value
                PlusDEDE.password = config.plugins.iptvplayer.plusdede_password.value
            else:
                PlusDEDE.password = ''
                PlusDEDE.login = ''
                rm(self.COOKIE_FILE)

        if True != self.loggedIn or PlusDEDE.login != config.plugins.iptvplayer.plusdede_login.value or\
            PlusDEDE.password != config.plugins.iptvplayer.plusdede_password.value:

            if True != self.loggedIn and PlusDEDE.login == config.plugins.iptvplayer.plusdede_login.value and\
                PlusDEDE.password == config.plugins.iptvplayer.plusdede_password.value:
                sts, data = self.getPage(self.getMainUrl())
                if sts:
                    token = ph.getSearchGroups(data, '''(<meta[^>]+?_token[^>]+?/>)''')[0]
                    token = ph.getSearchGroups(token, '''content=['"]([^"^']+?)['"]''')[0]
                    if '' != token and '/logout' in data:
                        self.HTTP_HEADER['X-CSRF-TOKEN'] = token
                        self.AJAX_HEADER['X-CSRF-TOKEN'] = token
                        self.loggedIn = True
                        return True

            PlusDEDE.login = config.plugins.iptvplayer.plusdede_login.value
            PlusDEDE.password = config.plugins.iptvplayer.plusdede_password.value
            self.saveLoginMarker()

            rm(self.COOKIE_FILE)

            self.loggedIn = False

            if '' == PlusDEDE.login.strip() or '' == PlusDEDE.password.strip():
                self.sessionEx.open(MessageBox, _('The host %s requires registration. \nPlease fill your login and password in the host configuration. Available under blue button.' % self.getMainUrl()), type=MessageBox.TYPE_ERROR, timeout=10)
                return False

            url = self.getFullUrl('/login?popup=1')
            sts, data = self.getPage(url)
            if not sts:
                return False

            sts, tmp = ph.getDataBetweenNodes(data, ('<form', '>'), ('</form', '>'))
            if not sts:
                return False

            actionUrl = self.getFullUrl(ph.getSearchGroups(tmp, '''action=['"]([^'^"]+?)['"]''')[0])
            tmp = ph.getAllItemsBetweenMarkers(tmp, '<input', '>')
            post_data = {}
            for item in tmp:
                name = ph.getSearchGroups(item, '''name=['"]([^'^"]+?)['"]''')[0]
                value = ph.getSearchGroups(item, '''value=['"]([^'^"]+?)['"]''')[0]
                post_data[name] = value

            post_data.update({'email': PlusDEDE.login, 'password': PlusDEDE.password})

            # fill captcha
            #############################################################################################
            imgUrl = self.getFullUrl(ph.getSearchGroups(data, '''<img[^>]+?src=['"]([^"^']+?)['"]''')[0])
            if isValidUrl(imgUrl):
                header = dict(self.HTTP_HEADER)
                header['Accept'] = 'image/png,image/*;q=0.8,*/*;q=0.5'
                params = dict(self.defaultParams)
                params.update({'maintype': 'image', 'subtypes': ['jpeg', 'png'], 'check_first_bytes': [b'\xFF\xD8', b'\xFF\xD9', b'\x89\x50\x4E\x47'], 'header': header})
                filePath = GetTmpDir('.iptvplayer_captcha.jpg')
                ret = self.cm.saveWebFile(filePath, imgUrl.replace('&amp;', '&'), params)
                if not ret.get('sts'):
                    SetIPTVPlayerLastHostError(_('Fail to get "%s".') % imgUrl)
                    return False

                params = deepcopy(IPTVMultipleInputBox.DEF_PARAMS)
                params['accep_label'] = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<button', '>', 'submit'), ('</button', '>'))[1])

                params['title'] = _('Captcha')
                params['status_text'] = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<label', '>', 'captcha'), ('</label', '>'))[1])
                params['with_accept_button'] = True
                params['list'] = []
                item = deepcopy(IPTVMultipleInputBox.DEF_INPUT_PARAMS)
                item['label_size'] = (300, 80)
                item['input_size'] = (480, 25)
                item['icon_path'] = filePath
                item['title'] = _('Answer')
                item['input']['text'] = ''
                params['list'].append(item)

                ret = 0
                retArg = self.sessionEx.waitForFinishOpen(IPTVMultipleInputBox, params)
                printDBG(retArg)
                if retArg and len(retArg) and retArg[0]:
                    printDBG(retArg[0])
                    name = ph.getDataBetweenNodes(data, ('<input', '>', 'captcha'), ('</input', '>'))[1]
                    printDBG(name)
                    name = ph.getSearchGroups(name, '''name=['"]([^"^']+?)['"]''')[0]
                    printDBG(name)
                    post_data['captcha'] = retArg[0][0]
            #############################################################################################

            httpParams = dict(self.defaultParams)
            httpParams['header'] = dict(self.AJAX_HEADER)
            httpParams['header']['Referer'] = url
            httpParams['header']['X-CSRF-TOKEN'] = ph.getSearchGroups(data, '''(<meta[^>]+?_token[^>]+?/>)''')[0]
            httpParams['header']['X-CSRF-TOKEN'] = ph.getSearchGroups(httpParams['header']['X-CSRF-TOKEN'], '''content=['"]([^"^']+?)['"]''')[0]
            error = ''
            sts, data = self.cm.getPage(actionUrl, httpParams, post_data)
            try:
                tmp = json.loads(data)['content']
                printDBG(tmp)
                tmp = ph.getAllItemsBetweenNodes(tmp, ('<div', '>', 'alert'), ('</div', '>'))
                tab = []
                for t in tmp:
                    t = ph.cleanHtml(t)
                    if t == '':
                        continue
                    tab.append(t)
                error = ', '.join(tab)
            except Exception:
                printExc()

            sts, data = self.getPage(self.getMainUrl())
            if sts and '/logout' in data:
                self.HTTP_HEADER['X-CSRF-TOKEN'] = httpParams['header']['X-CSRF-TOKEN']
                self.AJAX_HEADER['X-CSRF-TOKEN'] = httpParams['header']['X-CSRF-TOKEN']
                self.loggedIn = True
            else:
                if error == '':
                    error = _('Login failed.')
                SetIPTVPlayerLastHostError(error)
                printDBG('tryTologin failed')
        return self.loggedIn

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        self.tryTologin()

        HostBase.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        mode = self.currItem.get("mode", '')

        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    #MAIN MENU
        if name is None:
            self.listMainMenu({'name': 'category'}, 'list_genres')
        elif category == 'list_filters':
            self.listFilters(self.currItem, 'list_items')
        elif category == 'list_items':
            self.listItems(self.currItem, 'explore_item')
        elif category == 'list_lists':
            self.listLists(self.currItem, 'list_items')
        elif category == 'explore_item':
            self.exploreItem(self.currItem, 'list_episodes')
        elif category == 'list_episodes':
            self.listEpisodes(self.currItem)
    #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        HostBase.endHandleService(self, refresh)


    def withArticleContent(self, cItem):
        return cItem.get('good_for_fav', False)
