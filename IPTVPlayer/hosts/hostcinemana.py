# -*- coding: utf-8 -*-

from urllib.parse import quote_plus, urljoin

from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultHeader, isValidUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def gettytul():
    return 'Cinemana'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__({'history': 'cinemana', 'cookie': 'cinemana.cookie'})

        self.MAIN_URL = 'https://cinemana.work/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/fH4LnHX/cinemana.png'

        self.HEADER = getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, url, addParams=None, post_data=None):
        addParams = dict(self.defaultParams) if addParams is None else addParams
        addParams['cloudflare_params'] = {'cookie_file': self.COOKIE_FILE, 'User-Agent': self.HEADER['User-Agent']}
        return self.cm.getPageCFProtection(url, addParams, post_data)
        #return self.cm.getPage(url, addParams, post_data)

    def mainMenu(self, cItem):
        printDBG("Cinemana.listMainMenu")
        MAIN_CAT_TAB = [
            {'category':'listCatItems', 'subCat': 'movies', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category':'listCatItems', 'subCat': 'series', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
                       {'category':'listCatItems', 'subCat': 'search', 'title': _('Search'), 'search_item': True},
            {'category':'listCatItems', 'subCat': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("Cinemana.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("subCat", '')

        if category == 'movies':
            NEW_CAT_TAB = [
                {'category': 'listItems', 'title': _('أفــلام أجنـبيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies/')},
                {'category': 'listItems', 'title': _('أفــلام عــربــيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/page/arabic/')},]
        elif category == 'series':
            NEW_CAT_TAB = [
                {'category': 'listItems', 'title': _('رمــضـــان'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/watch=category/مسلسلات-رمضان-2024/')},
                {'category': 'listItems', 'title': _('مسـلـسـلات أجنـبيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/watch=category/مسلسلات-اجنبي/')},
                {'category': 'listItems', 'title': _('مسـلـسـلات عــربــيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/watch=category/مسلسلات-عربية/')},
                {'category': 'listItems', 'title': _('مسـلـسـلات تـركـيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/watch=category/مسلسلات-تركية/')},
                {'category': 'listItems', 'title': _('مسـلـسـلات آسـيـويـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/watch=category/مسلسلات-اسيوية/')},
                {'category': 'listItems', 'title': _('مسـلـسـلات هنــديـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/watch=category/مسلسلات-هندية/')}]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("Cinemana.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)
        titlesTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = ph.getDataBetweenMarkers(data, ('<div', '>', 'Paginate'), ('</div', '>'), True)[1]
        nextPage = self.getFullUrl(ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        tmp = ph.getDataBetweenMarkers(data, ('<div', '>', 'ArcPage'), ('<div', '>', 'clear'), True)[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, ('<div', '>', 'ItemBlock'), ('</a', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(ph.getSearchGroups(item, '''url\((.+?)\)''')[0])
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<h3', '>'), ('</h3', '>'), False)[1])


            desc = 'desc'

            if title not in titlesTab:
                titlesTab.append(title)

                params = dict(cItem)
                params.update({'category': 'exploreItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': ph.std_url(icon), 'desc': desc})
                self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("Cinemana.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<div', '>', 'Story'), ('</div', '>'), False)[1])

        Season = ph.getDataBetweenMarkers(data, ('<div', '>', 'tab-content season-scroll'), ('<script', '>', 'text/javascript'), True)[1]
        if Season:
            tmp = ph.getAllItemsBetweenMarkers(Season, ('<li', '>'), ('</li', '>'))
            for item in tmp:
                url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = ph.cleanHtml(ph.getDataBetweenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': 'desc'})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("Cinemana.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/?s={}&type=all'.format(quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getCustomLinksForVideo(self, cItem):
        printDBG("Cinemana.getLinksForVideo [%s]" % cItem)
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmpID = ph.cleanHtml(ph.getSearchGroups(data, '''data-id=['"]([^"^']+?)['"]''')[0])
        tmpUrl = self.getFullUrl('wp-content/themes/EEE/Inc/Ajax/Single/Server.php')

        tmp = ph.getDataBetweenMarkers(data, ('<div', '>', 'ServersList'), ('</div', '>'), True)[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, '<li', ('</li', '>'))
        for item in tmp:
            tmpServ = ph.cleanHtml(ph.getSearchGroups(item, '''data-server=['"]([^"^']+?)['"]''')[0])
            title = ph.cleanHtml(ph.getDataBetweenNodes(item, ('data-server', '>'), ('</li', '>'), False)[1])

            sts, data = self.getPage(tmpUrl, post_data={'post_id': tmpID, 'server': tmpServ})
            if not sts:
                return

            url = self.getFullUrl(ph.getSearchGroups(data, '''SRC=['"]([^"^']+?)['"]''', ignoreCase=True)[0])

            if not isValidUrl(url):
                url = urljoin('http://', url)

            if title != '':
                title = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(cItem['title'], title, self.up.getHostName(url, True)))

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})

        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        printDBG("Cinemana.getVideoLinks [%s]" % videoUrl)

        if isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getCustomArticleContent(self, cItem):
        printDBG("Cinemana.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = ph.getDataBetweenMarkers(data, ('<aside', '>', 'SingleAside'), ('<script', '>', 'text/javascript'), True)[1]

        Info = ph.cleanHtml(ph.getDataBetweenNodes(tmp, ('<i', '>', 'fa fa-star'), ('</span', '>'), False)[1]).replace('/', '')
        if Info != '':
            otherInfo['imdb_rating'] = Info

        Info = ph.cleanHtml(ph.getDataBetweenNodes(tmp, ('genre', '>'), ('</li', '>'), False)[1])
        if Info != '':
            otherInfo['genre'] = Info

        Info = ph.cleanHtml(ph.getDataBetweenNodes(tmp, ('<i', '>', 'fa-clock'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['duration'] = Info

        Info = ph.cleanHtml(ph.getDataBetweenNodes(tmp, ('quality', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        Info = ph.cleanHtml(ph.getDataBetweenNodes(tmp, ('release-year', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = ph.cleanHtml(ph.getDataBetweenNodes(tmp, ('category', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['category'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': otherInfo}]

