﻿# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getBaseUrl, isValidUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc,   rm, MergeDicts
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import getDirectM3U8Playlist
###################################################

###################################################
# FOREIGN import
###################################################
import urllib.request
import urllib.parse
import urllib.error
import base64
try:
    import json
except Exception:
    import simplejson as json
from Components.config import config, ConfigText, getConfigListEntry
###################################################


###################################################
# E2 GUI COMMPONENTS
###################################################
from Screens.MessageBox import MessageBox
###################################################

###################################################
# Config options for HOST
###################################################
config.plugins.iptvplayer.vumedicom_login = ConfigText(default="", fixed_size=False)
config.plugins.iptvplayer.vumedicom_password = ConfigText(default="", fixed_size=False)


def GetConfigList():
    optionList = []
    optionList.append(getConfigListEntry(_("login") + ":", config.plugins.iptvplayer.vumedicom_login))
    optionList.append(getConfigListEntry(_("password") + ":", config.plugins.iptvplayer.vumedicom_password))
    return optionList
###################################################


def gettytul():
    return 'https://vumedi.com/'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'vumedi.com', 'cookie': 'vumedi.comcookie'})

        self.USER_AGENT = 'Mozilla/5.0'
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Accept': 'text/html'}
        self.AJAX_HEADER = dict(self.HEADER)
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'})

        self.MAIN_URL = 'https://www.vumedi.com/'
        self.DEFAULT_ICON_URL = 'https://pbs.twimg.com/media/DZTZrVhW4AAekGB.jpg'

        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.loggedIn = None
        self.login = ''
        self.password = ''

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)
        return self.cm.getPage(baseUrl, addParams, post_data)

    def getFullUrl(self, url, baseUrl=None):
        if not isValidUrl(url) and baseUrl is not None:
            if url.startswith('/'):
                baseUrl = getBaseUrl(baseUrl)
            else:
                baseUrl = baseUrl.rsplit('/', 1)[0] + '/'
        return HostBase.getFullUrl(self, url.replace('&#038;', '&').replace('&amp;', '&'), baseUrl)

    def listMainMenu(self, cItem):
        printDBG("VUMEDI.listMainMenu")

        MAIN_CAT_TAB = [{'category': 'list_spec', 'title': _('Specialities'), 'url': self.getMainUrl()},
                        {'category': 'list_sort', 'title': _('Browse videos'), 'url': self.getFullUrl('/video/browse/')},
                        {'category': 'search', 'title': _('Search'), 'search_item': True},
                        {'category': 'search_history', 'title': _('Search history')}]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCategories(self, cItem, nextCategory1, nextCategory2):
        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        cUrl = self.cm.meta['url']
        self.setMainUrl(cUrl)

        if 'news-feeds' in data:
            params = dict(cItem)
            params.update({'good_for_fav': False, 'name': 'category', 'category': nextCategory1, 'url': cUrl, 'title': _('News Feed')})
            self.addDir(params)

        data = ph.getDataBetweenNodes(data, ('<nav', '>', 'secondary-nav'), ('</nav', '>'), False)[1]
        data = ph.getAllItemsBetweenMarkers(data, '<li', '</li>')
        for sItem in data:
            sItem = sItem.split('</a>', 1)
            if len(sItem) < 2:
                continue
            sTitle = ph.cleanHtml(sItem[0])
            sUrl = self.getFullUrl(ph.getSearchGroups(sItem[0], '''href=['"]([^"^']+?)['"]''')[0].split('#', 1)[0], cUrl)
            categories = []
            sItem = ph.getAllItemsBetweenMarkers(sItem[1], '<a', '</a>')
            for item in sItem:
                url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0], cUrl)
                title = ph.cleanHtml(item)
                params = dict(cItem)
                params.update({'good_for_fav': True, 'name': 'category', 'category': nextCategory2, 'title': title, 'url': url})
                categories.append(params)

            if len(categories):
                if sUrl != '':
                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'name': 'category', 'category': nextCategory2, 'title': _('--All--'), 'url': sUrl})
                    categories.insert(0, params)
                params = dict(cItem)
                params.update({'name': 'category', 'category': 'sub_items', 'title': sTitle, 'sub_items': categories})
                self.addDir(params)
            elif sUrl != '':
                params = dict(cItem)
                params.update({'good_for_fav': True, 'name': 'category', 'category': nextCategory2, 'title': sTitle, 'url': sUrl})
                self.addDir(params)

    def listTopics(self, cItem, nextCategory):
        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        cUrl = self.cm.meta['url']
        self.setMainUrl(cUrl)

        data = ph.getAllItemsBetweenNodes(data, ('<h2', '>', 'filters'), ('</div', '>'))
        for sItem in data:
            sItem = sItem.split('</h2>', 1)
            if len(sItem) < 2:
                continue
            sTitle = ph.cleanHtml(sItem[0])
            if sTitle.endswith(':'):
                sTitle = sTitle[:-1]
            categories = []
            sItem = ph.getAllItemsBetweenMarkers(sItem[1], '<a', '</a>')
            for item in sItem:
                url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0], cUrl)
                title = ph.cleanHtml(item)
                params = dict(cItem)
                params.update({'good_for_fav': True, 'name': 'category', 'category': nextCategory, 'title': title, 'url': url})
                categories.append(params)

            if len(categories):
                params = dict(cItem)
                params.update({'name': 'category', 'category': 'sub_items', 'title': sTitle, 'sub_items': categories})
                self.addDir(params)

        params = dict(cItem)
        params.update({'good_for_fav': True, 'name': 'category', 'category': nextCategory, 'url': cUrl, 'title': _('--All--')})
        if len(self.currList):
            self.currList.insert(0, params)
        else:
            self.listSort(params, 'list_items')

    def listSpecialities(self, cItem, nextCategory):
        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        cUrl = self.cm.meta['url']
        self.setMainUrl(cUrl)

        data = ph.getDataBetweenNodes(data, ('<div', '>', 'navbarSpecDropdown'), ('</div', '>'), False)[1]
        data = ph.getAllItemsBetweenMarkers(data, '<a', '</a>')
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0], cUrl)
            title = ph.cleanHtml(item)
            params = dict(cItem)
            params.update({'good_for_fav': True, 'name': 'category', 'category': nextCategory, 'title': title, 'url': url})
            self.addDir(params)

    def listSort(self, cItem, nextCategory):
        printDBG("VUMEDI.listItems [%s]" % cItem)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        cUrl = self.cm.meta['url']
        self.setMainUrl(cUrl)

        data = ph.getDataBetweenNodes(data, ('<', '>', 'sort-dropdown"'), ('</div', '>'), False)[1]
        data = ph.getAllItemsBetweenMarkers(data, '<a', '</a>')
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0], cUrl)
            if url == '':
                url = cUrl
            title = ph.cleanHtml(item)
            params = dict(cItem)
            params.update({'name': 'category', 'category': nextCategory, 'title': title, 'url': url})
            self.addDir(params)

    def listNewsFeed(self, cItem):
        printDBG("VUMEDI.listNewsFeed [%s]" % cItem)
        page = cItem.get('page', 0)
        url = self.getFullUrl('/beats/{0}/?is_long=true'.format(page))

        params = dict(self.defaultParams)
        params['header'] = MergeDicts(self.AJAX_HEADER, {'Referer': cItem['url']})

        sts, data = self.getPage(url, params)
        if not sts:
            return
        try:
            data = json.loads(data)
            nextPage = data.get('start', -1)
            data = data['beats']

            self.listVideoItems(cItem, data)

            if nextPage > page:
                params = dict(cItem)
                params.update({'good_for_fav': False, 'title': _("Next page"), 'page': nextPage})
                self.addDir(params)

        except Exception:
            printExc()

    def listVideoItems(self, cItem, data):

        data = ph.getAllItemsBetweenNodes(data, ('<div', '>', 'video-item'), ('</ul', '>'), False)
        for item in data:
            t = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<', '>', 'video-duration'), ('</', '>'), False)[1]).upper()
            if ':' not in t and 'VIDEO' not in t:
                continue
            tmp = ph.getDataBetweenNodes(item, ('<h', '>', '_title'), ('</h', '>'), False)[1]

            url = self.getFullUrl(ph.getSearchGroups(tmp, '''\shref=['"]([^'^"]+?)['"]''')[0].strip())
            if url == '':
                continue

            icon = self.getFullIconUrl(ph.getSearchGroups(item, '''\ssrc=['"]([^"^']+?\.(?:jpe?g|png)(?:\?[^'^"]*?)?)['"]''')[0])
            title = ph.cleanHtml(tmp)

            desc = [t]
            tmp = []
            for marker in ['_author', '_desc']:
                t = ph.cleanHtml(ph.getDataBetweenNodes(item, ('<', '>', marker), ('</', '>'), False)[1])
                if t == '':
                    continue
                tmp.append(t)
            desc.append(' | '.join(tmp))

            tmp = []
            for t in ph.getAllItemsBetweenMarkers(item, '<li', '</li>'):
                t = ph.cleanHtml(t)
                if t == '':
                    continue
                tmp.append(t)
            desc.append(' | '.join(tmp))

            params = dict(cItem)
            params.update({'good_for_fav': True, 'title': title, 'url': url, 'icon': icon, 'desc': '[/br]'.join(desc)})
            self.addVideo(params)

    def listItems(self, cItem):
        printDBG("VUMEDI.listItems [%s]" % cItem)
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        cUrl = self.cm.meta['url']
        self.setMainUrl(cUrl)

        nextPage = ph.getDataBetweenNodes(data, ('<ul', '>', 'pagination'), ('</ul', '>'))[1]
        nextPage = ph.getSearchGroups(nextPage, '''<a[^>]+?href=['"]([^'^"]+?)['"][^>]*?>%s</a>''' % (page + 1))[0]

        self.listVideoItems(cItem, data)

        if nextPage:
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': _("Next page"), 'page': page + 1, 'url': self.getFullUrl(nextPage, cUrl)})
            self.addDir(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("VUMEDI.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        self.tryTologin()

        cItem = dict(cItem)
        cItem['url'] = self.getFullUrl('/search/?q=') + urllib.parse.quote(searchPattern)
        cItem['category'] = 'list_items'
        self.listItems(cItem)

    def getCustomLinksForVideo(self, cItem):
        printDBG("VUMEDI.getCustomLinksForVideo [%s]" % cItem)
        self.tryTologin()

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return []
        self.setMainUrl(self.cm.meta['url'])

        playerBase = 'http://player.ooyala.com/'
        # first check simple method
        baseUrl = playerBase + 'hls/player/all/%s.m3u8'
        videoId = ph.getSearchGroups(data, '''data\-video=['"]([^'^"]+?)['"]''')[0]
        retTab = getDirectM3U8Playlist(baseUrl % videoId, checkExt=False, checkContent=True, sortWithMaxBitrate=999999999)
        if 0 == len(retTab):
            data = ph.getDataBetweenNodes(data, ('var ', '{', 'playerParam'), ('}', ';'))[1]
            partnerId = ph.getSearchGroups(data, '''['"]?playerBrandingId['"]?\s*\:\s*['"]([^'^"]+?)['"]''')[0]
            pcode = ph.getSearchGroups(data, '''['"]?pcode['"]?\s*\:\s*['"]([^'^"]+?)['"]''')[0]

            url = playerBase + 'player_api/v1/content_tree/embed_code/%s/%s?' % (pcode, videoId)
            sts, data = self.getPage(url)
            if not sts:
                return []
            try:
                printDBG(data)
                data = json.loads(data)['content_tree']
                key = list(data.keys())[0]
                data = data[key]

                embedCode = data['embed_code']
                pcode = data.get('asset_pcode', embedCode)

                url = playerBase + 'sas/player_api/v2/authorization/embed_code/%s/%s?device=html5&domain=%s' % (pcode, embedCode, getBaseUrl(self.getMainUrl(), True))
                sts, data = self.getPage(url)
                if not sts:
                    return []
                printDBG(data)
                data = json.loads(data)['authorization_data'][key]['streams']
                for item in data:
                    url = ''
                    if item['url']['format'] == 'encoded':
                        url = base64.b64decode(item['url']['data'])
                    if not isValidUrl(url):
                        continue

                    if item['delivery_type'] == 'mp4':
                        name = 'mp4 %s %sx%s %sfps' % (item['video_codec'], item['width'], item['height'], item['framerate'])
                        retTab.append({'name': name, 'url': url, 'need_resolve': 0})
                    elif item['delivery_type'] == 'hls':
                        retTab.extend(getDirectM3U8Playlist(url, checkExt=False, checkContent=True, sortWithMaxBitrate=999999999))
            except Exception:
                printExc()
        return retTab

    def tryTologin(self):
        printDBG('tryTologin start')
        if None == self.loggedIn or self.login != config.plugins.iptvplayer.vumedicom_login.value or\
            self.password != config.plugins.iptvplayer.vumedicom_password.value:

            self.login = config.plugins.iptvplayer.vumedicom_login.value
            self.password = config.plugins.iptvplayer.vumedicom_password.value

            rm(self.COOKIE_FILE)

            self.loggedIn = False

            if '' == self.login.strip() or '' == self.password.strip():
                self.sessionEx.open(MessageBox, _('The host %s requires registration. \nPlease fill your login and password in the host configuration. Available under blue button.' % self.getMainUrl()), type=MessageBox.TYPE_ERROR, timeout=10)
                return False

            sts, data = self.getPage(self.getFullUrl('/accounts/login/'))
            if not sts:
                return False
            cUrl = self.cm.meta['url']
            self.setMainUrl(cUrl)

            sts, data = ph.getDataBetweenNodes(data, ('<form', '>'), ('</form', '>'))
            if not sts:
                return False
            actionUrl = self.cm.getFullUrl(ph.getSearchGroups(data, '''action=['"]([^'^"]+?)['"]''')[0], getBaseUrl(cUrl))
            if actionUrl == '':
                actionUrl = cUrl

            post_data = {}
            inputData = ph.getAllItemsBetweenMarkers(data, '<input', '>')
            inputData.extend(ph.getAllItemsBetweenMarkers(data, '<button', '>'))
            for item in inputData:
                name = ph.getSearchGroups(item, '''name=['"]([^'^"]+?)['"]''')[0]
                value = ph.getSearchGroups(item, '''value=['"]([^'^"]+?)['"]''')[0].replace('&amp;', '&')
                post_data[name] = value

            post_data.update({'username': self.login, 'password': self.password})

            httpParams = dict(self.defaultParams)
            httpParams['header'] = dict(httpParams['header'])
            httpParams['header']['Referer'] = cUrl

            sts, data = self.cm.getPage(actionUrl, httpParams, post_data)
            if sts and '/logout' in data:
                printDBG('tryTologin OK')
                self.loggedIn = True

            if not self.loggedIn:
                errorMessage = [_('Login failed.')]
                if sts:
                    errorMessage.append(ph.cleanHtml(ph.getDataBetweenNodes(data, ('<div', '>', 'alert-warning'), ('<', '>'), False)[1]))
                self.sessionEx.open(MessageBox, '\n'.join(errorMessage), type=MessageBox.TYPE_ERROR, timeout=10)
                printDBG('tryTologin failed')
        return self.loggedIn

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        HostBase.handleService(self, index, refresh, searchPattern, searchType)

        self.tryTologin()

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        mode = self.currItem.get("mode", '')

        printDBG("handleService: || name[%s], category[%s] " % (name, category))
        self.currList = []

    #MAIN MENU
        if name is None:
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'list_spec':
            self.listSpecialities(self.currItem, 'list_cats')
        elif category == 'news_feed':
            self.listNewsFeed(self.currItem)
        elif category == 'list_cats':
            self.listCategories(self.currItem, 'news_feed', 'list_topics')
        elif category == 'list_topics':
            self.listTopics(self.currItem, 'list_sort')
        elif category == 'list_sort':
            self.listSort(self.currItem, 'list_items')
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'sub_items':
            self.currList = self.currItem.get('sub_items', [])
    #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        HostBase.endHandleService(self, refresh)

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)
