# -*- coding: utf-8 -*-

import json
import re
from unicodedata import category
from urllib.parse import quote, quote_plus
from warnings import catch_warnings
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.libs.urlparser import urlparser
from Plugins.Extensions.IPTVPlayer.libs.youtubeparser import YouTubeParser
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Components.config import config

def gettytul():
    return 'Movie2k'

class IPTVHost(HostBase):

    def __init__(self, params: dict = None):
        super().__init__({'history': 'movie2k', 'cookie': 'movie2k.cookie'})
        self.MAIN_URL = 'https://www1.movie2k.ch/'
        self.WATCH_URL = 'https://api.movie2k.ch/data/watch/?_id=%s'
        self.DEFAULT_ICON_URL = 'https://www1.movie2k.ch/images/movie2k/logo.png'

        self.USER_AGENT = getDefaultUserAgent()
        self.HEADER = {'User-Agent': self.USER_AGENT }
        self.defaultParams = {'header':self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.ICON_PATH = 'https://image.tmdb.org/t/p/w300'
        #self.api_path = 'https://api.movie2k.ch/data/browse/'
        self.api_path = 'https://api.tmdb.club/data/getimg/?_id='#63d493e6039b39e422244dc3'
        self.filter_cache = {}

    def mainMenu(self, cItem):
        printDBG(f"Movie2k.mainMenu cItem [{cItem}]")

        MAIN_CAT_TAB = [
            {'category': 'show_items', 'title':'Filme', 'order_by':'releases','type_':'movies', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_items', 'title':'Serien', 'order_by':'releases','type_':'tvseries', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_items', 'title':'Trending', 'order_by':'trending','type_':'movies', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_filter', 'title':'Filter', 'type_':'movies', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True, 'icon': self.DEFAULT_ICON_URL },
            {'category': 'search_history', 'title': _('Search history')     , 'icon': self.DEFAULT_ICON_URL }
        ]

        self.listsTab(MAIN_CAT_TAB, cItem)

        self.filter_cache['rating'] = range(9,0, -1)
        self.filter_cache['year'] = range(2022,1990, -1)
        self.filter_cache['genre'] = [
                    'Action',
					'Abenteuer',
					'Animation',
					'Biographie',
					'Komödie',
					'Krimi',
					'Documentation',
					'Drama',
					'Familie',
					'Fantasy',
					'Geschichte',
					'Horror',
					'Musik',
					'Mystery',
					'Romantic',
					'Reality-TV',
					'Sport',
					'Thriller',
					'Krieg',
					'Western']
        self.filter_cache['order_by'] = ['Trending', 'Kinofilme', 'Neu', 'Views', 'Votes', 'Requested','Updates', 'Name', 'Server', 'Rating']
        self.filter_cache['type_']={'Alle':'', 'Filme': 'movies', 'Serien':'tvseries'}

    def show_episodes(self, cItem):
        _id = cItem.get('_id', None)

        url = self.WATCH_URL % _id
        sts, data = self.cm.getPage(url, self.defaultParams)
        if not sts:
            return

        for movie in self.json_data['movies']:
            if movie['_id']==_id:
                for season in movie['tmdb']['tv']['tv_seasons_details']:
                    title = season['name']
                    self.addMarker({'title':title})
                    for  eps in season['episodes']:
                        pass



    def show_filter(self, cItem):

        filter_=cItem.get('cfilter', None)

        nextCategory= cItem['category']
        if not filter_:
            filter_ = 'type_'
        elif filter_ == 'type_':
            filter_ = 'genre'
        elif filter_ == 'genre':
            filter_ = 'year'
        elif filter_ == 'year':
            filter_ = 'order_by'
            nextCategory = 'show_items' if cItem['type_'] !='Serien' else 'show_episodes'


        for elem in self.filter_cache[filter_]:
            param = dict(cItem)
            param[filter_]=elem if filter_ != 'type_' else self.filter_cache['type_'][elem]
            param.update({'category':nextCategory, 'title':str(elem), 'icon':self.DEFAULT_ICON_URL, 'cfilter': filter_ , 'good_for_fav': True})
            self.addDir(param)

    def show_items(self, cItem):
        printDBG(f"Movie2k.show_items cItem [{cItem}]")

        page = cItem.get('page', 1)
        type_ = cItem.get('type_', '')
        order_by = cItem.get('order_by', 'releases')
        year = cItem.get('year', '')
        genre = cItem.get('genre', '')

        url = f"https://api.movie2k.ch/data/browse/?lang=2&keyword=&year={year}&rating=&votes=&genre={genre}&country=&cast=&directors=&type={type_}&order_by={order_by}&page={page}"
        sts, data = self.cm.getPage(url, self.defaultParams)
        if not sts:
            return

        self.json_data = json.loads(data)
        movies = self.json_data.get('movies', [])
        for movie in movies:
            param = dict(cItem)
            desc = f"Genre: {movie.get('genres', '')}"
            desc +=f" | Year: {movie.get('year', '')}"
            desc +=f" | Rating: {movie.get('rating', '')}"
            desc +=' | Story: ' + movie.get('storyline', '')
            icon = movie.get('poster_path','')
            if icon:
                icon = self.ICON_PATH + icon
            else:
                icon = self.api_path + movie.get('_id')
            param.update({'title':movie['title'], 'icon':icon, 'streams':movie.get('streams', []),'desc':desc, 'good_for_fav': True, 'with_mini_cover':True})
            if movie['tv']==1:
                param.update({'category': 'show_episodes', '_id':movie['_id']})
                self.addDir(param)
            else:
                self.addVideo(param)

        currentPage = int(self.json_data['pager']['currentPage'])
        totalPages = int(self.json_data['pager']['totalPages'])
        if currentPage < totalPages:
            param = dict(cItem)
            param.update({'page': currentPage+1})
            self.addNext(param)



    def list_search_result(self, cItem, searchPattern, searchType):
        url = f'{self.MAIN_URL}/search?q={quote(searchPattern)}'
        self.show_items(cItem|{'url':url})


    def getCustomLinksForVideo(self, cItem):
        printDBG(f"Movie2k.getCustomLinksForVideo cItem [{cItem}]")
        urlTab = []
        streams = cItem.get('streams', [])

        for stream in streams:
            url = stream['stream']
            urlTab.append({'url':url, 'name':urlparser.getDomain(url), 'need_resolve':1})

        return urlTab

    def getCustomVideoLinks(self, url):
        printDBG(f"Movie2k.getCustomVideoLinks videoUrl [{url}]")
        return self.up.getVideoLinkExt(url)

    def getCustomArticleContent(self, cItem):
        printDBG(f"movieking.getCustomArticleContent [{cItem}]" )
        retTab = []
        url = cItem['url']
        sts, data0 = self.cm.getPage(url, self.defaultParams)
        if not sts:
            return retTab
        if data1:=re.search(r'<div class="row movies-list-wrap">.*?<h1.*?>(.*?)</h1>.*?<p.*?>(.*?)</p>.*?<strong>Genre:.*?</strong>(.*?)</p>.*?<strong>Veröffentlicht:.*?</strong>(.*?)</p>.*?<strong>Spielzeit:.*?</strong>(.*?)</p>', data0, re.S):
            title = ph.cleanHtml(data1[1])
            desc = ph.cleanHtml(data1[2])
            icon = cItem['icon']
            genre = ",".join(re.findall('<a.*?>(.*?)</a>', data1[3], re.S))

            otherinfo = {'released':data1[4].strip(), 'duration':data1[5].strip(), 'genre': genre}
            retTab.extend([{'title': title, 'text': desc, 'images': [{'title': '', 'url': icon}], 'other_info': otherinfo}])
        return retTab


    def withArticleContent(self, cItem):
        if cItem.get('type', 'video') != 'video' and cItem.get('category', 'unk') != 'showitms':
            return False
        return True

    def listSearchResult(self, cItem, searchPattern, searchType):
        return super().listSearchResult(cItem, searchPattern, searchType)