﻿# -*- coding: utf-8 -*-

###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import DisplayList, HostBase, DisplayItem, DisplayItemType
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, IsExecutable, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.tools.iptvfilehost import IPTVFileHost
from Plugins.Extensions.IPTVPlayer.libs.youtubeparser import YouTubeParser
from Plugins.Extensions.IPTVPlayer.libs import ph
###################################################

###################################################
# FOREIGN import
###################################################
try:
    import json
except Exception:
    import simplejson as json
import re
from urllib.parse import quote_plus
import urllib.parse
import urllib.error
from Components.config import config, ConfigDirectory, getConfigListEntry
###################################################

###################################################
# Config options for HOST
###################################################
config.plugins.iptvplayer.urllistpath = ConfigDirectory(default="/hdd/")


def GetConfigList():
    optionList = [
        getConfigListEntry(_("Sort by:"), config.plugins.iptvplayer.ytSortBy),
        getConfigListEntry(_("Path to ytlist.txt, urllist.txt"), config.plugins.iptvplayer.urllistpath),
        getConfigListEntry(_("Video format:"), config.plugins.iptvplayer.ytformat),
        getConfigListEntry(_("Default video quality:"), config.plugins.iptvplayer.ytDefaultformat),
        getConfigListEntry(_("Use default video quality:"), config.plugins.iptvplayer.ytUseDF),
        getConfigListEntry(_("Age-gate bypass:"), config.plugins.iptvplayer.ytAgeGate)
    ]
    # temporary, the ffmpeg must be in right version to be able to merge file without transcoding
    # checking should be moved to setup
    if IsExecutable('ffmpeg'):
        optionList.append(getConfigListEntry(_("Allow dash format:"), config.plugins.iptvplayer.ytShowDash))
        if config.plugins.iptvplayer.ytShowDash.value != 'false':
            optionList.append(getConfigListEntry(_("Allow VP9 codec:"), config.plugins.iptvplayer.ytVP9))
    return optionList
###################################################
###################################################


def gettytul():
    return 'https://youtube.com/'


class IPTVHost(HostBase):

    def __init__(self):
        printDBG("Youtube.__init__")
        super().__init__( {'history': 'ytlist', 'cookie': 'youtube.cookie'},True,[DisplayItemType.VIDEO, DisplayItemType.AUDIO])
        self.UTLIST_FILE = 'ytlist.txt'
        self.DEFAULT_ICON_URL = 'https://www.vippng.com/png/full/85-853653_patreon-logo-png-transparent-background-youtube-logo.png'
        self.MAIN_GROUPED_TAB = [{'category': 'listCategory', 'title': _("User links"), 'desc': _("User links stored in the ytlist.txt file.")},
                                 {'category': 'search', 'title': _("Search"), 'desc': _("Search youtube materials "), 'search_item': True},
                                 {'category': 'listFeeds_', 'title': _("Trending"), 'desc': _("Browse youtube trending feeds")},
                                 {'category': 'search_history', 'title': _("Search history"), 'desc': _("History of searched phrases.")}]

        self.SEARCH_TYPES = [("Video", "video"),
                               (_("Channel"), "channel"),
                               (_("Playlist"), "playlist"),
                               (_("Live"), "live")]

        self.ytp = YouTubeParser()
        self.currFileHost = None

    def _getCategory(self, url):
        printDBG("Youtube._getCategory")
        if '/playlist?list=' in url:
            category = 'playlist'
        elif url.split('?')[0].endswith('/playlists'):
            category = 'playlists'
        elif None is not re.search('/watch\?v=[^\&]+?\&list=', url):
            category = 'traylist'
        elif 'user/' in url or (('channel/' in url or '/c/' in url or '/@') and not url.endswith('/live')):
            category = 'channel'
        else:
            category = 'video'
        return category

    def mainMenu(self, cItem):
        printDBG("Youtube.listsMainMenu")
        for item in self.MAIN_GROUPED_TAB:
            params = {'name': 'category'}
            params.update(item)
            self.addDir(params)

    def listCategory(self, cItem, searchMode=False):
        printDBG("Youtube.listCategory cItem[%s]" % cItem)

        sortList = True
        filespath = config.plugins.iptvplayer.urllistpath.value
        if 'sub_file_category' not in cItem:
            self.currFileHost = IPTVFileHost()
            self.currFileHost.addFile(filespath + self.UTLIST_FILE, encoding='utf-8')
            tmpList = self.currFileHost.getGroups(sortList)
            if 0 < len(tmpList):
                params = dict(cItem)
                params.update({'sub_file_category': 'all', 'group': 'all', 'title': _("--All--")})
                self.addDir(params)
            for item in tmpList:
                if '' == item:
                    title = _("--Other--")
                else:
                    title = item
                params = dict(cItem)
                params.update({'sub_file_category': 'group', 'title': title, 'group': item})
                self.addDir(params)
        else:
            if 'all' == cItem['sub_file_category']:
                tmpList = self.currFileHost.getAllItems(sortList)
                for item in tmpList:
                    params = dict(cItem)
                    category = self._getCategory(item['url'])
                    params.update({'good_for_fav': True, 'title': item['full_title'], 'url': item['url'], 'desc': item['url'], 'category': category})
                    if 'video' == category:
                        self.addVideo(params)
                    elif 'more' == category:
                        self.addNext(params)
                    else:
                        self.addDir(params)
            elif 'group' == cItem['sub_file_category']:
                tmpList = self.currFileHost.getItemsInGroup(cItem['group'], sortList)
                for item in tmpList:
                    if '' == item['title_in_group']:
                        title = item['full_title']
                    else:
                        title = item['title_in_group']
                    params = dict(cItem)
                    category = self._getCategory(item['url'])
                    params.update({'good_for_fav': True, 'title': title, 'url': item['url'], 'desc': item['url'], 'category': category})
                    if 'video' == category:
                        self.addVideo(params)
                    elif 'more' == category:
                        self.addNext(params)
                    else:
                        self.addDir(params)

    def playlists(self, cItem):
        printDBG('Youtube.playlists cItem[%s]' % (cItem))
        category = cItem.get("category", '')
        url = cItem.get("url", '')
        page = cItem.get("page", '1')

        if "playlists" == category:
            self.currList = self.ytp.getListPlaylistsItems(url, category, page, cItem)

        for item in self.currList:
            if item['category'] in ["channel", "playlist", "traylist"]:
                item['good_for_fav'] = True

    def listFeeds(self, cItem):
        printDBG('Youtube.listFeeds cItem[%s]' % (cItem))
        if cItem['category'] == "listFeeds":
            sts, data = self.cm.getPage(cItem['url'])
            data2 = ph.getAllItemsBetweenMarkers(data, "videoRenderer", "watchEndpoint")
            for item in data2:
                url = "https://www.youtube.com/watch?v=" + ph.getDataBetweenMarkers(item, 'videoId":"', '","thumbnail":', False)[1]
                # icon = re.search(',{"url":"(.*?)[=]*?\\\\', item, re.S)[1]
                icon = re.search('},{"url":"([^"]+?)"',item, re.S)[1]
                title = ph.getDataBetweenMarkers(item, '"title":{"runs":[{"text":"', '"}]', False)[1]
                desc = _("Channel") + ': ' + ph.getDataBetweenMarkers(item, 'longBylineText":{"runs":[{"text":"', '","navigationEndpoint"', False)[1] + "\n" + _("Release:") + ' ' + ph.getDataBetweenMarkers(item, '"publishedTimeText":{"simpleText":"', '"},"lengthText":', False)[1] + "\n" + _("Duration:") + ' ' + ph.getDataBetweenMarkers(item, '"lengthText":{"accessibility":{"accessibilityData":{"label":"', '"}},"simpleText":', False)[1] + "\n" + ph.getDataBetweenMarkers(item, '"viewCountText":{"simpleText":"', '"},"navigationEndpoint":', False)[1]
                params = {'title': title, 'url': url, 'icon': icon, 'desc': desc}
                self.addVideo(params)

    def listFeeds_(self, cItem):
        printDBG('Youtube.listFeeds cItem[%s]' % (cItem))
        title = _("Trending")
        url = "https://www.youtube.com/feed/trending"
        params = {'category': 'listFeeds', 'title': title, 'url': url}
        self.addDir(params)
        title = _("Music")
        url = "https://www.youtube.com/feed/trending?bp=4gINGgt5dG1hX2NoYXJ0cw%3D%3D"
        params = {'category': 'listFeeds', 'title': title, 'url': url}
        self.addDir(params)
        title = _("Games")
        url = "https://www.youtube.com/feed/trending?bp=4gIcGhpnYW1pbmdfY29ycHVzX21vc3RfcG9wdWxhcg%3D%3D"
        params = {'category': 'listFeeds', 'title': title, 'url': url}
        self.addDir(params)
        title = _("Movies")
        url = "https://www.youtube.com/feed/trending?bp=4gIKGgh0cmFpbGVycw%3D%3D"
        params = {'category': 'listFeeds', 'title': title, 'url': url}
        self.addDir(params)
        title = _("Live")
        url = "https://www.youtube.com/channel/UC4R8DWoMoI7CAwX8_LjQHig"
        params = {'category': 'listFeeds', 'title': title, 'url': url}
        self.addDir(params)
        title = _("News")
        url = "https://www.youtube.com/channel/UCYfdidRxbB8Qhf0Nx7ioOYw"
        params = {'category': 'listFeeds', 'title': title, 'url': url}
        self.addDir(params)
        title = _("Sport")
        url = "https://www.youtube.com/channel/UCEgdi0XIXXZ-qJOFPf4JSKw"
        params = {'category': 'listFeeds', 'title': title, 'url': url}
        self.addDir(params)


    def sub_items(self, cItem):
        printDBG("Youtube.sub_items")
        self.currList = DisplayList(cItem['sub_items'])

    def traylist(self, cItem):
        printDBG('Youtube.traylist cItem[%s]' % (cItem))

        url = strwithmeta(cItem.get("url", ''))
        page = cItem.get("page", '1')
        self.currList = DisplayList(self.ytp.getVideosFromPlaylist(url, "traylist", page, cItem))
        self.currList = DisplayList(self.ytp.getVideosFromTraylist(url, "traylist", page, cItem))

    def playlist(self, cItem):
        printDBG('Youtube.playlist cItem[%s]' % (cItem))

        url = strwithmeta(cItem.get("url", ''))
        page = cItem.get("page", '1')
        self.currList = DisplayList(self.ytp.getVideosFromPlaylist(url, "playlist", page, cItem))

    def channel(self, cItem):
        printDBG('Youtube.channel cItem[%s]' % (cItem))

        category = cItem.get("category", '')
        url = strwithmeta(cItem.get("url", ''))
        page = cItem.get("page", '1')

        if 'browse' not in url and 'ctoken' not in url:
            if url.endswith('/videos'):
                url = url + '?flow=list&view=0&sort=dd'
            else:
                url = url + '/videos?flow=list&view=0&sort=dd'
            tmp = self.ytp.getVideosFromChannelList(url, "channel", page, cItem)
            if len(tmp) > 0:
                params = {'good_for_fav': False, 'category': 'sub_items', 'title': _('Videos'), 'sub_items': tmp}
                self.addDir(params)
            url = url.replace('videos', 'streams')
            tmp = self.ytp.getVideosFromChannelList(url, "channel", page, cItem)
            if len(tmp) > 0:
                params = {'good_for_fav': False, 'category': 'sub_items', 'title': _('Live streams'), 'sub_items': tmp}
                self.addDir(params)
            url = url.replace('streams', 'playlists')
            tmp = self.ytp.getPlaylistsFromChanelList(url, "channel", page, cItem)
            if len(tmp) > 0:
                params = {'good_for_fav': False, 'category': 'sub_items', 'title': _('Playlists'), 'sub_items': tmp}
                self.addDir(params)
        else:
            self.currList = DisplayList(self.ytp.getVideosFromChannelList(url, "channel", page, cItem))


    def listSearchResult(self, cItem, pattern, searchType):
        page = cItem.get("page", '1')
        url = cItem.get("url", "")

        if url:
            printDBG("URL ricerca -----------> %s" % url)
            tmpList = self.ytp.getSearchResult(quote_plus(pattern), searchType, page, 'search', config.plugins.iptvplayer.ytSortBy.value, url)
        else:
            tmpList = self.ytp.getSearchResult(quote_plus(pattern), searchType, page, 'search', config.plugins.iptvplayer.ytSortBy.value)

        for item in tmpList:
            item.update({'name': 'category'})
            if 'video' == item['type']:
                self.addVideo(item)
            elif 'more' == item['type']:
                self.addNext(item)
            else:
                if item['category'] in ["channel", "playlist", "traylist"]:
                    item['good_for_fav'] = True
                    item['with_mini_cover'] = True
                self.addDir(item)

    def getCustomLinksForVideo(self, cItem):
        printDBG("Youtube.getCustomLinksForVideo cItem[%s]" % cItem)
        urlTab = self.up.getVideoLinkExt(cItem['url'])
        if config.plugins.iptvplayer.ytUseDF.value and 0 < len(urlTab):
            return [urlTab[0]]
        return urlTab

    def getFavouriteData(self, cItem):
        printDBG('Youtube.getFavouriteData')
        return json.dumps(cItem)

    def getCustomLinksForFavourite(self, fav_data):
        printDBG('Youtube.getCustomLinksForFavourite')
        links = []
        try:
            cItem = json.loads(fav_data)
            links = self.getCustomLinksForVideo(cItem)
        except Exception:
            printExc()
            return self.getCustomLinksForVideo({'url': fav_data})
        return links

    def setInitListFromFavouriteItem(self, fav_data):
        printDBG('Youtube.setInitListFromFavouriteItem')
        try:
            params = json.loads(fav_data)
        except Exception:
            params = {}
            printExc()
        self.addDir(params)
        return True


    def getSuggestionsProvider(self, index):
        printDBG('Youtube.getSuggestionsProvider')
        from Plugins.Extensions.IPTVPlayer.suggestions.google import SuggestionsProvider
        return SuggestionsProvider(True)

    def getSearchTypes(self):
        return self.SEARCH_TYPES

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)
