# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, RetHost, ArticleContent, RetStatus, handleServiceDecorator
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import DaysInMonth, printDBG, printExc,   MergeDicts
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import getDirectM3U8Playlist
from Plugins.Extensions.IPTVPlayer.libs import ph

#from Plugins.Extensions.IPTVPlayer.libs.cloudscraper import create_scraper
###################################################

###################################################
# FOREIGN import
###################################################
import re
import urllib.request
import urllib.parse
import urllib.error
import base64
try:
    import json
except Exception:
    import simplejson as json
from copy import deepcopy
###################################################

def gettytul():
    return 'https://hdfilme.cx'


class IPTVHost(HostBase):

    MAIN_URL = 'https://hdfilme.cx'
    SEARCH_URL = MAIN_URL + '/movie-search'
    DEFAULT_ICON = "https://raw.githubusercontent.com/StoneOffStones/plugin.video.xstream/c88b2a6953febf6e46cf77f891d550a3c2ee5eea/resources/art/sites/hdfilme.png" #"http://hdfilme.tv/public/site/images/logo.png"

    MAIN_CAT_TAB = [{'icon': DEFAULT_ICON, 'category': 'list_filters', 'filter': 'genre', 'title': _('Movies'), 'url': MAIN_URL + '/filme1'},
                    {'icon': DEFAULT_ICON, 'category': 'list_filters', 'filter': 'genre', 'title': _('Series'), 'url': MAIN_URL + '/serien1'},
                    {'icon': DEFAULT_ICON, 'category': 'list_filters', 'filter': 'genre', 'title': _('Trailers'), 'url': MAIN_URL + '/trailer'},
                    {'icon': DEFAULT_ICON, 'category': 'search', 'title': _('Search'), 'search_item': True},
                    {'icon': DEFAULT_ICON, 'category': 'search_history', 'title': _('Search history')}]

    def __init__(self):
        super().__init__( {'history': '  HDFilmeTV.cc', 'cookie': 'hdfilmenet.cookie'})
        self.USER_AGENT = getDefaultUserAgent()
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Accept': 'text/html'}
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.filtersCache = {'genre': [], 'country': [], 'sort': []}
        self.seasonCache = {}
        self.cacheLinks = {}


    def c_int(self, n):
        if n.isdigit():
            return int(n)
        else:
            return 0

    def getPage(self, baseUrl, params={}, post_data=None):
        if params == {}:
            params = self.defaultParams
        return self.cm.getPage(baseUrl, params, post_data)

    def getPageCF(self, baseUrl, params={}, post_data=None):
        #return True, self.scraper.get(baseUrl).text
        if params == {}:
            params = self.defaultParams
        params['cloudflare_params'] = {'domain': 'hdfilme.cx', 'cookie_file': self.COOKIE_FILE, 'User-Agent': self.USER_AGENT, 'full_url_handle': self.getFullUrl}
        return self.cm.getPageCFProtection(baseUrl, params, post_data)

    def getIconUrl(self, url):
        url = self.getFullUrl(url)
        if url == '':
            return ''
        cookieHeader = self.cm.getCookieHeader(self.COOKIE_FILE)
        return strwithmeta(url, {'Cookie': cookieHeader, 'User-Agent': self.USER_AGENT})

    def getMovieDatainJS(self, data):
        printDBG("HDFilmeTV.getMovieDatainJs")
        # example:
        #    var movieData = {
        #            id : 13810,
        #            name : "The Super",
        #            url : "https://hdfilme.cx/the-super-13810-stream"
        #    };

        movieData = {}
        code = ph.getDataBetweenMarkers(data, 'var movieData = {', '}', False)[1]
        printDBG("movie data code: -----------------")
        printDBG(code)
        printDBG("-----------------")
        movie_id = ph.getSearchGroups(code, "id : ([0-9]+?),")[0]
        movie_name = ph.getSearchGroups(code, '''name : ['"]([^'^"]+?)['"]''')[0]
        movie_url = ph.getSearchGroups(code, '''url : ['"]([^'^"]+?)['"]''')[0]
        movieData = {'id': movie_id, 'name': movie_name, 'url': movie_url}
        printDBG(str(movieData))
        return movieData

    def getTrailerUrlinJS(self, data, movieData):
        printDBG("HDFilmeTV.getTrailerUrlinJs")

        #    function load_trailer() {
        #           $( "#play-area-wrapper" ).load( "/movie/load-trailer/" + movieData.id + "/0?trailer=bEplMVBHYVRHNDJoNjFWRWNXMFNiNm1zc0NvbllFNG1XU3JYM2xPbWVCcHVGMHNBU3FPRzJlTGdCWTBqcjZ5bg==", function() {
        #               $("html, body").animate({ scrollTop: $('#play-area-wrapper').offset().top }, 1000);
        #           console.log( "Load was performed." );
        #
        #           });
        #    }

        trailerUrl = ''
        code = ph.getDataBetweenMarkers(data, 'function load_trailer() {', '}', False)[1]
        printDBG("load_trailer code: -----------------")
        printDBG(code)
        printDBG("-----------------")
        trailerUrl = ph.getSearchGroups(code, ".load\( \"(.*?)\",")[0]
        trailerUrl = self.getFullUrl(trailerUrl.replace("\" + movieData.id + \"", movieData['id']))
        return trailerUrl

    def fillFiltersCache(self, cItem):
        printDBG("HDFilmeTV.fillFiltersCache")
        self.filtersCache = {'genre': [], 'country': [], 'sort': []}

        params = self.defaultParams | {'user-agent': self.USER_AGENT, 'referer': self.MAIN_URL, "accept-encoding": "gzip", "accept": "text/html"}
        printDBG("^^^^^^^^^^^^^^^^^^^^^^^")
        printDBG(str(params))

        sts, data = self.getPageCF(cItem['url'], params)
        if not sts:
            return

        #headers = { 'User-Agent':self.USER_AGENT}
        #data = self.session.get(cItem['url'], headers=headers).text

        cookieHeader = self.cm.getCookieHeader(self.COOKIE_FILE)
        printDBG(cookieHeader)

        self.x_csrf_token=ph.getSearchGroups(data, "window.livewire_token = '([^']+?)'")[0]

        wire = ph.getAllItemsBetweenMarkers(data, ('<div','>', "wire:id="), '</div>')


        # self.cf_r_params = ph.getSearchGroups(data, "__CF\$cv\$params'\]=.*r:'([^']+?)'")[0]
        # self.getPageCF("https://hdfilme.cx/cdn-cgi/bm/cv/result?req_id="+self.cf_r_params, params)


        for w in wire:
            if 'movies-listing' in w:
                self.post_data=ph.getSearchGroups(w,'wire:initial-data="([^"]+?)"')[0]
                self.post_data=self.post_data.replace('&quot;', '"')
                break

        for filter in [{'m': 'name="genre"', 'key': 'genre'}, {'m': 'name="country"', 'key': 'country'}, {'m': 'name="orderBy"', 'key': 'sort'}]:
            filterData = []

            filterData = ph.getDataBetweenMarkers(data, ('<select', '>', filter['m']), '</select>')[1]
            #printDBG("^^^^^^^^^^^^^^^^^^")
            #printDBG(filterData[0])
            optionsData = []
            optionsData = ph.findall(filterData, ('<option', '>'), '</option>')
            #printDBG("^^^^^^^^^^^^^^^^^^")
            #printDBG(str(optionsData))
            optionsData.sort()
            #printDBG(str(optionsData))
            #printDBG("^^^^^^^^^^^^^^^^^^")
            #printDBG("^^^^^^^^^^^^^^^^^^")

            old_value = ""
            for item in optionsData:
                title = ph.cleanHtml(item)
                value = ph.getSearchGroups(item, '''value=['"]([^'^"]+?)['"]''')[0]
                if value == '':
                    continue

                if value != old_value:
                    old_value = value
                    self.filtersCache[filter['key']].append({'title': title, filter['key']: value})

            if len(self.filtersCache[filter['key']]) and filter['key'] != 'sort':
                self.filtersCache[filter['key']].insert(0, {'title': _('--All--'), filter['key']: ''})

            # add sort_type to sort filter
        orderLen = len(self.filtersCache['sort'])
        for idx in range(orderLen):
            item = deepcopy(self.filtersCache['sort'][idx])
            # desc
            self.filtersCache['sort'][idx].update({'title': '\u2193 ' + self.filtersCache['sort'][idx]['title'], 'sort_type': 'desc'})
            # asc
            item.update({'title': '\u2191 ' + item['title'], 'sort_type': 'asc'})
            self.filtersCache['sort'].append(item)

    def listFilters(self, cItem, nextCategory, nextFilter):
        filter = cItem.get('filter', '')
        printDBG("HDFilmeTV.listFilters filter[%s] nextFilter[%s]" % (filter, nextFilter))
        tab = self.filtersCache.get(filter, [])
        if len(tab) == 0:
            self.fillFiltersCache(cItem)
        tab = self.filtersCache.get(filter, [])
        params = dict(cItem)
        params['category'] = nextCategory
        params['filter'] = nextFilter
        self.listsTab(tab, params)

    def listItems(self, cItem, nextCategory):
        printDBG("HDFilmeTV.listItems")

        itemsPerPage = 30
        page = cItem.get('page', 1)
        url = cItem['url']

        query = {}
        if 'search_pattern' in cItem:
            query = {'key': cItem['search_pattern'], 'page': page}
        else:
            query = {'page': page, 'category': cItem['genre'], 'country': cItem['country'], 'sort': cItem['sort'], 'sort_type': cItem['sort_type']}

        url += "?" + urllib.parse.urlencode(query)

        sts, data = self.getPageCF(url)
        if not sts:
            return

        #printDBG(data)

        data0=re.findall('<a class="block relative".+?href="([^"]+?)".+?title="([^"]+?)".*?data-src="([^"]+?)".*?</a>', data, re.S)
        if data0:
            for elem in data0:
                title = elem[1]
                url=elem[0]
                img=elem[2]
                params = dict(cItem)
                params.update({'good_for_fav': True, 'category': nextCategory, 'title': title, 'url': self.getFullUrl(url), 'icon': self.getIconUrl(img)})
                #printDBG("----> movie item ------> " + str(params))
                self.addDir(params)

        data1 = re.search('Seite.*(\d+)\/(\d+)', data)
        if data1:
            currentpage=int(data1[1])
            maxpage=int(data1[2])

            if page == currentpage and page < maxpage:
                params = dict(cItem)
                params.update({'good_for_fav': False, 'title': _('Next page'), 'page': (page + 1)})
                self.addMore(params)

    def exploreItem(self, cItem):
        printDBG("HDFilmeTV.exploreItem")
        params = dict(self.defaultParams)
        params['header']['Referer'] = cItem['url']

        sts, data = self.getPageCF(cItem['url'], params)
        #printDBG(data)
        if not sts:
            return

        movieId = ph.getSearchGroups(data, '''&quot;movieId&quot;:(.*?),''')[0]
        printDBG("movieId ------->" + movieId)

        trailerUrl = ''
        linksPageUrl = ''

        trailer_matcher = re.search('''<template x-if="openTrailer">.*?src="([^"]+?)".*?</template>''', data, re.S)
        if trailer_matcher:
            trailerUrl = trailer_matcher[1]
        stream_matcher = re.search('''<div class="actions mt-5 relative">.*?<a href="([^"]+?)" title="([^"]+?)".*?Stream.*?</a>''', data, re.S)
        if stream_matcher:
            linksPageUrl = stream_matcher[1]

        # trailer section
        self.addVideo(cItem | {'good_for_fav': False, 'title': '%s [%s]' % (cItem['title'], _('Trailer')), 'urls': [{'name': 'trailer', 'url': strwithmeta(trailerUrl, {'trailer': 1}), 'need_resolve': 1}]})

        # find links page url
        # example
        #<a title="The Ranch staffel 4 Stream" class="btn btn-xemnow pull-right" style="margin-left:5px" href="https://hdfilme.cx/the-ranch-staffel-4-13803-stream/folge-1">
        printDBG("HDFilmeTV.exploreItem. Find url of page with links - often url + '/deutsch' ")

        params = dict(self.defaultParams)
        params['header']['Referer'] = cItem['url']
        sts, linkspage_data = self.getPageCF(linksPageUrl, params)
        if not sts:
            return

        #printDBG(data)
        #printDBG(">>>>>>>>>>>>>>>>>>>>>>>>")
        csrf_token = re.search('''name="csrf-token" content="([^"]+?)"''', linkspage_data, re.S)[1]
        post_data = re.search('''wire:initial-data="([^"]+?player[^"]+?)"''', linkspage_data, re.S)[1].replace('&quot;', '"').replace('\\', '')

        params = dict(self.defaultParams)
        params['header']['Referer'] = linksPageUrl
        params['header']["X-CSRF-TOKEN"] = csrf_token
        params['header']["X-Livewire"] = True
        params['header']["Origin"]= "https://hdfilme.cx"
        params['header']["Sec-Fetch-Site"] = "same-origin"
        params['header']["Sec-Fetch-Mode"] = "cors"
        params['header']["Sec-Fetch-Dest"] = "empty"
        params['header']["Content-Type"] = "application/json"

            #"Connection": "keep-alive",


            # "Accept": "text/html, application/xhtml+xml",

            # "Accept-Encoding": "gzip, deflate, br",
            # "Accept-Language": "de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7,ar;q=0.6"

        params['raw_post_data'] = True
        sts, data = self.getPageCF("https://hdfilme.cx/livewire/message/player", params, post_data=post_data.encode())
        if not sts:
            return
        printDBG(data)
        printDBG(">>>>>>>>>>>>>>>>>>>>>>>>")

    def getCustomLinksForVideo(self, cItem):
        printDBG("HDFilmeTV.getCustomLinksForVideo [%s]" % cItem)
        return cItem.get('urls', [])

    def getCustomVideoLinks(self, videoUrl):
        printDBG("HDFilmeTV.getCustomVideoLinks [%s]" % videoUrl)
        urlTab = []

        if isinstance(videoUrl, strwithmeta):
            if 'trailer' in videoUrl.meta:
                printDBG(f"--------> Trailer Url: {videoUrl}" )
                return self.up.getVideoLinkExt(videoUrl)

            if 'episodeId' in videoUrl.meta:
                episode_id = videoUrl.meta['episodeId']
            if 'movieId' in videoUrl.meta:
                movie_id = videoUrl.meta['movieId']
            else:
                movie_id = ''

        params = MergeDicts(self.defaultParams, {'user-agent': self.USER_AGENT, 'referer': videoUrl, "accept-encoding": "gzip", "accept": "text/html"})

        sts, data = self.getPageCF(videoUrl, params)
        printDBG(data)
        if not sts:
            return []

        url = ph.getDataBetweenMarkers(data, '$( "#play-area-wrapper" ).load( "', '"', False)[1]

        if len(url) > 1:

            if movie_id == '':
                url = self.getFullUrl(url + episode_id + "?")
            else:
                url = self.getFullUrl(url + movie_id + '/' + episode_id + "?")
            printDBG("video link---->" + url)
            sts, tmp = self.getPage(url, params)
            #printDBG (tmp)
            #printDBG ("+++++++++++++++++")

            if sts:
                url = ph.getDataBetweenMarkers(tmp, 'window.urlVideo = "', '"', False)[1]
                url = strwithmeta(url, {'Accept': '*/*', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()[:-1], 'User-Agent': self.USER_AGENT})
                url.meta['iptv_m3u8_seg_download_retry'] = 10
                urlTab.extend(getDirectM3U8Playlist(url, checkExt=False, checkContent=True, sortWithMaxBitrate=999999999))

                printDBG("------->" + url)

                '''
                url = strwithmeta(url, {'Accept':'*/*', 'Referer':self.getMainUrl(), 'Origin':self.getMainUrl()[:-1], 'User-Agent':self.defaultParams['header']['User-Agent']})
                printDBG(">> TYPE: " + type)
                if 'mp4' in type or 'flv' in type:
                    urlTab.append({'name':str(item['label']), 'url':url})
                elif 'hls' in type or 'm3u8' in type:
                    url.meta['iptv_m3u8_seg_download_retry'] = 10
                    urlTab.extend(getDirectM3U8Playlist(url, checkExt=False, checkContent=True, sortWithMaxBitrate=999999999))

                if len(urlTab):
                    return urlTab
                '''
        return urlTab

    def getFavouriteData(self, cItem):
        printDBG('HDFilmeTV.getFavouriteData')
        return json.dumps(cItem)

    def getCustomLinksForFavourite(self, fav_data):
        printDBG('HDFilmeTV.getCustomLinksForFavourite')
        links = []
        try:
            cItem = json.loads(fav_data)
            links = self.getCustomLinksForVideo(cItem)
        except Exception:
            printExc()
        return links

    def setInitListFromFavouriteItem(self, fav_data):
        printDBG('HDFilmeTV.setInitListFromFavouriteItem')
        try:
            params = json.loads(fav_data)
        except Exception:
            params = {}
            printExc()
        self.addDir(params)
        return True

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("HDFilmeTV.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)
        cItem['search_pattern'] = urllib.parse.quote(searchPattern)
        cItem['url'] = self.SEARCH_URL
        self.listItems(cItem, 'explore_item')

    def getCustomArticleContent(self, cItem):
        printDBG("HDFilmeTV.getCustomArticleContent [%s]" % cItem)
        retTab = []

        sts, data = self.getPage(cItem['url'], self.defaultParams)
        if not sts:
            return retTab

        data = ph.getDataBetweenMarkers(data, '<div id="main">', '<div class="row">')[1]

        icon = self.getFullUrl(ph.getSearchGroups(data, '''src=['"]([^'^"]+?)['"]''')[0])
        if icon == '':
            icon = cItem.get('icon', '')

        title = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<b class="text-blue title-film">', '</b>', False)[1])
        if title == '':
            title = cItem['title']

        desc = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<div class="caption">', '</div>', False)[1])

        descData = ph.getDataBetweenMarkers(data, '<div class="movie-info pull-left">', '</div>', False)[1]
        descData = ph.getAllItemsBetweenMarkers(descData, '<p', '</p>', caseSensitive=False)
        descTabMap = {"Genre": "genre",
                      "IMDB": "rating",
                      "Bewertung": "rated",
                      "Veröffentlichungsjahr": "year",
                      "Regisseur": "director",
                      "Schauspieler": "actors",
                      "Staat": "country",
                      "Zeit": "duration",
                      }

        otherInfo = {}
        for item in descData:
            item = item.split('</span>')
            if len(item) < 2:
                continue
            key = ph.cleanHtml(item[0]).replace(':', '').strip()
            val = ph.cleanHtml(item[1])
            for dKey in descTabMap:
                if dKey in key:
                    if descTabMap[dKey] == 'rating':
                        val += ' IMDB'
                    otherInfo[descTabMap[dKey]] = val
                    break

        views = ph.getSearchGroups(data, '''Aufrufe[^>]*?([0-9]+?)[^0-9]''')[0]
        if views != '':
            otherInfo['views'] = views

        return [{'title': ph.cleanHtml(title), 'text': desc, 'images': [{'title': '', 'url': self.getIconUrl(icon)}], 'other_info': otherInfo}]

    @handleServiceDecorator
    def handleService(self, index, refresh=0, searchPattern='', searchType=''):

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        mode = self.currItem.get("mode", '')
        filter = self.currItem.get("filter", '')

        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))

    #MAIN MENU
        if name is None:
            self.listsTab(self.MAIN_CAT_TAB, {'name': 'category'})
        elif category == 'list_filters':
            if filter == 'genre':
                nextFilter = 'country'
                nextCategory = 'list_filters'
            elif filter == 'country':
                nextFilter = 'sort'
                nextCategory = 'list_filters'
            else:
                nextFilter = ''
                nextCategory = 'list_items'
            self.listFilters(self.currItem, nextCategory, nextFilter)
        elif category == 'list_items':
            self.listItems(self.currItem, 'explore_item')
        elif category == 'explore_item':
            self.exploreItem(self.currItem)
    #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

