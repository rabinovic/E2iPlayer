# -*- coding: utf-8 -*-
import json
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Components.config import config, ConfigYesNo, getConfigListEntry

from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import getDirectM3U8Playlist
from urllib.parse import quote
from datetime import timedelta


config.plugins.iptvplayer.Ronemocom_pin = ConfigYesNo(default=True)

def GetConfigList():
    optionList = []
    optionList.append(
        getConfigListEntry(_("Pin protection for plugin") + " :", config.plugins.iptvplayer.Ronemocom_pin))
    return optionList

def gettytul():
    return 'https://ronemo.com/'

class IPTVHost(HostBase):

    def __init__(self):

        super().__init__( {'history': 'ronemo.com', 'cookie': 'ronemo.com.cookie'})

        self.DEFAULT_ICON_URL = 'https://ronemo.com/assets/images/logo64.png'

        self.MAIN_URL = gettytul()
        self.USER_AGENT=getDefaultUserAgent()
        self.HEADER = {
                        'User-Agent': self.USER_AGENT,
                        'Accept': 'application/json, text/plain, */*',
                        'Accept-Encoding': 'gzip, deflate',
                        'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
                        'Content-Type': 'application/json',
                        'Referer': 'https://ronemo.com/',
                    }

        self.defaultParams = {
                                'header': self.HEADER,
                                'use_cookie': True,
                                'load_cookie': True,
                                'save_cookie': True,
                                'cookiefile': self.COOKIE_FILE
                            }

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)
        return self.cm.getPage(baseUrl, addParams, post_data)


    def mainMenu(self, cItem):
        printDBG("ronemo.listMain")

        MAIN_CAT_TAB = [
            {'category': 'exploreItem', 'title': _('Main'), 'url': "https://ronemo.com/api/home/get-videos?userName=&urlCategory=&h_inside=1&userId=undefined&firstVisit=0&vid=mlnReINU", "start":0},
            {'category': 'exploreItem', 'title': _('Most popular'), 'url': "https://ronemo.com/api/video/get-most-videos?mostSortType=mostPopular", "start":0},
            {'category': 'exploreItem', 'title': _('Most viewed'), 'url': "https://ronemo.com/api/video/get-most-videos?mostSortType=mostView", "start":0},
            {'category': 'exploreItem', 'title': _('Most liked'), 'url': "https://ronemo.com/api/video/get-most-videos?mostSortType=mostLikes", "start":0},
            {'category': 'listCategories', 'title': _('Categories')},
            {'category': 'search', 'title': _('Search'), 'search_item': True, },
            {'category': 'search_history', 'title': _('Search history'), }
            ]

        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCategories(self, cItem):
        printDBG("ronemo.listCategories")
        CAT_TAB = [
            {'category': 'exploreItem', 'title': _('Autos & vehicles'), 'url': 'https://ronemo.com/api/home/get-videos?userName=&urlCategory=autos-vehicles&h_inside=1&userId=undefined&firstVisit=0&fp=6a12ee8f2da67eb1716a8eaa90b4c1f8', "start":0},
            {'category': 'exploreItem', 'title': _('Comedy'), 'url': "https://ronemo.com/api/home/get-videos?idLastPost=&userName=&urlCategory=comedy&h_inside=1&userId=undefined&firstVisit=0&fp=6a12ee8f2da67eb1716a8eaa90b4c1f8", "start":0},
            {'category': 'exploreItem', 'title': _('Education'), 'url': "https://ronemo.com/api/home/get-videos?idLastPost=&userName=&urlCategory=education&h_inside=1&userId=undefined&firstVisit=0&fp=6a12ee8f2da67eb1716a8eaa90b4c1f8", "start":0},
            {'category': 'exploreItem', 'title': _('Entertainment'), 'url': "https://ronemo.com/api/home/get-videos?idLastPost=&userName=&urlCategory=entertainment&h_inside=1&userId=undefined&firstVisit=0&fp=6a12ee8f2da67eb1716a8eaa90b4c1f8", "start":0},
            {'category': 'exploreItem', 'title': _('Film & Animation'), 'url': "https://ronemo.com/api/home/get-videos?idLastPost=&userName=&urlCategory=film-animation&h_inside=1&userId=undefined&firstVisit=0&fp=6a12ee8f2da67eb1716a8eaa90b4c1f8", "start":0},
            {'category': 'exploreItem', 'title': _('Gaming'), 'url': "https://ronemo.com/api/home/get-videos?idLastPost=&userName=&urlCategory=gaming&h_inside=1&userId=undefined&firstVisit=0&fp=6a12ee8f2da67eb1716a8eaa90b4c1f8", "start":0},
            {'category': 'exploreItem', 'title': _('How To & Style'), 'url': "https://ronemo.com/api/home/get-videos?idLastPost=&userName=&urlCategory=how-to-style&h_inside=1&userId=undefined&firstVisit=0&fp=6a12ee8f2da67eb1716a8eaa90b4c1f8", "start":0},
            {'category': 'exploreItem', 'title': _('Music'), 'url': "https://ronemo.com/api/home/get-videos?idLastPost=&userName=&urlCategory=music&h_inside=1&userId=undefined&firstVisit=0&fp=6a12ee8f2da67eb1716a8eaa90b4c1f8", "start":0},
            {'category': 'exploreItem', 'title': _('News'), 'url': "https://ronemo.com/api/home/get-videos?idLastPost=&userName=&urlCategory=news-politics&h_inside=1&userId=undefined&firstVisit=0&fp=6a12ee8f2da67eb1716a8eaa90b4c1f8", "start":0},
            {'category': 'exploreItem', 'title': _('Pets & Animals'), 'url': "https://ronemo.com/api/home/get-videos?idLastPost=&userName=&urlCategory=pets-animals&h_inside=1&userId=undefined&firstVisit=0&fp=6a12ee8f2da67eb1716a8eaa90b4c1f8", "start":0},
            {'category': 'exploreItem', 'title': _('Science & Technology'), 'url': "https://ronemo.com/api/home/get-videos?idLastPost=&userName=&urlCategory=science-technology&h_inside=1&userId=undefined&firstVisit=0&fp=6a12ee8f2da67eb1716a8eaa90b4c1f8", "start":0},
            {'category': 'exploreItem', 'title': _('Sports'), 'url': "https://ronemo.com/api/home/get-videos?idLastPost=&userName=&urlCategory=sports&h_inside=1&userId=undefined&firstVisit=0&fp=6a12ee8f2da67eb1716a8eaa90b4c1f8", "start":0},
            {'category': 'exploreItem', 'title': _('Travel & Events'), 'url': "https://ronemo.com/api/home/get-videos?idLastPost=&userName=&urlCategory=travel-events&h_inside=1&userId=undefined&firstVisit=0&fp=6a12ee8f2da67eb1716a8eaa90b4c1f8", "start":0},
            ]

        self.listsTab(CAT_TAB, cItem)

    def exploreItem(self, cItem):
        printDBG(f"Ronemocom.exploreItem [cItem] {cItem}")
        url = cItem['url']
        start = cItem.get("start", 0)

        sts, data = self.getPage(f"{url}&start={start}", addParams=self.defaultParams)
        if not sts: return

        jsData = json.loads(data)

        resultList=[]
        try:
            resultList = jsData["results"]
        except Exception:
            resultList = jsData["data"]

        for result in resultList:
            printDBG(f"Ronemocom.exploreItem [data] {result}")

            uuid = result["uuid"]
            title = result["name"]
            duration = result["duration"]

            desc = f"{_('Duration')}: {str(timedelta(milliseconds=duration)).split('.', maxsplit=1)[0]}"

            thumb_file = result["thumb_file"]

            icon = f"https://thumb.ronemo.com/{uuid}/{thumb_file}s.jpg"

            #link = "https://ronemo.com/video/%s" % uuid

            params = dict(cItem)
            params.update({'good_for_fav': True, 'title': title, 'icon': icon, 'uuid':uuid, 'desc':desc})
            self.addVideo(params)

        start += len(resultList)
        params = dict(cItem)
        params.update({"start": start, "title": _("Next page")})
        self.addNext(params)

    def getCustomLinksForVideo(self, cItem):
        printDBG(f"Ronemocom.getCustomLinksForVideo [cItem]: {cItem}")
        uuid = cItem['uuid']

        video_info_url = f"https://ronemo.com/api/hls/{uuid}/f/playlist.m3u8"
        #sts, data = self.getPage(video_info_url, addParams=self.defaultParams)
        #if not sts: return

        #video_link = json.loads(data).get('link', '')
        video_info_url = strwithmeta(video_info_url, {'iptv_proto':'m3u8', 'User-Agent': self.USER_AGENT, 'Referer':'https://ronemo.com/', 'Origin':'https://ronemo.com'})

        return getDirectM3U8Playlist(video_info_url,variantCheck=False, sortWithMaxBitrate=99999999)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"Ronemocom.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        cItem = dict(cItem)
        cItem.update({'category': 'exploreItem', 'start': 0, 'url': f"https://ronemo.com/api/video/search?key={quote(searchPattern)}&scrollId="})
        self.exploreItem(cItem)

    def isProtectedByPinCode(self):
        return config.plugins.iptvplayer.Ronemocom_pin.value

    def withArticleContent(self, cItem):
        if 'video' == cItem.get('type', '') or 'exploreItem' == cItem.get('category', ''):
            return True
        return False

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)
