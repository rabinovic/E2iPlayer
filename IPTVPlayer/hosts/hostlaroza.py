# -*- coding: utf-8 -*-

import re
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, handleServiceDecorator
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc

def gettytul():
    return 'Loaroza'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__({'history': 'laroza.one'})

        self.MAIN_URL = 'https://z.laroza.one'
        self.DEFAULT_ICON = self.MAIN_URL + '/uploads/custom-logo.png'


    def getPage(self, baseUrl, addParams=None, post_data=None):
        baseUrl = ph.std_url(baseUrl)
        if addParams is None:
            addParams = dict(self.defaultParams)
        return self.cm.getPage(baseUrl, addParams, post_data)

    def mainMenu(self, cItem):
        MAIN_CAT_TAB = [
            {'category': 'show_movies', 'title':'افلام عربية', 'url': self.MAIN_URL + '/category.php?cat=arabic-movies10', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_movies', 'title':'افلام اجنبية', 'url': self.MAIN_URL + '/category.php?cat=english-movies2', 'icon':self.DEFAULT_ICON_URL},

            {'category': 'show_series', 'title': 'مسلسلات عربية', 'url': self.MAIN_URL + '/category.php?cat=arabic-series22', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'مسلسلات أجنبية', 'url': self.MAIN_URL + '/category.php?cat=english-series3', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2023', 'url': self.MAIN_URL + '/category.php?cat=ramadan-2023', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2022', 'url': self.MAIN_URL + '/category.php?cat=22ramadan-2022', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2021', 'url': self.MAIN_URL + '/category.php?cat=22ramadan-2021', 'icon':self.DEFAULT_ICON_URL},

            {'category': 'search', 'title': _('Search'), 'search_item': True, 'icon': self.DEFAULT_ICON_URL },
            {'category': 'search_history', 'title': _('Search history')     , 'icon': self.DEFAULT_ICON_URL }
        ]

        self.listsTab(MAIN_CAT_TAB, cItem)

    def show_movies(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        if result := re.findall('itemprop="itemListElement".*?itemprop="url" content="([^"]+?)".*?itemprop="name" content="([^"]+?)".*?itemprop="thumbnailUrl" content="([^"]+?)".*?</li>', data, re.S):
            for e in result:
                link = e[0]
                title = e[1]
                icon = ph.std_url(e[2])
                self.addVideo({'good_for_fav':True, 'title': title, 'url':link, 'icon':icon, 'desc': '', 'with_mini_cover':True})

        if nextPage := re.search('<a href="([^"]+?)">&raquo;</a>', data, re.S):
            self.addNext(cItem|{'url':self.MAIN_URL+'/'+ nextPage[1]})

    def show_series(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        if result := re.findall('itemprop="itemListElement".*?itemprop="url" content="([^"]+?)".*?itemprop="name" content="([^"]+?)".*?itemprop="thumbnailUrl" content="([^"]+?)".*?</li>', data, re.S):
            for e in result:
                link = e[0]
                title = e[1]
                icon = ph.std_url(e[2])
                self.addDir({'good_for_fav':True, 'category':'show_seasons', 'title': title, 'url':link, 'icon':icon, 'desc': '', 'with_mini_cover':True})

        if nextPage := re.search('<a href="([^"]+?)">&raquo;</a>', data, re.S):
            self.addNext(cItem|{'url':self.MAIN_URL+'/'+ nextPage[1]})

    def show_seasons(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        self.addVideo(cItem)

        self.addMarker({'title':'الموسم'})

        sts, html = self.getPage(url)
        if not sts:
            return

        data0 = re.findall('<div class="SeasonsEpisodes" style="display:none;" data-serie="([^"]+?)">(.*?)</div>', html, re.S)
        for e in data0:
            season= 'الموسم ' + e[0]
            eps = e[1]
            self.addDir( cItem|{'title': season, 'eps':eps, 'category':'show_episodes'})

    def show_episodes(self, cItem):
        cItem = dict(cItem)
        eps = cItem['eps']
        data = re.findall('<a class="" href="([^"]+?)" title=.*?><em>(.*?)</em><span>(.*?)</span>', eps, re.S)
        for e in data:
            link = e[0]
            name = e[1]+e[2]
            self.addVideo(cItem | {'title': name, 'url':link})

    def getCustomLinksForVideo(self,cItem):
        urlTab = []

        url = cItem['url'].replace('video', 'play')
        if url.startswith('./'):
            url = self.MAIN_URL + url
        sts, data = self.getPage(url)
        if not sts:
            return

        data0 = re.search('<div id="WatchServers".*?</div>', data, re.S)[0]
        data1 = re.findall("<button .*?data-embed='([^']+?)'", data0, re.S)
        for e in data1:
            urlTab.append({'name': self.up.getHostName(e), 'url': e, 'need_resolve':1 })
        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        urlTab = []
        urlTab = self.up.getVideoLinkExt(videoUrl)
        return urlTab


    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("ShoofLive.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)

        url = cItem.get('url', None)
        url = url if url else f'{self.MAIN_URL}/search.php?keywords={ph.std_url(searchPattern)}'

        sts, data = self.getPage(url)
        if not sts:
            return

        if result := re.findall('itemprop="itemListElement".*?itemprop="url" content="([^"]+?)".*?itemprop="name" content="([^"]+?)".*?itemprop="thumbnailUrl" content="([^"]+?)".*?</li>', data, re.S):
            for e in result:
                link = e[0]
                title = e[1]
                icon = ph.std_url(e[2])
                self.addVideo({'good_for_fav':True, 'title': title, 'url':link, 'icon':icon, 'desc': '', 'with_mini_cover':True})

        if nextPage := re.search('<a href="([^"]+?)">&raquo;</a>', data, re.S):
            self.addNext(cItem|{'url':self.MAIN_URL+'/'+ nextPage[1]})

    @handleServiceDecorator
    def handleService(self, index, refresh=0, searchPattern='', searchType=''):

        name =   self.currItem.get("name", '')
        category = self.currItem.get("category", '')

        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))

        # MAIN MENU
        if name is None and category == '':
            self.mainMenu({'name': 'category'})
        elif category == 'show_movies':
            self.show_movies(self.currItem)
        elif category == 'show_series':
            self.show_series(self.currItem)
        elif category == 'show_seasons':
            self.show_seasons(self.currItem)
        elif category == 'show_episodes':
            self.show_episodes(self.currItem)
        #SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
        #HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)
