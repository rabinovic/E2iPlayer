# -*- coding: utf-8 -*-

import re
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent

def gettytul():
    return 'AlfajerTv'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__({'history': 'alfajrtv.co', 'cookie': 'alfajrtv.cookie'})

        self.MAIN_URL = 'https://fajer.show'
        self.DEFAULT_ICON = self.MAIN_URL + '/wp-content/uploads/2019/09/fslogo.png'

        self.USER_AGENT = getDefaultUserAgent()
        self.HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html'}
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True,
                              'cookiefile': self.COOKIE_FILE, 'with_metadata': True}

    def getPage(self, baseUrl, addParams=None, post_data=None):
        baseUrl = ph.std_url(baseUrl)
        if addParams is None:
            addParams = dict(self.defaultParams)
        return self.cm.getPage(baseUrl, addParams, post_data)

    def mainMenu(self, cItem):
        MAIN_CAT_TAB = [
            {'category': 'show_movies', 'title':'افلام عربية', 'url': self.MAIN_URL + '/genre/arabic-movies/', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_movies', 'title':'افلام اجنبية', 'url': self.MAIN_URL + '/genre/english-movies/', 'icon':self.DEFAULT_ICON_URL},

            {'category': 'show_series', 'title': 'مسلسلات عربية', 'url': self.MAIN_URL + '/genre/arabic-series/', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'مسلسلات أجنبية', 'url': self.MAIN_URL + '/genre/english-series/', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2024', 'url': self.MAIN_URL + '/genre/ramadan2024', 'icon':self.DEFAULT_ICON_URL},
            {'category': 'show_series', 'title': 'رمضان 2023', 'url': self.MAIN_URL + '/genre/ramadan2023', 'icon':self.DEFAULT_ICON_URL},

            {'category': 'search', 'title': _('Search'), 'search_item': True, 'icon': self.DEFAULT_ICON_URL },
            {'category': 'search_history', 'title': _('Search history')     , 'icon': self.DEFAULT_ICON_URL }
        ]

        self.listsTab(MAIN_CAT_TAB, cItem)

    def show_movies(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        result = re.findall('<article.*?>(.*?)</article>', data, re.S)

        for e in result:
            if '<div class="poster">' in e:
                res_ = re.search('<img src="([^"]+?)".*?<h3><a href="([^"]+?)">(.*?)</a></h3>', e, re.S)
                icon = ph.std_url(res_[1])
                link = res_[2]
                title = res_[3]
                self.addVideo(cItem | {'good_for_fav':True, 'title': title, 'url':link, 'icon':icon , 'with_mini_cover':True})

        if nextPage := re.search('''class="pagination".*class="current">.*?<a href='([^']+?)'.*?class="inactive">''', data, re.S):
                self.addNext(cItem| {'url': nextPage[1]})

    def show_series(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']

        sts, data = self.getPage(url)
        if not sts:
            return

        result = re.findall('<article.*?>(.*?)</article>', data, re.S)

        for e in result:
            if '<div class="poster">' in e:
                res_ = re.search('<img src="([^"]+?)".*?<h3><a href="([^"]+?)">(.*?)</a></h3>', e, re.S)
                icon = ph.std_url(res_[1])
                link = res_[2]
                title = res_[3]
                self.addDir({'good_for_fav':True, 'title': title, 'category':'show_seasons', 'url':link,'icon':icon, 'with_mini_cover':True})

        if nextPage := re.search('''class=['"]pagination['"].*class=['"]current['"]>.*?<a href=['"]([^'"]+?)['"].*?class=['"]inactive['"]>''', data, re.S):
            if nextPage[1] not in url:
                self.addNext(cItem| {'url': nextPage[1]})

    def show_seasons(self, cItem):
        cItem = dict(cItem)

        url = cItem['url']
        sts, html = self.getPage(url)
        if not sts:
            return

        if data0:=re.search('''<div id=['"]seasons['"](.*?)class=['"]sbox['"]''', html, re.S):
            print(data0[1])
            if data1 := re.findall('''<span class=['"]title['"]>(.*?)<i>(.*?)</ul></div>''', data0[1]):
                if len(data1) > 1:
                    for e in data1:
                        season = e[0]
                        eps = e[1]
                        self.addDir(cItem | {'title': season, 'category':'show_eps', 'eps': eps})

                else:
                    self.show_eps(cItem | {'eps': data1[0][1]})

    def show_eps(self, cItem):
        cItem = dict(cItem)

        eps = cItem['eps']

        if data0 := re.findall('''<li.*?>.*?<a href=['"]([^'"]+?)['"].*?<img src=['"]([^'"]+?)['"].*?class=['"]numerando['"]>(.*?)</div>.*?</li>''', eps, re.S):
            for elem in data0:
                link = elem[0]
                icon = ph.std_url(elem[1])
                title = elem[2]
                self.addVideo(cItem | {'title': title, 'url':link, 'icon': icon})


    def getCustomLinksForVideo(self,cItem):
        urlTab = []

        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return

        if data1 := re.findall('player-option.*?data-type="([^"]+?)" data-post="([^"]+?)" data-nume="([^"]+?)".*?vid_title">(.*?)</p>', data, re.S):
            for e in data1:
                type = e[0]
                post = e[1]
                num = e[2]
                name = e[3]

                url = self.up.decorateUrl(url, {'type':type, 'post':post, 'num':num})
                urlTab.append({'name': name, 'url': url, 'need_resolve':1 })
        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        urlTab = []

        type = videoUrl.meta['type']
        post =videoUrl.meta['post']
        num = videoUrl.meta['num']

        data = {
                "action": "doo_player_ajax",
                "post": post,
                "nume": num,
                "type": type
                }
        sts, data = self.getPage(self.MAIN_URL +'/wp-admin/admin-ajax.php', post_data=data)
        if not sts:
            return

        videoUrl = re.search("src='([^']+?)'", data, re.S)
        urlTab = self.up.getVideoLinkExt(videoUrl[1])
        return urlTab


    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("ShoofLive.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)

        url = cItem.get('url', None)
        url = url if url else f'{self.MAIN_URL}/?s={ph.std_url(searchPattern)}'

        sts, data = self.getPage(url)
        if not sts:
            return

        if result := re.findall('class="result-item">.*?<a href="([^"]+?)".*?<img src="([^"]+?)" alt="([^"]+?)".*?class="(?:tvshows|movies)">(.*?)<', data, re.S):
            for e in result:
                link = e[0]
                icon = ph.std_url(e[1])
                title = e[2]
                if "فيلم"  in e[3]:
                    self.addVideo({'good_for_fav':True, 'title': title, 'url':link, 'icon':icon, 'with_mini_cover':True})
                else:
                    self.addDir({'good_for_fav':True, 'title': title, 'category':'show_seasons', 'url':link,'icon':icon, 'with_mini_cover':True})

        if navigation:=re.search('<div class="navigation".*?</div>', data, re.S):
            if nextPage := re.search('(?:.<a href="([^"]+?)").*&laquo;</a>', navigation[0], re.S):
                self.addNext(cItem| {'url': nextPage[1]})


    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)
