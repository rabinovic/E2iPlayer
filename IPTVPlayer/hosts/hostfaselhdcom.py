# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs.pCommon import isValidUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, MergeDicts
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import getDirectM3U8Playlist
from Plugins.Extensions.IPTVPlayer.libs import ph
###################################################

###################################################
# FOREIGN import
###################################################
import urllib.parse
import re
import urllib.request
import urllib.parse
import urllib.error
###################################################


def gettytul():
    return 'https://faselhd.co/'


class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'faselhd.com', 'cookie': 'faselhd.com.cookie'})
        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'
        self.MAIN_URL = 'https://www.faselhd.co/'
        self.DEFAULT_ICON_URL = self.getFullUrl('https://i2.wp.com/www.faselhd.com/wp-content/themes/adbreak/images/logo.png')
        self.HTTP_HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html', 'Accept-Encoding': 'gzip, deflate', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}
        self.AJAX_HEADER = dict(self.HTTP_HEADER)
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest', 'Accept-Encoding': 'gzip, deflate', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Accept': 'application/json, text/javascript, */*; q=0.01'})

        self.cacheLinks = {}
        self.defaultParams = {'header': self.HTTP_HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        self.MAIN_CAT_TAB = [
                             {'category': 'search', 'title': _('Search'), 'search_item': True},
                             {'category': 'search_history', 'title': _('Search history')},
                            ]

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)
        baseUrl = self.cm.iriToUri(baseUrl)
        addParams['cloudflare_params'] = {'cookie_file': self.COOKIE_FILE, 'User-Agent': self.USER_AGENT}
        sts, data = self.cm.getPageCFProtection(baseUrl, addParams, post_data)
        if sts and 'http-equiv="refresh"' in data:
            url = ph.find(data, ('<meta', '>', 'http-equiv="refresh"'))[1]
            url = self.getFullUrl(ph.getattr(url.replace(';', ' '), 'URL'), self.cm.meta['url'])
            return self.cm.getPageCFProtection(url, addParams, post_data)
        return sts, data

    def getFullIconUrl(self, url):
        url = HostBase.getFullIconUrl(self, url)
        if url != '' and self.up.getDomain(url) in self.getMainUrl():
            url = 'https://i2.wp.com/' + url.split('://', 1)[-1]
        return url

    def mainMenu(self, cItem):
        printDBG("FaselhdCOM.listMainMenu")
        sts, data = self.getPage(self.getMainUrl())
        if sts:
            data = ph.getDataBetweenNodes(data, ('<div', '>', 'class="menu'), ('</div', '>'))[1]
            data = re.compile('(<li[^>]*?>|</li>|<ul[^>]*?>|</ul>)').split(data)
            if len(data) > 1:
                try:
                    cTree = self.listToDir(data[1:-1], 0)[0]
                    params = dict(cItem)
                    params['c_tree'] = cTree['list'][0]
                    params['category'] = 'listCategories'
                    self.listCategories(params)
                except Exception:
                    printExc()

        self.listsTab(self.MAIN_CAT_TAB, cItem)

    def listCategories(self, cItem):
        printDBG("FaselhdCOM.listCategories")
        try:
            cTree = cItem['c_tree']
            for item in cTree['list']:
                title = ph.cleanHtml(ph.getDataBetweenMarkers(item['dat'], '<a', '</a>')[1])
                url = self.getFullUrl(ph.getSearchGroups(item['dat'], '''href=['"]([^'^"]+?)['"]''')[0])
                if '/promo' in url:
                    return
                if 'list' not in item:
                    if isValidUrl(url) and title != '':
                        params = dict(cItem)
                        params.update({'good_for_fav': False, 'category': 'listItems', 'title': title, 'url': url})
                        self.addDir(params)
                elif len(item['list']) == 1 and title != '':
                    params = dict(cItem)
                    params.update({'good_for_fav': False, 'c_tree': item['list'][0], 'title': title, 'url': url})
                    self.addDir(params)
        except Exception:
            printExc()

    def listItems(self, cItem):
        printDBG("FaselhdCOM.listItems [%s]" % cItem)
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        baseData = data

        nextPage = ph.getDataBetweenNodes(data, ('<div', '>', 'pagination'), ('</div', '>'))[1]
        nextPage = self.getFullUrl(ph.getSearchGroups(nextPage, '''<a[^>]+?href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        data = ph.getAllItemsBetweenNodes(data, ('<div', '>', 'one-movie'), ('</a', '>'))
        printDBG(data)
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
            icon = self.getFullIconUrl(ph.getSearchGroups(item, '''[\s\-]src=['"]([^'^"]+?)['"]''')[0])
            title = ph.cleanHtml(ph.getDataBetweenMarkers(item, '<h1', '</h1>')[1])
            if title == '':
                title = ph.cleanHtml(ph.getSearchGroups(item, '''alt=['"]([^'^"]+?)['"]''')[0])
            if title == '':
                continue

            desc = []
            tmp = ph.getDataBetweenNodes(item, ('<div', '>', 'movie-meta'), ('</div', '>'))[1]
            tmp = ph.getAllItemsBetweenMarkers(tmp, '<span', '</span>')
            for t in tmp:
                label = ''
                if 'fa-star' in t:
                    label = _('Rating:')
                elif 'fa-eye' in t:
                    label = _('Views:')
                t = ph.cleanHtml(t)
                if t != '':
                    if label != '':
                        desc.append('%s %s' % (label, t))
                    else:
                        desc.append(t)

            if '/seasons/' in self.cm.meta['url'] and not cItem.get('sub_view'):
                title = '%s - %s' % (cItem['title'], title)
                self.addDir(MergeDicts(cItem, {'url': url, 'title': title, 'sub_view': True}))
            else:
                params = dict(cItem)
                params.update({'good_for_fav': True, 'title': title, 'url': url, 'icon': icon, 'desc': '[/br]'.join(desc)})
                if cItem.get('f_list_episodes'):
                    self.addVideo(params)
                else:
                    params['category'] = 'explore_item'
                    self.addDir(params)

        if not cItem.get('sub_view'):
            data = ph.findall(baseData, ('<span', '>', 'sub-view'), '</span>')
            for item in data:
                if 'display:none' in item:
                    continue
                url = self.getFullUrl(ph.getattr(item, 'href'), self.cm.meta['url'])
                title = '%s - %s' % (cItem['title'], ph.cleanHtml(item))
                self.addDir(MergeDicts(cItem, {'url': url, 'title': title, 'sub_view': True}))

        if isValidUrl(nextPage):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItem(self, cItem, nextCategory):
        printDBG("FaselhdCOM.exploreItem")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        url = ph.getDataBetweenNodes(data, ('<meta', '>', 'refresh'), ('<', '>'))[1]
        url = self.getFullUrl(ph.getSearchGroups(url, '''url=['"]([^'^"]+?)['"]''', 1, True)[0])

        if isValidUrl(url):
            sts, tmp = self.getPage(url)
            if sts:
                data = tmp

        tmp = ph.getDataBetweenNodes(data, ('<div', '>', 'movie-btns'), ('</div', '>'))[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, '<a', '</a>')
        for item in tmp:
            if 'youtube-play' not in item:
                continue
            title = ph.cleanHtml(item)
            url = self.getFullUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''', 1, True)[0])
            if isValidUrl(url):
                params = dict(cItem)
                params.update({'good_for_fav': False, 'url': url, 'title': '%s - %s' % (cItem['title'], title)})
                self.addVideo(params)

        tmp = ph.getDataBetweenNodes(data, ('<div', '>', 'movies-servers'), ('<div', '>', 'container'))[1]
        if tmp != '':
            params = dict(cItem)
            self.addVideo(params)
        else:
            cItem = dict(cItem)
            cItem.update({'category': nextCategory, 'page': 1, 'f_list_episodes': True})
            if isValidUrl(url):
                cItem['url'] = url
            self.listItems(cItem)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("FaselhdCOM.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)
        if 1 == cItem.get('page', 1):
            cItem['category'] = 'listItems'
            cItem['url'] = self.getFullUrl('/?s=') + urllib.parse.quote_plus(searchPattern)
        self.listItems(cItem)

    def getCustomLinksForVideo(self, cItem):
        printDBG("FaselhdCOM.getCustomLinksForVideo [%s]" % cItem)

        if 1 == self.up.checkHostSupport(cItem.get('url', '')):
            videoUrl = cItem['url'].replace('youtu.be/', 'youtube.com/watch?v=')
            return self.up.getVideoLinkExt(videoUrl)

        retTab = []
        dwnTab = []

        cacheKey = cItem['url']
        cacheTab = self.cacheLinks.get(cacheKey, [])
        if len(cacheTab):
            return cacheTab

        self.cacheLinks = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        dwnLink = ph.getDataBetweenNodes(data, ('<a', '>', 'download_direct_link'), ('</a', '>'))[1]
        dwnLink = ph.getSearchGroups(dwnLink, '''href=['"](https?://[^"^']+?)['"]''')[0]

        data = ph.getDataBetweenNodes(data, ('<div', '>', 'movies-servers'), ('<script', '>'))[1]

        tmp = ph.getDataBetweenMarkers(data, '<ul', '</ul>')[1]
        tmp = ph.getAllItemsBetweenMarkers(tmp, '<li', '</li>')
        for item in tmp:
            marker = ph.getSearchGroups(item, '''href=['"]#([^"^']+?)['"]''')[0]
            name = ph.cleanHtml(item)
            dat = ph.getDataBetweenNodes(data, ('<div', '>', marker), ('<iframe', '>'))[1]
            url = self.getFullUrl(ph.getSearchGroups(dat, '''<iframe[^>]+?src=['"]([^"^']+?)['"]''', 1, True)[0])
            if url == '':
                url = ph.getSearchGroups(item, '''href\s*?=\s*?['"]([^'^"]+?)['"]''')[0]
                tmp = url.split('embed.php?url=', 1)
                if 2 == len(tmp):
                    url = urllib.parse.unquote(tmp[-1])
            retTab.append({'name': name, 'url': self.getFullUrl(url), 'need_resolve': 1})

        if isValidUrl(dwnLink):
            sts, data = self.getPage(dwnLink)
            if not sts:
                return
            data = ph.getDataBetweenNodes(data, ('<div', '>', 'other_servers'), ('</div', '>'))[1]
            data = ph.getAllItemsBetweenMarkers(data, '<a', '</a>')
            for item in data:
                url = ph.getSearchGroups(item, '''href=['"](https?://[^"^']+?)['"]''')[0]
                if 1 != self.up.checkHostSupport(url):
                    continue
                name = ph.cleanHtml(item)
                dwnTab.append({'name': name, 'url': url, 'need_resolve': 1})

        retTab.extend(dwnTab)
        if len(retTab):
            self.cacheLinks[cacheKey] = retTab
        return retTab

    def getCustomVideoLinks(self, baseUrl):
        printDBG("FaselhdCOM.getCustomVideoLinks [%s]" % baseUrl)
        videoUrl = strwithmeta(baseUrl)
        urlTab = []

        # mark requested link as used one
        if len(list(self.cacheLinks.keys())):
            for key in self.cacheLinks:
                for idx in range(len(self.cacheLinks[key])):
                    if videoUrl in self.cacheLinks[key][idx]['url']:
                        if not self.cacheLinks[key][idx]['name'].startswith('*'):
                            self.cacheLinks[key][idx]['name'] = '*' + self.cacheLinks[key][idx]['name'] + '*'
                        break

        urlTab = self.up.getVideoLinkExt(videoUrl)
        if 0 == len(urlTab):
            sts, data = self.getPage(videoUrl)
            if not sts:
                return []

            hlsUrl = ph.getSearchGroups(data, '''["'](https?://[^'^"]+?\.m3u8(?:\?[^"^']+?)?)["']''', ignoreCase=True)[0]
            printDBG("hlsUrl||||||||||||||||| " + hlsUrl)
            if hlsUrl != '':
                hlsUrl = strwithmeta(hlsUrl, {'User-Agent': self.defaultParams['header']['User-Agent'], 'Referer': baseUrl})
                urlTab = getDirectM3U8Playlist(hlsUrl, checkContent=True, sortWithMaxBitrate=999999999)

            if 0 == len(urlTab):
                data = ph.getDataBetweenMarkers(data, '.setup(', ')')[1]
                videoUrl = ph.getSearchGroups(data, '''['"]?file['"]?\s*:\s*['"](https?://[^'^"]+?)['"]''')[0]
                if isValidUrl(videoUrl):
                    videoUrl = strwithmeta(videoUrl, {'User-Agent': self.defaultParams['header']['User-Agent'], 'Referer': baseUrl})
                    urlTab.append({'name': 'direct', 'url': videoUrl})

        return urlTab

    def getCustomArticleContent(self, cItem, data=None):
        printDBG("FaselhdCOM.getCustomArticleContent [%s]" % cItem)

        retTab = []

        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return []

        url = ph.getDataBetweenNodes(data, ('<meta', '>', 'refresh'), ('<', '>'))[1]
        url = self.getFullUrl(ph.getSearchGroups(url, '''url=['"]([^'^"]+?)['"]''', 1, True)[0])

        if isValidUrl(url):
            sts, tmp = self.getPage(url)
            if sts:
                data = tmp

        data = ph.getDataBetweenNodes(data, ('<header', '>'), ('<style', '>'))[1]
        desc = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<p', '</p>')[1])
        title = ph.cleanHtml(ph.getDataBetweenMarkers(data, '<h1', '</h1>')[1])
        icon = self.getFullIconUrl(ph.getSearchGroups(data, '''\ssrc=['"]([^'^"]+?)['"]''')[0])

        keysMap = {'دولة المسلسل': 'country',
                   'حالة المسلسل': 'status',
                   'اللغة': 'language',
                   'توقيت الحلقات': 'duration',
                   'الموسم': 'seasons',
                   'الحلقات': 'episodes',

                   'تصنيف الفيلم': 'genres',
                   'مستوى المشاهدة': 'age_limit',
                   'سنة الإنتاج': 'year',
                   'مدة الفيلم': 'duration',
                   'تقييم IMDB': 'imdb_rating',
                   'بطولة': 'actors',
                   'جودة الفيلم': 'quality'}
        data = ph.getAllItemsBetweenNodes(data, ('<i', '>', 'fa-'), ('</span', '>'))
        printDBG(data)
        for item in data:
            tmp = ph.cleanHtml(item).split(':')
            marker = tmp[0].strip()
            value = tmp[-1].strip().replace(' , ', ', ')

            printDBG(">>>>>>>>>>>>>>>>>> marker[%s] -> value[%s]" % (marker, value))

            #marker = ph.getSearchGroups(item, '''(\sfa\-[^'^"]+?)['"]''')[0].split('fa-')[-1]
            #printDBG(">>>>>>>>>>>>>>>>>> " + marker)
            if marker not in keysMap:
                continue
            if value == '':
                continue
            otherInfo[keysMap[marker]] = value

        if title == '':
            title = cItem['title']
        if desc == '':
            desc = cItem.get('desc', '')
        if icon == '':
            icon = cItem.get('icon', self.DEFAULT_ICON_URL)

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc), 'images': [{'title': '', 'url': self.getFullUrl(icon)}], 'other_info': otherInfo}]


    def withArticleContent(self, cItem):
        return cItem.get('good_for_fav', False)

