# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, DisplayItem, DisplayItemType, handleServiceDecorator
from Plugins.Extensions.IPTVPlayer.libs.pCommon import isValidUrl
from Plugins.Extensions.IPTVPlayer.libs import ph

from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
###################################################

###################################################
# FOREIGN import
###################################################
import urllib.request
import urllib.parse
import urllib.error
try:
    import json
except Exception:
    import simplejson as json

import re
###################################################


def gettytul():
    return 'http://movizland.com/'


class IPTVHost(HostBase):
    HEADER = {'User-Agent': 'Mozilla/5.0', 'Accept': 'text/html'}
    MAIN_URL = 'http://new.movizland.cyou'
    SEARCH_URL = MAIN_URL + '?s='
    DEFAULT_ICON_URL = "http://new.movizland.cyou/movizland/images/logo.png"

    MAIN_CAT_TAB = [
                    {'category': 'categories', 'title': 'الأفلام', 'url': MAIN_URL },
                    {'category': 'categories', 'title': 'المسلسلات', 'url': MAIN_URL},
                    {'category': 'search', 'title': _('Search'), 'search_item': True, },
                    {'category': 'search_history', 'title': _('Search history'), }]

    def __init__(self):
        super().__init__( {'history': '  MovizlandCom.tv', 'cookie': 'movizlandcom.cookie'}, True, [DisplayItemType.VIDEO, DisplayItemType.AUDIO])
        self.USER_AGENT = 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/37.0.2062.120 Chrome/37.0.2062.120 Safari/537.36'
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Accept': 'text/html'}
        self.defaultParams = {'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, params={}, post_data=None):
        params['cloudflare_params'] = {'domain': 'm.movizland.com', 'cookie_file': self.COOKIE_FILE, 'User-Agent': self.USER_AGENT}
        return self.cm.getPageCFProtection(baseUrl, params, post_data)

    def listCategories(self, cItem, nextCategory):
        printDBG("MovizlandCom.listCategories")

        sts, data = self.getPage(cItem['url'], self.defaultParams)
        if not sts:
            return

        dropdown = re.search(f'''<li class="NonClick" data-hold="{cItem['title']}".*?<li class="NonClick"''', data, re.S)[0]
        cats = re.findall('<li><a href="[^"]+?".*?</a>', dropdown, re.S)

        for item in cats:
            f = re.search('href="([^"]+?").*</i>(.*?)</a>', item, re.S)
            url =f[1]
            title = f[2]
            params = dict(cItem)
            params.update({'category': nextCategory, 'title': title, 'url': url})
            self.addDir(params)

    def listItems(self, cItem):
        printDBG("MovizlandCom.listItems")

        url = cItem['url']

        sts, data = self.getPage(url, self.defaultParams)
        if not sts:
            return

        nextpage = re.search('<a href="([^"]+?)">الصفحة التالية &laquo;</a>', data, re.S)
        if nextpage:
            param = dict(cItem)
            param.update({'url':nextpage[1]})
            self.addNext(param)

        items = re.findall('div class="BlockItem"(.*?)<div class="BlockTitle"', data, re.S)
        for item in items:
            res = re.search('href="([^"]+?)".*?<img.*?src="([^"]+?)".*?alt="([^"]+?)"', item, re.S)
            if not res:
                continue

            url = res[1]
            icon = res[2]
            title = res[3]

            if 'EPSNumber' in item:
                param = dict(cItem)
                param.update({'category':'list_episodes', 'title': title, 'url': url, 'icon':ph.std_url(icon), 'with_mini_cover':True})
                self.addDir(param)
            else:
                self.addVideo({'good_for_fav':True, 'title': title, 'url': url, 'icon':ph.std_url(icon)})

    def list_episodes(self, cItem):

        self.addVideo(dict(cItem))

        sts, data = self.getPage(cItem['url'], self.defaultParams)
        if not sts:
            return

        episodes = re.findall('<div class="EpisodeItem">.*?href="([^"]+?)".*?<span>(.*?)</span>.*?<em>(.*?)</em>', data, re.S)
        if episodes:
            self.addMarker({'title': 'Episodes'})
            for episode in episodes:
                param = dict(cItem)
                param.update({'url':episode[0], 'title':f'{episode[1]} {episode[2]}'})
                self.addVideo(param)

    def getCustomLinksForVideo(self, cItem):
        printDBG("MovizlandCom.getCustomLinksForVideo [%s]" % cItem)
        urlTab = []

        sts, data = self.getPage(cItem['url'], self.defaultParams)
        if not sts:
            return []

        servers_section = re.search('div class="ServersEmbeds".*?<div class="EmbedCode"', data, re.S)

        servers = re.findall('<li.*?data-server.*?>(.*?)<.*?data-srcout="([^"]+?)".*?</code', servers_section[0], re.S)

        for server, url in servers:
            url = url.replace('-', '')
            urlTab.append({'name': server, 'url': url, 'need_resolve': 1})
        return urlTab

    def getCustomVideoLinks(self, url):
        printDBG("MovizlandCom.getCustomVideoLinks [%s]" % url)
        urlTab = []
        if url.startswith('http'):
            urlTab = self.up.getVideoLinkExt(url)
        return urlTab

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("MovizlandCom.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        cItem = dict(cItem)
        cItem['url'] = self.SEARCH_URL + urllib.parse.quote_plus(searchPattern)
        self.listItems(cItem)

    def getFavouriteData(self, cItem):
        printDBG("MovizlandCom.getFavouriteData")
        return str(cItem['url'])

    def getCustomLinksForFavourite(self, fav_data):
        printDBG("MovizlandCom.getCustomLinksForFavourite")
        return self.getCustomLinksForVideo({'url': fav_data})

    def getCustomArticleContent(self, cItem):
        printDBG("MovizlandCom.getCustomArticleContent [%s]" % cItem)

        retTab = []

        otherInfo = {}

        url = cItem['url'].replace('://m.', '://')
        sts, data = self.getPage(url)
        if not sts:
            return []
        cUrl = data.meta['url']

        desc = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<div', '>', 'contentMovie'), ('</div', '>'))[1])
        tmp = ph.getDataBetweenNodes(data, ('<div', '>', 'poster-movie'), ('</div', '>'))[1]
        icon = self.getFullIconUrl(ph.getSearchGroups(tmp, '''<img[^>]+?src=['"]([^"^']+?)['"]''')[0])
        title = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<em', '>', 'befores'), ('</em', '>'))[1])

        itemsList = []

        tmp = ph.cleanHtml(ph.getDataBetweenNodes(data, ('<span', '>', 'ratings'), ('</span', '>'), False)[1])
        if tmp != '':
            itemsList.append((_('Rating:'), tmp))

        data = ph.getAllItemsBetweenMarkers(data, '<btns', '</btns>')
        for item in data:
            item = item.split('</span>', 1)
            if len(item) < 2:
                continue
            key = ph.cleanHtml(item[0])
            if key == '':
                cotninue
            val = []
            item = ph.getAllItemsBetweenMarkers(item[1], '<a', '</a>')
            for it in item:
                it = ph.cleanHtml(it)
                if it:
                    val.append(it)

            if len(val):
                itemsList.append((key, ', '.join(val)))

        if title == '':
            title = cItem['title']
        if icon == '':
            icon = cItem.get('icon', self.DEFAULT_ICON_URL)
        if desc == '':
            desc = cItem.get('desc', '')

        return [{'title': ph.cleanHtml(title), 'text': ph.cleanHtml(desc), 'images': [{'title': '', 'url': self.getFullUrl(icon)}], 'other_info': {'custom_items_list': itemsList}}]

    @handleServiceDecorator
    def handleService(self, index, refresh=0, searchPattern='', searchType='', selItem=None):

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')

        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))


        if name is None:
            self.listsTab(self.MAIN_CAT_TAB, {'name': 'category'})
        elif category == 'categories':
                self.listCategories(self.currItem, 'list_items')
        elif category == 'list_items':
                self.listItems(self.currItem)
        elif category == 'list_episodes':
            self.list_episodes(self.currItem)
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video':
            return False
        return True
