# -*- coding: utf-8 -*-

import re

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
from Plugins.Extensions.IPTVPlayer.libs import ph

from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc

def gettytul():
    return 'ShoofVod'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__({'history': 'shoofvod.com'})

        self.DEFAULT_ICON = 'http://shoofvod.com/images/logo.png'
        self.MAIN_URL = 'http://shoofvod.com/'

        self.menu_cache={}

    def mainMenu(self, cItem):

        sts, data = self.cm.getPage(self.MAIN_URL, self.defaultParams)
        if not sts:
            return

        main_menu = re.search('''class=" top-menu"(.*?)</div><!-- /top-menu -->''', data,  re.S)[1]
        menu_items = re.findall('''<div class="col-sm-3 col-xs-6 col-xxs-12".*?class="title"><a href="([^"]+?)">(.*?)</a>(.*?)/col-xs-2''', main_menu, re.S)
        for menu_item in menu_items:
            url = menu_item[0]
            title = menu_item[1]
            sub_menus = re.findall('''<a href="([^"]+?)">(.*?)</a>''', menu_item[2], re.S)
            self.menu_cache[title] = [(url,'الكل')]
            self.menu_cache[title].extend([x for x in sub_menus])
            self.addDir({'title':title, 'category':'list_sub_menu'})
        self.addDir({'category':'search','title': _('Search'), 'search_item':True})
        self.addDir({'category': 'search_history', 'title': _('Search history')})


    def list_sub_menu(self, cItem):
        title = cItem['title']
        for elm in self.menu_cache[title]:
            self.addDir({'title':elm[1], 'url':elm[0], 'category':'listItems'})


    def listItems(self, cItem):
        printDBG("listItems citem [%s]" % cItem)

        url = self.getFullUrl(cItem['url'])

        sts, data = self.cm.getPage(url, self.defaultParams)
        if not sts:
            return

        items = re.findall('''<div class="col-md-3 col-sm-4 col-xs-4 col-xxs-6 item">.*?<a href="([^"]+?)".*?img src="([^"]+?)".*?<h4>(.*?)</h4>.*?</div>''', data, re.S)
        for item in items:
            url = self.getFullUrl(item[0])
            title = item[2].replace("مشاهدة","").replace("مترجمة","").replace("مترجم","").replace("فيلم","").replace("مسلسل","")
            if 'vidpage_' in url:
                self.addVideo({'title':title, 'url':url, 'icon':ph.std_url(item[1])})
            else:
                param = dict(cItem)
                param.update({'title':title, 'url':url, 'icon':ph.std_url(item[1]), 'with_mini_cover':True})
                self.addDir(param)

        nextPage = re.search('<li class="">.*?<a href="([^"]+?)">التالي</a>', data, re.S)
        if nextPage:
            self.addNext(cItem | {'url':self.getFullUrl(nextPage[1])})

    def getCustomLinksForVideo(self, cItem):
        printDBG("getCustomLinksForVideo cItem [%s]" % cItem)
        urlTab = []

        url = cItem['url']
        url = url.replace('vidpage_', 'Play/')

        sts, data = self.cm.getPage(url, self.defaultParams)
        if not sts:
            return

        url = re.search('iframe src="([^"]+?)"', data, re.S)[1]
        if url.startswith('//'):
           url = 'http:' + url

        sts, data = self.cm.getPage(url, self.defaultParams)
        if not sts:
            return

        url = re.search('<source src="([^"]+?)"', data, re.S)[1]
        if url.startswith('//'):
           url = 'http:' + url

        urlTab.append({'name': 'video', 'url': url, 'need_resolve': 0})

        return urlTab


    def getCustomArticleContent(self, cItem):
        return super().getCustomArticleContent(cItem)

    def getCustomVideoLinks(self, url):
        return super().getCustomVideoLinks(url)

    def listSearchResult(self, cItem, searchPattern, searchType):
        return super().listSearchResult(cItem, searchPattern, searchType)