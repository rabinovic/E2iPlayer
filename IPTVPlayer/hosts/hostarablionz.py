# -*- coding: utf-8 -*-

import re

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase, handleServiceDecorator
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultUserAgent
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.libs import ph


def gettytul():
    return 'ArabLionz'

class IPTVHost(HostBase):

    def __init__(self):
        super().__init__( {'history': 'arblionz', 'cookie': 'arblionz.cookie'})
        self.MAIN_URL = 'https://arlionztv.store' # alternativ https://arlionz.one
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/861LmCL/Sans-titre.png'
        self.USER_AGENT = getDefaultUserAgent()
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Connection': 'keep-alive', 'Accept-Encoding':'gzip', 'Content-Type':'application/x-www-form-urlencoded','Referer':self.getMainUrl(), 'Origin':self.getMainUrl()}
        self.defaultParams = {'header':self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams=None, post_data = None):
        baseUrl=ph.std_url(baseUrl)
        if addParams is None: addParams = dict(self.defaultParams)
        sts, data = self.cm.getPageCFProtection(baseUrl, addParams, post_data)
        userAgent = self.defaultParams.get('header', {}).get('User-Agent', '')
        if data.meta.get('cf_user', self.USER_AGENT) != userAgent:
            self.defaultParams['header']['User-Agent'] = data.meta['cf_user']
        return sts, data

    def listMainMenu(self, cItem):
        printDBG(f"arblionz.listMainMenu cItem [{cItem}]")

        MAIN_CAT_TAB = [
                        {'category':'showmenu1', 'sub_mode':0, 'title': 'افلام', 'icon':self.DEFAULT_ICON_URL},
                        {'category':'showmenu1', 'sub_mode':2, 'title': 'مسلسلات', 'icon':self.DEFAULT_ICON_URL},
                        {'category':'showmenu1', 'sub_mode':4, 'title':'انمي و كارتون','url':self.MAIN_URL+'/category/مصارعه/', 'icon':self.DEFAULT_ICON_URL},
                        {'category':'showmenu1','sub_mode':6, 'title': 'عروض اخري', 'count':1,'data':'none','code':'', 'icon':self.DEFAULT_ICON_URL},
                        {'category':'showitms','sub_mode':2, 'title': 'رمضان 2022', 'url':self.MAIN_URL+'/category/series/arabic-series/%d9%85%d8%b3%d9%84%d8%b3%d9%84%d8%a7%d8%aa-%d8%b1%d9%85%d8%b6%d8%a7%d9%86-2022/', 'icon':self.DEFAULT_ICON_URL},
                        {'category':'search','title': _('Search'), 'search_item':True,'page':1, 'icon':self.DEFAULT_ICON_URL},
                    ]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def showmenu1(self, cItem):
        printDBG(f"arblionz.showmenu1 cItem [{cItem}]")
        gnr = cItem['sub_mode']
        sts, data = self.getPage(self.MAIN_URL+'/eg/')
        if sts:
            data0 = re.findall('(?:ChildsCats">|enresCats">)(.*?)</ul', data, re.S)
            if data0:
                #self.addMarker({'title':'اقسام فرعية', 'icon':cItem['icon']})
                data2 = re.findall('<li.*?href="(.*?)".*?>(.*?)</li>', data0[gnr], re.S)
                for elm in data2:
                    url = elm[0]
                    title = ph.cleanHtml(elm[1])
                    if not url.startswith('http'):
                        url=self.MAIN_URL+url
                    self.addDir({'category': 'showitms', 'url': url, 'title':title, 'desc':'', 'icon':cItem['icon']})
                #self.addMarker({'title':'حسب النوع', 'icon':cItem['icon']})
                data2 = re.findall('<li.*?href="(.*?)".*?>(.*?)</li>', data0[gnr+1], re.S)
                for elm in data2:
                    url = elm[0]
                    title = ph.cleanHtml(elm[1])
                    if not url.startswith('http'):
                        url=self.MAIN_URL+url
                    self.addDir({'category': 'showitms', 'url': url, 'title':title, 'desc':'', 'icon':cItem['icon'], 'with_mini_cover':True})

    def showitms(self, cItem):
        printDBG(f"arblionz.showitems cItem [{cItem}]")

        url = cItem['url']

        if url.startswith('http') or (url.startswith('/')):
            sts, data = self.getPage(url)
            if not sts:
                return
            data1=re.findall('Posts--Single--Box">.*?href="(.*?)".*?title="(.*?)".*?image="(.*?)"', data, re.S)
            if data1:
                for elm in data1:
                    url = self.getFullUrl(elm[0])
                    desc, title = ph.uniform_title(elm[1])
                    image = ph.std_url(elm[2])
                    self.addDir(cItem | {'category':'showelms', 'good_for_fav':True, 'title': title,'url':url, 'icon':image,'desc': desc, 'with_mini_cover':True})

            next_ = re.search('<li><a href="([^"]+?)">&raquo;</a></li>', data, re.S)
            if next_:
                self.addNext(cItem | {'url':next_[1], 'title':_('Next page'), 'desc':'Next page'})

    def showelms(self,cItem):
        printDBG(f"arblionz.showelms cItem [{cItem}]")
        cItem = dict(cItem)
        url = cItem['url']
        self.addVideo({'good_for_fav':True, 'title': cItem['title'],'url':url, 'icon':cItem['icon']})

        sts, data = self.getPage(cItem['url'])
        if sts:
            data1=re.findall('class="JsutNumber.*?href="(.*?)".*?>(.*?)</a>', data, re.S)
            if data1:
                #self.addMarker({'title': _('Episodes'),'icon':cItem['icon']})
                for elm in data1:
                    url   = self.getFullUrl(elm[0])
                    desc, title = ph.uniform_title(elm[1])
                    self.addVideo({'good_for_fav':True, 'title': title,'url':url,'icon':cItem['icon'], 'desc': desc})
            next_ = re.search('<li><a href="([^"]+?)">&raquo;</a></li>', data, re.S)
            if next_:
                self.addNext(cItem | { 'url':next_[1], 'title':_('Next page'), 'desc':'Next page'})

    def getCustomLinksForVideo(self, cItem):
        printDBG(f"arblionz.getCustomLinksForVideo cItem [{cItem}]")
        urlTab = []
        url=cItem['url']
        sts, data = self.getPage(url)
        if sts:
            Liste_els = re.search('"watch".*?data-id="(.*?)"', data, re.S)
            if Liste_els:
                URL = self.MAIN_URL + '/PostServersWatch/'+Liste_els[1]
                params = dict(self.defaultParams)
                params['header']['X-Requested-With'] = 'XMLHttpRequest'
                sts, data = self.getPage(URL,params)
                if sts:
                    Liste_els = re.findall('<li.*?data-i="(.*?)".*?data-id="(.*?)".*?<em>(.*?)<', data, re.S)
                    for (i,id_,titre_) in Liste_els:
                        URL = ''+i+'|||'+id_
                        urlTab.append({'name':titre_, 'url':URL, 'need_resolve':1})
        return urlTab

    def getCustomVideoLinks(self, videoUrl):
        printDBG(f"arblionz.getCustomVideoLinks videoUrl [{videoUrl}]")
        urlTab = []
        i,id_ = videoUrl.split('|||')
        URL = self.MAIN_URL + '/Embedder/'+id_+'/'+i
        params = dict(self.defaultParams)
        params['header']['X-Requested-With'] = 'XMLHttpRequest'
        sts, data = self.getPage(URL,params,{})
        if sts:
            Liste_els = re.findall('<iframe.*?src=["\'](.*?)["\']', data, re.IGNORECASE)
            if Liste_els:
                URL = Liste_els[0]
                urlTab = self.up.getVideoLinkExt(URL)
        return urlTab


    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"arblionz.listSearchResult  cItem [{cItem}], searchPattern [{searchPattern}], searchType [{searchType}]")

        url = cItem.get('url', f'{self.MAIN_URL}/search/{searchPattern}/page/1/')

        sts, data = self.cm.getPage(ph.std_url(url))
        if sts:
            data0 = re.findall('Posts--Single--Box">.*?href="(.*?)".*?title="(.*?)".*?image="(.*?)"',  data, re.S)
            if data0:
                for elm in data0:
                    link = elm[0]
                    desc, title = ph.uniform_title(elm[1])
                    icon = ph.std_url(elm[2])
                    self.addDir({'category': 'showelms', 'title': title, 'url': link, 'icon': icon, 'good_for_fav': True, 'desc':desc})
            next_ = re.search('<li><a href="([^"]+?)">&raquo;</a></li>', data, re.S)
            if next_:
                self.addNext(cItem | {'category': 'search_next_page', 'url':next_[1], 'title':_('Next page'), 'desc':'Next page'})

    def getCustomArticleContent(self, cItem):
        printDBG(f"arblionz.getCustomVideoLinks cItem [{cItem}]")
        otherInfo1 = {}
        title = cItem['title']
        icon = cItem.get('icon', '')
        desc = cItem.get('desc', '')
        sts, data = self.cm.getPage(ph.std_url(cItem['url']))
        if sts:
            lst_dat = re.findall('class="font-size-16 text-white mt-2">(.*?)</span>', data, re.S)
            for elm in lst_dat:
                elm = ph.cleanHtml(elm)
                if (':' in elm) and ('مدة' in elm):
                    otherInfo1['duration'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('اللغة' in elm):
                    otherInfo1['language'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('ترجمة' in elm):
                    otherInfo1['translation'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('جودة' in elm):
                    otherInfo1['quality'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('انتاج' in elm):
                    otherInfo1['production'] = elm.split(':', 1)[1].strip()
                elif (':' in elm) and ('سنة' in elm):
                    otherInfo1['year'] = elm.split(':', 1)[1].strip()

            lst_dat0 = re.findall('class="font-size-16 d-flex align-items-center mt-3">(.*?)</div>', data, re.S)
            if lst_dat0:
                otherInfo1['genre'] = ph.cleanHtml(lst_dat0[0])

            lst_dat0 = re.findall('header-link text-white">.*?<h2>(.*?)</div>', data, re.S)
            if lst_dat0:
                desc = ph.cleanHtml(lst_dat0[0])

        return [{'title': title, 'text': desc, 'images': [{'title': '', 'url': icon}], 'other_info': otherInfo1}]

    @handleServiceDecorator
    def handleService(self, index, refresh=0, searchPattern='', searchType=''):

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')

        printDBG(f"handleService: |||| name[{name}], category[{category}]")

        self.cacheLinks = {}

        # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category'})
        elif category == 'showmenu1':
            self.showmenu1(self.currItem)
        elif category == 'showfilter':
            self.showfilter(self.currItem)
        elif category == 'showitms':
            self.showitms(self.currItem)
        elif category == 'showelms':
            self.showelms(self.currItem)
        # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
        # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

    def withArticleContent(self, cItem):
        if cItem.get('type', 'video') != 'video' and cItem.get('category', 'unk') != 'showitms':
            return False
        return True
