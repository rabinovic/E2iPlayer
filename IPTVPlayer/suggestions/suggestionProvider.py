# -*- coding: utf-8 -*-

from abc import ABC, abstractmethod

class SuggestionProvider(ABC):

    @abstractmethod
    def getName(self) -> str:
        """Name of suggestionProvider"""

    @abstractmethod
    def getSuggestions(self, text, locale) -> list:
        """List of suggestions"""
