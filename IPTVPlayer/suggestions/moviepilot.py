# -*- coding: utf-8 -*-
#
import urllib.request
import urllib.parse
import urllib.error

from Plugins.Extensions.IPTVPlayer.suggestions.suggestionProvider import SuggestionProvider
try:
    import json
except Exception:
    import simplejson as json

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.pCommon import common


class MoviepilotSuggestionsProvider(SuggestionProvider):

    def __init__(self):
        self.cm = common()

    def getName(self):
        return _("Moviepilot Suggestions")

    def getSuggestions(self, text, locale):
        url = 'https://www.moviepilot.de/api/search?type=suggest&v=2&q=' + urllib.parse.quote(text)
        sts, data = self.cm.getPage(url)
        if sts:
            retList = []
            for item in json.loads(data):
                if 'title' in item:
                    retList.append(item['title'])
            return retList
        return None
