# -*- coding: utf-8 -*-

import base64
import gzip
import json
import random
import re
try:
    import ssl
except:
    pass

import time
import urllib.error
import urllib.request

from http.cookiejar import MozillaCookieJar, Cookie
from io import BytesIO
from urllib.parse import urljoin, urlparse, urlunparse, urlencode, quote_plus, unquote as parse_unquote

from Components.config import config, ConfigText, configfile

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import ( IPTVPlayerSleep, IPTVPlayerNotificationList)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.e2ijs import js_execute_ext
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (
    GetDefaultLang, GetJSScriptFile, IsExecutable,
    IsHttpsCertValidationEnabled, iptv_system, printDBG, printExc)
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def DecodeGzipped(data):
    buf = BytesIO(data)
    f = gzip.GzipFile(fileobj=buf)
    return f.read()


def EncodeGzipped(data):
    f = BytesIO()
    gzf = gzip.GzipFile(mode="wb", fileobj=f, compresslevel=1)
    gzf.write(data)
    gzf.close()
    encoded = f.getvalue()
    f.close()
    return encoded


class NoRedirection(urllib.request.HTTPRedirectHandler):
    def http_error_302(self, req, fp, code, msg, headers):
        infourl = urllib.request.addinfourl(fp, headers, req.get_full_url())
        infourl.code = code
        return infourl
    http_error_300 = http_error_302
    http_error_301 = http_error_302
    http_error_303 = http_error_302
    http_error_307 = http_error_302


class MultipartPostHandler(urllib.request.BaseHandler):
    handler_order = urllib.request.HTTPHandler.handler_order - 10

    def http_request(self, request):
        data = request.get_data()
        if data is not None and not isinstance(data, str):
            content_type, data = self.encode_multipart_formdata(data)
            request.add_unredirected_header('Content-Type', content_type)
            request.add_data(data)
        return request

    def encode_multipart_formdata(self, fields):
        LIMIT = '-----------------------------14312495924498'
        CRLF = '\r\n'
        L = []
        for (key, value) in fields:
            L.append('--' + LIMIT)
            L.append('Content-Disposition: form-data; name="%s"' % key)
            L.append('')
            L.append(value)
        L.append('--' + LIMIT + '--')
        L.append('')
        body = CRLF.join(L)
        content_type = 'multipart/form-data; boundary=%s' % LIMIT
        return content_type, body

    https_request = http_request

user_agents = {
            'firefox': [
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/119.0'
            ],
            'iphone_3_0': ['Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16',],
            'chrome': [
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36',
            ]
        }

def getDefaultUserAgent(browser='chrome'):
    return random.choice(user_agents.get(browser, user_agents[random.choice(list(user_agents.keys()))] ))

def getDefaultHeader(browser='chrome'):
    return {'User-Agent': getDefaultUserAgent(browser),
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate',
                'DNT': 1
            }

def getBaseUrl(url, domainOnly=False):
    uri = urlparse(url)
    if domainOnly:
        domain = f'{uri.netloc}'
    else:
        domain = f'{uri.scheme}://{uri.netloc}/'
    return domain

def build_http_query(query):
    def _process(query, data, key_prefix):
        if isinstance(data, dict):
            for key, value in data.items():
                key = '%s[%s]' % (key_prefix, key) if key_prefix else key
                _process(query, value, key)
        elif isinstance(data, list):
            for idx, val in enumerate(data):
                _process(query, val, f'{key_prefix}[{idx}]')
        else:
            query.append((key_prefix, data))
    _query = []
    _process(_query, query, '')
    return _query

def get_params_from_url_with_meta(url, baseHeaderOutParams=None):
    from Plugins.Extensions.IPTVPlayer.iptvdm.iptvdh import DMHelper
    HANDLED_HTTP_HEADER_PARAMS = DMHelper.HANDLED_HTTP_HEADER_PARAMS #['Host', 'User-Agent', 'Referer', 'Cookie', 'Accept',  'Range']
    outParams = {}
    tmpParams = {}
    postData = None
    if isinstance(url, strwithmeta):
        if None is not baseHeaderOutParams:
            tmpParams['header'] = baseHeaderOutParams
        else:
            tmpParams['header'] = {}
        for key in url.meta:
            if key in HANDLED_HTTP_HEADER_PARAMS:
                tmpParams['header'][key] = url.meta[key]
        if 0 < len(tmpParams['header']):
            outParams = tmpParams
        if 'iptv_proxy_gateway' in url.meta:
            outParams['proxy_gateway'] = url.meta['iptv_proxy_gateway']
        if 'iptv_http_proxy' in url.meta:
            outParams['http_proxy'] = url.meta['iptv_http_proxy']
    return outParams, postData

def report_http_error(pType, url, msg):
    domain = getBaseUrl(url, True)
    messages = []
    messages.append(_('HTTPS connection error "%s"\n') % msg)
    messages.append(_('It looks like your current configuration do not allow to connect to the https://%s/.\n') % domain)
    if pType == 'verify' and IsHttpsCertValidationEnabled():
        messages.append(_('You can disable HTTPS certificates validation in the E2iPlayer configuration to suppress this problem.'))
    IPTVPlayerNotificationList().push('\n'.join(messages), 'error', 40, pType + domain, 40)

def clearCookie(cookiefile, leaveNames:list=None, removeNames=None, ignoreDiscard=True, ignoreExpires=False):
    leaveNames=[] if leaveNames is None else leaveNames
    try:
        toRemove = []
        cj = MozillaCookieJar()
        cj.load(cookiefile, ignore_discard=ignoreDiscard)
        for cookie in cj:
            if cookie.name not in leaveNames and (None is removeNames or cookie.name in removeNames):
                toRemove.append(cookie)
        for cookie in toRemove:
            cj.clear(cookie.domain, cookie.path, cookie.name)
        cj.save(cookiefile, ignore_discard=ignoreDiscard)
    except Exception:
        printExc()
        return False
    return True

def url_encode_non_ascii(b:bytes):
    return re.sub(b'[\x80-\xFF]', lambda c: '%%%02x' % ord(c.group(0)), b)

def isValidUrl(url:str):
    return url.startswith('http://') or url.startswith('https://')

class common:

    HEADER = None

    def __init__(self, proxyURL='', useProxy=False, useMozillaCookieJar=True):
        self.proxyURL = proxyURL
        self.useProxy = useProxy
        self.geolocation = {}
        self.meta = {} # metadata from previus request

        self.HOST = getDefaultUserAgent()

        self.curlSession = None
        if not useMozillaCookieJar:
            raise Exception("You should stop use parameter useMozillaCookieJar it change nothing, because from only MozillaCookieJar can be used")

    @staticmethod
    def getFullUrl(url:str, mainUrl:str='http://fake/'):
        if not url:
            return ''
        if url.startswith('./'):
            url = url[1:]

        currUrl = mainUrl
        mainUrl = getBaseUrl(currUrl)

        if url.startswith('//'):
            proto = mainUrl.split('://', 1)[0]
            url = proto + ':' + url
        elif url.startswith('://'):
            proto = mainUrl.split('://', 1)[0]
            url = proto + url
        elif url.startswith('/'):
            url = mainUrl + url[1:]
        elif 0 < len(url) and '://' not in url:
            if currUrl == mainUrl:
                url = mainUrl + url
            else:
                url = urljoin(currUrl, url)
        return url

    def getCountryCode(self, lower=True):
        if 'countryCode' not in self.geolocation:
            sts, data = self.getPage('http://ip-api.com/json')
            if sts:
                try:
                    self.geolocation['countryCode'] = json.loads(data)['countryCode']
                except Exception:
                    printExc()
        return self.geolocation.get('countryCode', '').lower()

    @staticmethod
    def getCookieItem(cookiefile, item):
        cookiesDict = common.getCookieItems(cookiefile)
        return cookiesDict.get(item, '')

    @staticmethod
    def getCookie(cookiefile, ignoreDiscard=True, ignoreExpires=False):
        cj = None
        try:
            cj = MozillaCookieJar()
            cj.load(cookiefile, ignore_discard=ignoreDiscard)
        except Exception:
            printExc()
        return cj

    @staticmethod
    def getCookieItems(cookiefile, ignoreDiscard=True, ignoreExpires=False):
        cookiesDict = {}
        try:
            cj = common.getCookie(cookiefile, ignoreDiscard, ignoreExpires)
            for cookie in cj:
                cookiesDict[cookie.name] = cookie.value
        except Exception:
            printExc()
        return cookiesDict

    @staticmethod
    def addCookieItem(cookiefile, cookieDict, ignoreDiscard=True, ignoreExpires=False):
        printDBG(f"pCommon.addCookieItem {json.dumps(cookieDict)} to file '{cookiefile}'")

        cj = None
        # create a Cookie object from cookieDict
        cookieName = cookieDict.get('name', '')
        cookieValue = cookieDict.get('value', '')
        cookiePort = cookieDict.get('port', None)
        domainParts = cookieDict.get('domain', '').split('/')
        while len(domainParts) and ('http' in domainParts[0] or len(domainParts[0]) == 0):
            del(domainParts[0])

        cookiePath = cookieDict.get('path', '/')
        cookieExpires = cookieDict.get('expires', None)

        if cookiePort is None:
            cookiePortSpecified = False
        else:
            cookiePortSpecified = True
        if not domainParts:
            cookieDomain = ''
            cookieDomainSpecified = False
            cookieDomainDot = False
        else:
            cookieDomain = domainParts[0]
            cookieDomainSpecified = True
            if cookieDomain.startswith("."):
                cookieDomainDot = True
            else:
                cookieDomainDot = False

        if not (cookieName and cookieValue):
            printDBG("cookie not valid : %s " % json.dumps(cookieDict))
            return

        try:
            c = Cookie(
                version=0,
                name=cookieName,
                value=cookieValue,
                port=cookiePort,
                port_specified=cookiePortSpecified,
                domain=cookieDomain,
                domain_specified=cookieDomainSpecified,
                domain_initial_dot=cookieDomainDot,
                path=cookiePath,
                path_specified=True,
                secure=False,
                expires=cookieExpires,
                discard=True,
                comment=None,
                comment_url=None,
                rest={'HttpOnly': None},
                rfc2109=False)
        except:
            printExc()
            return

        try:
            # load cookies from cookiefile
            cj = MozillaCookieJar()
            cj.load(cookiefile, ignore_discard=ignoreDiscard)

            #add new cookie
            cj.set_cookie(c)

            # save in cookiefile
            cj.save(cookiefile, ignore_discard=ignoreDiscard)
        except Exception:
            printExc()

    @staticmethod
    def getCookieHeader(cookiefile, allowedNames:list=None, unquote=True, ignoreDiscard=True, ignoreExpires=False):

        allowedNames=[] if allowedNames is None else allowedNames
        ret = ''
        try:
            cookiesDict = common.getCookieItems(cookiefile, ignoreDiscard, ignoreExpires)
            for key, value in cookiesDict.items():
                if allowedNames and key not in allowedNames:
                    continue
                if unquote:
                    value = parse_unquote(value)
                ret += f'{key}={value};'
        except Exception:
            printExc()
        return ret

    @staticmethod
    def fillHeaderItems(metadata, responseHeaders:dict, camelCase=False, collectAllHeaders=False):
        returnKeys = ['content-type', 'content-disposition', 'content-length', 'location']
        if camelCase:
            sourceKeys = ['Content-Type', 'Content-Disposition', 'Content-Length', 'Location']
        else:
            sourceKeys = returnKeys
        for idx, returnKey in enumerate(returnKeys):
            if sourceKeys[idx] in responseHeaders:
                metadata[returnKey] = responseHeaders[sourceKeys[idx]]
                #printDBG(sourceKeys[idx] + " ---->  " + responseHeaders[sourceKeys[idx]])

        #printDBG(str(responseHeaders))

        if collectAllHeaders:
            if "Access-Control-Allow-Headers" in responseHeaders:
                acah = responseHeaders["Access-Control-Allow-Headers"]
                acah_keys = acah.split(',')

                for key in acah_keys:
                    key = key.strip()
                    if key in responseHeaders:
                        metadata[key.lower()] = responseHeaders[key]
                        #printDBG(key + " ---->  " + responseHeaders[key])

            for header, value in responseHeaders.items():
                metadata[header.lower()] = value
                printDBG(header + " ---->  " + value)

    def handleCharset(self, params, data, metadata):
        def strDecode(text,  setErrors = 'strict'):
            if isinstance(text, str):
                retVal = text
            else:
                try:
                    retVal = text.decode(encoding='utf-8', errors=setErrors)
                except Exception:
                    retVal = text.decode(encoding='utf-8', errors='ignore')
            return retVal

        try:
            if params.get('return_data', False) and params.get('convert_charset', True):
                encoding = ''
                if 'content-type' in metadata:
                    encoding = ph.getSearchGroups(metadata['content-type'], '''charset=([A-Za-z0-9\-]+)''', 1, True)[0].strip().upper()

                if encoding == '' and params.get('search_charset', False):
                    encoding = ph.getSearchGroups(strDecode(data,'ignore'), '''(<meta[^>]+?Content-Type[^>]+?>)''', ignoreCase=True)[0]
                    encoding = ph.getSearchGroups(encoding, '''charset=([A-Za-z0-9\-]+)''', 1, True)[0].strip().upper()
                if encoding not in ['', 'UTF-8']:
                    printDBG(">> encoding[%s]" % encoding)
                    try:
                        data = data.decode(encoding)
                    except Exception:
                        printExc()
                    metadata['orig_charset'] = encoding
                else:
                    try:
                        data = strDecode(data)
                    except Exception:
                        data = strDecode(data, 'ignore')
        except Exception:
            printExc()
        return data, metadata

    def getPage(self, url, addParams:dict=None, post_data=None):
        ''' wraps getURLRequestData '''

        addParams= {} if addParams is None else addParams

        try:
            addParams['url'] = url
            if 'return_data' not in addParams:
                addParams['return_data'] = True
            response = self.getURLRequestData(addParams, post_data)
            status = True
        except urllib.error.HTTPError as e:
            try:
                printExc()
                if e.code == 308:
                    return self.getPage(e.fp.info().get('Location', ''), addParams, post_data)
                status = False
                response = e
                if addParams.get('return_data', False):
                    self.meta = {}
                    metadata = self.meta
                    metadata['url'] = e.fp.geturl()
                    metadata['status_code'] = e.code
                    self.fillHeaderItems(metadata, e.fp.info(), True, collectAllHeaders=addParams.get('collect_all_headers'))

                    data = e.fp.read(addParams.get('max_data_size', -1))
                    if e.fp.info().get('Content-Encoding', '') == 'gzip':
                        data = DecodeGzipped(data)

                    data, metadata = self.handleCharset(addParams, data, metadata)
                    response = strwithmeta(data, metadata)
                    e.fp.close()
            except Exception:
                printExc()
        except urllib.error.URLError as e:
            printExc()
            errorMsg = str(e)
            if 'ssl_protocol' not in addParams and 'TLSV1_ALERT_PROTOCOL_VERSION' in errorMsg:
                try:
                    newParams = dict(addParams)
                    newParams['ssl_protocol'] = 'TLSv1_2'
                    return self.getPage(url, newParams, post_data)
                except Exception:
                    pass
            if 'VERSION' in errorMsg:
                self.reportHttpsError('version', url, errorMsg)
            elif 'VERIFY_FAILED' in errorMsg:
                self.reportHttpsError('verify', url, errorMsg)
            elif 'SSL' in errorMsg or 'unknown url type: https' in errorMsg: #GET_SERVER_HELLO
                self.reportHttpsError('other', url, errorMsg)

            response = None
            status = False

        except Exception:
            printExc()
            response = None
            status = False

        # if addParams['return_data'] and status and not isinstance(response, str):
        #     status = False

        return (status, response)

    def getPageCFProtection(self, baseUrl, params={}, post_data=None):

        cf_user = params.get('header', {}).get('User-Agent', '')
        header = {'Referer': baseUrl, 'User-Agent': cf_user, 'Accept-Encoding': 'text'}
        header.update(params.get('header', {}))
        params.update({'with_metadata': True, 'use_cookie': True, 'save_cookie': True, 'load_cookie': True, 'cookiefile': params.get('cookiefile', ''), 'header': header})
        params.update({'CFProtection': True})
        start_time = time.time()
        sts, data = self.getPage(baseUrl, params, post_data)

        if not sts and None is not data:
            from Plugins.Extensions.IPTVPlayer.libs.recaptcha_mye2i import UnCaptchaReCaptcha
            recaptcha = UnCaptchaReCaptcha(lang=GetDefaultLang())
            token = recaptcha.processCaptcha(start_time, baseUrl, captchaType='CF')
            if token != '':
                #printDBG('token: [%s]' % token)
                r = json.loads(base64.b64decode(token))
                printDBG('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
                printDBG(r)
                printDBG('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<')
                if cf_user != r.get('user_agent', ''):
                    cf_user = r.get('user_agent', '')
                    config.plugins.iptvplayer.cloudflare_user = ConfigText(default="", fixed_size=False)
                    config.plugins.iptvplayer.cloudflare_user.value = cf_user
                    config.plugins.iptvplayer.cloudflare_user.save()
                    configfile.save()
                params['header']['User-Agent'] = cf_user

                if cookies := r['cookie']:
                    params['cookie_items'] = {'cf_clearance': cookies['value']}
                sts, data = self.getPage(baseUrl, params, post_data)
                if not sts:
                    printDBG('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
                    printDBG(data)
                    printDBG('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<')

        data = strwithmeta(data, {'cf_user': cf_user})

        return sts, data


    @staticmethod
    def convertWebp(file_path, png=False):
        printDBG("PCommon.convertWebp %s" % file_path)

        if IsExecutable('ffmpeg'):
            png_path = file_path.replace('.webp', '.png')
            output_path = file_path.replace('.webp', '.jpg')
            if png:
                command = f"ffmpeg -i {file_path} {png_path} && test -e {png_path} && rm {file_path} && mv {png_path} {output_path}"
            else:
                command = f"ffmpeg -i {file_path} {output_path} && test -e {output_path} && rm {file_path} "

            printDBG("Send command %s" % command)
            iptv_system(command)
            time.sleep(1)

    def saveWebFile(self, file_path, url, addParams={}, post_data=None):
        addParams = dict(addParams)

        outParams, postData = get_params_from_url_with_meta(url)
        addParams.update(outParams)
        if 'header' not in addParams and 'host' not in addParams:
            host = getDefaultUserAgent()
            header = {'User-Agent': host, 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
            addParams['header'] = header
        addParams['return_data'] = False

        bRet = False
        downDataSize = 0
        dictRet = {}
        try:
            if '"url":' in url:
                url = re.search('"url":"([^"]+?)"', url, re.S)[1]
            sts, downHandler = self.getPage(url, addParams, post_data)

            if addParams.get('ignore_content_length', False):
                meta = downHandler.info()
                contentLength = int(meta.getheaders("Content-Length")[0])
            else:
                contentLength = None

            checkFromFirstBytes = addParams.get('check_first_bytes', [])
            OK = True
            if 'maintype' in addParams and addParams['maintype'] != downHandler.headers.get_content_maintype():
                printDBG("common.getFile wrong maintype! requested[%r], retrieved[%r]" % (addParams['maintype'], downHandler.headers.maintype))
                if not checkFromFirstBytes:
                    downHandler.close()
                OK = False

            if OK and 'subtypes' in addParams:
                OK = False
                for item in addParams['subtypes']:
                    if item == downHandler.headers.get_content_subtype():
                        OK = True
                        break

            if OK or checkFromFirstBytes:
                blockSize = addParams.get('block_size', 8192)
                fileHandler = None
                while True:
                    buffer = downHandler.read(blockSize)
                    if checkFromFirstBytes:
                        OK = False
                        for item in checkFromFirstBytes:
                            bitem = ph.ensure_bytes(item)
                            if buffer.startswith(bitem):
                                # change extension of file
                                if bitem in [b'\xFF\xD8', b'\xFF\xD9']:
                                    printDBG("SaveWebFile. It's a jpeg")
                                elif bitem == b'\x89\x50\x4E\x47':
                                    printDBG("SaveWebFile. It's a png")
                                    #file_path = file_path.replace('.jpg','.png')
                                elif bitem in [b'GIF87a', b'GIF89a']:
                                    printDBG("SaveWebFile. It's a gif")
                                    #file_path = file_path.replace('.jpg','.gif')
                                elif bitem == b'RI':
                                    printDBG("SaveWebFile. It's a webp")
                                    file_path = file_path.replace('.jpg', '.webp')
                                OK = True
                                break
                        if not OK:
                            break
                        else:
                            checkFromFirstBytes = []

                    if not buffer:
                        break
                    downDataSize += len(buffer)
                    if len(buffer):
                        if fileHandler is None:
                            fileHandler = open(file_path, "wb")
                        fileHandler.write(buffer)
                if fileHandler is not None:
                    fileHandler.close()
                downHandler.close()
                if None is not contentLength:
                    if contentLength == downDataSize:
                        bRet = True
                elif downDataSize > 0:
                    bRet = True

                # decode webp to jpeg
                if file_path.endswith(".webp"):
                    if addParams.get('webp_convert_to_png', False):
                        common.convertWebp(file_path, png=True)
                    else:
                        common.convertWebp(file_path)
        except Exception:
            printExc("common.getFile download file exception")
        dictRet.update({'sts': bRet, 'fsize': downDataSize})
        return dictRet

    @staticmethod
    def getUrllibSSLProtocolVersion(protocolName):
        if not isinstance(protocolName, str):
            IPTVPlayerNotificationList().push('getUrllibSSLProtocolVersion error.', 'error', 40)
            return protocolName
        if protocolName == 'TLSv1_2':
            return ssl.PROTOCOL_TLSv1_2
        elif protocolName == 'TLSv1_1':
            return ssl.PROTOCOL_TLSv1_1
        return None

    def getURLRequestData(self, params:dict={}, post_data=None):

        def urlOpen(req, customOpeners, timeout):
            if len(customOpeners) > 0:
                opener = urllib.request.build_opener(*customOpeners)
                if timeout is not None:
                    response = opener.open(req, timeout=timeout)
                else:
                    response = opener.open(req, timeout=10)
            else:
                if timeout is not None:
                    response = urllib.request.urlopen(req, timeout=timeout)
                else:
                    response = urllib.request.urlopen(req, timeout=10)
            return response

        # if ThreadIdUtil.is_main_thread():
        #     msg1 = _('It is not allowed to call getURLRequestData from main thread.')
        #     msg2 = _('You should never perform block I/O operations in the __init__.')
        #     IPTVPlayerNotificationList().push('\s'.join([msg1, msg2]), 'error', 40)
        #     raise Exception("Wrong usage!")

        if 'max_data_size' in params and not params.get('return_data', False):
            raise Exception("return_data == False is not accepted with max_data_size.\nPlease also note that return_data == False is deprecated!")

        cj = MozillaCookieJar()
        response = None
        req = None
        out_data = None
        self.meta = {}
        metadata = self.meta

        timeout = params.get('timeout', None)

        printDBG("params: %s" % params)
        printDBG("self.HOST:%s" % self.HOST)
        printDBG("self.HEADER:%s" % self.HEADER)


        host = params.get('host', self.HOST)
        headers = params.get('header', {'User-Agent': host} if self.HEADER is None else self.HEADER)

        if 'User-Agent' not in headers:
            headers['User-Agent'] = host

        printDBG('pCommon - getURLRequestData() -> params: ' + str(params))
        printDBG('pCommon - getURLRequestData() -> headers: ' + str(headers))

        customOpeners = []
        #cookie support
        if 'use_cookie' not in params and 'cookiefile' in params and ('load_cookie' in params or 'save_cookie' in params):
            params['use_cookie'] = True

        if params.get('use_cookie', False):
            if params.get('load_cookie', False):
                try:
                    cj.load(params['cookiefile'], ignore_discard=True)
                except IOError:
                    printDBG('Cookie file [%s] not exists' % params['cookiefile'])
                except Exception:
                    printExc()
            try:
                for cookieKey in list(params.get('cookie_items', {}).keys()):
                    printDBG("cookie_item[%s=%s]" % (cookieKey, params['cookie_items'][cookieKey]))
                    cookieItem = Cookie(version=0, name=cookieKey, value=params['cookie_items'][cookieKey], port=None, port_specified=False, domain='', domain_specified=False, domain_initial_dot=False, path='/', path_specified=True, secure=False, expires=None, discard=True, comment=None, comment_url=None, rest={'HttpOnly': None}, rfc2109=False)
                    cj.set_cookie(cookieItem)
            except Exception:
                printExc()
            customOpeners.append(urllib.request.HTTPCookieProcessor(cj))

        if params.get('no_redirection', False):
            customOpeners.append(NoRedirection())

        if None is not params.get('ssl_protocol', None):
            sslProtoVer = common.getUrllibSSLProtocolVersion(params['ssl_protocol'])
        else:
            sslProtoVer = None
        # debug
        #customOpeners.append(urllib2.HTTPSHandler(debuglevel=1))
        #customOpeners.append(urllib2.HTTPHandler(debuglevel=1))
        if not IsHttpsCertValidationEnabled():
            try:
                if sslProtoVer is not None:
                    ctx = ssl._create_unverified_context(sslProtoVer)
                else:
                    ctx = ssl._create_unverified_context()
                customOpeners.append(urllib.request.HTTPSHandler(context=ctx))
            except Exception:
                pass
        elif sslProtoVer is not None:
            ctx = ssl.SSLContext(sslProtoVer)
            customOpeners.append(urllib.request.HTTPSHandler(context=ctx))

        #proxy support
        if self.useProxy:
            http_proxy = self.proxyURL
        else:
            http_proxy = ''
        #proxy from parameters (if available) overwrite default one
        if 'http_proxy' in params:
            http_proxy = params['http_proxy']
        if '' != http_proxy:
            printDBG('getURLRequestData USE PROXY')
            customOpeners.append(urllib.request.ProxyHandler({"http": http_proxy}))
            customOpeners.append(urllib.request.ProxyHandler({"https": http_proxy}))

        pageUrl = params['url']
        proxy_gateway = params.get('proxy_gateway', '')
        if proxy_gateway != '':
            pageUrl = proxy_gateway.format(quote_plus(pageUrl, ''))
        printDBG("pageUrl: [%s]" % pageUrl)

        if None is not post_data:
            printDBG('pCommon - getURLRequestData() -> post data: ' + str(post_data))
            if params.get('raw_post_data', False):
                dataPost = post_data
            elif params.get('multipart_post_data', False):
                customOpeners.append(MultipartPostHandler())
                dataPost = post_data
            else:
                dataPost = urlencode(post_data).encode()
            req = urllib.request.Request(pageUrl, dataPost, headers, method="POST")
        else:
            req = urllib.request.Request(pageUrl, None, headers, method="GET")

        if not params.get('return_data', False):
            out_data = urlOpen(req, customOpeners, timeout)
        else:
            gzip_encoding = False
            try:
                response = urlOpen(req, customOpeners, timeout)
                if response.info().get('Content-Encoding') == 'gzip':
                    gzip_encoding = True
                try:
                    metadata['url'] = response.geturl()
                    metadata['status_code'] = response.getcode()
                    common.fillHeaderItems(metadata, response.info(), True, collectAllHeaders=params.get('collect_all_headers'))
                except Exception:
                    pass

                _max = params.get('max_data_size', -1)
                if _max == -1:
                    data = response.read()
                else:
                    data = response.read(_max)
                response.close()
            except urllib.error.HTTPError as e:
                ignoreCodeRanges = params.get('ignore_http_code_ranges', [(404, 404), (500, 500)])
                ignoreCode = False
                metadata['status_code'] = e.code
                for ignoreCodeRange in ignoreCodeRanges:
                    if e.code >= ignoreCodeRange[0] and e.code <= ignoreCodeRange[1]:
                        ignoreCode = True
                        break

                if ignoreCode:
                    printDBG('!!!!!!!! %s: getURLRequestData - handled' % e.code)
                    if e.fp.info().get('Content-Encoding', '') == 'gzip':
                        gzip_encoding = True
                    try:
                        metadata['url'] = e.fp.geturl()
                        common.fillHeaderItems(metadata, e.fp.info(), True, collectAllHeaders=params.get('collect_all_headers'))
                    except Exception:
                        pass
                    _max = params.get('max_data_size', -1)
                    if _max == -1:
                        data = e.fp.read()
                    else:
                        data = e.fp.read(_max)
                    #e.msg
                    #e.headers
                elif e.code == 503:
                    if params.get('use_cookie', False):
                        new_cookie = e.fp.info().get('Set-Cookie', '')
                        printDBG("> new_cookie[%s]" % new_cookie)
                        cj.save(params['cookiefile'], ignore_discard=True)
                    raise e
                else:
                    if e.code in [300, 302, 303, 307] and params.get('use_cookie', False) and params.get('save_cookie', False):
                        new_cookie = e.fp.info().get('Set-Cookie', '')
                        printDBG("> new_cookie[%s]" % new_cookie)
                        #for cookieKey in params.get('cookie_items', {}).keys():
                        #    cj.clear('', '/', cookieKey)
                        cj.save(params['cookiefile'], ignore_discard=True)
                    raise e
            try:
                if gzip_encoding:
                    printDBG('Content-Encoding == gzip')
                    out_data = DecodeGzipped(data)
                else:
                    out_data = data
            except Exception as e:
                printExc()
                if params.get('max_data_size', -1) == -1:
                    msg1 = _("Critical Error – Content-Encoding gzip cannot be handled!")
                    msg2 = _("Last error:\n%s" % str(e))
                    IPTVPlayerNotificationList().push('%s\n\n%s' % (msg1, msg2), 'error', 20)
                out_data = data

        if params.get('use_cookie', False) and params.get('save_cookie', False):
            try:
                cj.save(params['cookiefile'], ignore_discard=True)
            except Exception as e:
                printExc()
                raise e

        out_data, metadata = self._handle_charset(params, out_data, metadata)
        if params.get('with_metadata', False) and params.get('return_data', False):
            out_data = strwithmeta(out_data, metadata)

        return out_data

    def _handle_charset(self, params, data, metadata):
        try:
            if params.get('return_data', False) and params.get('convert_charset', True):
                encoding = ''
                if 'content-type' in metadata:
                    encoding = ph.getSearchGroups(metadata['content-type'], '''charset=([A-Za-z0-9\-]+)''', 1, True)[0].strip().upper()
                if encoding == '' and params.get('search_charset', False):
                    if isinstance(_data, bytes):
                        _data = data.decode('utf-8', 'ignore')
                    encoding = ph.getSearchGroups(_data, '''(<meta[^>]+?Content-Type[^>]+?>)''', ignoreCase=True)[0]
                    encoding = ph.getSearchGroups(encoding, '''charset=([A-Za-z0-9\-]+)''', 1, True)[0].strip().upper()
                if encoding not in ['', 'UTF-8']:
                    printDBG(">> encoding[%s]" % encoding)
                    try:
                        data = data.decode(encoding)
                    except Exception:
                        printExc()
                    metadata['orig_charset'] = encoding
                else:
                    try:
                        data = data.decode('utf-8', 'strict')
                    except Exception:
                        data = data.decode('utf-8', 'ignore')
        except Exception:
            printExc()

        return data, metadata

    @staticmethod
    def iriToUri(iri):
        try:
            parts = urlparse(ph.ensure_str(iri))
            encodedParts = []
            for parti, part in enumerate(parts):
                newPart = part
                try:
                    if parti == 1:
                        newPart = part.encode('idna')
                    else:
                        newPart = url_encode_non_ascii(part.encode('utf-8'))
                except Exception:
                    printExc()
                encodedParts.append(ph.ensure_str(newPart))
            return urlunparse(encodedParts)
        except Exception:
            printExc()
        return iri
