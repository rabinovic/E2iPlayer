﻿# -*- coding: utf-8 -*-

###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.pCommon import getDefaultHeader, getBaseUrl, isValidUrl
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, GetCookieDir, MergeDicts, rm
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import SetIPTVPlayerLastHostError
from Plugins.Extensions.IPTVPlayer.components.ihost import HostBase
###################################################

###################################################
# FOREIGN import
###################################################
from Components.config import config, ConfigText, getConfigListEntry
############################################

###################################################
# E2 GUI COMMPONENTS
###################################################
from Screens.MessageBox import MessageBox
###################################################

###################################################
# Config options for HOST
###################################################
config.plugins.iptvplayer.internetowa_login = ConfigText(default="", fixed_size=False)
config.plugins.iptvplayer.internetowa_password = ConfigText(default="", fixed_size=False)


def GetConfigList():
    optionList = []
    optionList.append(getConfigListEntry('internetowa.ws ' + _("email") + ':', config.plugins.iptvplayer.internetowa_login))
    optionList.append(getConfigListEntry('internetowa.ws ' + _("password") + ':', config.plugins.iptvplayer.internetowa_password))
    return optionList

###################################################


class InternetowaApi(HostBase):

    def __init__(self):
        HostBase.__init__(self)
        self.MAIN_URL = 'https://internetowa.ws/'
        self.DEFAULT_ICON_URL = self.getFullIconUrl('/img/internetowa-logo-new-3.png')
        self.HTTP_HEADER = getDefaultHeader(browser='chrome')

        self.COOKIE_FILE = GetCookieDir('internetowa.ws.cookie')

        self.http_params = {}
        self.http_params.update({'header': self.HTTP_HEADER, 'save_cookie': True, 'load_cookie': True, 'cookiefile': self.COOKIE_FILE})
        self.loggedIn = False
        self.login = None
        self.password = None

    def tryTologin(self):
        printDBG('tryTologin start')

        if None == self.loggedIn or self.login != config.plugins.iptvplayer.internetowa_login.value or\
            self.password != config.plugins.iptvplayer.internetowa_password.value:

            rm(self.COOKIE_FILE)

            self.login = config.plugins.iptvplayer.internetowa_login.value
            self.password = config.plugins.iptvplayer.internetowa_password.value

            rm(self.COOKIE_FILE)
            sts, data = self.cm.getPage(self.getFullUrl('/logowanie/'), self.http_params)
            if sts:
                self.setMainUrl(self.cm.meta['url'])

            self.loggedIn = False
            if '' == self.login.strip() or '' == self.password.strip():
                return False

            if sts:
                params = dict(self.http_params)
                params['header'] = MergeDicts(self.HTTP_HEADER, {'Referer': self.getFullUrl('/logowanie/')})

                post_data = {'email': self.login, 'password': self.password}
                sts, data = self.cm.getPage(self.getFullUrl('/logowanie/'), params, post_data)

            if sts and '/wyloguj' in data:
                printDBG('tryTologin OK')
                self.loggedIn = True
            else:
                msgTab = [_('Login failed.')]
                if sts:
                    msgTab.append(ph.cleanHtml(ph.getDataBetweenNodes(data, ('<div', '>', 'errorBox'), ('</div', '>'), False)[1]))
                self.sessionEx.waitForFinishOpen(MessageBox, '\n'.join(msgTab), type=MessageBox.TYPE_ERROR, timeout=10)
                printDBG('tryTologin failed')
        return self.loggedIn

    def getList(self, cItem):
        printDBG("InternetowaApi.getChannelsList")
        self.tryTologin()

        channelsTab = []

        if cItem.get('priv_cat') is None:
            sts, data = self.cm.getPage(self.getMainUrl(), self.http_params)
            if not sts:
                return []

            sectionsTitles = {}
            tmp = ph.getDataBetweenNodes(data, ('<select', '>', 'switchView'), ('</select', '>'), False)[1]
            tmp = ph.getAllItemsBetweenMarkers(tmp, '<option', '</option>')
            for item in tmp:
                marker = ph.getSearchGroups(item, '''value=['"]([^"^']+?)['"]''')[0]
                sectionsTitles[marker] = ph.cleanHtml(item)

            data = ph.getDataBetweenNodes(data, ('<div', '>', 'channelbiggrid'), ('<style', '>'))[1]
            data = ph.rgetAllItemsBetweenNodes(data, ('</div', '>'), ('<div', '>', 'channelbiggrid'))
            for section in data:
                tmp = ph.getSearchGroups(section, '''<div[^>]+?id=['"]([^'^"]+?)home['"]''')[0]
                sTitle = sectionsTitles.get(tmp, tmp.upper())
                subItems = []
                section = section.split('</h2>')

                for idx in range(1, len(section), 2):
                    sTitle2 = ph.cleanHtml(section[idx - 1])
                    subItems2 = []
                    subSections = ph.getAllItemsBetweenMarkers(section[idx], '<a', '</a>')
                    for item in subSections:
                        url = self.getFullIconUrl(ph.getSearchGroups(item, '''href=['"]([^'^"]+?)['"]''')[0])
                        if not isValidUrl(url):
                            continue
                        title = ph.cleanHtml(item)
                        if title == '':
                            title = ph.cleanHtml(ph.getSearchGroups(item, '''title=['"]([^'^"]+?)['"]''')[0])
                        icon = self.getFullUrl(ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
                        type = title.lower()
                        type = 'audio' if 'radio' in type or 'rmf ' in type else 'video'
                        subItems2.append(MergeDicts(cItem, {'type': type, 'title': title, 'url': url, 'icon': icon}))
                    if len(subItems2):
                        subItems.append(MergeDicts(cItem, {'priv_cat': 'sub_items', 'title': sTitle2, 'sub_items': subItems2}))
                if len(subItems) == 1:
                    channelsTab.append(subItems[0])
                elif len(subItems):
                    channelsTab.append(MergeDicts(cItem, {'priv_cat': 'sub_items', 'title': sTitle, 'sub_items': subItems}))
        else:
            channelsTab = cItem['sub_items']
        return channelsTab

    def getVideoLink(self, cItem):
        printDBG("InternetowaApi.getVideoLink")
        urlsTab = []

        sts, data = self.cm.getPage(cItem['url'], self.http_params)
        if not sts:
            return urlsTab
        cUrl = self.cm.meta['url']

        SetIPTVPlayerLastHostError(ph.cleanHtml(ph.getDataBetweenNodes(data, ('<div', '>', 'nostream'), ('</div', '>'), False)[1]))

        data = ph.getAllItemsBetweenMarkers(data, '<iframe', '>', caseSensitive=False)
        printDBG(data)
        for item in data:
            url = self.getFullUrl(ph.getSearchGroups(item, '''src=['"]([^'^"]+?)['"]''')[0])
            if not isValidUrl(url):
                continue
            if 1 == self.up.checkHostSupport(url):
                url = strwithmeta(url, {'Referer': cUrl})
                urlsTab.extend(self.up.getVideoLinkExt(url))
            else:
                params = dict(self.http_params)
                params['header'] = MergeDicts(self.HTTP_HEADER, {'Referer': cUrl})
                sts, tmp = self.cm.getPage(url, params)
                if not sts:
                    continue
                tmp2 = ph.getDataBetweenMarkers(tmp, '<audio', '</audio>', False)[1]
                tmp2 = ph.getDataBetweenMarkers(tmp, '<audio', '</audio>', False)[1]
                tmp = ph.getAllItemsBetweenMarkers(tmp2, '<source', '>')
                for it in tmp:
                    printDBG(it)
                    type = ph.getSearchGroups(it, '''type=['"]([^"^']+?)['"]''')[0].lower().split('/', 1)
                    mediaUrl = self.cm.getFullUrl(ph.getSearchGroups(it, '''src=['"]([^"^']+?)['"]''')[0], self.cm.meta['url'])
                    if type[0] in ('audio', 'video'):
                        mediaUrl = strwithmeta(mediaUrl, {'User-Agent': params['header']['User-Agent'], 'Referer': self.cm.meta['url']})
                        urlsTab.append({'name': '[%s] %s' % (type[1], getBaseUrl(url, True)), 'url': mediaUrl, 'need_resolve': 0})
        return urlsTab
