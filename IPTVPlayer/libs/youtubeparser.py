# -*- coding: utf-8 -*-

import json
import re
import urllib.request
import urllib.parse
import urllib.error
from urllib.parse import urlparse, urlunparse, parse_qsl
from datetime import timedelta

from Components.config import config, ConfigSelection, ConfigYesNo

from Plugins.Extensions.IPTVPlayer.components.ihost import DisplayList
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.pCommon import common, isValidUrl
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import decorateUrl
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import getDirectM3U8Playlist, getMPDLinksWithMeta
from Plugins.Extensions.IPTVPlayer.libs.youtube_dl.extractor.youtube import YoutubeIE
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, IsExecutable
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta

###################################################
# Config options for HOST
###################################################
config.plugins.iptvplayer.ytformat = ConfigSelection(default="mp4", choices=[("flv, mp4", "flv, mp4"), ("flv", "flv"), ("mp4", "mp4")])
config.plugins.iptvplayer.ytDefaultformat = ConfigSelection(default="720", choices=[("0", _("the worst")), ("144", "144p"), ("240", "240p"), ("360", "360p"), ("720", "720p"), ("1080", "1080p"), ("1440", "1440p"), ("2160", "2160p"), ("9999", _("the best"))])
config.plugins.iptvplayer.ytUseDF = ConfigYesNo(default=True)
config.plugins.iptvplayer.ytAgeGate = ConfigYesNo(default=False)
config.plugins.iptvplayer.ytVP9 = ConfigYesNo(default=False)
config.plugins.iptvplayer.ytShowDash = ConfigSelection(default="auto", choices=[("auto", _("Auto")), ("true", _("Yes")), ("false", _("No"))])
config.plugins.iptvplayer.ytSortBy = ConfigSelection(default="A", choices=[("A", _("Relevance")), ("I", _("Upload date")), ("M", _("View count")), ("E", _("Rating"))])

class YouTubeParser():

    def __init__(self):
        self.cm = common()
        self.HTTP_HEADER = {
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
                            'X-YouTube-Client-Name': '1',
                            'X-YouTube-Client-Version': '2.20201112.04.01',
                            'X-Requested-With': 'XMLHttpRequest'
                            }
        self.http_params = {'header': self.HTTP_HEADER, 'return_data': True}
        self.postdata = {}
        self.sessionToken = ""

    @staticmethod
    def isDashAllowed():
        value = config.plugins.iptvplayer.ytShowDash.value
        printDBG("ALLOW DASH: >> %s" % value)
        if value == "true" and IsExecutable('ffmpeg'):
            return True
        if value == "auto" and IsExecutable('ffmpeg') and IsExecutable(config.plugins.iptvplayer.exteplayer3path.value):
            return True
        return False

    @staticmethod
    def isVP9Allowed():
        value = config.plugins.iptvplayer.ytVP9.value
        printDBG("1. ALLOW VP9: >> %s" % value)
        value = YouTubeParser.isDashAllowed() and value
        printDBG("2. ALLOW VP9: >> %s" % value)
        return value

    @staticmethod
    def isAgeGateAllowed():
        value = config.plugins.iptvplayer.ytAgeGate.value
        printDBG("ALLOW Age-Gate bypass: >> %s" % value)
        return value

    def checkSessionToken(self, data):
        if not self.sessionToken:
            token = ph.getSearchGroups(data, '''"XSRF_TOKEN":"([^"]+?)"''')[0]
            if token:
                printDBG("Update session token: %s" % token)
                self.sessionToken = token
                self.postdata = {"session_token": token}

    def getDirectLinks(self, url, formats='flv, mp4', dash=True, dashSepareteList=False, allowVP9=None, allowAgeGate=None):
        printDBG('YouTubeParser.getDirectLinks')
        linksList = []
        try:
            if isValidUrl(url) and '/channel/' in url and url.endswith('/live'):
                sts, data = self.cm.getPage(url)
                if sts:
                    videoId = ph.getSearchGroups(data, '''<meta[^>]+?itemprop=['"]videoId['"][^>]+?content=['"]([^'^"]+?)['"]''')[0]
                    if videoId == '':
                        videoId = ph.getSearchGroups(data, '''['"]REDIRECT_TO_VIDEO['"]\s*\,\s*['"]([^'^"]+?)['"]''')[0]
                    if videoId != '':
                        url = 'https://www.youtube.com/watch?v=' + videoId
            linksList = YoutubeIE()._real_extract(url, allowVP9=allowVP9, allowAgeGate=allowAgeGate)
        except Exception:
            printExc()
            if dashSepareteList:
                return [], []
            return []

        reNum = re.compile('([0-9]+)')
        retHLSList = []
        retList = []
        dashList = []
        # filter dash
        dashAudioLists = []
        dashVideoLists = []
        if dash:
            # separete audio and video links
            for item in linksList:
                if 'mp4a' == item['ext']:
                    dashAudioLists.append(item)
                elif item['ext'] in ('mp4v', 'webmv'):
                    dashVideoLists.append(item)
                elif 'mpd' == item['ext']:
                    tmpList = getMPDLinksWithMeta(ph.ensure_str(item['url']), checkExt=False)
                    printDBG(tmpList)
                    for elem in tmpList:
                        elem['format'] = f"{elem.get('height', 0)}x{elem.get('width', 0)}"
                        elem['ext'] = "mpd"
                        elem['dash'] = True
                    dashList.extend(tmpList)
            # sort by quality -> format

            dashAudioLists.sort(key=lambda x: x['format'], reverse=True)
            dashVideoLists.sort(key=lambda x: x['format'], reverse=True)

        for item in linksList:
            if -1 < formats.find(item['ext']):
                if 'yes' == item['m3u8']:
                    if format_ := re.search('([0-9]+?)p$', item['format']):
                        item['format'] = format_.group(1) + "x"
                        item['ext'] = item['ext'] + "_M3U8"
                        item['url'] = decorateUrl(ph.ensure_str(item['url']), {"iptv_proto": "m3u8"})
                        retHLSList.append(item)
                else:
                    if format_ := re.search('([0-9]+?x[0-9]+?$)', item['format']):
                        item['format'] = format_.group(1)
                        item['url'] = decorateUrl(ph.ensure_str(item['url']))
                        retList.append(item)

        if dashAudioLists:
            # use best audio
            for item in dashVideoLists:
                item = dict(item)
                item["url"] = decorateUrl("merge://audio_url|video_url", {'audio_url': dashAudioLists[0]['url'], 'video_url': ph.ensure_str(item['url'])})
                dashList.append(item)

        # try to get hls format with alternative method
        if 0 == len(retList):
            try:
                video_id = YoutubeIE()._extract_id(url)
                url = f'http://www.youtube.com/watch?v={video_id}&gl=US&hl=en&has_verified=1'
                sts, data = self.cm.getPage(url, {'header': {'User-agent': 'Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10'}})
                if sts:
                    data = data.replace('\\"', '"').replace('\\\\\\/', '/')
                    hlsUrl = ph.getSearchGroups(data, '''"hlsvp"\s*:\s*"(https?://[^"]+?)"''')[0]
                    hlsUrl = json.loads(f'"{hlsUrl}"')
                    if isValidUrl(hlsUrl):
                        hlsList = getDirectM3U8Playlist(hlsUrl)
                        if len(hlsList):
                            dashList = []
                            for item in hlsList:
                                item['format'] = f"{item.get('with', 0)}x{item.get('heigth', 0)}"
                                item['ext'] = "m3u8"
                                item['m3u8'] = True
                                retList.append(item)
            except Exception as ex:
                printExc()
            if 0 == len(retList):
                retList = retHLSList

            if dash:
                try:
                    sts, data = self.cm.getPage(url, {'header': {'User-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'}})
                    data = data.replace('\\"', '"').replace('\\\\\\/', '/').replace('\\/', '/')
                    dashUrl = ph.getSearchGroups(data, '''"dashmpd"\s*:\s*"(https?://[^"]+?)"''')[0]
                    dashUrl = json.loads(f'"{dashUrl}"')
                    if '?' not in dashUrl:
                        dashUrl += '?mpd_version=5'
                    else:
                        dashUrl += '&mpd_version=5'
                    if isValidUrl(dashUrl):
                        dashList = getMPDLinksWithMeta(dashUrl, checkExt=False)
                        for elem in dashList:
                            elem['format'] = f"{elem.get('height', 0)}x{elem.get('width', 0)}"
                            elem['ext'] = "mpd"
                            elem['dash'] = True
                except Exception:
                    printExc()

        for elem in retList:
            if elem.get('m3u8', False):
                elem['url'] = strwithmeta(elem['url'], {'iptv_m3u8_live_start_index': -30})

        if dashSepareteList:
            return retList, dashList
        else:
            retList.extend(dashList)
            return retList

    def updateQueryUrl(self, url, queryDict):
        urlParts = urlparse(url)
        query = dict(parse_qsl(urlParts[4]))
        query.update(queryDict)
        new_query = urllib.parse.urlencode(query)
        new_url = urlunparse((urlParts[0], urlParts[1], urlParts[2], urlParts[3], new_query, urlParts[5]))
        return new_url

    def findKeys(self, node, kv):
        if isinstance(node, list):
            for i in node:
                for x in self.findKeys(i, kv):
                    yield x
        elif isinstance(node, dict):
            if kv in node:
                yield node[kv]
            for j in node.values():
                for x in self.findKeys(j, kv):
                    yield x

    def getThumbnailUrl(self, thumbJson, maxWidth=1000, hq=False):

        url = ''
        try:
            thumbJson2 = []
            try:
                thumbJson2 = thumbJson["thumbnail"]["thumbnails"]
            except Exception:
                pass
            if len(thumbJson2) == 0:
                thumbJson2 = thumbJson["thumbnails"][0]['thumbnails']
                print(thumbJson2)
            thumbJson = thumbJson2
            width = 0
            i = 0

            while i < len(thumbJson):
                img = thumbJson[i]
                width = img['width']
                if width < maxWidth:
                    url = ph.ensure_str(img['url'])
                i = i + 1

            if hq or (not config.plugins.iptvplayer.allowedcoverformats.value) or config.plugins.iptvplayer.allowedcoverformats.value != 'all':
#                if 'hqdefault' in url:
#                    url = url.replace('hqdefault', 'hq720')
                if '?' in url:
                    url = url.split('?')[0]
        except Exception:
            printExc()

        return url

    def getVideoData(self, videoJson):

        if videoId:= videoJson.get("videoId", ""):
            url = f'http://www.youtube.com/watch?v={videoId}'
            try:
                title = videoJson['title']['runs'][0]['text']
            except:
                title = videoJson['title']['simpleText']

            title = ph.ensure_str(title)
            badges = []
            videoBadges = videoJson.get("badges", [])
            for videoBadge in videoBadges:
                try:
                    badgeLabel = ph.ensure_str(videoBadge["metadataBadgeRenderer"]["label"])
                    badges.append(badgeLabel.upper())
                except Exception:
                    pass

            if badges:
                title = title + " [" + (' , '.join(badges)) + "]"

            icon = self.getThumbnailUrl(videoJson)

            desc = []
            try:
                if duration := videoJson["lengthText"]["simpleText"]:
                    desc.append(_("Duration: %s") % ph.ensure_str(duration))
            except:
                pass

            try:
                if views:= videoJson["viewCountText"]["simpleText"]:
                    desc.append(ph.ensure_str(views))
            except:
                pass

            try:
                if time:= videoJson["publishedTimeText"]["simpleText"]:
                    desc.append(ph.ensure_str(time))
            except:
                time = ''

            try:
                owner = videoJson["ownerText"]["runs"][0]["text"]
            except:
                try:
                    owner = videoJson["longBylineText"]["runs"][0]["text"]
                except:
                    owner = ""
            owner = ph.ensure_str(owner)

            if desc:
                desc = " | ".join(desc) + "\n" + owner
            else:
                desc = owner

            try:
                desc = desc + "\n" + ph.ensure_str(videoJson["descriptionSnippet"]["runs"][0]["text"])
            except:
                pass

            return {'type': 'video', 'category': 'video', 'title': title, 'url': ph.ensure_str(url), 'icon': icon, 'time': time, 'desc': desc}

        return {}

    def getChannelData(self, chJson):

        if chId:= chJson.get("channelId", ""):
            url = 'https://www.youtube.com/channel/%s' % chId
            title = chJson['title']['simpleText']

            icon = self.getThumbnailUrl(chJson)

            try:
                desc = chJson["descriptionSnippet"]["runs"][0]["text"]
            except:
                desc = ""

            return {'type': 'category', 'category': 'channel', 'title': title, 'url': ph.ensure_str(url), 'icon': icon, 'time': '', 'desc': desc}

        else:
            return {}

    def getPlaylistData(self, plJson):

        if plId:= plJson.get("playlistId", ""):
            url = f"https://www.youtube.com/playlist?list={plId}"
            try:
                title = plJson['title']['simpleText']
            except:
                title = plJson['title']['runs'][0]['text']

            icon = self.getThumbnailUrl(plJson)
            try:
                videoCount = plJson['videoCount']
            except:
                videoCount = plJson['videoCountShortText']['simpleText']

            desc = _("videos: %s") % videoCount
            try:
                by = plJson["longBylineText"]["runs"][0]["text"]
                desc = desc + "\n" + by
            except:
                pass
            return {'type': 'category', 'category': 'playlist', 'title': title, 'url': ph.ensure_str(url), 'icon': icon, 'time': '', 'desc': desc}
        else:
            return {}

    def getMenuItemData(self, itemJson):

        try:
            title = itemJson['title']['simpleText']
            icon = self.getThumbnailUrl(itemJson)

            try:
                feedId = itemJson["navigationEndpoint"]["browseEndpoint"]["params"]
                url = f"https://www.youtube.com/feed/trending?bp={feedId}&pbj=1"
                cat = "feeds_" + title
            except:
                try:
                    url = "https://www.youtube.com" + itemJson["navigationEndpoint"]["commandMetadata"]["webCommandMetadata"]["url"]
                except:
                    printExc()
                    return {}

            if '/channel/' in url or '/@' in url:
                return {'type': 'category', 'category': 'channel', 'title': title, 'url': ph.ensure_str(url), 'icon': icon, 'time': '', 'desc': ''}
            else:
                return {'type': 'feed', 'category': cat, 'title': title, 'url': ph.ensure_str(url), 'icon': icon, 'time': '', 'desc': ''}

        except:
            printExc()
            return {}

    def getFeedsList(self, url):
        printDBG('YouTubeParser.getFeedList')

        currList = DisplayList()
        try:
            sts, data = self.cm.getPage(url, self.http_params)
            if sts:
                self.checkSessionToken(data)

                data2 = ph.getDataBetweenMarkers(data, "window[\"ytInitialData\"] =", "};", False)[1]
                if len(data2) == 0:
                    data2 = ph.getDataBetweenMarkers(data, "var ytInitialData =", "};", False)[1]

                try:
                    response = json.loads(data2 + "}")

                    submenu = response['contents']['twoColumnBrowseResultsRenderer']['tabs'][0]['tabRenderer']['content']['sectionListRenderer']['subMenu']
                    for item in submenu["channelListSubMenuRenderer"]["contents"]:

                        if menuJson := item.get("channelListSubMenuAvatarRenderer", ''):
                            params = self.getMenuItemData(menuJson)

                            if params:
                                printDBG(str(params))
                                currList.append(params)
                except:
                    printExc()

        except Exception:
            printExc()

        return currList

    def getVideoFromFeed(self, url):
        printDBG('YouTubeParser.getVideosFromFeed')

        currList = DisplayList()
        try:
            sts, data = self.cm.getPage(url, self.http_params)
            if sts:
                self.checkSessionToken(data)

                try:
                    response = json.loads(data)

                    rr = {}
                    for r in response:
                        if r.get("response", ""):
                            rr = r
                            break

                    if not rr:
                        return []

                    r1 = rr["response"]["contents"]['twoColumnBrowseResultsRenderer']['tabs'][0]['tabRenderer']['content']['sectionListRenderer']['contents']
                    r2 = r1[0]['itemSectionRenderer']['contents'][0]["shelfRenderer"]["content"]["expandedShelfContentsRenderer"]["items"]

                    for item in r2:
                        chJson = item.get('channelRenderer', '')
                        videoJson = item.get('videoRenderer', '')
                        plJson = item.get('playlistRenderer', '')

                        params = {}
                        if videoJson:
                            # it is a video
                            params = self.getVideoData(videoJson)
                        elif chJson:
                            # it is a channel
                            params = self.getChannelData(chJson)
                        elif plJson:
                            # it is a playlist
                            params = self.getPlaylistData(plJson)

                        if params:
                            printDBG(str(params))
                            currList.append(params)
                except:
                    printExc()
        except Exception:
            printExc()

        return currList

    ########################################################
    # Tray List PARSER
    ########################################################

    def getVideosFromTraylist(self, url, category, page, cItem):
        printDBG('YouTubeParser.getVideosFromTraylist')
        return self.getVideosApiPlayList(url, category, page, cItem)

        currList = DisplayList()
        try:
            sts, data = self.cm.getPage(url, self.http_params)
            if sts:
                sts, data = ph.getDataBetweenMarkers(data, 'class="playlist-videos-container', '<div class="watch-sidebar-body">', False)
                data = data.split('class="yt-uix-scroller-scroll-unit')
                del data[0]
                return
        except Exception:
            printExc()
            return []

        return currList
    # end getVideosFromPlaylist

    ########################################################
    # PLAYLIST PARSER
    ########################################################
    def getVideosFromPlaylist(self, url, category, page, cItem):
        printDBG('YouTubeParser.getVideosFromPlaylist')
        currList = DisplayList()
        try:
            sts, data = self.cm.getPage(url, self.http_params)
            if sts:
                self.checkSessionToken(data)

                data2 = ph.getDataBetweenMarkers(data, "window[\"ytInitialData\"] =", "};", False)[1]
                if len(data2) == 0:
                    data2 = ph.getDataBetweenMarkers(data, "var ytInitialData =", "};", False)[1]

                response = json.loads(data2 + "}")

                r1 = response['contents']['twoColumnBrowseResultsRenderer']['tabs'][0]['tabRenderer']['content']['sectionListRenderer']['contents']

                r2 = []
                for i in r1:
                    r2.extend(i['itemSectionRenderer']['contents'])

                for r3 in r2:
                    if pl:= r3.get('playlistVideoListRenderer', ''):
                        pl2 = pl.get('contents', [])

                        for p in pl2:
                            if videoJson:= p.get('playlistVideoRenderer', ''):
                                params = self.getVideoData(videoJson)
                                if params:
                                    try:
                                        params['title'] = f'{videoJson["index"]["simpleText"]}. - {params["title"]} '
                                    except:
                                        pass
                                    printDBG(str(params))
                                    currList.append(params)

        except Exception:
            printExc()

        return currList

    ########################################################
    # CHANNEL LIST PARSER
    ########################################################

    def getVideosFromChannelList(self, url, category, page, cItem):
        printDBG('YouTubeParser.getVideosFromChannelList page[%s]' % (page))
        currList = []

        try:
            url = strwithmeta(url)
            if 'post_data' in url.meta:
                http_params = dict(self.http_params)
                http_params['header']['Content-Type'] = 'application/json'
                http_params['raw_post_data'] = True
                sts, data = self.cm.getPage(url, http_params, url.meta['post_data'])
            else:
                sts, data = self.cm.getPage(url, self.http_params)

            if sts:
                if 'browse' in url:
                    # next pages
                    response = json.loads(data)['onResponseReceivedActions']

                    rr = {}
                    for r in response:
                        if r.get("appendContinuationItemsAction", ""):
                            rr = r
                            break

                    if not rr:
                        return []

                    r1 = rr["appendContinuationItemsAction"]
                    r4 = r1.get("continuationItems", [])

                else:
                    # first page of videos
                    self.checkSessionToken(data)
                    data2 = ph.getDataBetweenMarkers(data, "window[\"ytInitialData\"] =", "};", False)[1]
                    if len(data2) == 0:
                        data2 = ph.getDataBetweenMarkers(data, "var ytInitialData =", "};", False)[1]

                    response = json.loads(data2 + "}")

                    r1 = response['contents']['twoColumnBrowseResultsRenderer']['tabs']
                    r2 = {}
                    for tab in r1:
                        try:
                            if tab['tabRenderer']['content']:
                                r2 = tab['tabRenderer']['content']
                        except:
                            pass

                        if r2:
                            break

                    r4 = r2['richGridRenderer']['contents']

                nextPage = ''
                for r5 in r4:
                    videoJson = r5.get("richItemRenderer", "")
                    nP = r5.get('continuationItemRenderer', '')
                    if videoJson:
                        videoJson = videoJson.get("content", {})
                        videoJson = videoJson.get("videoRenderer", "")
                        params = self.getVideoData(videoJson)
                        if params:
                            printDBG(str(params))
                            currList.append(params)
                    if nP != '':
                        nextPage = nP

                if nextPage:
                    ctoken = nextPage["continuationEndpoint"]["continuationCommand"].get('token', '')
                    ctit = nextPage["continuationEndpoint"]["clickTrackingParams"]
                    try:
                        label = nextPage["nextContinuationData"]["label"]["runs"][0]["text"]
                    except:
                        label = _("Next Page")

                    urlNextPage = "https://www.youtube.com/youtubei/v1/browse?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8"
                    post_data = {'context': {'client': {'clientName': 'WEB', 'clientVersion': '2.20201021.03.00', }}, }
                    post_data['continuation'] = ctoken
                    post_data['context']['clickTracking'] = {'clickTrackingParams': ctit}
                    post_data = json.dumps(post_data).encode('utf-8')
                    urlNextPage = strwithmeta(urlNextPage, {'post_data': post_data})
                    params = {'type': 'more', 'category': category, 'title': label, 'page': str(int(page) + 1), 'url': ph.ensure_str(urlNextPage)}
                    printDBG(str(params))
                    currList.append(params)

        except Exception:
            printExc()

        return currList

    ########################################################
    # PLAYLISTS PARSER
    ########################################################
    def getPlaylistsFromChanelList(self, url, category, page, cItem):
        printDBG('YouTubeParser.getPlaylistsFromChanelList page[%s]' % (page))
        currList = []

        try:
            url = strwithmeta(url)
            if 'post_data' in url.meta:
                http_params = dict(self.http_params)
                http_params['header']['Content-Type'] = 'application/json'
                http_params['raw_post_data'] = True
                sts, data = self.cm.getPage(url, http_params, url.meta['post_data'])
            else:
                sts, data = self.cm.getPage(url, self.http_params)

            if sts:
                if 'browse' in url:
                    # next pages
                    response = json.loads(data)['onResponseReceivedActions']

                    rr = {}
                    for r in response:
                        if r.get("appendContinuationItemsAction", ""):
                            rr = r
                            break

                    if not rr:
                        return []

                    r1 = rr["appendContinuationItemsAction"]
                    r4 = r1.get("continuationItems", [])

                else:
                    # first page of videos
                    self.checkSessionToken(data)
                    data2 = ph.getDataBetweenMarkers(data, "window[\"ytInitialData\"] =", "};", False)[1]
                    if len(data2) == 0:
                        data2 = ph.getDataBetweenMarkers(data, "var ytInitialData =", "};", False)[1]

                    response = json.loads(data2 + "}")

                    r1 = response['contents']['twoColumnBrowseResultsRenderer']['tabs']
                    r2 = {}
                    for tab in r1:
                        try:
                            if tab['tabRenderer']['content']:
                                r2 = tab['tabRenderer']['content']
                        except:
                            pass

                        if r2:
                            break

                    r4 = r2['sectionListRenderer']['contents'][0]['itemSectionRenderer']['contents'][0]['shelfRenderer']['content']['horizontalListRenderer']['items']

                nextPage = ''
                for r5 in r4:
                    videoJson = r5.get("gridPlaylistRenderer", "")
                    nP = r5.get('continuationItemRenderer', '')
                    if videoJson:
                        if params:= self.getPlaylistData(videoJson):
                            printDBG(str(params))
                            currList.append(params|{'with_mini_cover':True})
                    if nP != '':
                        nextPage = nP

                if nextPage:
                    ctoken = nextPage["continuationEndpoint"]["continuationCommand"].get('token', '')
                    ctit = nextPage["continuationEndpoint"]["clickTrackingParams"]
                    try:
                        label = nextPage["nextContinuationData"]["label"]["runs"][0]["text"]
                    except:
                        label = _("Next Page")

                    urlNextPage = "https://www.youtube.com/youtubei/v1/browse?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8"
                    post_data = {'context': {'client': {'clientName': 'WEB', 'clientVersion': '2.20201021.03.00', }}, }
                    post_data['continuation'] = ctoken
                    post_data['context']['clickTracking'] = {'clickTrackingParams': ctit}
                    post_data = json.dumps(post_data).encode('utf-8')
                    urlNextPage = strwithmeta(urlNextPage, {'post_data': post_data})
                    params = {'type': 'more', 'category': category, 'title': label, 'page': str(int(page) + 1), 'url': ph.ensure_str(urlNextPage)}
                    printDBG(str(params))
                    currList.append(params)

        except Exception:
            printExc()

        return currList

    def getListPlaylistsItems(self, url, category, page, cItem):
        printDBG('YouTubeParser.getListPlaylistsItems page[%s]' % (page))
        currList = []
        try:
            url = strwithmeta(url)
            if 'post_data' in url.meta:
                http_params = dict(self.http_params)
                http_params['header']['Content-Type'] = 'application/json'
                http_params['raw_post_data'] = True
                sts, data = self.cm.getPage(url, http_params, url.meta['post_data'])
            else:
                sts, data = self.cm.getPage(url, self.http_params)

            if sts:
                #self.cm.ph.writeToFile('/mnt/new2/yt.html', data)
                if '1' == page:
                    sts,data = ph.getDataBetweenMarkers(data, '<div class="yt-lockup clearfix', 'footer-container')
                else:
                    data = json.loads(data)
                    data = data['load_more_widget_html'] + '\n' + data['content_html']

                # nextPage
                match = re.search('data-uix-load-more-href="([^"]+?)"', data)
                if not match:
                    nextPage = ""
                else:
                    nextPage = match.group(1).replace('&amp;', '&')

                itemsTab = data.split('<div class="yt-lockup clearfix')
                printDBG(itemsTab[0])
                currList = self.parseListBase(itemsTab, 'playlist')
                if '' != nextPage:
                    item = dict(cItem)
                    item.update({'title': 'Następna strona', 'page': str(int(page) + 1), 'url': 'http://www.youtube.com' + nextPage})
                    currList.append(item)
        except Exception:
            printExc()

        return currList
    # end getListPlaylistsItems

    ########################################################
    # SEARCH PARSER
    ########################################################
    #def getVideosFromSearch(self, pattern, page='1'):
    def getSearchResult(self, pattern, searchType, page, nextPageCategory, sortBy='A', url=''):
        printDBG('YouTubeParser.getSearchResult pattern[%s], searchType[%s], page[%s]' % (pattern, searchType, page))
        currList = DisplayList()

        try:
            #url = 'http://www.youtube.com/results?search_query=%s&filters=%s&search_sort=%s&page=%s' % (pattern, searchType, sortBy, page)

            nextPage = {}
            nP = {}
            nP_new = {}
            r2 = []

            if url:
                # next page search
                url = strwithmeta(url)
                if 'post_data' in url.meta:
                    http_params = dict(self.http_params)
                    http_params['header']['Content-Type'] = 'application/json'
                    http_params['raw_post_data'] = True
                    sts, data = self.cm.getPage(url, http_params, url.meta['post_data'])
                else:
                    sts, data = self.cm.getPage(url, self.http_params, self.postdata)

                if sts:
                    response = json.loads(data)

            else:
                # new search
                # url = 'http://www.youtube.com/results?search_query=%s&filters=%s&search_sort=%s' % (pattern, searchType, sortBy)
                url = f'https://www.youtube.com/results?search_query={pattern}&sp='
                match searchType:
                    case 'video':
                        url += f'CA{sortBy}SAhAB'
                    case 'channel':
                        url += f'CA{sortBy}SAhAC'
                    case 'playlist':
                        url += f'CA{sortBy}SAhAD'
                    case 'live':
                        url += 'EgJAAQ%253D%253D'

                sts, data = self.cm.getPage(url, self.http_params)

                if sts:
                    self.checkSessionToken(data)
                    data2 = ph.getDataBetweenMarkers(data, "window[\"ytInitialData\"] =", "};", False)[1]
                    if len(data2) == 0:
                        data2 = ph.getDataBetweenMarkers(data, "var ytInitialData =", "};", False)[1]

                    data2 = ph.ensure_str(data2.strip()) #just cleaning and ensuring we're working with string
                    #json simple schema verification and correction
                    jsonStarts = data2.count('{')
                    jsonEnds = data2.count('}')
                    printDBG('youtuberparser.YouTubeParser().getSearchResult correcting json string by adding "}" %s time(s) at the end' % (jsonStarts - jsonEnds))
                    while jsonEnds < jsonStarts:
                        data2 = data2 + '}'
                        jsonEnds += 1
                    open("/tmp/data2.txt", "w").write(data2)
                    response = json.loads(data2)

            if not sts:
                return []

            # search videos
            r2 = list(self.findKeys(response, 'videoRenderer'))

            for item in r2:
                if params:= self.getVideoData(item):
                    printDBG(str(params))
                    currList.append(params)

            # search channels

            r2 = list(self.findKeys(response, 'channelRenderer'))

            for item in r2:
                if params:= self.getChannelData(item):
                    printDBG(str(params))
                    currList.append(params)

            #search playlists

            r2 = list(self.findKeys(response, 'playlistRenderer'))

            for item in r2:
                if params:= self.getPlaylistData(item):
                    printDBG(str(params))
                    currList.append(params)

            nP = list(self.findKeys(response, "nextContinuationData"))
            nP_new = list(self.findKeys(response, "continuationEndpoint"))

            if nP:
                nextPage = nP[0]
                ctoken = nextPage["continuation"]
                itct = nextPage["clickTrackingParams"]
                try:
                    label = nextPage["label"]["runs"][0]["text"]
                except:
                    label = _("Next Page")

                urlNextPage = self.updateQueryUrl(url, {'pbj': '1', 'ctoken': ctoken, 'continuation': ctoken, 'itct': itct})
                params = {'type': 'more', 'category': "search_next_page", 'title': label, 'page': str(int(page) + 1), 'url': ph.ensure_str(urlNextPage)}
                printDBG(str(params))
                currList.append(params)

            elif nP_new:
                nextPage = nP_new[0]

                ctoken = nextPage["continuationCommand"]["token"]
                itct = nextPage["clickTrackingParams"]
                label = _("Next Page")

                urlNextPage = "https://www.youtube.com/youtubei/v1/search?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8"
                post_data = {'context': {'client': {'clientName': 'WEB', 'clientVersion': '2.20201021.03.00', }}, }
                post_data['continuation'] = ctoken
                post_data['context']['clickTracking'] = {'clickTrackingParams': itct}
                post_data = json.dumps(post_data).encode('utf-8')
                urlNextPage = strwithmeta(urlNextPage, {'post_data': post_data})
                params = {'type': 'more', 'category': "search_next_page", 'title': label, 'page': str(int(page) + 1), 'url': ph.ensure_str(urlNextPage)}
                printDBG(str(params))
                currList.append(params)

        except Exception:
            printExc()

        return currList

    ########################################################
    # PLAYLIST API
    ########################################################
    def getVideosApiPlayList(self, url, category, page, cItem):
        printDBG('YouTubeParser.getVideosApiPlayList url[%s]' % url)
        playlistID = ph.getSearchGroups(url + '&', 'list=([^&]+?)&')[0]
        baseUrl = 'https://www.youtube.com/playlist?list=%s' % playlistID

        currList = DisplayList()
        if baseUrl != '':
            sts, data = self.cm.getPage(baseUrl, self.http_params)
            data = ph.getDataBetweenMarkers(data, "var ytInitialData =", "};", False)[1]
            try:
                data = json.loads(data + '}')['contents']
                data = data['twoColumnBrowseResultsRenderer']['tabs'][0]['tabRenderer']['content']['sectionListRenderer']['contents'][0]['itemSectionRenderer']['contents'][0]['playlistVideoListRenderer']['contents']
                for item in data:
                    item = item.get('playlistVideoRenderer')
                    url = 'http://www.youtube.com/watch?v=' + item['videoId']
                    title = item['title']['runs'][0]['text']
                    img = self.getThumbnailUrl(item)
                    time = item['lengthSeconds']
                    if '' != time:
                        time = str(timedelta(seconds=int(time)))
                    if time.startswith("0:"):
                        time = time[2:]
                    desc = item['title']['accessibility']['accessibilityData']['label']
                    params = {'type': 'video', 'category': 'video', 'title': title, 'url': ph.ensure_str(url), 'icon': img, 'time': time, 'desc': desc}
                    currList.append(params)
            except Exception:
                printExc()
        return currList
