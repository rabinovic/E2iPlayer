﻿# -*- coding: utf-8 -*-

import re
import unicodedata
from binascii import hexlify
from urllib.parse import quote

from Plugins.Extensions.IPTVPlayer.libs.crypto.hash.md5Hash import MD5
from Plugins.Extensions.IPTVPlayer.libs.youtube_dl.utils import \
    cleanHtml as yt_cleanHtml

# flags:
NONE = 0
START_E = 1
START_S = 2
END_E = 4
END_S = 8
IGNORECASE = 16
I = 16

# pre-compiled regular expressions
IFRAME_SRC_URI_RE = re.compile(r'''<iframe[^>]+?src=(['"])([^>]*?)(?:\1)''', re.I)
IMAGE_SRC_URI_RE = re.compile(r'''<img[^>]+?src=(['"])([^>]*?\.(?:jpe?g|png)(?:\?[^\1]*?)?)(?:\1)''', re.I)
A_HREF_URI_RE = re.compile(r'''<a[^>]+?href=(['"])([^>]*?)(?:\1)''', re.I)
STRIP_HTML_COMMENT_RE = re.compile("<!--[\s\S]*?-->")

# add short aliases
IFRAME = IFRAME_SRC_URI_RE
IMG = IMAGE_SRC_URI_RE
A = A_HREF_URI_RE


def ensure_str(s, encoding='utf-8', errors='strict'):
    """Coerce *s* to `str`.
      - `str` -> `str`
      - `bytes` -> decoded to `str`
    """
    # Optimization: Fast return for the common case.
    if isinstance(s,str):
        return s

    if isinstance(s, bytes):
        return s.decode(encoding, errors)

    raise TypeError("not expecting type '%s'" % type(s))

def ensure_bytes(s, encoding='utf-8', errors='strict'):
    """Coerce **s** to six.binary_type.`
      - `str` -> encoded to `bytes`
      - `bytes` -> `bytes`
    """
    if s is None:
        return s
    if isinstance(s, bytes):
        return s
    if isinstance(s, str):
        return s.encode(encoding, errors)
    raise TypeError("not expecting type '{type(s)}'")

def getattr(data, attrmame, flags=0):
    if flags & IGNORECASE:
        sData = data.lower()
        m = f'{attrmame.lower()}='
    else:
        sData = data
        m = f'{attrmame}='
    sidx = 0
    while True:
        sidx = sData.find(m, sidx)
        if sidx == -1:
            return ''
        if data[sidx - 1] in ('\t', ' ', '\n', '\r'):
            break
        sidx += len(m)
    sidx += len(m)
    z = data[sidx]
    if z not in ('"', "'"):
        return ''
    eidx = sidx + 1
    while eidx < len(data):
        if data[eidx] == z:
            return data[sidx + 1:eidx]
        eidx += 1
    return ''


def search(data, pattern, flags=0, limits=-1):
    tab = []
    if isinstance(pattern, str):
        reObj = re.compile(pattern, re.IGNORECASE if flags & IGNORECASE else 0)
    else:
        reObj = pattern
    if limits == -1:
        limits = reObj.groups
    match = reObj.search(data)
    for idx in range(limits):
        try:
            value = match.group(idx + 1)
        except Exception:
            value = ''
        tab.append(value)
    return tab


def all(tab, data, start, end):
    for it in tab:
        if data.find(it, start, end) == -1:
            return False
    return True


def any(tab, data, start, end):
    for it in tab:
        if data.find(it, start, end) != -1:
            return True
    return False


def none(tab, data, start, end):
    return not any(tab, data, start, end)


def check(arg1, arg2=None):
    if arg2 is None and isinstance(arg1, str):
        return lambda data, ldata, s, e: ldata.find(arg1, s, e) != -1

    return lambda data, ldata, s, e: arg1(arg2, ldata, s, e)


def findall(data, start, end=('',), flags=START_E | END_E, limits=-1):

    start = start if isinstance(start, tuple) or isinstance(start, list) else (start,)
    end = end if isinstance(end, tuple) or isinstance(end, list) else (end,)

    if len(start) < 1 or len(end) < 1:
        return []

    itemsTab = []

    n1S = start[0]
    n1E = start[1] if len(start) > 1 else ''
    match1P = start[2] if len(start) > 2 else None
    match1P = check(match1P) if isinstance(match1P, str) else match1P

    n2S = end[0]
    n2E = end[1] if len(end) > 1 else ''
    match2P = end[2] if len(end) > 2 else None
    match2P = check(match2P) if isinstance(match2P, str) else match2P

    lastIdx = 0
    search = 1

    if not (flags & IGNORECASE):
        sData = data
    else:
        sData = data.lower()
        n1S = n1S.lower()
        n1E = n1E.lower()
        n2S = n2S.lower()
        n2E = n2E.lower()

    while True:
        if search == 1:
            # node 1 - start
            idx1 = sData.find(n1S, lastIdx)
            if -1 == idx1:
                return itemsTab
            lastIdx = idx1 + len(n1S)
            idx2 = sData.find(n1E, lastIdx)
            if -1 == idx2:
                return itemsTab
            lastIdx = idx2 + len(n1E)

            if match1P and not match1P(data, sData, idx1 + len(n1S), idx2):
                continue

            search = 2
        else:
            # node 2 - end
            tIdx1 = sData.find(n2S, lastIdx)
            if -1 == tIdx1:
                return itemsTab
            lastIdx = tIdx1 + len(n2S)
            tIdx2 = sData.find(n2E, lastIdx)
            if -1 == tIdx2:
                return itemsTab
            lastIdx = tIdx2 + len(n2E)

            if match2P and not match2P(data, sData, tIdx1 + len(n2S), tIdx2):
                continue

            if flags & START_S:
                itemsTab.append(data[idx1:idx2 + len(n1E)])

            idx1 = idx1 if flags & START_E else idx2 + len(n1E)
            idx2 = tIdx2 + len(n2E) if flags & END_E else tIdx1

            itemsTab.append(data[idx1:idx2])

            if flags & END_S:
                itemsTab.append(data[tIdx1:tIdx2 + len(n2E)])

            search = 1

        if limits > 0 and len(itemsTab) >= limits:
            break
    return itemsTab


def rfindall(data, start, end=('',), flags=START_E | END_E, limits=-1):

    start = start if isinstance(start, tuple) or isinstance(start, list) else (start,)
    end = end if isinstance(end, tuple) or isinstance(end, list) else (end,)

    if len(start) < 1 or len(end) < 1:
        return []

    itemsTab = []

    n1S = start[0]
    n1E = start[1] if len(start) > 1 else ''
    match1P = start[2] if len(start) > 2 else None
    match1P = check(match1P) if isinstance(match1P, str) else match1P

    n2S = end[0]
    n2E = end[1] if len(end) > 1 else ''
    match2P = end[2] if len(end) > 2 else None
    match2P = check(match2P) if isinstance(match2P, str) else match2P

    lastIdx = len(data)
    search = 1

    if not (flags & IGNORECASE):
        sData = data
    else:
        sData = data.lower()
        n1S = n1S.lower()
        n1E = n1E.lower()
        n2S = n2S.lower()
        n2E = n2E.lower()

    while True:
        if search == 1:
            # node 1 - end
            idx1 = sData.rfind(n1S, 0, lastIdx)
            if -1 == idx1:
                return itemsTab
            lastIdx = idx1
            idx2 = sData.find(n1E, idx1 + len(n1S))
            if -1 == idx2:
                return itemsTab

            if match1P and not match1P(data, sData, idx1 + len(n1S), idx2):
                continue

            search = 2
        else:
            # node 2 - start
            tIdx1 = sData.rfind(n2S, 0, lastIdx)
            if -1 == tIdx1:
                return itemsTab
            lastIdx = tIdx1
            tIdx2 = sData.find(n2E, tIdx1 + len(n2S), idx1)
            if -1 == tIdx2:
                return itemsTab

            if match2P and not match2P(data, sData, tIdx1 + len(n2S), tIdx2):
                continue

            if flags & START_S:
                itemsTab.insert(0, data[idx1:idx2 + len(n1E)])

            s1 = tIdx1 if flags & START_E else tIdx2 + len(n2E)
            s2 = idx2 + len(n1E) if flags & END_E else idx1

            itemsTab.insert(0, data[s1:s2])

            if flags & END_S:
                itemsTab.insert(0, data[tIdx1:tIdx2 + len(n2E)])

            search = 1

        if limits > 0 and len(itemsTab) >= limits:
            break
    return itemsTab


def find(data, start, end=('',), flags=START_E | END_E):
    ret = findall(data, start, end, flags, 1)
    if ret:
        return True, ret[0]
    return False, ''


def rfind(data, start, end=('',), flags=START_E | END_E):
    ret = rfindall(data, start, end, flags, 1)
    if ret:
        return True, ret[0]
    return False, ''


def strip_doubles(data, pattern):
    while -1 < data.find(pattern + pattern) and '' != pattern:
        data = data.replace(pattern + pattern, pattern)
    return data


def cleanHtml(string):
    string = ensure_str(string)

    string = string.replace('<', ' <')
    string = string.replace('&nbsp;', ' ')
    string = string.replace('&nbsp', ' ')
    string = yt_cleanHtml(string)
    string = string.replace('\n', ' ').replace('\r', ' ').replace('\t', ' ')
    return strip_doubles(string, ' ').strip()


def std_url(url):
        url1=url
        #printDBG('url0='+url1)
        if r'\u0' in url1:
            url1 = url1.encode()
            url1 = str(url1.decode('unicode_escape',errors='ignore'))
        if '%' not in url1:
            url1=url1.replace('\\/','/')
            url1=url1.replace('://','repl1')
            url1=url1.replace('?','repl2')
            url1=url1.replace('&','repl3')
            url1=url1.replace('=','repl4')
            url1=url1.replace(':','repl5')
            #url1=unquote(url1)
            #printDBG(url1)
            url1=quote(url1)
            url1=url1.replace('repl1','://')
            url1=url1.replace('repl2','?')
            url1=url1.replace('repl3','&')
            url1=url1.replace('repl4','=')
            url1=url1.replace('repl5',':')
            #printDBG('url1='+url1)
        return url1

def decodeHtml(text):
    text = text.replace('  ...  ', '')
    text = text.replace('  by', '')
    text = text.replace('-&gt;', 'and')
    text = text.replace('…  ...    …', '')
    text = text.replace('／', '/')
    text = text.replace('\/', '/')
    text = text.replace('\\x22', '"')
    text = text.replace('\u00a0', ' ')
    text = text.replace('\u00b2', '²')
    text = text.replace('\u00b3', '³')
    text = text.replace('\u00c4', 'Ä')
    text = text.replace('\u00c9', 'É')
    text = text.replace('\u00d6', 'Ö')
    text = text.replace('\u00dc', 'Ü')
    text = text.replace('\u00df', 'ß')
    text = text.replace('\u00e1', 'á')
    text = text.replace('\u00e4', 'ä')
    text = text.replace('\u00e8', 'è')
    text = text.replace('\u00e9', 'é')
    text = text.replace('\u00f3', 'ó')
    text = text.replace('\u00f6', 'ö')
    text = text.replace('\u00f8', 'ø')
    text = text.replace('\u00fc', 'ü')
    text = text.replace('\u0105', 'ą')
    text = text.replace('\u0107', 'ć')
    text = text.replace('\u0119', '_')
    #text = text.replace('\u0119', 'ę')
    text = text.replace('\u0142', 'ł')
    text = text.replace('\u0142a', '_')
    text = text.replace('\u0144', 'ń')
    text = text.replace('\u0144', 'ń')
    text = text.replace('\u015a', '_')
    #text = text.replace('\u015a', 'Ś')
    text = text.replace('\u015b', '_')
    #text = text.replace('\u015b', 'ś')
    text = text.replace('\u017c', 'ż')
    text = text.replace('\u0308', '̈')
    text = text.replace('\u2013', "-")
    text = text.replace('\u2018', '‘')
    text = text.replace('\u2019', '’')
    text = text.replace('\u201c', '“')
    text = text.replace('\u201e', '„')
    text = text.replace('\u2026', '...')
    text = text.replace('\u202fh', 'h')
    text = text.replace('&#034;', '\'')
    text = text.replace('&#038;', '&')
    #text = text.replace('&#039;', '\'')
    text = text.replace('&#039;', "'")
    #text = text.replace('&#039', '\'')
    text = text.replace('&#133;', '')
    text = text.replace('&#160;', ' ')
    text = text.replace('&#174;', '')
    text = text.replace('&#196;', 'Ä')
    text = text.replace('&#214;', 'Ö')
    text = text.replace('&#220;', 'Ü')
    text = text.replace('&#223;', 'ß')
    text = text.replace('&#225;', 'a')
    text = text.replace('&#228;', 'ä')
    text = text.replace('&#233;', 'e')
    text = text.replace('&#243;', 'o')
    text = text.replace('&#246;', 'ö')
    text = text.replace('&#252;', 'ü')
    text = text.replace('&#34;', "'")
    text = text.replace('&#39;', '\'')
    text = text.replace('&#4', '')
    text = text.replace('&#40;', '')
    text = text.replace('&#41;', ')')
    text = text.replace('&#58;', ':')
    text = text.replace('&#8211;', '-')
    #text = text.replace('&#8211;', "-")
    text = text.replace('&#8216;', "'")
    #text = text.replace('&#8217;', '’')
    text = text.replace('&#8217;', "'")
    text = text.replace('&#8220;', '“')
    #text = text.replace('&#8220;', "'")
    text = text.replace('&#8221;', '"')
    #text = text.replace('&#8222;', ',')
    text = text.replace('&#8222;', '„')
    #text = text.replace('&#8230;', '...')
    text = text.replace('&#8230;', '…')
    text = text.replace('&#x27;', "'")
    text = text.replace('&aacute;', 'a')
    text = text.replace('&acute;', '\'')
    text = text.replace('&amp;', '&')
    text = text.replace('&amp;#39;', '\'')
    text = text.replace('&amp;quot;', '"')
    text = text.replace('&apos;', "'")
    text = text.replace('&atilde;', "'")
    text = text.replace('&auml;', 'ä')
    text = text.replace('&Auml;', 'Ä')
    text = text.replace('&bdquo;', '"')
    text = text.replace('&colon;', ':')
    text = text.replace('&comma;', ',')
    text = text.replace('&commmat;', ' ')
    text = text.replace('&eacute;', 'e')
    text = text.replace('&excl;', '!')
    text = text.replace('&gt;', '>')
    text = text.replace('&lbrack;', '[')
    text = text.replace('&ldquo;', '"')
    text = text.replace('&lowbar;', '_')
    text = text.replace('&lpar;', '(')
    text = text.replace('&lsquo;', '\'')
    text = text.replace('&middot;', '')
    text = text.replace('&nbsp;', '')
    text = text.replace('&ndash;', '-')
    text = text.replace('&ntilde;', 'n')
    text = text.replace('&num;', '#')
    text = text.replace('&oacute;', 'ó')
    text = text.replace('&ouml;', 'ö')
    text = text.replace('&percnt;', '%')
    text = text.replace('&period;', '.')
    text = text.replace('&plus;', '+')
    text = text.replace('&quot_', '\"')
    text = text.replace('&quot;', '\"')
    text = text.replace('&rdquo;', '"')
    text = text.replace('&rpar;', ')')
    text = text.replace('&rsqb;', ']')
    text = text.replace('&rsquo;', '\'')
    text = text.replace('&rsquo;', '\'')
    text = text.replace('&semi;', '')
    text = text.replace('&sol;', '/')
    text = text.replace('&szlig;', 'ß')
    text = text.replace('&uuml;', 'ü')
    text = text.replace('&Uuml;', 'Ü')
    text = text.replace('#038;', '')
    text = text.replace('#8217;', "'")

    return text

def listToDir(cList, idx):
    cTree = {'dat': ''}
    deep = 0
    while (idx + 1) < len(cList):
        if cList[idx].startswith('<ul') or cList[idx].startswith('<li'):
            deep += 1
            nTree, idx, nDeep = listToDir(cList, idx + 1)
            if 'list' not in cTree:
                cTree['list'] = []
            cTree['list'].append(nTree)
            deep += nDeep
        elif cList[idx].startswith('</ul>') or cList[idx].startswith('</li>'):
            deep -= 1
            idx += 1
        else:
            cTree['dat'] += cList[idx]
            idx += 1
        if deep < 0:
            break
    return cTree, idx, deep

def getSearchGroups(data, pattern, grupsNum=1, ignoreCase=False):
    return search(data, pattern, IGNORECASE if ignoreCase else 0, grupsNum)

def getDataBetweenReMarkers(data, pattern1, pattern2, withMarkers=True):
    match1 = pattern1.search(data)
    if None == match1 or -1 == match1.start(0):
        return False, ''
    match2 = pattern2.search(data[match1.end(0):])
    if None == match2 or -1 == match2.start(0):
        return False, ''
    if withMarkers:
        return True, data[match1.start(0): (match1.end(0) + match2.end(0))]
    else:
        return True, data[match1.end(0): (match1.end(0) + match2.start(0))]

def getDataBetweenMarkers(data, marker1, marker2, withMarkers=True, caseSensitive=True):
    flags = 0
    if withMarkers:
        flags |= START_E | END_E
    if not caseSensitive:
        flags |= IGNORECASE
    return find(data, marker1, marker2, flags)

def getAllItemsBetweenMarkers(data, marker1, marker2, withMarkers=True, caseSensitive=True):
    flags = 0
    if withMarkers:
        flags |= START_E | END_E
    if not caseSensitive:
        flags |= IGNORECASE
    return findall(data, marker1, marker2, flags)

def rgetAllItemsBetweenMarkers(data, marker1, marker2, withMarkers=True, caseSensitive=True):
    flags = 0
    if withMarkers:
        flags |= START_E | END_E
    if not caseSensitive:
        flags |= IGNORECASE
    return rfindall(data, marker1, marker2, flags)

def rgetDataBetweenMarkers2(data, marker1, marker2, withMarkers=True, caseSensitive=True):
    flags = 0
    if withMarkers:
        flags |= START_E | END_E
    if not caseSensitive:
        flags |= IGNORECASE
    return rfind(data, marker1, marker2, flags)

def rgetDataBetweenMarkers(data, marker1, marker2, withMarkers=True):
    # this methods is not working as expected, but is is used in many places
    # so I will leave at it is, please use rgetDataBetweenMarkers2
    idx1 = data.rfind(marker1)
    if -1 == idx1:
        return False, ''
    idx2 = data.rfind(marker2, idx1 + len(marker1))
    if -1 == idx2:
        return False, ''
    if withMarkers:
        idx2 = idx2 + len(marker2)
    else:
        idx1 = idx1 + len(marker1)
    return True, data[idx1:idx2]

def getDataBetweenNodes(data, node1, node2, withNodes=True, caseSensitive=True):
    flags = 0
    if withNodes:
        flags |= START_E | END_E
    if not caseSensitive:
        flags |= IGNORECASE
    return find(data, node1, node2, flags)

def getAllItemsBetweenNodes(data, node1, node2, withNodes=True, numNodes=-1, caseSensitive=True):
    flags = 0
    if withNodes:
        flags |= START_E | END_E
    if not caseSensitive:
        flags |= IGNORECASE
    return findall(data, node1, node2, flags, limits=numNodes)

def rgetDataBetweenNodes(data, node1, node2, withNodes=True, caseSensitive=True):
    flags = 0
    if withNodes:
        flags |= START_E | END_E
    if not caseSensitive:
        flags |= IGNORECASE
    return rfind(data, node1, node2, flags)

def rgetAllItemsBetweenNodes(data, node1, node2, withNodes=True, numNodes=-1, caseSensitive=True):
    flags = 0
    if withNodes:
        flags |= START_E | END_E
    if not caseSensitive:
        flags |= IGNORECASE
    return rfindall(data, node1, node2, flags, limits=numNodes)
# this method is useful only for developers
# to dump page code to the file

def writeToFile(file, data, mode="w"):
    #helper to see html returned by ajax
    file_path = file
    text_file = open(file_path, mode)
    text_file.write(data)
    text_file.close()

def getNormalizeStr(txt, idx=None):
    POLISH_CHARACTERS = {'ą': 'a', 'ć': 'c', 'ę': 'ę', 'ł': 'l', 'ń': 'n', 'ś': 's', 'ż': 'z', 'ź': 'z',
                         'Ą': 'A', 'Ć': 'C', 'Ę': 'E', 'Ł': 'L', 'Ń': 'N', 'Ś': 'S', 'Ż': 'Z', 'Ź': 'Z',
                         'á': 'a', 'é': 'e', 'í': 'i', 'ñ': 'n', 'ó': 'o', 'ú': 'u', 'ü': 'u',
                         'Á': 'A', 'É': 'E', 'Í': 'I', 'Ñ': 'N', 'Ó': 'O', 'Ú': 'U', 'Ü': 'U',
                        }
    if isinstance(txt,bytes):
        txt = txt.decode('utf-8')
    if None is not idx:
        txt = txt[idx]
    nrmtxt = unicodedata.normalize('NFC', txt)
    ret_str = []
    for item in nrmtxt:
        if ord(item) > 128:
            item = POLISH_CHARACTERS.get(item)
            if item:
                ret_str.append(item)
        else: # pure ASCII character
            ret_str.append(item)
    return ''.join(ret_str)

def isalpha(txt, idx=None):
    return getNormalizeStr(txt, idx).isalpha()

def uniform_title(title,year_op=0):
        title=title.replace('مشاهدة وتحميل مباشر','').replace('مشاهدة','').replace('اون لاين','')
        tag_type   = ['مدبلج للعربية','مترجمة للعربية','مترجم للعربية', 'مدبلجة', 'مترجمة' , 'مترجم' , 'مدبلج', 'مسلسل', 'عرض', 'انمي', 'فيلم']
        tag_qual   = ['1080p','720p','WEB-DL','BluRay','DVDRip','HDCAM','HDTC','HDRip', 'HD', '1080P','720P','DVBRip','TVRip','DVD','SD']
        tag_saison = [('الموسم الثاني','02'),('الموسم الاول','01'),('الموسم الثالث','03'),('الموسم الرابع','04'),('الموسم الخامس','05'),('الموسم السادس','06'),('الموسم السابع','07'),('الموسم الثامن','08'),('الموسم التاسع','09'),('الموسم العاشر','10')]
        type_ =  'Type: '
        qual =  'Quality: '
        sais =  'Saison: '
        desc=''
        saison=''

        for elm in tag_saison:
            if elm[0] in title:
                sais=sais+elm[1]
                title = title.replace(elm[0],'')
                break

        for elm in tag_type:
            if elm in title:
                title = title.replace(elm,'')
                type_ = type_+elm+' | '
        for elm in tag_qual:
            if elm in title:
                #re_st = re.compile(re.escape(elm.lower()), re.IGNORECASE)
                #title=re_st.sub('', title)
                title = title.replace(elm,'')
                qual = qual+elm+' | '

        data = re.findall('((?:19|20)\d{2})', title, re.S)
        if data:
            year_ = data[-1]
            title = title.replace(year_, '')
            if year_op==0:
                desc = 	 'Year: '+year_+'\n'
            elif year_op==-1:
                desc = 	''
            elif year_op==1:
                desc = 	 'Year: '+year_+'\n'
            elif year_op==2:
                desc = 	year_

        if year_op<2:
            if sais !=  'Saison: ':
                desc = desc+sais+'\n'
            if type_!= 'Type: ':
                desc = desc+type_[:-3]+'\n'
            if qual !=  'Quality: ':
                desc = desc+qual[:-3]+'\n'

        pat = 'موسم.*?([0-9]{1,2}).*?حلقة.*?([0-9]{1,2})'

        data = re.search(pat, title, re.S)
        if data:
            sa = data[1]
            ep = data[2]
            if len(sa)==1: sa='0'+sa
            if len(ep)==1: ep='0'+ep
            ep_out = 'S'+sa+'E'+ep
            title = ep_out+' '+re.sub(pat,'',title)
            title = title.replace('ال ','')

        return desc, cleanHtml(title).replace('()','').strip()

def getItemHash(displayItem):
    hashAlg = MD5()
    if isinstance(displayItem, dict):
        hashData = f"{displayItem.get('title').lower()}_{displayItem.get('type').lower()}"
    else:
        hashData = f"{displayItem.title.lower()}_{displayItem.type.name.lower()}"

    hashData = hexlify(hashAlg(hashData))
    return ensure_str(hashData)

def eval_all_packed_func(data):
    result=[]
    scripts = re.findall(r"(eval\s?\(function\(p,a,c,k,e,d.*?)</script>", data, re.S)
    for script in scripts:
        if not script.endswith('\n'):
            script = script + "\n"
        # mods
        script = script.replace("eval(function(p,a,c,k,e,d", "pippo = function(p,a,c,k,e,d")
        script = script.replace("return p}(", "print(p)}\n\npippo(")
        script = script.replace("))\n", ");\n")

        # duktape
        from Plugins.Extensions.IPTVPlayer.tools.e2ijs import js_execute
        ret = js_execute(script)
        if decoded := ret['data']:
            result.append(decoded)
    return result
