# -*- coding: utf-8 -*-

import base64
import binascii
import hashlib
import json
import re
import string
import time
from binascii import hexlify, unhexlify
import random
from urllib.parse import urljoin, urlparse, urlunparse, parse_qs, unquote, urlencode

from Components.config import config

from Plugins.Extensions.IPTVPlayer.components.captcha_helper import CaptchaHelper
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import SetIPTVPlayerLastHostError, IPTVPlayerSleep
from Plugins.Extensions.IPTVPlayer.iptvdm.iptvdh import DMHelper
from Plugins.Extensions.IPTVPlayer.libs.dehunt import dehunt

from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, CSelOneLink, GetCookieDir, rm, \
                                                        GetJSScriptFile
from Plugins.Extensions.IPTVPlayer.tools.e2ijs import js_execute, js_execute_ext

from Plugins.Extensions.IPTVPlayer.libs import aadecode, ph
from Plugins.Extensions.IPTVPlayer.libs import pyaes
from Plugins.Extensions.IPTVPlayer.libs.demjson import decode as demjson_loads
from Plugins.Extensions.IPTVPlayer.libs.pCommon import common, getDefaultUserAgent, getDefaultHeader, getBaseUrl, isValidUrl
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import unpackJSPlayerParams, \
                                                               VIDUPME_decryptPlayerParams,    \
                                                               SAWLIVETV_decryptPlayerParams,  \
                                                               TEAMCASTPL_decryptPlayerParams, \
                                                               KINGFILESNET_decryptPlayerParams, \
                                                               captchaParser, \
                                                               getDirectM3U8Playlist, \
                                                               getMPDLinksWithMeta, \
                                                               decorateUrl \


from Plugins.Extensions.IPTVPlayer.components.asynccall import iptv_execute, MainSessionWrapper
from Plugins.Extensions.IPTVPlayer.libs.youtube_dl.utils import cleanHtml, get_domain

from Screens.MessageBox import MessageBox
class urlparser:
    def __init__(self):
        self.cm = common()
        self.pp = pageParser()
        self.setHostsMap()

    @staticmethod
    def getDomain(url, onlyDomain=True):
        uri = urlparse(url)
        if onlyDomain:
            domain = f'{uri.netloc}'
        else:
            domain = f'{uri.scheme}://{uri.netloc}/'
        return domain

    @staticmethod
    def decorateUrl(url, metaParams=None):
        if metaParams is None: metaParams={}
        return decorateUrl(url, metaParams)

    @staticmethod
    def decorateParamsFromUrl(baseUrl, overwrite=False):
        printDBG("urlparser.decorateParamsFromUrl ---> %s" % baseUrl)
        tmp = baseUrl.split('|')
        baseUrl = strwithmeta(tmp[0].strip(), strwithmeta(baseUrl).meta)
        KEYS_TAB = list(DMHelper.HANDLED_HTTP_HEADER_PARAMS)
        KEYS_TAB.extend(["iptv_audio_url", "iptv_proto", "Host", "Accept", "MPEGTS-Live", "PROGRAM-ID"])
        if 2 == len(tmp):
            baseParams = tmp[1].strip()
            try:
                params = parse_qs(baseParams)
                printDBG("PARAMS FROM URL [%s]" % params)
                for key in list(params.keys()):
                    if key not in KEYS_TAB:
                        continue
                    if not overwrite and key in baseUrl.meta:
                        continue
                    try:
                        baseUrl.meta[key] = params[key][0]
                    except Exception:
                        printExc()
            except Exception:
                printExc()
        baseUrl = urlparser.decorateUrl(baseUrl)
        return baseUrl

    def preparHostForSelect(self, v, resolveLink=False):
        valTab = []
        i = 0
        if len(v) > 0:
            for url in (list(v.values()) if isinstance(v,dict) else v):
                if 1 == self.checkHostSupport(url):
                    hostName = self.getHostName(url, True)
                    i = i + 1
                    if resolveLink:
                        url = self.getVideoLink(url)
                    if isinstance(url, str) and url.startswith('http'):
                        valTab.append({'name': (str(i) + '. ' + hostName), 'url': url})
        return valTab

    def setHostsMap(self):
        parser2HosterMap = {
            #self.pp.parserANAFAST: ['anafast.cc', 'anafast.online'],
            self.pp.parserCLIPWATCHINGCOM: ['clipwatching.com'],
            self.pp.parserDAILYMOTION: ['dailymotion.com',],
            self.pp.parserDOOD: ['doodstream.com','doodstream.co','dood.cx','dood.la','dood.pm','dood.re','dood.so','dood.to',
                                'dood.watch','dood.ws','dood.wf', 'dood.yt', 'dooood.com', 'doods.pro','ds2play.com','d0o0d.com',
                                  'do0od.com', 'd0000d.com', 'd000d.com'],
            self.pp.parserEVOLOADIO: ['evoload.io',],
            # self.pp.parserFEMBED: ['javstream.top', 'javpoll.com', 'suzihaza.com', 'fembed.net', 'ezsubz.com', 'reeoov.tube',
            #                         'diampokusy.com', 'filmvi.xyz', 'vidsrc.xyz', 'i18n.pw', 'vanfem.com', 'fembed9hd.com',
            #                         'votrefilms.xyz', 'watchjavnow.xyz', 'ncdnstm.xyz', 'albavide.xyz', 'kitabmarkaz.xyz',
            #                         'mycloudzz.com', 'dmcdn.xyz', 'dmcdn2.xyz', 'gdstream.net'],
            # self.pp.parserFILEMOON: ['filemoon.sx','filemoon.to','filemoon.in','filemoon.link','filemoon.nl', 'filemoon.wf', 'cinegrab.com',
            #                          'filemoon.eu', 'filemoon.art', 'moonmov.pro'],
            self.pp.parserFLASHXTV: ['flashx.co', 'flashx.net', 'flashx.pw', 'flashx.tv'],
            self.pp.parserHIGHLOADTO: ['highload.to'],
            self.pp.parserIMDBCOM: ['imdb.com',],
            self.pp.parserKINOGERRE: ['kinoger.pw','kinoger.be', 'kinoger.ru','kinoger.re','kinoger.to',],
            self.pp.parserLINKBOXTO: ['linkbox.to', ],
            self.pp.parserMIXDROP: ['mixdrop.bz', 'mixdrop.ch','mixdrop.club','mixdrop.co','mixdrop.sx','mixdrop.to',
                                    'mixdrp.co','mixdrp.to', 'mixdrop.gl', 'mixdrop.vc', 'mdbekjwqa.pw', 'mdy48tn97.com',
                                    'mixdropjmk.pw','mdzsmutpcvykb.net', 'mixdrop.si'],
            self.pp.parserNETUTV: ['netu.to','netu.tv', 'waaw.to', 'waaw.tv','hqq.none','hqq.to', 'hqq.tv','hqq.watch',],
            self.pp.parserMOSHAHDANET: ['moshahda.net'],
            self.pp.parserOKRU: ['ok.ru'],
            self.pp.parserONLYSTREAM: ['upstream.to', 'vtube.to','vupload.com','vtplay.net', 'vtbe.net', 'vtbe.to', 'vtube.network'],
            self.pp.parserONLYSTREAMTV: ['yodbox.com', 'dropload.io'],
            self.pp.parserPROTONVIDEO: ['protonvideo.to'],
            self.pp.parserSCCTONLINE: ['fsst.online'],
            self.pp.parserSKYVID: ['skyvid.cyou'],
            self.pp.parserSTREAMLARE: ['streamlare.com' ],
            self.pp.parserSTREAMSB: ['sbembed.com', 'sbembed1.com', 'sbplay.org', 'sbvideo.net', 'streamsb.net', 'sbplay.one',
                                    'cloudemb.com', 'playersb.com', 'tubesb.com', 'sbplay1.com', 'embedsb.com', 'watchsb.com',
                                    'sbplay2.com', 'japopav.tv', 'viewsb.com', 'sbplay2.xyz', 'sbfast.com', 'sbfull.com',
                                    'javplaya.com', 'ssbstream.net', 'p1ayerjavseen.com', 'sbthe.com', 'vidmovie.xyz',
                                    'sbspeed.com', 'streamsss.net', 'sblanh.com', 'tvmshow.com', 'sbanh.com', 'streamovies.xyz',
                                    'embedtv.fun', 'sblongvu.com', 'arslanrocky.xyz', 'sbchill.com'],
            self.pp.parserSTREAMTAPE: ['streamtape.com', 'strtape.cloud', 'streamtape.net', 'streamta.pe', 'streamtape.site', 'strcloud.link', 'strtpe.link',
                                       'streamtape.cc', 'scloud.online', 'stape.fun', 'streamtape.to','streamadblockplus.com', 'shavetape.cash', 'streamta.site'],
            self.pp.parserSTREAMZ: ['streamz.ws','streamz.cc','streamz.vg','streamzz.to',],
            self.pp.parserSUPERVIDEO: ['supervideo.tv'],
            self.pp.parserSVETACDNIN: ['svetacdn.in'],
            self.pp.parserTHEVIDEOME: ['thevideo.me','thevideome.com'],
            self.pp.parserTUBELOADCO: ['tubeload.co'],
            self.pp.parserUPTOSTREAMCOM: ['uptobox.com', 'uptostream.com'],
            self.pp.parserUSERLOADCO: ['userload.co'],
            self.pp.parserUSTREAMIN: ['u-stream.in'],
            self.pp.parserVCRYPT: ['streamcrypt.net'],
            self.pp.parserVEEV: ['veev.to'],
            self.pp.parserVIDBOMCOM: ['vadbom.com', 'vidbam.org', 'vadbam.com', 'vadbam.net', 'myviid.com', 'myviid.net', 'myvid.com',
                                        'vidshare.com', 'vedsharr.com', 'vedshar.com', 'vedshare.com', 'vadshar.com', 'vidshar.org'],
            self.pp.parserVIDCLOUD: ['vidcloud.co'],
            # self.pp.parserVIDEOBIN: ['videobin.co'],
            self.pp.parserVIDGUARD: ['vidguard.to', 'vgfplay.com', 'vgembed.com', 'moflix-stream.day','v6embed.xyz', 'vid-guard.com',
                                     'vembed.net', 'fslinks.org','embedv.net', 'bembed.net'],
            self.pp.parserVIDMOLYME: ['ashortl.ink','vidmoly.me','vidmoly.net','vidmoly.to'],
            self.pp.parserVIDOZANET: ['vidoza.co', 'vidoza.net'],
            self.pp.parserVIDSPEEDS: ['vidspeeds.com'],
            self.pp.parserVIMEOCOM: ['vimeo.com'],
            self.pp.parserVIVOSX: ['vivo.sx'],
            self.pp.parserVK: ['vk.com'] ,
            self.pp.parserVOESX: ['voe.sx', 'voe-unblock.com', 'voe-unblock.net', 'voeunblock.com',
                                    'voeunbl0ck.com', 'voeunblck.com', 'voeunblk.com', 'voe-un-block.com',
                                    'voeun-block.net', 'un-block-voe.net', 'v-o-e-unblock.com',
                                    'audaciousdefaulthouse.com', 'launchreliantcleaverriver.com',
                                    'reputationsheriffkennethsand.com', 'fittingcentermondaysunday.com',
                                    'housecardsummerbutton.com', 'fraudclatterflyingcar.com',
                                    'bigclatterhomesguideservice.com', 'uptodatefinishconferenceroom.com',
                                    'realfinanceblogcenter.com', 'tinycat-voe-fashion.com',
                                    '20demidistance9elongations.com', 'telyn610zoanthropy.com', 'toxitabellaeatrebates306.com',
                                    'greaseball6eventual20.com', '745mingiestblissfully.com', '19turanosephantasia.com',
                                    '30sensualizeexpression.com', '321naturelikefurfuroid.com', '449unceremoniousnasoseptal.com',
                                    'guidon40hyporadius9.com', 'cyamidpulverulence530.com', 'boonlessbestselling244.com',
                                    'antecoxalbobbing1010.com', 'matriculant401merited.com', 'scatch176duplicities.com',
                                    'availedsmallest.com', 'counterclockwisejacky.com', 'simpulumlamerop.com',
                                    'metagnathtuggers.com', 'gamoneinterrupted.com', 'chromotypic.com', 'crownmakermacaronicism.com', 'generatesnitrosate.com'],
            self.pp.parserVSHAREEU: ['vshare.eu'],
            # self.pp.parserWOLFSTREAM: ['wolfstream.tv', ],
            self.pp.parserYOUTUBE: ['youtube.com'] ,
            self.pp.parserUNIVERSAL: ['streamdav.com', 'reviewrate.net','uqload.com', 'filelions.online', 'filelions.com', 'filelions.to', 'ajmidyadfihayh.sbs', 'alhayabambi.sbs', 'techradar.ink',
                                      'moflix-stream.click', 'azipcdn.com', 'mlions.pro', 'alions.pro', 'dlions.pro','filelions.live','motvy55.store', 'filelions.xyz', 'lumiawatch.top',
                                      'fajer.live', 'show.alfajertv.com','streamvid.net' ,'streamhide.to', 'guccihide.com', 'zillastream.com','goved.org','govad.xyz','film77.xyz','hdup2.xyz', 'hdup.one',
                                      'filemoon.sx','filemoon.to','filemoon.in','filemoon.link','filemoon.nl', 'filemoon.wf', 'cinegrab.com',
                                     'filemoon.eu', 'filemoon.art', 'moonmov.pro','videobin.co'],

        }

        self.hostMap = {}
        for parser,hosters in parser2HosterMap.items():
            for hoster in hosters:
                if self.hostMap.get(hoster,None):
                    printExc(f'{parser.__name__} : {hoster} already assigned.')
                self.hostMap[hoster]= parser

    @staticmethod
    def getHostName(url, nameOnly=False, withPort=False):
        hostName = strwithmeta(url).meta.get('host_name', '')
        if not hostName:
            match = re.search('https?://(?:www.)?(.+?)/', url)
            if match:
                hostName = match.group(1)
                if not withPort and ':' in hostName:
                    hostName = hostName.split(':')[0]
                if nameOnly:
                    n = hostName.split('.')
                    try:
                        hostName = n[-2]
                    except Exception:
                        printExc()
            hostName = hostName.lower()
        printDBG("_________________getHostName: [%s] -> [%s]" % (url, hostName))
        return hostName

    def getParser(self, url, host=None):
        if host is None:
            host = urlparser.getHostName(url)
        parser = self.hostMap.get(host, None)
        if parser is None:
            host2 = host[host.find('.') + 1:]
            printDBG('urlparser.getParser II try host[%s]->host2[%s]' % (host, host2))
            parser = self.hostMap.get(host2, None)
            if parser is None:
                parser = self.pp.parserUNIVERSAL
        return parser

    def checkHostSupport(self, url):
        # -1 - not supported
        #  0 - unknown
        #  1 - supported
        host = self.getHostName(url)

        # quick fix
        if host == 'facebook.com' and 'likebox.php' in url or 'like.php' in url or '/groups/' in url:
            return 0

        ret = 0
        parser = self.getParser(url, host)
        if parser:
            return 1
        if self.isHostsNotSupported(host):
            return -1
        return ret

    def isHostsNotSupported(self, host):
        return host in ['rapidgator.net', 'oboom.com']

    def getVideoLinkExt(self, url):
        videoTab = []
        try:
            ret = self.getVideoLink(url, True)

            if isinstance(ret, str):
                if 0 < len(ret):
                    host = self.getHostName(url)
                    videoTab.append({'name': host, 'url': ret})
            elif isinstance(ret, list) or isinstance(ret, tuple):
                videoTab = ret

            for elem in videoTab:
                if not isValidUrl(url):
                    continue
                url = strwithmeta(elem['url'])
                if 'User-Agent' not in url.meta:
                    url.meta['User-Agent'] = getDefaultUserAgent()
                    elem['url'] = url
        except Exception:
            printExc()

        return videoTab

    def getVideoLink(self, url, acceptsList=False):
        try:
            url = self.decorateParamsFromUrl(url)
            nUrl = ''
            parser = self.getParser(url)
            if None is not parser:
                nUrl = parser(url)
            else:
                host = self.getHostName(url)
                if self.isHostsNotSupported(host):
                    SetIPTVPlayerLastHostError(_('Hosting "%s" not supported.') % host)
                else:
                    SetIPTVPlayerLastHostError(_('Hosting "%s" unknown.') % host)

            if isinstance(nUrl, (list, tuple)):
                if acceptsList:
                    return nUrl
                if len(nUrl) > 0:
                    return nUrl[0]['url']
                return False
            return nUrl
        except Exception:
            printExc()
        return False


class pageParser(CaptchaHelper):
    HTTP_HEADER = {'User-Agent': getDefaultUserAgent(),
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'Content-type': 'application/x-www-form-urlencoded'}

    def __init__(self):
        self.cm = common()
        self.captcha = captchaParser()
        self.ytParser = None
        self.moonwalkParser = None
        self.vevoIE = None
        self.bbcIE = None
        self.sportStream365ServIP = None

        #config
        self.COOKIE_PATH = GetCookieDir('')
        self.jscode = {}
        self.jscode['jwplayer'] = 'window=this; function stub() {}; function jwplayer() {return {setup:function(){print(JSON.stringify(arguments[0]))}, onTime:stub, onPlay:stub, onComplete:stub, onReady:stub, addButton:stub}}; window.jwplayer=jwplayer;'

    def getPageCF(self, baseUrl, addParams={}, post_data=None):
        addParams['cloudflare_params'] = {'cookie_file': addParams['cookiefile'], 'User-Agent': addParams['header']['User-Agent']}
        sts, data = self.cm.getPageCFProtection(baseUrl, addParams, post_data)
        return sts, data

    def getYTParser(self):
        if self.ytParser is None:
            try:
                from .youtubeparser import YouTubeParser
                self.ytParser = YouTubeParser()
            except Exception:
                printExc()
                self.ytParser = None
        return self.ytParser

    def getVevoIE(self):
        if self.vevoIE is None:
            try:
                from Plugins.Extensions.IPTVPlayer.libs.youtube_dl.extractor.vevo import VevoIE
                self.vevoIE = VevoIE()
            except Exception:
                self.vevoIE = None
                printExc()
        return self.vevoIE

    def getBBCIE(self):
        if self.bbcIE is None:
            try:
                from Plugins.Extensions.IPTVPlayer.libs.youtube_dl.extractor.bbc import BBCCoUkIE
                self.bbcIE = BBCCoUkIE()
            except Exception:
                self.bbcIE = None
                printExc()
        return self.bbcIE

    def getMoonwalkParser(self):
        if self.moonwalkParser is None:
            try:
                from .moonwalkcc import MoonwalkParser
                self.moonwalkParser = MoonwalkParser()
            except Exception:
                printExc()
                self.moonwalkParser = None
        return self.moonwalkParser

    def _getSources(self, data):
        printDBG('>>>>>>>>>> _getSources')
        urlTab = []
        tmp = ph.getDataBetweenMarkers(data, 'sources', ']')[1]
        if tmp != '':
            tmp = tmp.replace('\\', '')
            tmp = tmp.split('}')
            urlAttrName = 'file'
            sp = ':'
        else:
            tmp = ph.getAllItemsBetweenMarkers(data, '<source', '>', withMarkers=True)
            urlAttrName = 'src'
            sp = '='
        printDBG(tmp)
        for item in tmp:
            url = ph.getSearchGroups(item, r'''['"]?{0}['"]?\s*{1}\s*['"](https?://[^"^']+)['"]'''.format(urlAttrName, sp))[0]
            if not isValidUrl(url):
                continue
            name = ph.getSearchGroups(item, r'''['"]?label['"]?\s*''' + sp + r'''\s*['"]?([^"^'^\,^\{]+)['"\,\{]''')[0]

            printDBG('---------------------------')
            printDBG('url:  ' + url)
            printDBG('name: ' + name)
            printDBG('+++++++++++++++++++++++++++')
            printDBG(item)

            if 'flv' in item:
                if name == '':
                    name = '[FLV]'
                urlTab.insert(0, {'name': name, 'url': url})
            elif 'mp4' in item:
                if name == '':
                    name = '[MP4]'
                urlTab.append({'name': name, 'url': url})

        return urlTab

    def _findLinks(self, data, serverName='', linkMarker=r'''['"]?file['"]?[ ]*:[ ]*['"](http[^"^']+)['"][,}]''', m1='sources', m2=']', contain='', meta={}):
        linksTab = []

        def _isSmil(data):
            return data.split('?')[0].endswith('.smil')

        def _getSmilUrl(url):
            if _isSmil(url):
                SWF_URL = ''
                # get stream link
                sts, data = self.cm.getPage(url)
                if sts:
                    base = ph.getSearchGroups(data, 'base="([^"]+?)"')[0]
                    src = ph.getSearchGroups(data, 'src="([^"]+?)"')[0]
                    #if ':' in src:
                    #    src = src.split(':')[1]
                    if base.startswith('rtmp'):
                        return base + '/' + src + ' swfUrl=%s pageUrl=%s' % (SWF_URL, url)
            return ''

        subTracks = []
        subData = ph.getDataBetweenReMarkers(data, re.compile(r'''['"]?tracks['"]?\s*?:'''), re.compile(']'), False)[1].split('}')
        for item in subData:
            kind = ph.getSearchGroups(item, r'''['"]?kind['"]?\s*?:\s*?['"]([^"^']+?)['"]''')[0].lower()
            if kind != 'captions':
                continue
            src = ph.getSearchGroups(item, r'''['"]?file['"]?\s*?:\s*?['"](https?://[^"^']+?)['"]''')[0]
            if src == '':
                continue
            label = ph.getSearchGroups(item, r'''label['"]?\s*?:\s*?['"]([^"^']+?)['"]''')[0]
            format = src.split('?', 1)[0].split('.')[-1].lower()
            if format not in ['srt', 'vtt']:
                continue
            if 'empty' in src.lower():
                continue
            subTracks.append({'title': label, 'url': src, 'lang': 'unk', 'format': 'srt'})

        srcData = ph.getDataBetweenMarkers(data, m1, m2, False)[1].split('},')
        for item in srcData:
            item += '},'
            if contain != '' and contain not in item:
                continue
            link = ph.getSearchGroups(item, linkMarker)[0].replace('\/', '/')
            if '%3A%2F%2F' in link and '://' not in link:
                link = unquote(link)
            link = strwithmeta(link, meta)
            label = ph.getSearchGroups(item, r'''['"]?label['"]?[ ]*:[ ]*['"]([^"^']+)['"]''')[0]
            if _isSmil(link):
                link = _getSmilUrl(link)
            if '://' in link:
                proto = 'mp4'
                if link.startswith('rtmp'):
                    proto = 'rtmp'
                if link.split('?')[0].endswith('m3u8'):
                    tmp = getDirectM3U8Playlist(link)
                    linksTab.extend(tmp)
                else:
                    linksTab.append({'name': '%s %s' % (proto + ' ' + serverName, label), 'url': link})
                printDBG('_findLinks A')

        if 0 == len(linksTab):
            printDBG('_findLinks B')
            link = ph.getSearchGroups(data, linkMarker)[0].replace(r'\/', '/')
            link = strwithmeta(link, meta)
            if _isSmil(link):
                link = _getSmilUrl(link)
            if '://' in link:
                proto = 'mp4'
                if link.startswith('rtmp'):
                    proto = 'rtmp'
                linksTab.append({'name': proto + ' ' + serverName, 'url': link})

        if subTracks:
            for elem in linksTab:
                elem['url'] = urlparser.decorateUrl(elem['url'], {'external_sub_tracks': subTracks})

        return linksTab

    def _parserUNIVERSAL_A(self, baseUrl, embedUrl, _findLinks, _preProcessing=None, httpHeader={}, params={}):
        HTTP_HEADER = {'User-Agent': getDefaultUserAgent()}
        if 'Referer' in strwithmeta(baseUrl).meta:
            HTTP_HEADER['Referer'] = strwithmeta(baseUrl).meta['Referer']
        HTTP_HEADER.update(httpHeader)

        if 'embed' not in baseUrl and '{0}' in embedUrl:
            video_id = ph.getSearchGroups(baseUrl + '/', '/([A-Za-z0-9]{12})[/.-]')[0]
            url = embedUrl.format(video_id)
        else:
            url = baseUrl

        params = dict(params)
        params.update({'header': HTTP_HEADER})
        post_data = None

        if params.get('cfused', False):
            sts, data = self.getPageCF(url, params, post_data)
        else:
            sts, data = self.cm.getPage(url, params, post_data)
        if not sts:
            return False

        #printDBG(data)
        data = re.sub(r"<!--[\s\S]*?-->", "", data)
        #data = re.sub("/\*[\s\S]*?\*/", "", data)

        errMarkers = ['File was deleted', 'File Removed', 'File Deleted.', 'File Not Found', 'FILES HAVE BEEN PERMANENTLY DELETED']
        for errMarker in errMarkers:
            if errMarker in data:
                SetIPTVPlayerLastHostError(errMarker)

        if _preProcessing is not None:
            data = _preProcessing(data)
        printDBG("Data: " + data)

        # get JS player script code from confirmation page
        vplayerData = ''
        tmp = ph.getAllItemsBetweenNodes(data, ('<script', '>'), ('</script', '>'), False)
        for item in tmp:
            if 'eval(' in item and 'vplayer' in item:
                vplayerData = item

        if vplayerData != '':
            jscode = base64.b64decode('''ZnVuY3Rpb24gc3R1Yigpe31mdW5jdGlvbiBqd3BsYXllcigpe3JldHVybntzZXR1cDpmdW5jdGlvbigpe3ByaW50KEpTT04uc3RyaW5naWZ5KGFyZ3VtZW50c1swXSkpfSxvblRpbWU6c3R1YixvblBsYXk6c3R1YixvbkNvbXBsZXRlOnN0dWIsb25SZWFkeTpzdHViLGFkZEJ1dHRvbjpzdHVifX12YXIgZG9jdW1lbnQ9e30sd2luZG93PXRoaXM7''')
            jscode += vplayerData
            vplayerData = ''
            tmp = []
            ret = js_execute(jscode)
            if ret['sts'] and 0 == ret['code']:
                vplayerData = ret['data'].strip()

        if vplayerData != '':
            data += vplayerData
        else:
            mrk1 = ">eval("
            mrk2 = 'eval("'
            if mrk1 in data:
                m1 = mrk1
            elif mrk2 in data:
                m1 = mrk2
            else:
                m1 = "eval("
            tmpDataTab = ph.getAllItemsBetweenMarkers(data, m1, '</script>', False)
            for tmpData in tmpDataTab:
                data2 = tmpData
                tmpData = None
                # unpack and decode params from JS player script code
                tmpData = unpackJSPlayerParams(data2, VIDUPME_decryptPlayerParams)
                if tmpData == '':
                    tmpData = unpackJSPlayerParams(data2, VIDUPME_decryptPlayerParams, 0)

                if None is not tmpData:
                    data = data + tmpData

        printDBG("-*-*-*-*-*-*-*-*-*-*-*-*-*-\nData: %s\n-*-*-*-*-*-*-*-*-*-*-*-*-*-\n" % data)
        return _findLinks(data)

    def parserDAILYMOTION(self, baseUrl):
        printDBG("parserDAILYMOTION %s" % baseUrl)

        # source from https://github.com/ytdl-org/youtube-dl/blob/master/youtube_dl/extractor/dailymotion.py
        COOKIE_FILE = self.COOKIE_PATH + "dailymotion.cookie"
        HTTP_HEADER = {"User-Agent": getDefaultUserAgent()}
        httpParams = {'header': HTTP_HEADER, 'use_cookie': True, 'save_cookie': False, 'load_cookie': False, 'cookiefile': COOKIE_FILE}

        _VALID_URL = r'''(?ix)
                    https?://
                        (?:
                            (?:(?:www|touch)\.)?dailymotion\.[a-z]{2,3}/(?:(?:(?:embed|swf|\#)/)?video|swf)|
                            (?:www\.)?lequipe\.fr/video
                        )
                        /(?P<id>[^/?_]+)(?:.+?\bplaylist=(?P<playlist_id>x[0-9a-z]+))?
                    '''

        mobj = re.match(_VALID_URL, baseUrl)
        video_id = mobj.group('id')

        if not video_id:
            printDBG("parserDAILYMOTION -- Video id not found")
            return []

        printDBG("parserDAILYMOTION video id: %s " % video_id)

        urlsTab = []

        sts, data = self.cm.getPage(baseUrl, httpParams)

        metadataUrl = 'https://www.dailymotion.com/player/metadata/video/' + video_id

        sts, data = self.cm.getPage(metadataUrl, httpParams)

        if sts:
            try:
                metadata = json.loads(data)

                printDBG("----------------------")
                printDBG(json.dumps(data))
                printDBG("----------------------")

                error = metadata.get('error')
                if error:
                    title = error.get('title') or error['raw_message']

                    # See https://developer.dailymotion.com/api#access-error
                    #if error.get('code') == 'DM007':
                    #    allowed_countries = try_get(media, lambda x: x['geoblockedCountries']['allowed'], list)
                    #    self.raise_geo_restricted(msg=title, countries=allowed_countries)
                    #raise ExtractorError(
                    #    '%s said: %s' % (self.IE_NAME, title), expected=True)

                    printDBG("Error accessing metadata: %s " % title)
                    return []

                for quality, media_list in list(metadata['qualities'].items()):
                    for m in media_list:
                        media_url = m.get('url')
                        media_type = m.get('type')
                        if not media_url or media_type == 'application/vnd.lumberjack.manifest':
                            continue

                        media_url = urlparser.decorateUrl(media_url, {'Referer': baseUrl})
                        if media_type == 'application/x-mpegURL':
                            tmpTab = getDirectM3U8Playlist(media_url, False, checkContent=True, sortWithMaxBitrate=99999999, cookieParams={'header': HTTP_HEADER, 'cookiefile': COOKIE_FILE, 'use_cookie': True, 'save_cookie': True})
                            cookieHeader = self.cm.getCookieHeader(COOKIE_FILE)

                            for tmp in tmpTab:
                                hlsUrl = ph.getSearchGroups(tmp['url'], r"""(https?://[^'^"]+?\.m3u8[^'^"]*?)#?""")[0]
                                redirectUrl = strwithmeta(hlsUrl, {'iptv_proto': 'm3u8', 'Cookie': cookieHeader, 'User-Agent': HTTP_HEADER['User-Agent']})
                                urlsTab.append({'name': 'dailymotion.com: %sp hls' % (tmp.get('heigth', '0')), 'url': redirectUrl, 'quality': tmp.get('heigth', '0')})
                        else:
                            urlsTab.append({'name': quality, 'url': media_url})
            except:
                printExc

        return urlsTab


    def parserYOUTUBE(self, url):
        def __getLinkQuality(itemLink):
            val = itemLink['format'].split('x', 1)[0].split('p', 1)[0]
            try:
                val = int(val) if 'x' in itemLink['format'] else int(val) - 1
                return val
            except Exception:
                return 0

        if None != self.getYTParser():
            try:
                formats = config.plugins.iptvplayer.ytformat.value
                height = config.plugins.iptvplayer.ytDefaultformat.value
                dash = self.getYTParser().isDashAllowed()
                vp9 = self.getYTParser().isVP9Allowed()
                age = self.getYTParser().isAgeGateAllowed()
            except Exception:
                printDBG("parserYOUTUBE default ytformat or ytDefaultformat not available here")
                formats = "mp4"
                height = "360"
                dash = False
                vp9 = False
                age = False

            tmpTab, dashTab = self.getYTParser().getDirectLinks(url, formats, dash, dashSepareteList=True, allowVP9=vp9, allowAgeGate=age)
            #tmpTab = CSelOneLink(tmpTab, __getLinkQuality, int(height)).getSortedLinks()
            #dashTab = CSelOneLink(dashTab, __getLinkQuality, int(height)).getSortedLinks()

            videoUrls = []
            for item in tmpTab:
                url = strwithmeta(item['url'], {'youtube_id': item.get('id', '')})
                videoUrls.append({'name': 'YouTube | {0}: {1}'.format(item['ext'], item['format']), 'url': url, 'format': item.get('format', '')})
            for item in dashTab:
                url = strwithmeta(item['url'], {'youtube_id': item.get('id', '')})
                if item.get('ext', '') == 'mpd':
                    videoUrls.append({'name': 'YouTube | dash: ' + item['name'], 'url': url, 'format': item.get('format', '')})
                else:
                    videoUrls.append({'name': 'YouTube | custom dash: ' + item['format'], 'url': url, 'format': item.get('format', '')})

            videoUrls = CSelOneLink(videoUrls, __getLinkQuality, int(height)).getSortedLinks()
            return videoUrls

        return False



    def parserVIDBOMCOM(self, baseUrl):
        printDBG("parserVIDBOMCOM baseUrl[%r]" % baseUrl)

        baseUrl = strwithmeta(baseUrl)
        HTTP_HEADER = getDefaultHeader(browser='chrome')
        HTTP_HEADER['Referer'] = baseUrl.meta.get('Referer', baseUrl)
        urlParams = {'with_metadata': True, 'header': HTTP_HEADER}

        sts, data = self.cm.getPage(baseUrl, urlParams)
        if not sts:
            return False

        cUrl = getBaseUrl(data.meta['url'])
        domain = urlparser.getDomain(cUrl)

        jscode = [self.jscode['jwplayer']]
        tmp = ph.getAllItemsBetweenNodes(data, ('<script', '>'), ('</script', '>'), False)
        for item in tmp:
            if 'eval(' in item and 'setup' in item:
                jscode.append(item)
        urlTab = []
        try:
            jscode = '\n'.join(jscode)
            ret = js_execute(jscode)
            tmp = json.loads(ret['data'])
            for item in tmp['sources']:
                url = item['file']
                type = item.get('type', '')
                if type == '':
                    type = url.split('.')[-1].split('?', 1)[0]
                type = type.lower()
                label = item['label']
                if 'mp4' not in type:
                    continue
                if url == '':
                    continue
                url = urlparser.decorateUrl(self.cm.getFullUrl(url, cUrl), {'Referer': baseUrl, 'User-Agent': HTTP_HEADER['User-Agent']})
                urlTab.append({'name': '{0} {1}'.format(domain, label), 'url': url})
        except Exception:
            printExc()
        if len(urlTab) == 0:
            items = ph.getDataBetweenReMarkers(data, re.compile(r'''sources\s*[=:]\s*\['''), re.compile('''\]'''), False)[1].split('},')
            printDBG(items)
            domain = urlparser.getDomain(baseUrl)
            for item in items:
                item = item.replace('\/', '/')
                url = ph.getSearchGroups(item, r'''(?:src|file)['"]?\s*[=:]\s*['"]([^"^']+?)['"]''')[0]
                if not url.lower().split('?', 1)[0].endswith('.mp4') or not isValidUrl(url):
                    continue
                type = ph.getSearchGroups(item, r'''type['"]?\s*[=:]\s*['"]([^"^']+?)['"]''')[0]
                res = ph.getSearchGroups(item, r'''res['"]?\s*[=:]\s*['"]([^"^']+?)['"]''')[0]
                if res == '':
                    res = ph.getSearchGroups(item, r'''label['"]?\s*[=:]\s*['"]([^"^']+?)['"]''')[0]
                lang = ph.getSearchGroups(item, r'''lang['"]?\s*[=:]\s*['"]([^"^']+?)['"]''')[0]
                url = urlparser.decorateUrl(self.cm.getFullUrl(url, cUrl), {'Referer': baseUrl, 'User-Agent': HTTP_HEADER['User-Agent']})
                urlTab.append({'name': domain + ' {0} {1}'.format(lang, res), 'url': url})
        return urlTab


    def parserVIDOZANET(self, baseUrl):
        printDBG("parserVIDOZANET baseUrl[%r]" % baseUrl)
        referer = strwithmeta(baseUrl).meta.get('Referer', '')
        baseUrl = strwithmeta(baseUrl, {'Referer': referer})
        domain = urlparser.getDomain(baseUrl)

        def _findLinks(data):
            tmp = ph.getDataBetweenMarkers(data, '<video', '</video>')[1]
            tmp = ph.getAllItemsBetweenMarkers(tmp, '<source', '>', False)
            videoTab = []
            for item in tmp:
                if 'video/mp4' not in item and 'video/x-flv' not in item:
                    continue
                tType = ph.getSearchGroups(item, '''type=['"]([^'^"]+?)['"]''')[0].replace('video/', '')
                tUrl = ph.getSearchGroups(item, '''src=['"]([^'^"]+?)['"]''')[0]
                printDBG(tUrl)
                if isValidUrl(tUrl):
                    videoTab.append({'name': '[%s] %s' % (tType, domain), 'url': strwithmeta(tUrl)})
            return videoTab

        return self._parserUNIVERSAL_A(baseUrl, 'https://vidoza.net/embed-{0}.html', _findLinks)

    def parserCLIPWATCHINGCOM(self, baseUrl):
        printDBG("parserCLIPWATCHINGCOM baseUrl[%r]" % baseUrl)
        urlTabs = []

        sts, data = self.cm.getPage(baseUrl)

        if sts:
            data = ph.getDataBetweenReMarkers(data, re.compile(r'''jwplayer\([^\)]+?player[^\)]+?\)\.setup'''), re.compile(';'))[1]

            if 'sources' in data:
                items = ph.getDataBetweenReMarkers(data, re.compile(r'''[\{\s]sources\s*[=:]\s*\['''), re.compile('''\]'''), False)[1].split('},')
                for item in items:
                    label = ph.getSearchGroups(item, 'label:[ ]*?"([^"]+?)"')[0]
                    src = ph.getSearchGroups(item, 'file:[ ]*?"([^"]+?)"')[0]
                    if isValidUrl(src):
                        src = urlparser.decorateUrl(src, {'Referer': baseUrl})
                        if 'm3u8' in src:
                            params = getDirectM3U8Playlist(src, checkExt=True, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999)
                            urlTabs.extend(params)
                        else:
                            params = {'name': 'mp4 ' + label, 'url': src}
                            urlTabs.append(params)

        return urlTabs

    # def parserWOLFSTREAM(self, baseUrl):
    #     printDBG("parserWOLFSTREAM baseUrl[%r]" % baseUrl)
    #     urlTabs = []
    #     HTTP_HEADER = getDefaultHeader(browser='chrome')
    #     #HTTP_HEADER['Referer'] = baseUrl.meta.get('Referer', baseUrl)
    #     urlParams = {'header': HTTP_HEADER}
    #     sts, html = self.cm.getPage(baseUrl, urlParams)

    #     match = re.search('sources: \[\{file:"([^"]+?)"\}\]', html, re.S)[1]
    #     if match:
    #         src = urlparser.decorateUrl(match, HTTP_HEADER)
    #         params = getDirectM3U8Playlist(src, checkExt=True, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999)
    #         urlTabs.extend(params)
    #     return urlTabs

    # def parserANAFAST(self, baseUrl):
    #     printDBG("parserANAFAST baseUrl[%r]" % baseUrl)
    #     urlTabs = []
    #     HTTP_HEADER = getDefaultHeader(browser='chrome')
    #     #HTTP_HEADER['Referer'] = baseUrl.meta.get('Referer', baseUrl)
    #     urlParams = {'header': HTTP_HEADER}
    #     sts, html = self.cm.getPage(baseUrl, urlParams)

    #     match = re.search('{file:"([^"]+?\.m3u8)"}', html, re.S)
    #     if match:
    #         src = urlparser.decorateUrl(match[1], HTTP_HEADER)
    #         params = getDirectM3U8Playlist(src, checkExt=True, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999)
    #         urlTabs.extend(params)
    #     return urlTabs


    def parserMOSHAHDANET(self, baseUrl):
        printDBG("parserMOSHAHDANET baseUrl[%r]" % baseUrl)
        sts, data = self.cm.getPage(baseUrl)
        if not sts:
            return False
        data = ph.getDataBetweenMarkers(data, 'method="POST"', '</Form>', False)[1]
        post_data = dict(re.findall(r'<input[^>]*name="([^"]*)"[^>]*value="([^"]*)"[^>]*>', data))
        HTTP_HEADER = {'User-Agent': getDefaultUserAgent(), 'Referer': baseUrl}
        try:
            sleep_time = int(ph.getSearchGroups(data, '<span id="cxc">([0-9])</span>')[0])
            IPTVPlayerSleep().sleep(sleep_time)
        except Exception:
            printExc()

        sts, data = self.cm.getPage(baseUrl, {'header': HTTP_HEADER}, post_data)
        if not sts:
            return False

        printDBG(data)
        return self._findLinks(data, 'moshahda.net')

        linksTab = []
        srcData = ph.getDataBetweenMarkers(data, 'sources:', '],', False)[1].strip()
        srcData = json.loads(srcData + ']')
        for link in srcData:
            if not isValidUrl(link):
                continue
            if link.split('?')[0].endswith('m3u8'):
                tmp = getDirectM3U8Playlist(link)
                linksTab.extend(tmp)
            else:
                linksTab.append({'name': 'mp4', 'url': link})
        return linksTab

        #return self._findLinks(data, 'moshahda.net', linkMarker=r'''['"](http[^"^']+)['"]''')


    def parserVOESX(self, baseUrl):
        printDBG("parserVOESX baseUrl[%r]" % baseUrl)
        sts, data = self.cm.getPage(baseUrl)
        if not sts:
            return False
        if '404 - Not found' in data:
            SetIPTVPlayerLastHostError('404 - Not found')
            return []

        hlsUrl = ph.getSearchGroups(data, r'''['"]?hls['"]?\s*?:\s*?['"]([^'^"]+?)['"]''')[0]
        if not hlsUrl:
            if r := re.search(r"let\s[^']*'([^']+)", data):
                r = json.loads(base64.b64decode(r.group(1)))
                hlsUrl = r.get('file')

        if hlsUrl.startswith('//'):
            hlsUrl = 'http:' + hlsUrl

        if isValidUrl(hlsUrl):
            hlsUrl = urlparser.decorateUrl(hlsUrl, {'iptv_proto': 'm3u8', 'Referer': baseUrl, 'Origin': urlparser.getDomain(baseUrl, False)})
            return getDirectM3U8Playlist(hlsUrl, checkExt=False, checkContent=True, sortWithMaxBitrate=999999999)
        return False


    def parserVSHAREEU(self, baseUrl):
        printDBG("parserVSHAREEU baseUrl[%r]" % baseUrl)
        # example video: http://vshare.eu/mvqdaea0m4z0.htm

        HTTP_HEADER = {'User-Agent': "Mozilla/5.0 (Linux; U; Android 4.1.1; en-us; androVM for VirtualBox ('Tablet' version with phone caps) Build/JRO03S) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30"}

        if 'embed' not in baseUrl:
            COOKIE_FILE = GetCookieDir('vshareeu.cookie')
            rm(COOKIE_FILE)
            params = {'header': HTTP_HEADER, 'cookiefile': COOKIE_FILE, 'use_cookie': True, 'save_cookie': True, 'load_cookie': True}
            sts, data = self.cm.getPage(baseUrl, params)
            if not sts:
                return False

            sts, data = ph.getDataBetweenMarkers(data, 'method="POST"', '</Form>', False, False)
            if not sts:
                return False

            post_data = dict(re.findall(r'<input[^>]*name="([^"]*)"[^>]*value="([^"]*)"[^>]*>', data))
            params['header']['Referer'] = baseUrl

            IPTVPlayerSleep().sleep(5)

            sts, data = self.cm.getPage(baseUrl, params, post_data)
            if not sts:
                return False
        else:
            sts, data = self.cm.getPage(baseUrl)
            if not sts:
                return False

        sts, tmp = ph.getDataBetweenMarkers(data, ">eval(", '</script>')
        if sts:
            # unpack and decode params from JS player script code
            tmp = unpackJSPlayerParams(tmp, VIDUPME_decryptPlayerParams, 0, r2=True)
            printDBG(tmp)
            data = tmp + data

        linksTab = self._findLinks(data, urlparser.getDomain(baseUrl))
        if 0 == len(linksTab):
            data = ph.getAllItemsBetweenMarkers(data, '<source ', '>', False, False)
            for item in data:
                url = ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0]
                if url.startswith('//'):
                    url = 'http:' + url
                if not url.startswith('http'):
                    continue

                if 'video/mp4' in item:
                    type = ph.getSearchGroups(item, '''type=['"]([^"^']+?)['"]''')[0]
                    res = ph.getSearchGroups(item, '''res=['"]([^"^']+?)['"]''')[0]
                    label = ph.getSearchGroups(item, '''label=['"]([^"^']+?)['"]''')[0]
                    if label == '':
                        label = res
                    url = urlparser.decorateUrl(url, {'Referer': baseUrl, 'User-Agent': HTTP_HEADER['User-Agent']})
                    linksTab.append({'name': '{0}'.format(label), 'url': url})
                elif 'mpegurl' in item:
                    url = urlparser.decorateUrl(url, {'iptv_proto': 'm3u8', 'Referer': baseUrl, 'Origin': urlparser.getDomain(baseUrl, False), 'User-Agent': HTTP_HEADER['User-Agent']})
                    tmpTab = getDirectM3U8Playlist(url, checkExt=True, checkContent=True)
                    linksTab.extend(tmpTab)

        for elm in linksTab:
            elm['url'] = strwithmeta(elm['url'], {'Referer': baseUrl, 'User-Agent': HTTP_HEADER['User-Agent']})

        return linksTab

    def parserVIVOSX(self, baseUrl):
        printDBG("parserVIVOSX baseUrl[%s]" % baseUrl)
        HTTP_HEADER = {'User-Agent': getDefaultUserAgent(), 'Accept': 'text/html', 'Accept-Encoding': 'gzip, deflate'}

        urlTab = []

        sts, data = self.cm.getPage(baseUrl, {'header': HTTP_HEADER})
        if not sts:
            return urlTab

        #printDBG("----------------------")
        #printDBG(data)
        #printDBG("----------------------")

        tmp = re.findall("InitializeStream\s?\((.*?)\)", data, re.S)

        #printDBG("*********")
        #printDBG(str(tmp))
        #printDBG("*********")

        if not tmp:
            return []

        j = demjson_loads(tmp[0])

        src = j.get('source', '')
        if src:
            jsCode = '''function Normalize(a, b) { return ++b ? String.fromCharCode((a = a.charCodeAt() + 47, a > 126 ? a - 94 : a)) : decodeURIComponent(a).replace(/[^ ]/g, this.Normalize) } ; s = Normalize('%s'); console.log(s);'''
            jsCode = jsCode % src
            ret = js_execute(jsCode)

            if ret['sts'] and 0 == ret['code']:
                url = ret['data'].replace('\n', '')

                printDBG("Found url %s" % url)

                if isValidUrl(url):
                    u = urlparser.decorateUrl(url, {'Referer': baseUrl})
                    label = j.get('quality', 'link')
                    params = {'name': label, 'url': u}
                    printDBG(str(params))
                    urlTab.append(params)

        return urlTab

    def parserFLASHXTV(self, baseUrl):
        printDBG("parserFLASHXTV baseUrl[%s]" % baseUrl)

        HTTP_HEADER = getDefaultHeader()

        COOKIE_FILE = GetCookieDir('flashxtv.cookie')
        params = {'header': HTTP_HEADER, 'cookiefile': COOKIE_FILE, 'use_cookie': True, 'save_cookie': True}

        def __parseErrorMSG(data):
            data = ph.getAllItemsBetweenMarkers(data, '<center>', '</center>', False, False)
            for item in data:
                if 'color="red"' in item or ('ile' in item and '<script' not in item):
                    SetIPTVPlayerLastHostError(cleanHtml(item))
                    break

        def __getJS(data, params):
            tmpUrls = re.compile("""<script[^>]+?src=['"]([^'^"]+?)['"]""", re.IGNORECASE).findall(data)
            printDBG(tmpUrls)
            codeUrl = 'https://www.flashx.tv/js/code.js'
            for tmpUrl in tmpUrls:
                if tmpUrl.startswith('.'):
                    tmpUrl = tmpUrl[1:]
                if tmpUrl.startswith('//'):
                    tmpUrl = 'https:' + tmpUrl
                if tmpUrl.startswith('/'):
                    tmpUrl = 'https://www.flashx.tv' + tmpUrl
                if isValidUrl(tmpUrl):
                    if ('flashx' in tmpUrl and 'jquery' not in tmpUrl and '/code.js' not in tmpUrl and '/coder.js' not in tmpUrl):
                        printDBG('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
                        sts, tmp = self.cm.getPage(tmpUrl.replace('\n', ''), params)
                    elif '/code.js' in tmpUrl or '/coder.js' in tmpUrl:
                        codeUrl = tmpUrl

            sts, tmp = self.cm.getPage(codeUrl, params)
            tmp = ph.getAllItemsBetweenMarkers(tmp, 'function', ';')
            for tmpItem in tmp:
                tmpItem = tmpItem.replace(' ', '')
                if '!=null' in tmpItem:
                    tmpItem = ph.getDataBetweenMarkers(tmpItem, 'get(', ')')[1]
                    tmpUrl = ph.getSearchGroups(tmpItem, """['"](https?://[^'^"]+?)['"]""")[0]
                    if not isValidUrl(tmpUrl):
                        continue
                    getParams = ph.getDataBetweenMarkers(tmpItem, '{', '}', False)[1]
                    getParams = getParams.replace(':', '=').replace(',', '&').replace('"', '').replace("'", '')
                    tmpUrl += '?' + getParams
                    sts, tmp = self.cm.getPage(tmpUrl, params)
                    break

        if baseUrl.split('?')[0].endswith('.jsp'):
            rm(COOKIE_FILE)
            sts, data = self.cm.getPage(baseUrl, params)
            if not sts:
                return False

            __parseErrorMSG(data)

            cookies = dict(re.compile(r'''cookie\(\s*['"]([^'^"]+?)['"]\s*\,\s*['"]([^'^"]+?)['"]''', re.IGNORECASE).findall(data))
            tmpParams = dict(params)
            tmpParams['cookie_items'] = cookies
            tmpParams['header']['Referer'] = baseUrl

            __getJS(data, tmpParams)

            data = ph.getDataBetweenReMarkers(data, re.compile('<form[^>]+?method="POST"', re.IGNORECASE), re.compile('</form>', re.IGNORECASE), True)[1]
            printDBG(data)
            printDBG("================================================================================")

            action = ph.getSearchGroups(data, '''action=['"]([^'^"]+?)['"]''', ignoreCase=True)[0]
            post_data = dict(re.compile(r'<input[^>]*name="([^"]*)"[^>]*value="([^"]*)"[^>]*>', re.IGNORECASE).findall(data))
            try:
                tmp = dict(re.findall(r'<button[^>]*name="([^"]*)"[^>]*value="([^"]*)"[^>]*>', data))
                post_data.update(tmp)
            except Exception:
                printExc()

            try:
                IPTVPlayerSleep().sleep(int(ph.getSearchGroups(data, '>([0-9])</span> seconds<')[0]) + 1)
            except Exception:
                printExc()

            if not post_data:
                post_data = None
            if not isValidUrl(action):
                action = urljoin(baseUrl, action)

            sts, data = self.cm.getPage(action, tmpParams, post_data)
            if not sts:
                return False

            printDBG(data)
            __parseErrorMSG(data)

            # get JS player script code from confirmation page
            tmp = ph.getAllItemsBetweenMarkers(data, ">eval(", '</script>', False)
            for item in tmp:
                printDBG("================================================================================")
                printDBG(item)
                printDBG("================================================================================")
                item = item.strip()
                if item.endswith(')))'):
                    idx = 1
                else:
                    idx = 0
                printDBG("IDX[%s]" % idx)
                for decFun in [SAWLIVETV_decryptPlayerParams, KINGFILESNET_decryptPlayerParams]:
                    decItem = unquote(unpackJSPlayerParams(item, decFun, idx))
                    printDBG('[%s]' % decItem)
                    data += decItem + ' '
                    if decItem != '':
                        break

            urls = []
            tmp = re.compile('''\{[^}]*?src[^}]+?video/mp4[^}]+?\}''').findall(data)
            for item in tmp:
                label = ph.getSearchGroups(item, '''['"]?label['"]?\s*:\s*['"]([^"^']+?)['"]''')[0]
                res = ph.getSearchGroups(item, '''['"]?res['"]?\s*:\s*[^0-9]?([0-9]+?)[^0-9]''')[0]
                name = '%s - %s' % (res, label)
                url = ph.getSearchGroups(item, '''['"]?src['"]?\s*:\s*['"]([^"^']+?)['"]''')[0]
                params = {'name': name, 'url': url}
                if params not in urls:
                    urls.append(params)

            return urls[::-1]

        if '.tv/embed-' not in baseUrl:
            baseUrl = baseUrl.replace('.tv/', '.tv/embed-')
        if not baseUrl.endswith('.html'):
            baseUrl += '.html'

        params['header']['Referer'] = baseUrl
        SWF_URL = 'http://static.flashx.tv/player6/jwplayer.flash.swf'
        id = ph.getSearchGroups(baseUrl + '/', 'c=([A-Za-z0-9]{12})[^A-Za-z0-9]')[0]
        if id == '':
            id = ph.getSearchGroups(baseUrl + '/', '[^A-Za-z0-9]([A-Za-z0-9]{12})[^A-Za-z0-9]')[0]
        if id == '':
            id = ph.getSearchGroups(baseUrl + '/', '[^A-Za-z0-9]([A-Za-z0-9]{32})[^A-Za-z0-9]')[0]
        baseUrl = 'http://www.flashx.tv/embed.php?c=' + id

        rm(COOKIE_FILE)
        params['max_data_size'] = 0
        self.cm.getPage(baseUrl, params)
        redirectUrl = self.cm.meta['url']

        id = ph.getSearchGroups(redirectUrl + '/', 'c=([A-Za-z0-9]{12})[^A-Za-z0-9]')[0]
        if id == '':
            id = ph.getSearchGroups(redirectUrl + '/', '[^A-Za-z0-9]([A-Za-z0-9]{12})[^A-Za-z0-9]')[0]
        if id == '':
            id = ph.getSearchGroups(redirectUrl + '/', '[^A-Za-z0-9]([A-Za-z0-9]{32})[^A-Za-z0-9]')[0]
        baseUrl = 'http://www.flashx.tv/embed.php?c=' + id

        params.pop('max_data_size', None)
        sts, data = self.cm.getPage(baseUrl, params)
        params['header']['Referer'] = redirectUrl
        params['load_cookie'] = True

        printDBG("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        printDBG(data)
        printDBG("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

        play = ''
        vid = ph.getSearchGroups(redirectUrl + '/', '[^A-Za-z0-9]([A-Za-z0-9]{12})[^A-Za-z0-9]')[0]
        vid = ph.getSearchGroups(redirectUrl + '/', '[^A-Za-z0-9]([A-Za-z0-9]{32})[^A-Za-z0-9]')[0]
        for item in ['playvid', 'playthis', 'playit', 'playme', 'playvideo']:
            if item + '-' in data:
                play = item
                break

        printDBG("vid[%s] play[%s]" % (vid, play))

        __getJS(data, params)

        url = ph.getSearchGroups(redirectUrl, """(https?://[^/]+?/)""")[0] + play + '-{0}.html?{1}'.format(vid, play)
        sts, data = self.cm.getPage(url, params)
        if not sts:
            return False
        printDBG(data)

        if 'fxplay' not in url and 'fxplay' in data:
            url = ph.getSearchGroups(data, '"(http[^"]+?fxplay[^"]+?)"')[0]
            sts, data = self.cm.getPage(url)
            if not sts:
                return False

        try:
            printDBG(data)
            __parseErrorMSG(data)

            tmpTab = ph.getAllItemsBetweenMarkers(data, ">eval(", '</script>', False, False)
            for tmp in tmpTab:
                tmp2 = ''
                for type in [0, 1]:
                    for fun in [SAWLIVETV_decryptPlayerParams, VIDUPME_decryptPlayerParams]:
                        tmp2 = unpackJSPlayerParams(tmp, fun, type=type)
                        printDBG(tmp2)
                        data = tmp2 + data
                        if tmp2 != '':
                            printDBG("+++")
                            printDBG(tmp2)
                            printDBG("+++")
                            break
                    if tmp2 != '':
                        break

        except Exception:
            printExc()

        retTab = []
        linksTab = re.compile("""["']*file["']*[ ]*?:[ ]*?["']([^"^']+?)['"]""").findall(data)
        linksTab.extend(re.compile("""["']*src["']*[ ]*?:[ ]*?["']([^"^']+?)['"]""").findall(data))
        linksTab = set(linksTab)
        for item in linksTab:
            if item.endswith('/trailer.mp4'):
                continue
            if isValidUrl(item):
                if item.split('?')[0].endswith('.smil'):
                    # get stream link
                    sts, tmp = self.cm.getPage(item)
                    if sts:
                        base = ph.getSearchGroups(tmp, 'base="([^"]+?)"')[0]
                        src = ph.getSearchGroups(tmp, 'src="([^"]+?)"')[0]
                        #if ':' in src:
                        #    src = src.split(':')[1]
                        if base.startswith('rtmp'):
                            retTab.append({'name': 'rtmp', 'url': base + '/' + src + ' swfUrl=%s live=1 pageUrl=%s' % (SWF_URL, redirectUrl)})
                elif '.mp4' in item:
                    retTab.append({'name': 'mp4', 'url': item})
        return retTab[::-1]



    def parserTHEVIDEOME(self, baseUrl):
        printDBG("parserTHEVIDEOME baseUrl[%s]" % baseUrl)
        #http://thevideo.me/embed-l03p7if0va9a-682x500.html
        HTTP_HEADER = {'User-Agent': getDefaultUserAgent()}
        COOKIE_FILE = GetCookieDir('thvideome.cookie')
        rm(COOKIE_FILE)
        params = {'header': HTTP_HEADER, 'with_metadata': True, 'cookiefile': COOKIE_FILE, 'use_cookie': True, 'save_cookie': True}

        sts, data = self.cm.getPage(baseUrl, params)
        if not sts:
            return False

        baseUrl = data.meta['url']
        urlsTab = []

        if '/embed/' in baseUrl or 'embed-' in baseUrl:
            url = baseUrl
        else:
            parsedUri = urlparse(baseUrl)
            path = '/embed/' + parsedUri.path[1:]
            parsedUri = parsedUri._replace(path=path)
            url = urlunparse(parsedUri)
            sts, data = self.cm.getPage(url, params)
            if not sts:
                return False

        videoCode = ph.getSearchGroups(data, r'''['"]video_code['"]\s*:\s*['"]([^'^"]+?)['"]''')[0]

        if videoCode:

            params['header']['Referer'] = url
            params['raw_post_data'] = True
            sts, data = self.cm.getPage(getBaseUrl(baseUrl) + 'api/serve/video/' + videoCode, params, post_data='{}')
            if sts:
                printDBG("----------------")
                printDBG(data)
                printDBG("----------------")

                dataJson = json.loads(data)
                for key in dataJson['qualities']:
                    urlsTab.append({'name': '[%s] %s' % (key, getBaseUrl(baseUrl)), 'url': strwithmeta(dataJson['qualities'][key], {'User-Agent': HTTP_HEADER['User-Agent'], 'Referer': getBaseUrl(baseUrl)})})

        else:
            #search for packed code
            allDecoded = ph.eval_all_packed_func(data)
            for decoded in allDecoded:
                #video.src({type:'video/mp4',src:'stream9253bce1c89c262dc27e84e36a137f23.mp4'})
                sources = re.findall("src\((\{.*?\})\)", decoded)

                for s in sources:
                    src = demjson_loads(s)
                    url = src.get('src', '')
                    if url:
                        if not url.startswith('http'):
                            url = self.cm.getFullUrl(url, getBaseUrl(baseUrl))

                        srcType = src.get('type', '')

                    url = urlparser.decorateUrl(url, {'Referer': baseUrl})
                    if 'm3u' in srcType or 'hls' in srcType:
                        params = getDirectM3U8Playlist(url, checkExt=True, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999)
                        printDBG(str(params))
                        urlsTab.extend(params)
                    else:
                        params = {'name': 'link', 'url': url}
                        printDBG(str(params))
                        urlsTab.append(params)

        return urlsTab

    def parserUPTOSTREAMCOM(self, baseUrl):
        printDBG("parserUPTOSTREAMCOM baseUrl[%s]" % baseUrl)
        #example https://uptostream.com/iframe/kfaru03fqthy
        #        https://uptostream.com/xjo9gegjzf8c
        #        https://uptostream.com/api/streaming/source/get?token=null&file_code=zxfcxyy8in9e

        urlTab = []
        m = re.search("(iframe/|file_code=)(?P<id>.*)$", baseUrl)

        if m:
            video_id = m.groupdict().get('id', '')
        else:
            video_id = baseUrl.split("/")[-1]

        if video_id:
            url2 = "https://uptostream.com/api/streaming/source/get?token=null&file_code=%s" % video_id

            sts, data = self.cm.getPage(url2)

            if sts:
                #printDBG(data)
                response = json.loads(data)
                if response.get("message", '') == "Success":
                    code = response["data"]["sources"]

                    code = code.replace(";let", ";var")
                    code = code + "\n console.log(sources);"
                    printDBG("---------- javascript code -----------")
                    printDBG(code)

                    ret = js_execute(code)
                    if ret['sts'] and 0 == ret['code']:
                        response = demjson_loads(ret['data'])

                        #printDBG(str(response))

                        for u in response:
                            printDBG(u)

                            url = u.get('src', '')

                            if url:
                                if 'label' in u:
                                    title = u.get('label', '')
                                else:
                                    title = ''

                                if url[-4:] == 'm3u8':
                                    urlTab.extend(getDirectM3U8Playlist(url, checkExt=True, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))
                                else:
                                    urlTab.append({'name': title, 'url': url})

        return urlTab

    def parserVIMEOCOM(self, baseUrl):
        printDBG("parserVIMEOCOM baseUrl[%s]" % baseUrl)

        if 'player' not in baseUrl:
            video_id = ph.getSearchGroups(baseUrl + '/', '/([0-9]+?)[/.]')[0]
            if video_id != '':
                url = 'https://player.vimeo.com/video/' + video_id
            else:
                sts, data = self.cm.getPage(baseUrl)
                if not sts:
                    return False
                url = ph.getSearchGroups(data, '''['"]embedUrl['"]\s*?:\s*?['"]([^'^"]+?)['"]''')[0]
        else:
            url = baseUrl

        baseUrl = strwithmeta(baseUrl)
        HTTP_HEADER = getDefaultHeader(browser='chrome')
        if 'Referer' in baseUrl.meta:
            HTTP_HEADER['Referer'] = baseUrl.meta['Referer']

        sts, data = self.cm.getPage(url, {'header': HTTP_HEADER})
        if not sts:
            return False

        urlTab = []

        tmp = ph.getDataBetweenMarkers(data, 'progressive', ']', False)[1]
        tmp = tmp.split('}')
        printDBG(tmp)
        for item in tmp:
            if 'video/mp4' not in item:
                continue
            quality = ph.getSearchGroups(item, '''quality['"]?:['"]([^"^']+?)['"]''')[0]
            url = ph.getSearchGroups(item, '''url['"]?:['"]([^"^']+?)['"]''')[0]
            if url.startswith('http'):
                urlTab.append({'name': 'vimeo.com {0}'.format(quality), 'url': url})

        hlsUrl = ph.getSearchGroups(data, '"hls"[^}]+?"url"\:"([^"]+?)"')[0]
        tab = getDirectM3U8Playlist(hlsUrl)
        urlTab.extend(tab)

        return urlTab

    def parserOKRU(self, baseUrl):

        # printDBG("parserOKRU baseUrl[%r]" % baseUrl)
        # HTTP_HEADER= { 'User-Agent':getDefaultUserAgent(),
        #                'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        #              }

        # params = {'header': HTTP_HEADER, 'with_metadata': True}

        # urlsTab = []
        # baseUrl = baseUrl.split('key')[0]
        # media_id = baseUrl.split('/')[-1]
        # url = "http://www.ok.ru/dk"
        # data = {'cmd': 'videoPlayerMetadata', 'mid': media_id}

        # sts, data = self.cm.getPage(url, params, data)
        # if not sts:
        #     return False

        # json_data = json.loads(data)
        # if 'error' in json_data:
        #     SetIPTVPlayerLastHostError('File Not Found or removed')
        #     return

        # json_data = json.loads(data)

        # if len(json_data['videos']) > 0:
        #     info = dict()
        #     info['urls'] = []
        #     for entry in json_data['videos']:
        #         url = strwithmeta(entry['url'], HTTP_HEADER)
        #         urlsTab.append({'name': entry['name'], 'url': url})
        # return urlsTab
        printDBG("parserOKRU baseUrl[%r]" % baseUrl)
        HTTP_HEADER = {'User-Agent': getDefaultUserAgent(),
                       'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                       }

        metadataUrl = ''
        if 'videoPlayerMetadata' not in baseUrl:
            sts, data = self.cm.getPage(baseUrl, {'header': HTTP_HEADER})
            if not sts:
                return False
            error = cleanHtml(ph.getDataBetweenNodes(data, ('<div', '>', 'vp_video_stub_txt'), ('</div', '>'), False)[1])
            if error == '':
                error = cleanHtml(ph.getDataBetweenNodes(data, ('<', '>', 'page-not-found'), ('</', '>'), False)[1])
            if error != '':
                SetIPTVPlayerLastHostError(error)

            tmpTab = re.compile('''data-options=['"]([^'^"]+?)['"]''').findall(data)
            for tmp in tmpTab:
                tmp = cleanHtml(tmp)
                tmp = json.loads(tmp)
                printDBG("====")
                printDBG(tmp)
                printDBG("====")

                tmp = tmp['flashvars']
                if 'metadata' in tmp:
                    data = json.loads(tmp['metadata'])
                    metadataUrl = ''
                    break
                else:
                    metadataUrl = unquote(tmp['metadataUrl'])
        else:
            metadataUrl = baseUrl

        if metadataUrl != '':
            url = metadataUrl
            sts, data = self.cm.getPage(url, {'header': HTTP_HEADER})
            if not sts:
                return False
            data = json.loads(data)

        urlsTab = []
        for item in data['videos']:
            url = item['url'] #.replace('&ct=4&', '&ct=0&') #+ '&bytes'#=0-7078'
            url = strwithmeta(url, {'Referer': baseUrl, 'User-Agent': HTTP_HEADER['User-Agent']})
            urlsTab.append({'name': item['name'], 'url': url})
        urlsTab = urlsTab[::-1]

        if 1: #0 == len(urlsTab):
            url = urlparser.decorateUrl(data['hlsManifestUrl'], {'iptv_proto': 'm3u8', 'Referer': baseUrl, 'User-Agent': HTTP_HEADER['User-Agent']})
            linksTab = getDirectM3U8Playlist(url, checkExt=False, variantCheck=False, checkContent=True)

            # for idx in range(len(linksTab)):
            #     meta = dict(linksTab[idx]['url'].meta)
            #     meta['iptv_proto'] = 'm3u8'
            #     url = linksTab[idx]['url']
            #     if url.endswith('/'):
            #         linksTab[idx]['url'] = strwithmeta(url + 'playlist.m3u8', meta)

            try:
                tmpUrlTab = sorted(linksTab, key=lambda item: -1 * int(item.get('bitrate', 0)))
                tmpUrlTab.extend(urlsTab)
                urlsTab = tmpUrlTab
            except Exception:
                printExc()
        return urlsTab


    def parserNETUTV(self, url):

        printDBG("parserNETUTV url[%s]" % url)
        if 'hqq.none' in urlparser.getDomain(url):
            url = strwithmeta(url.replace('hqq.none', 'hqq.watch'), strwithmeta(url).meta)

        url += '&'
        vid = ph.getSearchGroups(url, '''vi?d?=([0-9a-zA-Z]+?)[^0-9^a-z^A-Z]''')[0]
        hashFrom = ph.getSearchGroups(url, '''hash_from=([0-9a-zA-Z]+?)[^0-9^a-z^A-Z]''')[0]

        # User-Agent - is important!!!
        HTTP_HEADER = {'User-Agent': 'Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10', #'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:21.0) Gecko/20100101 Firefox/21.0',
                        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                        'Referer': 'http://hqq.watch/'
                      }

        COOKIE_FILE = self.COOKIE_PATH + "netu.tv.cookie"
        # remove old cookie file
#        rm(COOKIE_FILE)
        params = {'with_metadata': True, 'header': HTTP_HEADER, 'use_cookie': True, 'save_cookie': True, 'load_cookie': True, 'cookiefile': COOKIE_FILE}
        sts, ipData = self.cm.getPage('http://hqq.watch/player/ip.php?type=json', params)
        ipData = json.loads(ipData)

        printDBG("===")
        printDBG(ipData)
        printDBG("===")

        if 'hash.php?hash' in url:
            sts, data = self.cm.getPage(url, params)
            if not sts:
                return False
            data = re.sub('document\.write\(unescape\("([^"]+?)"\)', lambda m: unquote(m.group(1)), data)
            vid = ph.getSearchGroups(data, '''var\s+?vid\s*?=\s*?['"]([^'^"]+?)['"]''')[0]
            hashFrom = ph.getSearchGroups(data, '''var\s+?hash_from\s*?=\s*?['"]([^'^"]+?)['"]''')[0]

        if vid == '':
            printDBG('Lack of video id.')
            return False

        playerUrl = "https://hqq.watch/player/embed_player.php?vid=%s&autoplay=no" % vid
        if hashFrom != '':
            playerUrl += '&hash_from=' + hashFrom
        referer = strwithmeta(url).meta.get('Referer', playerUrl)

        #HTTP_HEADER['Referer'] = url
        sts, data = self.cm.getPage(playerUrl, params)
        if not sts:
            return False
        cUrl = data.meta['url']

#        def _getEvalData(data):
#            jscode = ['eval=function(t){return function(){print(arguments[0]);try{return t.apply(this,arguments)}catch(t){}}}(eval);']
#            tmp = self.cm.ph.getAllItemsBeetwenNodes(data, ('<script', '>'), ('</script', '>'), False)
#            for item in tmp:
#                if 'eval(' in item and 'check(' not in item:
#                    jscode.append(item)
#            ret = js_execute( '\n'.join(jscode) )
#            return ret['data']

#        tmp = _getEvalData(data)

        sub_tracks = []
        subData = ph.getDataBetweenMarkers(data, 'addRemoteTextTrack({', ');', False)[1]
        subData = self.cm.getFullUrl(ph.getSearchGroups(subData, '''src:\s?['"]([^'^"]+?)['"]''')[0], cUrl)
        if (subData.endswith('.srt') or subData.endswith('.vtt')):
            sub_tracks.append({'title': 'attached', 'url': subData, 'lang': 'unk', 'format': 'srt'})

        data = ph.getAllItemsBetweenMarkers(data, '<script>', '</script>', False)
        wise = ''
        tmp = ''
        for item in data:
            if 'orig_vid = "' in item:
                tmp = item
            if "w,i,s,e" in item:
                wise = item

        orig_vid = ph.getDataBetweenMarkers(tmp, 'orig_vid = "', '"', False)[1]
        jscode = ph.getDataBetweenMarkers(tmp, 'location.replace(', ');', False)[1]
        jscode = 'var need_captcha="0"; var server_referer="http://hqq.watch/"; var orig_vid="' + orig_vid + '"; print(' + jscode + ');'

        gt = self.cm.getCookieItem(COOKIE_FILE, 'gt')
        ret = js_execute(jscode)
        if ret['sts'] and 0 == ret['code']:
            secPlayerUrl = self.cm.getFullUrl(ret['data'].strip(), getBaseUrl(cUrl)).replace('$secured', '0') #'https://hqq.tv/'
            if 'need_captcha=1' in secPlayerUrl and ipData['need_captcha'] == 0 and gt != '':
                secPlayerUrl = secPlayerUrl.replace('need_captcha=1', 'need_captcha=0')

        HTTP_HEADER['Referer'] = referer
        sts, data = self.cm.getPage(secPlayerUrl, params)
        cUrl = self.cm.meta['url']

        sitekey = ph.search(data, r'''['"]?sitekey['"]?\s*?:\s*?['"]([^"^']+?)['"]''')[0]
        if sitekey != '':
            query = {}
            token, errorMsgTab = self.processCaptcha(sitekey, cUrl)
            if token == '':
                SetIPTVPlayerLastHostError('\n'.join(errorMsgTab))
                return False
            else:
                query['g-recaptcha-response'] = token
                tmp = ph.find(data, ('<form', '>', ), '</form>', flags=ph.I | ph.START_E)[1]
                if tmp:
                    printDBG(tmp)
                    action = ph.getattr(tmp, 'action')
                    if not action:
                        action = cUrl.split('?', 1)[0]
                    else:
                        self.cm.getFullUrl(action, cUrl)
                    tmp = ph.findall(tmp, '<input', '>', flags=ph.I)

                    for item in tmp:
                        name = ph.getattr(item, 'name')
                        value = ph.getattr(item, 'value')
                        if name != '':
                            query[name] = value
                    action += '?' + urlencode(query)
                    sts, data = self.cm.getPage(action, params)
                    if sts:
                        cUrl = self.cm.meta['url']

#        data = re.sub('document\.write\(unescape\("([^"]+?)"\)', lambda m: urllib_unquote(m.group(1)), data)
#        data += _getEvalData(data)
#        printDBG("+++")
#        printDBG(data)
#        printDBG("+++")

        def getUtf8Str(st):
            try:
                idx = 0
                st2 = ''
                while idx < len(st):
                    st2 += '\\u0' + st[idx:idx + 3]
                    idx += 3
                return st2.decode('unicode-escape').encode('UTF-8')
            except Exception:
                return ''

        linksCandidates = re.compile('''['"](#[^'^"]+?)['"]''').findall(data)
        try:
#            jscode = [data.rsplit('//document.domain="hqq.watch";')[-1]]
#            tmp = ph.findall(data, '//document.domain="hqq.watch";', '</script>', flags=0)
#            for item in tmp:
#                if 'var at' in item:
#                    jscode.append(item)
#                    break
#            jscode.append('var adb = "0/"; ext = "";')
            jscode = ['var token = ""; var adb = "0/"; var wasmcheck="1"; var videokeyorig="%s";' % vid]
            jscode.append(wise)
            tmp = ph.search(data, r'''(['"][^'^"]*?get_md5\.php[^;]+?);''')[0]
            jscode.append('print(%s)' % tmp)
            ret = js_execute('\n'.join(jscode))

            playerUrl = self.cm.getFullUrl(ret['data'].strip(), cUrl)
            params['header']['Accept'] = '*/*'
            params['header']['Referer'] = cUrl
            sts, data = self.cm.getPage(playerUrl, params)
            obf = json.loads(data)
            linksCandidates.insert(0, obf['obf_link'])
        except Exception:
            printExc()
            rm(COOKIE_FILE)
            SetIPTVPlayerLastHostError(_('Link protected with google recaptcha v2.') + '\n' + _('Download again'))
            return False

        printDBG("linksCandidates >> %s" % linksCandidates)
        retUrls = []
        for file_url in linksCandidates:
            if file_url.startswith('#') and 3 < len(file_url):
                file_url = getUtf8Str(file_url[1:])
            if file_url.startswith('//'):
                file_url = 'https:' + file_url
            if isValidUrl(file_url):
                file_url = urlparser.decorateUrl(file_url, {'iptv_livestream': False, 'User-Agent': HTTP_HEADER['User-Agent'], 'Referer': cUrl, 'external_sub_tracks': sub_tracks})
                if file_url.split('?')[0].endswith('.m3u8') or '/hls-' in file_url:
                    file_url = strwithmeta(file_url, {'iptv_proto': 'm3u8'})
                    retUrls.extend(getDirectM3U8Playlist(file_url, False, checkContent=True))
        return retUrls

    def parserVIDCLOUD(self, baseUrl):
        printDBG("parserVIDCLOUD baseUrl[%r]" % baseUrl)
        baseUrl = strwithmeta(baseUrl)
        cUrl = baseUrl
        HTTP_HEADER = getDefaultHeader(browser='chrome')
        HTTP_HEADER['Referer'] = baseUrl.meta.get('Referer', baseUrl)

        urlParams = {'with_metadata': True, 'header': HTTP_HEADER}
        sts, data = self.cm.getPage(baseUrl, urlParams)
        if not sts:
            return False
        cUrl = data.meta['url']

        urlsTab = []
        data = ph.getAllItemsBetweenMarkers(data, 'sources', ']', False)
        if not data:
            videoId = re.findall("embed/([0-9a-z]*?)(/.*|)$", baseUrl)
            if videoId:
                videoId = videoId[0][0]
                url = "https://vidcloud.co/player?fid={0}&page=embed".format(videoId)
                sts, data = self.cm.getPage(url, urlParams)
                if sts:
                    data = json.loads(data)
                    ret = data["html"]
                    printDBG(ret)
                    data = ph.getAllItemsBetweenMarkers(ret, 'sources', ']', False)
                else:
                    data = ''
        if data:
            printDBG(str(data))
            for sourceData in data:
                sourceData = ph.getAllItemsBetweenMarkers(sourceData, '{', '}')
                for item in sourceData:
                    #printDBG(item)
                    item_data = json.loads(item)
                    printDBG(str(item_data))
                    if 'file' in item_data:
                        video_url = item_data['file']
                    elif 'src' in item_data:
                        video_url = item_data['src']
                    else:
                        video_url = ''
                    if video_url:
                        if 'type' in item_data:
                            video_type = item_data['type']
                        else:
                            video_type = ''

                        if 'name' in item_data:
                            video_name = item_data['name']
                        else:
                            video_name = urlparser.getDomain(url)

                        video_name = video_name + ' ' + video_type

                        video_url = strwithmeta(video_url, {'Referer': cUrl})
                        urlsTab.append({'name': video_name, 'url': video_url})

        return urlsTab


    def parserSUPERVIDEO(self, baseUrl):
        printDBG("parserSUPERVIDEO baseUrl[%s]" % baseUrl)
        #example  https://supervideo.tv/embed-k9aicjz32dcj.html

        sts, data = self.cm.getPage(baseUrl)
        if not sts:
            return False

        printDBG(data)

        vidTab = []

        # readable script
        if 'player.updateSrc({src:' in data:

            url = ph.getSearchGroups(data, r"player.updateSrc\({src: \"([^\"]+?)\"")[0]
            printDBG(url)
            if url[-4:] == 'm3u8':
                vidTab.extend(getDirectM3U8Playlist(url, checkExt=True, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))
            else:
                vidTab.append({'name': 'link', 'url': url})

            return vidTab

        # with crypted script
        tmpTab = re.findall(r">(eval\(.*?)</script>", data, re.S)

        #printDBG("=======================================")
        #printDBG(str(tmpTab))

        for tmp in tmpTab:
            jscode = tmp.replace('eval(', 'print(')
            tmp2 = js_execute(jscode)['data']

            printDBG("=======================================")
            printDBG(tmp2)
            printDBG("=======================================")

            title = ph.getSearchGroups(tmp2, 'media:{title:"([^"]+?)"')[0]
            urls_text = ph.getDataBetweenNodes(tmp2, 'sources:[', ']')[1]

            printDBG(urls_text)

            if urls_text.startswith("sources:"):
                urls = demjson_loads(urls_text[8:])
                for u in urls:
                    printDBG(u)

                    if 'file' in u:
                        url = u.get('file', '')
                    else:
                        url = u

                    if 'label' in u:
                        title = u.get('label', '')

                    if not title:
                        title = 'link'

                    if url[-4:] == 'm3u8':
                        vidTab.extend(getDirectM3U8Playlist(url, checkExt=True, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))
                    else:
                        vidTab.append({'name': title, 'url': url})

        return vidTab



    # def parserFEMBED(self, baseUrl):
    #     printDBG("parserFEMBED baseUrl[%s]" % baseUrl)
    #     #example:
    #     #https://www.fembed.com/v/e706eb-elm180dp
    #     #https://www.fembed.com/api/source/e706eb-elm180dp
    #     #https://streamhoe.online/v/0w6p8blx3krz3r0
    #     #https://cercafilm.net/v/80w1lh8z4w8-1en
    #     #https://sonline.pro/v/g3drwf-mwjelpwr
    #     #https://gcloud.live/v/ln5grsnn05rp1w8

    #     sts, data = self.cm.getPage(baseUrl, {'with_metadata': True})

    #     if sts:
    #         new_url = data.meta['url']
    #         if new_url != baseUrl:
    #             printDBG("redirect to %s" % new_url)
    #             baseUrl = new_url

    #     baseUrl = baseUrl + '?'
    #     m = re.search("/(v|api/source)/(?P<id>.+)\?", baseUrl)

    #     if not m:
    #         return []

    #     video_id = m.group('id')
    #     url = urlparser.getDomain(baseUrl, False) + 'api/source/' + video_id
    #     h = {
    #             'User-Agent': getDefaultUserAgent(),
    #             'Accept': '*/*',
    #             'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    #             'Referer': baseUrl,
    #             'X-Requested-With': 'XMLHttpRequest',
    #     }

    #     sts, data = self.cm.getPage(url, {'header': h}, post_data={'r': '', 'd': 'www.fembed.com'})

    #     if not sts:
    #         return []

    #     printDBG(data)
    #     data = json.loads(data)

    #     if ('not found' in data['data']) or ('removed' in data['data']):
    #         SetIPTVPlayerLastHostError(data['data'])

    #         return []

    #     urlsTab = []
    #     for v in data['data']:
    #         urlsTab.append({'name': v['label'], 'url': v['file']})

    #     urlsTab = sorted(urlsTab, key=lambda k: int(re.sub(r'\D', '', k['name'])), reverse=True)

    #     return urlsTab

    def parserSTREAMZ(self, baseUrl):
        printDBG("parserSTREAMZ baseUrl[%s]" % baseUrl)

        HTTP_HEADER = getDefaultHeader(browser='chrome')
        referer = baseUrl.meta.get('Referer')
        if referer:
            HTTP_HEADER['Referer'] = referer
        urlParams = {'header': HTTP_HEADER}
        sts, data = self.cm.getPage(baseUrl, urlParams)
        if not sts:
            return False

        urlTab=[]

        allDecoded = ph.eval_all_packed_func(data)
        for decoded in allDecoded:
            data0=re.search('file:"([^"]+?)"', decoded)
            if not data0:
                data0=re.search("""src:'([^"]+?)'""", decoded)

            if data0 and data0[1]:
                hlsUrl = strwithmeta(data0[1], {'Origin': "https://" + urlparser.getDomain(baseUrl), 'Referer': baseUrl})
                urlTab.extend(getDirectM3U8Playlist(hlsUrl, checkExt=False, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))
        return urlTab

    def parserONLYSTREAM(self, baseUrl):
        printDBG("parserONLYSTREAM baseUrl[%s]" % baseUrl)

        HTTP_HEADER = getDefaultHeader(browser='chrome')
        referer = baseUrl.meta.get('Referer')
        if referer:
            HTTP_HEADER['Referer'] = referer
        urlParams = {'header': HTTP_HEADER}
        sts, data = self.cm.getPage(baseUrl, urlParams)
        if not sts:
            return False

        urlTab=[]

        allDecoded = ph.eval_all_packed_func(data)
        for decoded in allDecoded:
            data0=re.search('file:"([^"]+?)"', decoded)
            if data0 and data0[1]:
                hlsUrl = strwithmeta(data0[1], {'Origin': "https://" + urlparser.getDomain(baseUrl), 'Referer': baseUrl})
                urlTab.extend(getDirectM3U8Playlist(hlsUrl, checkExt=False, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))

        urlTab.extend(self._findLinks(data, meta={'Referer': baseUrl}))
        if 0 == len(urlTab):
            url = ph.getSearchGroups(data, r'''["'](https?://[^'^"]+?\.mp4(?:\?[^"^']+?)?)["']''', ignoreCase=True)[0]
            if url != '':
                url = strwithmeta(url, {'Origin': "https://" + urlparser.getDomain(baseUrl), 'Referer': baseUrl})
                urlTab.append({'name': 'mp4', 'url': url})
            hlsUrl = ph.getSearchGroups(data, r'''["'](https?://[^'^"]+?\.m3u8(?:\?[^"^']+?)?)["']''', ignoreCase=True)[0]
            if hlsUrl != '':
                hlsUrl = strwithmeta(hlsUrl, {'Origin': "https://" + urlparser.getDomain(baseUrl), 'Referer': baseUrl})
                urlTab.extend(getDirectM3U8Playlist(hlsUrl, checkExt=False, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))

        return urlTab

    def parserMIXDROP(self, baseUrl):
        printDBG("parserMIXDROP baseUrl[%s]" % baseUrl)
        # example :https://mixdrop.co/f/1f13jq
        #          https://mixdrop.co/e/1f13jq
        #          https://mixdrop.club/f/vn7de6q7t0j868/2/La_Missy_sbagliata_HD_2020_WEBDL_1080p.mp4

        baseUrl = strwithmeta(baseUrl)
        HTTP_HEADER = getDefaultHeader(browser='firefox')
        referer = baseUrl.meta.get('Referer')
        if referer:
            HTTP_HEADER['Referer'] = referer
        else:
            HTTP_HEADER['Referer'] = baseUrl
        urlParams = {'header': HTTP_HEADER}

        if '/f/' in baseUrl:
            url = baseUrl.replace('/f/', '/e/')
        else:
            url = baseUrl

        sts, data = self.cm.getPage(url, urlParams)
        if not sts:
            return []

        error = ph.getDataBetweenNodes(data, '<div class="tb error">', '</p>')[1]

        if error:
            text = cleanHtml(error)
            SetIPTVPlayerLastHostError(text)
            return []

        urlsTab = []
        sub_tracks = []
        # decrypt packed scripts
        scripts = re.findall(r"(eval\s?\(function\(p,a,c,k,e,d.*?)</script>", data, re.S)
        for script in scripts:
            script = script + "\n"
            # mods
            script = script.replace("eval(function(p,a,c,k,e,d", "pippo = function(p,a,c,k,e,d")
            script = script.replace("return p}(", "print(p)}\n\npippo(")
            script = script.replace("))\n", ");\n")

            # duktape
            ret = js_execute(script)
            decoded = ret['data']

            subData = unquote(ph.getSearchGroups(decoded, '''remotesub=['"](http[^'^"]+?)['"]''')[0])
            if (subData.startswith('https://') or subData.startswith('http://')) and (subData.endswith('.srt') or subData.endswith('.vtt')):
                sub_tracks.append({'title': 'attached', 'url': subData, 'lang': 'unk', 'format': 'srt'})

            link = ph.getSearchGroups(decoded, r'''["']((?:https?:)?//[^'^"]+?\.mp4(?:\?[^"^']+?)?)["']''', ignoreCase=True)[0]
            if link:
                if link.startswith('//'):
                    video_url = "https:" + link
                else:
                    video_url = link
                video_url = urlparser.decorateUrl(video_url, {'Referer': baseUrl, 'external_sub_tracks': sub_tracks})
                params = {'name': 'link', 'url': video_url}
                printDBG(params)
                urlsTab.append(params)

        return urlsTab

    def parserVCRYPT(self, baseUrl):
        printDBG("parserVCRYPT baseUrl[%s]" % baseUrl)

        sts, data = self.cm.getPage(baseUrl, {'header': {'User-Agent': getDefaultUserAgent()}, 'use_cookie': 1, 'save_cookie': 1, 'load_cookie': 1, 'cookiefile': GetCookieDir("vcrypt.cookie"), 'with_metadata': 1})
        #if not sts:
        #    return []

        red_url = self.cm.meta['url']
        printDBG('redirect to url: %s' % red_url)

        if red_url != baseUrl:
            return urlparser().getVideoLinkExt(red_url)
        else:
            # search <meta http-equiv="refresh" content="1;URL=https://vcrypt.net/wss1/uadzaa31nr4r">
            red_url = re.findall("URL=([^\"]+)", data)
            if red_url:
                return urlparser().getVideoLinkExt(red_url[0])
            else:
                printDBG(data)
                return []



    def parserSTREAMTAPE(self, baseUrl):
        printDBG(f"parserSTREAMTAPE baseUrl[{baseUrl}]")

        COOKIE_FILE = GetCookieDir('streamtape.cookie')

        httpParams = {
            'header': {
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36',
                'Accept': '*/*',
                'Accept-Encoding': 'gzip',
                'Referer': baseUrl.meta.get('Referer', baseUrl)
            },
            'use_cookie': True,
            'load_cookie': True,
            'save_cookie': True,
            'cookiefile': COOKIE_FILE
        }

        sts, data = self.cm.getPage(baseUrl, httpParams)

        urlTabs = []

        if sts:
#            printDBG("---------")
#            printDBG(data)
#            printDBG("---------")

            #search url in tag like <div id="videolink" style="display:none;">//streamtape.com/get_video?id=27Lbk7KlQBCZg02&expires=1589450415&ip=DxWsE0qnDS9X&token=Og-Vxdpku4x8</div>
            subTracksData = ph.getAllItemsBetweenMarkers(data, '<track ', '>', False, False)
            subTracks = []
            for track in subTracksData:
                if 'kind="captions"' not in track:
                    continue
                subUrl = ph.getSearchGroups(track, 'src="([^"]+?)"')[0]
                if subUrl.startswith('/'):
                    subUrl = urlparser.getDomain(baseUrl, False) + subUrl
                if subUrl.startswith('http'):
                    subLang = ph.getSearchGroups(track, 'srclang="([^"]+?)"')[0]
                    subLabel = ph.getSearchGroups(track, 'label="([^"]+?)"')[0]
                    subTracks.append({'title': subLabel + '_' + subLang, 'url': subUrl, 'lang': subLang, 'format': 'srt'})

            t = ph.getSearchGroups(data, '''innerHTML = ([^;]+?);''')[0] + ';'
            printDBG("parserSTREAMTAPE t[%s]" % t)
            t = t.replace('.substring(', '[', 1).replace(').substring(', ':][').replace(');', ':]') + '[1:]'
            t = eval(t)
            if t.startswith('/'):
                t = "https:/" + t
            if isValidUrl(t):
                cookieHeader = self.cm.getCookieHeader(COOKIE_FILE, unquote=False)
                params = {'Cookie': cookieHeader, 'Referer': httpParams['header']['Referer'], 'User-Agent': httpParams['header']['User-Agent']}
                params['external_sub_tracks'] = subTracks
                t = urlparser.decorateUrl(t, params)
                params = {'name': 'link', 'url': t}
                printDBG(params)
                urlTabs.append(params)

        return urlTabs

    def parserDOOD(self, baseUrl):
        printDBG("parserDOOD baseUrl [%s]" % baseUrl)

        httpParams = {
            'header': {
                'User-Agent': getDefaultUserAgent(),
                'Accept': '*/*',
                'Accept-Encoding': 'gzip',
                'Referer': baseUrl
            },
            'use_cookie': True,
            'load_cookie': True,
            'save_cookie': True,
            'cookiefile': GetCookieDir("dood.cookie"),
            'max_data_size': 0,
            'no_redirection': True
        }

        urlsTab = []

        if '/d/' in baseUrl:
            baseUrl = baseUrl.replace('/d/', '/e/')

        sts, data = self.cm.getPage(baseUrl, httpParams)
        url = self.cm.meta.get('location', '')
        if url != '':
            if url.startswith('/'):
                host = urlparser.getHostName(baseUrl)
                url = 'https://%s%s' % (host, url)
            baseUrl = url
            httpParams['header']['Referer'] = baseUrl
        del httpParams['max_data_size']
        del httpParams['no_redirection']
        sts, data = self.cm.getPage(baseUrl, httpParams)

#        if sts:
#            printDBG("-----------------------")
#            printDBG(data)
#            printDBG("-----------------------")

        subTracks = []
        #<track kind="captions" src="https://doodstream.com/srt/00705/s72n7d5hi6qc_Serbian.vtt" srclang="en" label="Serbian" default>
        tracks = ph.getAllItemsBetweenMarkers(data, '<track', '>', withMarkers=True)
        for track in tracks:
            track_kind = ph.getSearchGroups(track, '''kind=['"]([^'^"]+?)['"]''')[0]
            if 'caption' in track_kind:
                srtUrl = ph.getSearchGroups(track, '''src=['"]([^'^"]+?)['"]''')[0]
                srtLabel = ph.getSearchGroups(track, '''label=['"]([^'^"]+?)['"]''')[0]
                srtFormat = srtUrl[-3:]
                params = {'title': srtLabel, 'url': srtUrl, 'lang': srtLabel.lower()[:3], 'format': srtFormat}
                printDBG(str(params))
                subTracks.append(params)

        #$.get('/pass_md5/3526522-87-9-1595176733-d1cadb0bad545cdcc61809e26c0ccf93/p3yuk59uqm525k1zc9boovu4'
        #function makePlay(){for(var a="",t="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",n=t.length,o=0;10>o;o++)a+=t.charAt(Math.floor(Math.random()*n));return a+"?token=p3yuk59uqm525k1zc9boovu4&expiry="+Date.now();};
        if pass_md5_url_match := re.search(r"\$\.get\('(/pass_md5[^']+?)'", data, re.S):
            pass_md5_url = pass_md5_url_match[1]
        if makePlay_match := re.search(r"(function\s*makePlay.+?\{.*?\};)",data, re.S):
            makePlay = makePlay_match[1]

        if pass_md5_url and makePlay:
            pass_md5_url = self.cm.getFullUrl(pass_md5_url, getBaseUrl(baseUrl))
            sts, new_url = self.cm.getPage(pass_md5_url, httpParams)

            if sts:
                code = "var url = '%s';\n%s\nconsole.log(url + makePlay());" % (new_url, makePlay)

                printDBG("-----------------------")
                printDBG(code)
                printDBG("-----------------------")

                ret = js_execute(code)
                newUrl = ret['data'].replace("\n", "")
                if newUrl:
                    if subTracks:
                        newUrl = urlparser.decorateUrl(newUrl, {'Referer': baseUrl, 'external_sub_tracks': subTracks})
                    else:
                        newUrl = urlparser.decorateUrl(newUrl, {'Referer': baseUrl})
                    params = {'name': 'link', 'url': newUrl}
                    printDBG(str(params))
                    urlsTab.append(params)

        return urlsTab

    def get_streamsburl(self, host, media_id):
        def makeid(length):
            return ''.join([random.choice("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789") for i in range(length)])

        x = '{0}||{1}||{2}||streamsb'.format(makeid(12), media_id, makeid(12))
        c1 = hexlify(x.encode('utf8')).decode('utf8')
        x = '{0}||{1}||{2}||streamsb'.format(makeid(12), makeid(12), makeid(12))
        c2 = hexlify(x.encode('utf8')).decode('utf8')
        x = '{0}||{1}||{2}||streamsb'.format(makeid(12), c2, makeid(12))
        c3 = hexlify(x.encode('utf8')).decode('utf8')
#            return 'https://{0}/sources43/{1}/{2}'.format(urlparser.getDomain(baseUrl), c1, c3)
        return 'https://{0}/sources16/{1}'.format(host, c1)

    def parserSTREAMSB(self, baseUrl):
        SetIPTVPlayerLastHostError("StreamSB not implemented yet.")
        # HTTP_HEADER = {
        #         'User-Agent': getDefaultUserAgent(),
        #         'Accept': 'application/json, text/plain, */*',
        #         'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
        #         'watchsb': 'sbstream',
        #         'DNT': 1,
        #         'Connection': 'keep-alive',
        #         'Referer':baseUrl,
        #     }

        # urlParams = {'header': HTTP_HEADER}
        # sts, html = self.cm.getPage(baseUrl, urlParams)
        # if not sts: return []
        # sources = re.findall(r'download_video([^"]+)[^\d]+(?:\d+x)?(\d+)', html)
        # if sources:
        #     sources.sort(key=lambda x: int(x[1]), reverse=True)
        #     sources = [(x[1] + 'p', x[0]) for x in sources]
        #     code, mode, dl_hash = eval(helpers.pick_source(sources))
        #     dl_url = 'https://{0}/dl?op=download_orig&id={1}&mode={2}&hash={3}'.format(host, code, mode, dl_hash)

        #     sts, html = self.cm.getPage(dl_url, urlParams)
        #     if not sts: return []

        #     domain = base64.b64encode((rurl[:-1] + ':443').encode('utf-8')).decode('utf-8').replace('=', '')
        #     token = helpers.girc(html, rurl, domain)
        #     if token:
        #         payload = helpers.get_hidden(html)
        #         payload.update({'g-recaptcha-response': token})
        #         sts, req = self.cm.getPage(dl_url, urlParams, post_data=payload)
        #         if not sts: return []
        #         r = re.search('href="([^"]+).+?>(?:Direct|Download)', req)
        #         if r:
        #             return r.group(1) + helpers.append_headers(headers)

        # eurl = self.get_streamsburl(host, media_id)
        # headers.update({'watchsb': 'sbstream'})
        # sts, html = self.cm.getPage(eurl, urlParams)
        # if not sts: return []
        # data = json.loads(html).get("stream_data", {})
        # strurl = data.get('file') or data.get('backup')
        # if strurl:
        #     headers.pop('watchsb')
        #     return strurl + helpers.append_headers(headers)

    def parserKINOGERRE(self, baseUrl):
        urlTab = []

        if 'kinoger.be' in baseUrl:
            baseUrl = strwithmeta(baseUrl, {'header': {'Referer': 'https://kinoger.to'}})
            urlTab = self.parserUNIVERSAL(baseUrl)

        elif 'kinoger.pw' in baseUrl:
                COOKIE_FILE = GetCookieDir('kinoger.to.cookie')
                HTTP_HEADER = {
                            'User-Agent': getDefaultUserAgent(),
                            'Accept': '*/*',
                            'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
                            'Referer': 'https://kinoger.com/'
                        }

                urlParams = {'header': HTTP_HEADER}
                sts, data = self.cm.getPage(baseUrl, urlParams)
                if not sts:
                    return []

                match =re.search(r"urlPlay\s=\s'(http[^']+)", data, re.S)
                if match:
                        url  = strwithmeta(match[1], {
                            'User-Agent': getDefaultUserAgent(),
                            'Origin':'https://kinoger.pw',
                            'Referer':'https://kinoger.pw/'
                        })

                        urlTab.extend(getDirectM3U8Playlist(url, checkExt=False, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))

        elif 'kinoger.ru' in baseUrl:

                def content_decryptor(html_content,passphrase):
                    match = re.compile(r'''JScripts = '(.+?)';''', re.DOTALL).search(html_content)
                    if match:
                        # Parse the JSON string
                        json_obj = json.loads(match.group(1))

                        # Extract the salt, iv, and ciphertext from the JSON object
                        salt = binascii.unhexlify(json_obj["s"])
                        iv = binascii.unhexlify(json_obj["iv"])
                        ct = base64.b64decode(json_obj["ct"])

                        # Concatenate the passphrase and the salt
                        concated_passphrase = passphrase.encode() + salt

                        # Compute the MD5 hashes
                        md5 = [hashlib.md5(concated_passphrase).digest()]
                        result = md5[0]
                        i = 1
                        while len(result) < 32:
                            md5.append(hashlib.md5(md5[i - 1] + concated_passphrase).digest())
                            result += md5[i]
                            i += 1

                        # Extract the key from the result
                        key = result[:32]

                        # Decrypt the ciphertext using AES-256-CBC
                        aes = pyaes.AESModeOfOperationCBC(key, iv)
                        decrypter = pyaes.Decrypter(aes)
                        plain_text = decrypter.feed(ct)
                        plain_text += decrypter.feed()

                        # Return the decrypted data as a JSON object
                        return json.loads(plain_text.decode())
                    else:
                        return None

                COOKIE_FILE = GetCookieDir('kinoger.to.cookie')
                HTTP_HEADER = {
                            'User-Agent': getDefaultUserAgent(),
                            'Accept': '*/*',
                            'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
                            'Referer': 'https://kinoger.com/'
                        }

                urlParams = {'header': HTTP_HEADER, 'cookiefile': COOKIE_FILE, 'use_cookie': True, 'save_cookie': True}
                sts, sHtmlContent = self.cm.getPage(baseUrl, urlParams)
                if not sts:
                    return []
                if self.cm.meta['status_code']==404:
                    SetIPTVPlayerLastHostError("the page you attempted to access was video is not ready.")
                else:
                    decryptHtmlContent = content_decryptor(sHtmlContent, 'a7igbpIApajDyNe') # Decrypt Content
                    match = re.search('sources.*?file.*?(http[^"]+)', decryptHtmlContent, re.S)
                    if match:
                        hUrl = match[1].replace('\\', '')
                        hUrl = strwithmeta(hUrl, {'Referer': 'https://kinoger.ru/', 'Origin': 'https://kinoger.ru'})
                        urlTab.extend(getDirectM3U8Playlist(hUrl, checkExt=False, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))

        elif 'kinoger.re' in baseUrl:
            apiurl = 'https://kinoger.re/api/video/stream/get'
            HTTP_HEADER = getDefaultHeader(browser='chrome')
            post_data = {'Referer':baseUrl,
                'id': baseUrl.split('/')[-1]
            }
            urlParams = {'header': HTTP_HEADER}
            sts, data = self.cm.getPage(apiurl, urlParams, post_data=post_data)
            if not sts: return []
            pattern = '(?:"label":"([^"]+)",)*"file":"([^"]+)'
            tmp = re.findall(pattern, data)
            for elm in tmp:
                url = strwithmeta(elm[1].replace('\\',''), {
                    'User-Agent': getDefaultUserAgent(),
                    'Accept':'*/*',
                    'Accept-Encoding':'gzip, deflate, br',
                    'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
                    'Origin': 'https://kinoger.re',
                    'Referer': 'https://kinoger.re/',
                    'DNT': '1',
                    'Connection':'keep-alive'
                    })
                if url.endswith(".m3u8"):
                    urlTab.extend(getDirectM3U8Playlist(url, checkExt=False, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))
                else:
                    urlTab.append({'name': elm[0], 'url': url})

        return urlTab

    def parserEVOLOADIO(self, baseUrl):
        urlTab = []
        host, media_id = re.findall(r'(?://|\.)(evoload\.io)/(?:e|f|v)/([0-9a-zA-Z]+)', baseUrl)[0]
        surl = 'https://evoload.io/SecurePlayer'
        web_url = f'https://{host}/e/{media_id}'
        rurl = f'https://{host}/'
        headers = {'User-Agent': getDefaultUserAgent(),
                   'Referer': rurl}
        urlParams = {'header': headers}
        sts, html = self.cm.getPage(web_url, urlParams)
        if not sts:
            return
        passe = re.search(r'<div\s*id="captcha_pass"\s*value="(.+?)"></div>', html).group(1)
        headers.update({'Origin': rurl[:-1]})
        urlParams = {'header': headers}
        sts, crsv = self.cm.getPage('https://csrv.evosrv.com/captcha?m412548', urlParams)
        if not sts:
            return
        post = {"code": media_id, "csrv_token": crsv, "pass": passe, "token": "ok"}
        sts, shtml = self.cm.getPage(surl, urlParams, post_data=post)
        if not sts:
            return
        r = json.loads(shtml).get('stream')
        if r:
            surl = r.get('backup') if r.get('backup') else r.get('src')
            urlTab.append({'name': 'movie', 'url': strwithmeta(surl, headers)})
        else:
            SetIPTVPlayerLastHostError(shtml)
        return urlTab

    def parserUSERLOADCO(self, baseUrl):
        printDBG("parserUSERLOADCO baseUrl[%s]" % baseUrl)

        HTTP_HEADER = getDefaultHeader(browser='chrome')
        referer = baseUrl.meta.get('Referer')
        if referer:
            HTTP_HEADER['Referer'] = referer
        urlParams = {'header': HTTP_HEADER}
        sts, data = self.cm.getPage(baseUrl, urlParams)
        if not sts:
            return False

        urlTab = []
        allDecoded=ph.eval_all_packed_func(data)
        for data in allDecoded:
            morocco = ph.getSearchGroups(data, '''['"](AO.+?Aa)['"]''')[0]
            if morocco == '':
                morocco = ph.getSearchGroups(data, '''['"]([0-9a-zA-Z]{31})['"]''')[0]
            tmp = re.findall('''['"]([0-9a-z]{32})['"]''', data)
            for item in tmp:
                post_data = {'morocco': morocco, 'mycountry': item}
                sts, data = self.cm.getPage('https://userload.co/api/request/', urlParams, post_data)
                if not sts:
                    return False
                if 'http' in data:
                    break

            data = data.splitlines()[0]

            url = strwithmeta(data, {'Origin': "https://" + urlparser.getDomain(baseUrl), 'Referer': baseUrl})
            if 'm3u8' in url:
                urlTab.extend(getDirectM3U8Playlist(url, checkExt=False, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))
            else:
                urlTab.append({'name': 'mp4', 'url': url})

        return urlTab


    def parserSCCTONLINE(self, baseUrl):
        urlTab = []
        HTTP_HEADER = getDefaultHeader(browser='chrome')
        urlParams = {'header': HTTP_HEADER}
        sts, data = self.cm.getPage(baseUrl, urlParams)
        if not sts:
            return []

        files = ph.getDataBetweenMarkers(data, 'file:"', '"')[1]
        found = ph.getSearchGroups(files, 'file:"(.*)"')[0]

        splitted = found.split(',')

        for v in splitted:
            quality = ph.getSearchGroups(v, r'(\[.*?\])')[0]
            link = ph.getSearchGroups(v, '(https.*.mp4)')[0]

            urlParams['no_redirection']=True
            sts, data = self.cm.getPage(link, urlParams)
            url = self.cm.meta.get('location', '')

            if not quality:
                sts, data = self.cm.getPage(baseUrl, urlParams)
                if not sts:
                    return []

                urlTab.append({'name': '[360]', 'url': url})
            else:
                urlTab.append({'name': '%s' % quality, 'url': url})
        return urlTab

    def parserPROTONVIDEO(self, baseUrl):
        printDBG("parserPROTONVIDEO baseUrl[%s]" % baseUrl)
        urlTab = []

        idi = baseUrl.split('/')[4]

        url = "https://api.svh-api.ch/api/v4/player"
        HTTP_HEADER = getDerfaultHeader()
        HTTP_HEADER["Content-Type"]= "application/json"

        def aes(txt):
            from Plugins.Extensions.IPTVPlayer.libs.pyaes import Encrypter, AESModeOfOperationCBC
            import base64
            from binascii import unhexlify
            key = unhexlify('0123456789abcdef0123456789abcdef')
            iv = unhexlify('abcdef9876543210abcdef9876543210')
            aes = Encrypter(AESModeOfOperationCBC(key, iv))
            return base64.b64encode(aes.feed(txt) + aes.feed()).decode()

        token = aes(baseUrl.split('/')[4])

        data = { "idi": "%s" % idi, "token": "%s" % token }

        urlParams = {'header': HTTP_HEADER,  'raw_post_data': True}

        sts, data = self.cm.getPage(url, addParams=urlParams , post_data=json.dumps(data).encode("utf-8"))
        if not sts: return
        obj = json.loads(data)

        video_links = obj['file'].split(',')

        for link in video_links:
            searchgroup = ph.getSearchGroups(link, r"\[(.*)\]([^\]]+)", grupsNum=2 )
            video_link =searchgroup[1]
            quality = searchgroup[0]

            urlTab.append({'name': quality, 'url': video_link})
        return list(reversed(urlTab))

    def parserVIDSPEEDS(self, baseUrl):
        printDBG("parserVIDSPEEDS baseUrl[%r]" % baseUrl)
        elms = baseUrl.split('/')
        if 'embed-' not in baseUrl: url = '%s//%s/embed-%s.html' % (elms[0],elms[2],elms[-1].replace('.html', ''))
        else: url = baseUrl
        hlsTab = []
        mp4Tab = []

        HTTP_HEADER= { 'User-Agent':getDefaultUserAgent()}
        if 'Referer' in strwithmeta(baseUrl).meta:
            HTTP_HEADER['Referer'] = strwithmeta(baseUrl).meta['Referer']
        sts, data = self.cm.getPage(url, {'header':HTTP_HEADER})
        if not sts: return False

        data = data.replace('file:','"file":').replace('label:','"label":')
        lst_data = re.findall('sources.*?\[(.*?)\]', data, re.S)
        if lst_data:
            json = json.loads(lst_data[0])
            url = strwithmeta(json['file'], {'Referer':baseUrl})

            type_ = url.split('?', 1)[0].split('.')[-1].lower()
            if 'm3u8' in type_:
                hlsTab.extend(getDirectM3U8Playlist(url, checkExt=False, checkContent=True, sortWithMaxBitrate=999999999))
            elif 'mp4' in type_:
                mp4Tab.append({'name':'[MP4] ', 'url':url})

        videoTab = []
        videoTab.extend(hlsTab)
        videoTab.extend(mp4Tab)
        return videoTab

    def parserUSTREAMIN(self, baseUrl):
        '''
        https://start.u-stream.in/start/1f13d8fb2b1b6f37a314f6a557714466/3bdc787f86eea5abcc19d547f2cc9991
        https://start.u-cdn.top/start/1f13d8fb2b1b6f37a314f6a557714466/3bdc787f86eea5abcc19d547f2cc9991
        '''
        urlTab = []

        domain = urlparser.getDomain(baseUrl, onlyDomain=True)
        HTTP_HEADER = getDefaultHeader(browser='chrome')
        referer = baseUrl.meta.get('Referer')
        if referer: HTTP_HEADER['Referer'] = referer
        urlParams = {'header': HTTP_HEADER}
        sts, data = self.cm.getPage(baseUrl, urlParams)
        if not sts: return []

        token = ph.getSearchGroups(data, r"cfg\(w\)\}\('([^']+?)'")[0]
        if token:
            hash, id = baseUrl.split('/')[-2:]

            script1 = {'path': GetJSScriptFile('ustreamin1.byte')}

            ret = js_execute_ext([script1, {'code': "print(encodeUrl('%s:%s'));" % (hash, id)}])

            url = 'https://%s/ustGet.php?id=%s&token=%s' % (domain, id, ret['data'].strip())

            sts, data = self.cm.getPage(url, urlParams)
            if not sts: return []

            metadata = json.loads(data)

            for u in metadata["url"]:
                ret = js_execute_ext([script1, {'code': ' print(decodeStr("%s"));' % u}])
                url = ret['data'].strip()
                qualty = ph.getSearchGroups(url, r"/(480|720|1080)-")[0]
                if not qualty:
                    qualty = 'Low'
                urlTab.append({'url': url, 'name': qualty})
        else:
            hash = ph.getSearchGroups(data, r'"hash":[^:]*?"([^"]*?)"')[0]
            # printDBG("parserUSTREAMIN hash[%s]" % hash)
            id = ph.getSearchGroups(data, r'"id":[^:]*?"([^"]*?)"')[0]
            # printDBG("parserUSTREAMIN id[%s]" % id)

            url = "https://%s/dev.php?hash=%s&id=%s" % (domain.replace('start', 'get'), hash, id)

            sts, data = self.cm.getPage(url, urlParams)
            if not sts: return []

            metadata = json.loads(data)

            # printDBG("----------------------")
            # printDBG(json.dumps(data))
            # printDBG("----------------------")

            # printDBG("parserUSTREAMIN metadata['url']: %s" % metadata["url"])

            script2 = {'path': GetJSScriptFile('ustreamin2.byte')}

            for u in metadata["url"]:
                js = [script2, {'code': ' print(decodeUrl32("%s"));' % u}]
                ret = js_execute(js)
                url = ret['data'].strip()
                # url = decorateUrl(url)
                # url.meta['iptv_m3u8_custom_base_link'] = '' + url
                # urlTab.extend(getDirectM3U8Playlist(url, checkExt=False, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))

                qualty = ph.getSearchGroups(url, r"content/(\d*?)-")[0]
                if not qualty:
                    qualty = 'Low'
                urlTab.append({'url': url, 'name': qualty})  # strwithmeta(url, {'Referer':referer})})

        return urlTab

    def parserTUBELOADCO(self, baseUrl):
        printDBG("parserTUBELOADCO baseUrl[%s]" % baseUrl)
        HTTP_HEADER = getDefaultHeader(browser='chrome')
        referer = baseUrl.meta.get('Referer')
        if referer:
            HTTP_HEADER['Referer'] = referer
        urlParams = {'header': HTTP_HEADER}
        domain = urlparser.getDomain(baseUrl, False)
        sts, data = self.cm.getPage(baseUrl, urlParams)
        if not sts:
            return []
        jsUrl = self.cm.getFullUrl(ph.getSearchGroups(data, r'''src=\s?['"]([^'^"]+?main\.min\.js)['"]''')[0], baseUrl)
        sts, jsdata = self.cm.getPage(jsUrl, urlParams)
        if not sts:
            return []
        if 'function(h,u,n,t,e,r)' in jsdata:
            ff = re.findall(r'function\(h,u,n,t,e,r\).*?}\((".+?)\)\)', jsdata, re.DOTALL)[0]
            ff = ff.replace('"', '')
            h, u, n, t, e, r = ff.split(',')
            jsdata = dehunt(h, int(u), n, int(t), int(e), int(r))
#        printDBG("parserTUBELOADCO jsdata[%s]" % jsdata)
        jscode = ph.getSearchGroups(jsdata, r'''var\s[^=]+?=\s?([^;]+?);''', ignoreCase=True)[0]
        jsvar = ph.getSearchGroups(jscode, r'''([^.]+?)\.replace''', ignoreCase=True)[0]
        printDBG("parserTUBELOADCO jscode[%s]  jsvar[%s]" % (jscode, jsvar))
        data = ph.getAllItemsBetweenNodes(data, ('<script', '>'), ('</script', '>'), False)
        script = ''
        for item in data:
            if 'function(h,u,n,t,e,r)' in item:
                ff = re.findall(r'function\(h,u,n,t,e,r\).*?}\((".+?)\)\)', item, re.DOTALL)[0]
                ff = ff.replace('"', '')
                h, u, n, t, e, r = ff.split(',')
                script = dehunt(h, int(u), n, int(t), int(e), int(r))
                if jsvar in script:
                    break
#        printDBG("parserTUBELOADCO script[%s]" % script)
        jscode = script + '\n' + jsdata
        jscode = jscode.replace('atob', 'base64.b64decode')
        decode = ''
        vars = re.compile(r'var\s(.*?=[^{]+?;)').findall(jscode)
        exec('\n'.join(vars))
        urlTab = []
        if decode:
            urlTab.append({'name': 'mp4', 'url': strwithmeta(decode, {'Referer': baseUrl})})
        return urlTab

    def parserSTREAMLARE(self, baseUrl):
        urlTab = []
        referer = baseUrl.meta["Referer"]
        httpParams = {
            'header':{
                'User-Agent': getDefaultUserAgent(),
                'X-Requested-With': 'XMLHttpRequest',
                'Referer': referer
            }
        }

        m = re.search(r'(?://|\.)(streamlare\.com)/(?:e|v)/([0-9A-Za-z]+)', baseUrl)
        media_id = m.group(2)
        post_data = {'id': media_id}
        api_durl = 'https://streamlare.com/api/video/download/get'
        api_surl = 'https://streamlare.com/api/video/stream/get'
        sts, data = self.cm.getPage(api_surl, httpParams, post_data)
        if not sts: return

        surl = json.loads(data).get("result",{}).get('file', '')
        if surl:
            urlTab.append({'name': 'Original', 'url': surl})
        else:
            sts, data = self.cm.getPage(api_durl, httpParams, post_data)
            if not sts: return
            surl = json.loads(data).get('result', {}).get('Original', {}).get('url')
            if surl:
                urlTab.append({'name': 'Original', 'url': surl})
        return urlTab


    def parserIMDBCOM(self, baseUrl):
        urlTab = []
        sts, data = self.cm.getPage(baseUrl)
        if sts:
            data0 = re.findall(r'"([^"]+?m3u8[^"]+?)\\"', data, re.S)
            if data0:
                urlTab.append({'name': 'mp4', 'url': data0[0].replace('\\u002F','/')})

        return urlTab

    def parserSKYVID(self, baseUrl):
        urlTab = []
        sts, data = self.cm.getPage(baseUrl)
        if sts:
            data0 = re.search("'([^']+?m3u8)'", data, re.S)
            if data0:
                if data0[1].startswith('/'):
                    domain = urlparser.getDomain(baseUrl, True)
                    urlTab.append({'name': 'video', 'url': f'https://{domain}{data0[1]}'})

        return urlTab

    # def parserFILEMOON(self, baseUrl):
    #     urlTab = []
    #     sts, data = self.cm.getPage(baseUrl)
    #     if sts:
    #         allDecoded = ph.eval_all_packed_func(data)
    #         for decoded in allDecoded:
    #             data1 = re.findall(r'''sources:\s*\[{file:\s*["'](?P<url>[^"']+)''', decoded, re.S)
    #             for elm in data1:
    #                 urlTab.append({'name': 'video', 'url': elm})
    #     return urlTab

    def parserLINKBOXTO(self, baseUrl):
        urlTab = []
        id_ = baseUrl.split('?')[-1].split('=')[-1]
        url = f'https://www.linkbox.to/api/file/detail?itemId={id_}'
        sts, data = self.cm.getPage(url)
        if sts:
            json_data = json.loads(data)
            video_url = json_data.get('data', {}).get('url', '')
            if video_url:
                urlTab.append({'name': 'video', 'url': video_url})
        return urlTab

    def parserONLYSTREAMTV(self, baseUrl):
        printDBG("parserONLYSTREAMTV baseUrl[%s]" % baseUrl)
        urlTab = []
        HTTP_HEADER = getDefaultHeader(browser='chrome')
        referer = baseUrl.meta.get('Referer')
        if referer:
            HTTP_HEADER['Referer'] = referer
        urlParams = {'header': HTTP_HEADER}
        sts, data = self.cm.getPage(baseUrl, urlParams)
        if not sts:
            return False

        allDecoded=ph.eval_all_packed_func(data)
        for data in allDecoded:
            urlTab.extend(self._findLinks(data, meta={'Referer': baseUrl}))
            if 0 == len(urlTab):
                url = re.search(r'''["'](https?://[^'^"]+?\.mp4(?:\?[^"^']+?)?)["']''', data, re.S)
                if url:
                    url = strwithmeta(url[1], {'Origin': "https://" + urlparser.getDomain(baseUrl), 'Referer': baseUrl})
                    urlTab.append({'name': 'mp4', 'url': url})
                hlsUrl = re.search(r'''["'](https?://[^'^"]+?\.m3u8(?:\?[^"^']+?)?)["']''', data, re.S)
                if hlsUrl:
                    hlsUrl = strwithmeta(hlsUrl[1], {'Origin': "https://" + urlparser.getDomain(baseUrl), 'Referer': baseUrl})
                    urlTab.extend(getDirectM3U8Playlist(hlsUrl, checkExt=False, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))

        return urlTab



    # def parserVIDEOBIN(self, baseUrl):
    #     printDBG("parserVIDEOBIN baseUrl[%s]" % baseUrl)

    #     urlTab=[]
    #     HTTP_HEADER = getDefaultHeader(browser='chrome')
    #     urlParams = {'header': HTTP_HEADER}
    #     sts, data = self.cm.getPage(baseUrl, urlParams)
    #     if not sts:
    #         return False

    #     _srcs = re.search(r'sources\s*:\s*\[(.+?)\]', data, re.S)[0]
    #     if _srcs:
    #         links = re.findall('''["'](.*?)["']''', _srcs, re.S)
    #         for link in links:
    #             if link.endswith('m3u8'):
    #                 urlTab.extend(getDirectM3U8Playlist(link, checkExt=False, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))
    #             else:
    #                 urlTab.append({'name': 'mp4', 'url': link})

    #     return urlTab


    def parserVIDGUARD(self, baseUrl):
        printDBG("parserVIDGUARDTO baseUrl[%s]" % baseUrl)

        def sig_decode(url):
            sig = url.split('sig=')[1].split('&')[0]
            t = ''
            for v in unhexlify(sig):
                t += chr((v if isinstance(v, int) else ord(v)) ^ 2)
            t = list(base64.b64decode(t + '==')[:-5][::-1])
            for i in range(0, len(t) - 1, 2):
                t[i + 1], t[i] = t[i], t[i + 1]
            t = ''.join(chr((i if isinstance(i, int) else ord(i))) for i in t)
            url = url.replace(sig, ''.join(t)[:-5])
            return url

        HTTP_HEADER = getDefaultHeader(browser='chrome')
        referer = baseUrl.meta.get('Referer')
        if referer:
            HTTP_HEADER['Referer'] = referer
        urlParams = {'header': HTTP_HEADER}
        sts, data = self.cm.getPage(baseUrl, urlParams)
        if not sts:
            return []
        cUrl = self.cm.meta['url']

        urlTab = []
        r = re.search(r'eval\("window\.ADBLOCKER\s*=\s*false;\\n(.+?);"\);</script', data)
        if r:
            r = r.group(1).replace('\\u002b', '+')
            r = r.replace('\\u0027', "'")
            r = r.replace('\\u0022', '"')
            r = r.replace('\\/', '/')
            r = r.replace('\\\\', '\\')
            r = r.replace('\\"', '"')
            aa_decoded = aadecode.decode(r, alt=True)
            stream_url = json.loads(aa_decoded[11:]).get('stream')
            if stream_url:
                if isinstance(stream_url, list):
                    sources = [(x.get('Label'), x.get('URL')) for x in stream_url]
                    for item in sources:
                        url = item[1]
                        if not url.startswith('https://'):
                            url = re.sub(':/*', '://', url)
                        url = strwithmeta(sig_decode(url), {'Origin': urlparser.getDomain(baseUrl, False), 'Referer': cUrl})
                        urlTab.append({'name': item[0], 'url': url})
                else:
                    url = strwithmeta(sig_decode(stream_url), {'Origin': urlparser.getDomain(baseUrl, False), 'Referer': cUrl})
                    urlTab.append({'name': 'mp4', 'url': url})

        return urlTab

    def parserSVETACDNIN(self, baseUrl):
        printDBG("parserSVETACDNIN baseUrl[%s]" % baseUrl)
        HTTP_HEADER = getDefaultHeader(browser='chrome')
        referer = baseUrl.meta.get('Referer')
        if referer:
            HTTP_HEADER['Referer'] = referer
        urlParams = {'header': HTTP_HEADER}
        sts, data = self.cm.getPage(baseUrl, urlParams)
        if not sts:
            return []
        cUrl = self.cm.meta['url']
        data = ph.getSearchGroups(data, '''id="files" value=['"]([^"^']+?)['"]''')[0]
        data = re.findall(r'\[(\d*p)\]([^,^\s]*)[,\s]', data)
        urlTab = []
        for item in data:
            url = item[1].replace(r'\/', '/')
            if url.startswith('//'):
                url = 'http:' + url
            url = strwithmeta(url, {'Origin': urlparser.getDomain(baseUrl, False), 'Referer': cUrl})
            if url != '':
                urlTab.append({'name': item[0], 'url': url})
        return urlTab

    def parserHIGHLOADTO(self, baseUrl):
        printDBG("parserHIGHLOADTO baseUrl[%r]" % baseUrl)
        urlTab = []
        urlTab.append({'name': 'mp4', 'url': baseUrl})
        return urlTab

    def parserUNIVERSAL(self, baseUrl):
        printDBG("parserUNIVERSAL baseUrl[%r]" % baseUrl)
        urlTab=[]
        urlParams = {'header': getDefaultHeader(browser='chrome')}
        if isinstance(baseUrl, strwithmeta):
            if header := baseUrl.meta.get('header', {}):
                urlParams['header'].update(header)

        sts, data = self.cm.getPage(baseUrl, urlParams)

        errMarkers = ['404 Not Found', 'Video is processing now', 'File was deleted', 'File Removed', 'File Deleted.', 'File Not Found', 'FILES HAVE BEEN PERMANENTLY DELETED']
        for errMarker in errMarkers:
            if errMarker in data:
                SetIPTVPlayerLastHostError(errMarker)
                return

        if not sts:
            return False

        urlTab.extend(self._findLinks(data, meta={'Referer': baseUrl}))

        if not urlTab:
            data1 = re.search(r'source src="(?P<url>[^"]+?)"', data, re.S)
            if not data1:
                data1 = re.search(r'''sources:\s*\[{(?:file|src):\s*["'](?P<url>[^"']+)''', data, re.S)
            if not data1:
                data1 = re.search(r'var jw = {"file":"(?P<url>[^"]+?)"', data, re.S)
            if not data1:
                data1 = re.search(r'''sources:\s*\[['"](?P<url>[^'"]+)''', data, re.S)
            if not data1:
                data1 = re.search(r'''iframe id="iframe" src=[['"](?P<url>[^'"]+)''', data, re.S)
            if not data1:
                data1 = re.search(r'''"hls_ondemand":\s*['"](?P<url>[^'"]+)''', data, re.S)
            if data1:
                url = data1['url'].replace('amp;', '').replace('\\', '')
                if 'm3u' in url or 'hls' in url:
                    urlTab.extend(getDirectM3U8Playlist(url, checkExt=False, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))
                else:
                    urlTab.append({'name': 'mp4', 'url': url})

                return urlTab

            for decoded in ph.eval_all_packed_func(data):
                r = re.compile('file:"(?P<url>[^"]+?)",label:"(?P<name>[^"]+?)"')
                data1 = [m.groupdict() for m in r.finditer(decoded, re.S)]
                if not data1:
                    r = re.compile(r'sources:.*?\[\{.*?[src|file]:.*?"(?P<url>[^"]+?)"')
                    data1 = [m.groupdict() for m in r.finditer(decoded, re.S)]

                if data1:
                    for elm in data1:
                        url = elm.get('url', "")
                        name = elm.get('name', "-")
                        if url:
                            if 'm3u' in url or 'hls' in url:
                                url = strwithmeta(url, {'Referer':baseUrl})
                                urlTab.extend(getDirectM3U8Playlist(url, checkExt=False, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))
                            else:
                                urlTab.append({'name': name, 'url': url})

        if not urlTab:
            host = urlparser.getHostName(baseUrl)
            SetIPTVPlayerLastHostError(_('Hosting "%s" unkown. Or could not get Video links') % host)

        return urlTab

    def parserVEEV(self, baseUrl):
        printDBG("parserVEEV baseUrl[%r]" % baseUrl)

        def build_array(encoded_string):
            d = []
            c = list(encoded_string)
            count = js_int(c.pop(0))
            while count:
                current_array = []
                for _ in range(count):
                    current_array.insert(0, js_int(c.pop(0)))
                d.append(current_array)
                count = js_int(c.pop(0))

            return d

        def decode_url(etext, tarray):
            ds = etext
            for t in tarray:
                if t == 1:
                    ds = ds[::-1]
                ds = binascii.unhexlify(ds).decode('utf8')
                ds = ds.replace('dXRmOA==', '')

            return ds

        def veev_decode(etext):
            result = []
            lut = {}
            n = 256
            c = etext[0]
            result.append(c)
            for char in etext[1:]:
                code = ord(char)
                nc = char if code < 256 else lut.get(code, c + c[0])
                result.append(nc)
                lut[n] = c + nc[0]
                n += 1
                c = nc

            return ''.join(result)

        def js_int(x):
            return int(x) if x.isdigit() else 0

        linksTab = []

        HTTP_HEADER = getDefaultHeader(browser='chrome')
        HTTP_HEADER['Referer'] = baseUrl.meta.get('Referer', baseUrl)
        urlParams = {'header': HTTP_HEADER}
        sts, data = self.cm.getPage(baseUrl, urlParams)
        if not sts:
            return False
        media_id = baseUrl.split('/')[-1]
        f = re.search(r'{\s*fc:\s*"([^"]+)', data)
        if f:
            ch = veev_decode(f.group(1))
            params = {
                'op': 'player_api',
                'cmd': 'gi',
                'file_code': media_id,
                'ch': ch,
                'ie':  1
            }
            durl = urljoin(baseUrl, '/dl') + '?' + urlencode(params)
            sts, data = self.cm.getPage(durl, urlParams)
            if not sts:
                return False

            jresp = json.loads(data).get('file')
            if jresp:
                url = decode_url(veev_decode(jresp.get('dv')[0].get('s')), build_array(ch)[0])
                url = strwithmeta(url, {'Referer': baseUrl})
                linksTab.append({'name': "vid", 'url': url})
        return linksTab

    def parserVIDMOLYME(self, baseUrl):
        printDBG("parserVIDMOLYME baseUrl[%r]" % baseUrl)

        HTTP_HEADER = getDefaultHeader()
        referer = baseUrl.meta.get('Referer')
        if referer:
            HTTP_HEADER['Referer'] = referer
        urlParams = {'header': HTTP_HEADER}

        baseUrl = strwithmeta(baseUrl)
        if 'embed' not in baseUrl:
            video_id = re.search('/([A-Za-z0-9]{12})[/.]', baseUrl + '/', re.S)
            baseUrl = urlparser.getDomain(baseUrl, False) + '/embed-{0}.html'.format(video_id[1])
        sts, data = self.cm.getPage(baseUrl, urlParams)
        if not sts:
            return False
        cUrl = self.cm.meta['url']

        urlTab = []
        url = re.search('''sources[^'^"]*?['"]([^'^"]+?)['"]''', data, re.S)[1]
        if not url:
            url = re.search('''<iframe[^>]*?src=["'](http[^"^']+?)["']''', data, re.S)[1]
            sts, data = self.cm.getPage(url, urlParams)
            if not sts:
                return False
            cUrl = self.cm.meta['url']
            url = re.search( '''sources[^'^"]*?['"]([^'^"]+?)['"]''', data, re.S)[1]

        url = strwithmeta(url, {'Origin': urlparser.getDomain(cUrl, False), 'Referer': cUrl})
        urlTab.extend(getDirectM3U8Playlist(url, checkContent=True, sortWithMaxBitrate=999999999))

        return urlTab

    def parserVK(self, baseUrl):
        printDBG("parserVK url[%s]" % baseUrl)

        linksTab = []

        COOKIE_FILE = GetCookieDir('vkcom.cookie')
        HTTP_HEADER = {'User-Agent': getDefaultUserAgent(),
                       'Referer': 'https://vk.com/',
                       'Origin': 'https://vk.com'}
        params = {'header': HTTP_HEADER, 'cookiefile': COOKIE_FILE, 'use_cookie': True, 'save_cookie': True, 'load_cookie': True}

        from urllib.parse import parse_qs
        query = parse_qs(baseUrl.split('?')[1])

        try:
            oid, video_id = query['oid'][0], query['id'][0]
        except:
            oid, video_id = re.findall('(.*)_(.*)', baseUrl.split('?')[1])[0]

        def __get_sources(oid, video_id):
            sources_url = 'https://vk.com/al_video.php?act=show'
            data = {
                'act': 'show',
                'al': 1,
                'video': '{0}_{1}'.format(oid, video_id)
            }
            params['header'].update({'X-Requested-With': 'XMLHttpRequest'})
            sts, html = self.cm.getPage(sources_url, params, data)
            if not sts:
                return False

            if html.startswith('<!--'):
                html = html[4:]
            js_data = json.loads(html)
            payload = []
            sources = []
            for item in js_data.get('payload'):
                if isinstance(item, list):
                    payload = item
            if payload:
                for item in payload:
                    if isinstance(item, dict):
                        js_data = item.get('player').get('params')[0]
                for item in list(js_data.keys()):
                    if item.startswith('url'):
                        sources.append((item[3:], js_data.get(item)))
                if not sources:
                    str_url = js_data.get('hls')
                    if str_url:
                        sources = [('360', str_url)]
            return sources

        sources = __get_sources(oid, video_id)
        if sources:
            sources.sort(key=lambda x: int(x[0]), reverse=True)
            for i in sources:
                linksTab.append({'name': i[0], 'url': i[1]})
        else:
            html = ''#self.net.http_GET(self.get_url(host, media_id), headers=headers).content
            jd = re.search(r'var\s*playerParams\s*=\s*(.+?});', html)
            if jd:
                jd = json.loads(jd.group(1))
                source = jd.get('params')[0].get('hls')
                if source:
                    params['header'].pop('X-Requested-With')
                    return source #+ helpers.append_headers(headers)


        return linksTab

