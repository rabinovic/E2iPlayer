# -*- coding: utf-8 -*-
#
#  IPTV downloader creator
#
#  $Id$
#
#
from Components.config import config

from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, IsExecutable
from Plugins.Extensions.IPTVPlayer.libs.urlparser import urlparser
from Plugins.Extensions.IPTVPlayer.iptvdm.wgetdownloader import WgetDownloader
from Plugins.Extensions.IPTVPlayer.iptvdm.pwgetdownloader import PwgetDownloader
from Plugins.Extensions.IPTVPlayer.iptvdm.busyboxdownloader import BuxyboxWgetDownloader
from Plugins.Extensions.IPTVPlayer.iptvdm.m3u8downloader import M3U8Downloader
from Plugins.Extensions.IPTVPlayer.iptvdm.hlsdownloader import HLSDownloader
from Plugins.Extensions.IPTVPlayer.iptvdm.ehlsdownloader import EHLSDownloader
from Plugins.Extensions.IPTVPlayer.iptvdm.rtmpdownloader import RtmpDownloader
from Plugins.Extensions.IPTVPlayer.iptvdm.f4mdownloader import F4mDownloader
from Plugins.Extensions.IPTVPlayer.iptvdm.mergedownloader import MergeDownloader
from Plugins.Extensions.IPTVPlayer.iptvdm.ffmpegdownloader import FFMPEGDownloader
from Plugins.Extensions.IPTVPlayer.iptvdm.iptvdh import DMHelper



def IsUrlDownloadable(url):
    if None is not DownloaderCreator(url):
        return True
    return False


def DownloaderCreator(url):
    printDBG("DownloaderCreator url[%r]" % url)
    downloader = None

    url = urlparser.decorateUrl(url)
    iptv_proto = url.meta.get('iptv_proto', '')
    match iptv_proto:
        case 'm3u8':
            if config.plugins.iptvplayer.hlsdlpath.value != '':
                downloader = HLSDownloader()
            else:
                downloader = M3U8Downloader()
        case 'em3u8':
            if config.plugins.iptvplayer.hlsdlpath.value != '':
                downloader = EHLSDownloader()
        case 'f4m':
            downloader = F4mDownloader()
        case 'rtmp':
            downloader = RtmpDownloader()
        case 'https' | 'http':
            downloader = WgetDownloader()
        case 'ffmpeg':
            downloader = FFMPEGDownloader()
        case 'merge':
            if url.meta.get('prefered_merger') == 'hlsdl' and config.plugins.iptvplayer.hlsdlpath.value != '' and config.plugins.iptvplayer.prefer_hlsdl_for_pls_with_alt_media.value:
                downloader = HLSDownloader()
            elif IsExecutable('ffmpeg'):
                downloader = FFMPEGDownloader()
            else:
                downloader = MergeDownloader()
        case 'mpd' if IsExecutable('ffmpeg'):
            downloader = FFMPEGDownloader()

    return downloader


def UpdateDownloaderCreator(url):
    printDBG("UpdateDownloaderCreator url[%s]" % url)
    if url.startswith('https'):
        if IsExecutable(DMHelper.GET_WGET_PATH()):
            printDBG("UpdateDownloaderCreator WgetDownloader")
            return WgetDownloader()
        if IsExecutable('python'):
            printDBG("UpdateDownloaderCreator PwgetDownloader")
            return PwgetDownloader()
    else:
        if IsExecutable('wget'):
            printDBG("UpdateDownloaderCreator BuxyboxWgetDownloader")
            return BuxyboxWgetDownloader()
        if IsExecutable(DMHelper.GET_WGET_PATH()):
            printDBG("UpdateDownloaderCreator WgetDownloader")
            return WgetDownloader()
        if IsExecutable('python'):
            printDBG("UpdateDownloaderCreator PwgetDownloader")
            return PwgetDownloader()

    printDBG("UpdateDownloaderCreator downloader not available")
    return PwgetDownloader()
