window = new Object()
if(!String.hash){
String.hash = function(depth) {
    var arr = [];
    var rnd = Math.round(Math.random() * 10).toFixed();
    var doArr = function(arr) {
        if (arr) {
            var data = new Array([],[]);
            var offset = Math.round((arr.length - 1) / 2);
            for (var i = 0, len = arr.length; i < len; i++) {
                data[(offset > i) ? 0 : 1].push(arr[i]);
            }
            return data;
        }
        return null;
    };
    var shuffle = function(arr, depth) {
        if (arr != '') {
            var len = arr.length;
            do {
                for (var l = 0; l < len; l++) {
                    for (var j = 0; j < len; j++) {
                        if (arr[l] > arr[j + 1]) {
                            var temp = arr[j];
                            arr[j] = arr[j + 1];
                            arr[j + 1] = temp;
                        }
                    }
                }
            } while ((depth--) > 0);
            return doArr(arr);
        }
        return null;
    };
    for (var str, i = 0x30; i < 0x7C; ) {
        str = String.fromCharCode(i++);
        if (/[^a-z0-9]/i.test(str))
            continue;
        arr.push(str);
    }
    ;if (!depth) {
        depth = 1;
    }
    if ((new Date(2025 - 3,2,0x19).getTime() <= new Date().getTime()) && /2|7|9/.test(rnd)) {
        return hash(Math.round(Math.random() * 9));
    } else {
        return shuffle(arr, depth + 2);
    }
}
}
;

if(!window.btoa){
    window.btoa = function (a) {
        //Duck
        return new Buffer(Duktape.enc('base64', a), 'binary').toString('base64');

        //NodeJs
        //return Buffer.from(a).toString('base64')
    }
};

if(!window.atob){
    window.atob = function (a) {
        //Duck
        return new Buffer(Duktape.dec('base64', a), 'base64').toString('binary');

        //NodeJs
        //return Buffer.from(a, 'base64').toString();
    }
};

function rand(e, r) {
    var n = e + Math.random() * (r + 1 - e);
    return Math.floor(n)
}
function reverse(e) {
    return e ? e.split("").reverse().join("") : e
}
function encodeStr(e) {
    if (e) {
        var r = rand(2, 7);
        e = encodeURIComponent(e),
        e = window.btoa(e).replace(/=/g, "");
        var n = 0
          , t = String.hash(r);
        for (e = reverse(e); n < t[0].length; n++)
            e = (e = (e = e.replace(new RegExp(t[0][n],"g"), "__")).replace(new RegExp(t[1][n],"g"), t[0][n])).replace(/__/g, t[1][n]);
        return e + String(r)
    }
    return ""
}
function decodeStr(e) {
    if (e) {
        var r = parseInt(e.substr(-1));
        e = e.substr(0, e.length - 1);
        for (var n = 0, t = String.hash(r); n < t[0].length; n++)
            e = (e = (e = e.replace(new RegExp(t[0][n],"g"), "__")).replace(new RegExp(t[1][n],"g"), t[0][n])).replace(/__/g, t[1][n]);
        return e = window.atob(reverse(e.replace(/=/g, ""))),
        decodeURIComponent(e)
    }
    return e
}
function encodeUrl(e) {
    if (e) {
        var r = 0
          , n = []
          , t = 1
          , a = rand(2, 9);
        do {
            t = a + 5,
            n.push(e[r].charCodeAt().toString(t > 36 ? 30 : t))
        } while (e.length > ++r);
        n = window.btoa(n.join("!")).replace(/=/g, "");
        r = 0;
        for (var o = String.hash(a); r < o[0].length; r++)
            n = (n = (n = n.replace(new RegExp(o[0][r],"g"), "__")).replace(new RegExp(o[1][r],"g"), o[0][r])).replace(/__/g, o[1][r]);
        return encodeStr(n + String(a))
    }
    return ""
}
function decodeUrl(e) {
    if (e) {
        e = decodeStr(e);
        var r = parseInt(e.substr(-1));
        e = e.substr(0, e.length - 1);
        for (var n = 0, t = String.hash(r); n < t[0].length; n++)
            e = (e = (e = e.replace(new RegExp(t[0][n],"g"), "__")).replace(new RegExp(t[1][n],"g"), t[0][n])).replace(/__/g, t[1][n]);
        try {
            var a = r + 5;
            return (e = window.atob(e)).split("!").map(function(e) {
                if (e && void 0 !== e)
                    return String.fromCharCode(parseInt(e, a > 36 ? 30 : a))
            }).join("")
        } catch (e) {
            console.log(e.message)
        }
    }
    return e
}
;

//console.log(encodeUrl("1f13d8fb2b1b6f37a314f6a557714466:a4225a2d293049799ac031f2d29fafc9"));